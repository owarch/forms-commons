 
/**
 *
 * This program is part of:
 *
 *    FORMS [Forms-Oriented Research Management System]
 *
 * Copyright (C) 2009 OpenWare Architects.  All Rights Reserved.
 *
 * FORMS is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * FORMS is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with FORMS.  If not, see <http://www.gnu.org/licenses/>.
 *
 */
 

/*
 *
 * PBEInfo.java -  This class contains info used in Password Based Encryption 
 *                 in EncryptUtil.java                                        
 *
 */

package com.owarchitects.forms.commons.comp;

import java.io.*;
import java.util.*;
   
public class PBEInfo {

   private String password;
   private String content;
   
   public String getContent() {
      return content;
   }
   
   public String getPassword() {
      return password;
   }
   
   public void setContent(String content) {
      this.content=content;
   }
   
   public void setPassword(String password) {
      this.password=password;
   }

}
