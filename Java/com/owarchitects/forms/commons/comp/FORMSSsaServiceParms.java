 
/**
 *
 * This program is part of:
 *
 *    FORMS [Forms-Oriented Research Management System]
 *
 * Copyright (C) 2009 OpenWare Architects.  All Rights Reserved.
 *
 * FORMS is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * FORMS is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with FORMS.  If not, see <http://www.gnu.org/licenses/>.
 *
 */
 

/**
 * FORMSSsaServiceParms.java -- Implements parameters and methods used by Parms classes in FORMS
 *                           involving FORMSService data transfers.
 * Original Creation -- 03/2007 (MRH)
 */

package com.owarchitects.forms.commons.comp;

import java.io.*;
import java.util.*;
import java.lang.reflect.*;
import org.springframework.web.servlet.*;
import org.apache.commons.io.FileUtils;

public abstract class FORMSSsaServiceParms extends FORMSServiceParms implements Serializable {

   // shared parameters

   public String codeword;
   public String luserid;
   public String connectstring;
   public String checkstring;

   // 

   public void setCodeword(String codeword) {
      this.codeword=codeword;
   }

   public String getCodeword() {
      return codeword;
   }

   // 

   public void setLuserid(String luserid) {
      this.luserid=luserid;
   }

   public String getLuserid() {
      return luserid;
   }
   
   // 

   public void setConnectstring(String connectstring) {
      this.connectstring=connectstring;
   }

   public String getConnectstring() {
      return connectstring;
   }

   // 

   public void setCheckstring(String checkstring) {
      this.checkstring=checkstring;
   }

   public String getCheckstring() {
      return checkstring;
   }

   //

}

