 
/**
 *
 * This program is part of:
 *
 *    FORMS [Forms-Oriented Research Management System]
 *
 * Copyright (C) 2009 OpenWare Architects.  All Rights Reserved.
 *
 * FORMS is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * FORMS is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with FORMS.  If not, see <http://www.gnu.org/licenses/>.
 *
 */
 
package com.owarchitects.forms.commons.comp;

/*
 * FormRecordProcessorConstants.java 
 */

public class FormRecordProcessorConstants extends FORMSConstants {

   public static final int APPENDED = 1;
   public static final int REPLACED = 2;
   public static final int HOLDAPPEND = 3;
   public static final int ALREADY_EXISTS = 4;
   public static final int RECORD_LOCKED = 5;
   public static final int NO_MATCHING_RECORD = 6;
   public static final int WRITE_TO_SYNC = 7;
   public static final int PERMERROR = 8;
   public static final int NOENROLLMENT = 9;
   public static final int ECODEPROB = 10;
   public static final int ALREADY_NOTPERMITTED = 11;
   public static final int NOTPERMITTED = 12;
   public static final int ERROR = 99;

}

