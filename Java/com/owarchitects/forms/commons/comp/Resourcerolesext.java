 
/**
 *
 * This program is part of:
 *
 *    FORMS [Forms-Oriented Research Management System]
 *
 * Copyright (C) 2009 OpenWare Architects.  All Rights Reserved.
 *
 * FORMS is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * FORMS is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with FORMS.  If not, see <http://www.gnu.org/licenses/>.
 *
 */
 
package com.owarchitects.forms.commons.comp;

/**
 *  Resourcerolesext.java  - used by role utilities
 */

import java.io.Serializable;
import com.owarchitects.forms.commons.db.*;

public class Resourcerolesext implements Serializable {

    private String siteno;
    private String studyno;
    private String rolelevel;
    private boolean ischild;
    private Resourceroles resourceroles;

    //

    public String getSiteno() {
        return this.siteno;
    }

    public void setSiteno(String siteno) {
        this.siteno = siteno;
    }

    //

    public String getStudyno() {
        return this.studyno;
    }

    public void setStudyno(String studyno) {
        this.studyno = studyno;
    }

    //

    public String getRolelevel() {
        return this.rolelevel;
    }

    public void setRolelevel(String rolelevel) {
        this.rolelevel = rolelevel;
    }

    //

    public boolean getIschild() {
        return this.ischild;
    }

    public void setIschild(boolean ischild) {
        this.ischild = ischild;
    }

    //

    public Resourceroles getResourceroles() {
        return this.resourceroles;
    }

    public void setResourceroles(Resourceroles resourceroles) {
        this.resourceroles = resourceroles;
    }

    //

}

