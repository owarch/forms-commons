 
/**
 *
 * This program is part of:
 *
 *    FORMS [Forms-Oriented Research Management System]
 *
 * Copyright (C) 2009 OpenWare Architects.  All Rights Reserved.
 *
 * FORMS is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * FORMS is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with FORMS.  If not, see <http://www.gnu.org/licenses/>.
 *
 */
 
/*
 *
 * EcodeController.java - Display ECODE value for ID
 *
 */

package com.owarchitects.forms.commons.comp;

import com.owarchitects.forms.commons.db.*;
import java.util.*;
import java.io.*;
import javax.servlet.http.*;
import org.hibernate.*;
import org.springframework.web.servlet.*;
import org.springframework.context.support.*;
import org.springframework.orm.hibernate3.*;
import org.springframework.orm.hibernate3.*;
import org.springframework.transaction.support.TransactionSynchronizationManager;
import org.mla.html.table.*;
import org.json.simple.JSONObject;

public class EcodeController extends FORMSSsaServiceClientController {

   public ModelAndView submitRequest() throws FORMSException {

      try {

         FORMSAuth auth=this.getAuth();

         // use classname-based view
         mav=new ModelAndView("Ecode");

         if (getRequest().getServletPath().indexOf("callecode")>=0) {

            return callEcode(mav);

         } else if (getRequest().getServletPath().indexOf("getecode")>=0) {

            return getEcode(mav);

         } 

         return null;
   
      } catch (Exception e) { 
         throw new FORMSException("FORMS Controller exception:  ",e);
      }

   }

   // Pull ID field counts/form 
   private ModelAndView callEcode(ModelAndView mav) throws FORMSException,FORMSKeyException,FORMSSecurityException {

      // Get list of ID variables used in forms
      List hal=getMainDAO().execQuery(
         "select f from Formha f where f.studies.studyid=" + getAuth().getValue("studyid")
         );
      StringBuilder sb=new StringBuilder();
      Iterator i=hal.iterator();
      while (i.hasNext()) {
         Formha ha=(Formha)i.next();
         Long fid=new Long(ha.getFormhaid());
         sb.append(fid);
         if (i.hasNext()) sb.append(",");
      }
      List forml=getMainDAO().execQuery(
         "select f from Datatabledef f join fetch f.allinst where f.pformhaid in (" + sb + ")"
         );
      sb=new StringBuilder();
      ArrayList aidlist=new ArrayList();
      i=forml.iterator();
      while (i.hasNext()) {
         Datatabledef fdef=(Datatabledef)i.next();
         Long fid=new Long(fdef.getDtdefid());
         sb.append(fid);
         aidlist.add(fid);
         if (i.hasNext()) sb.append(",");
      }
      getAuth().setObjectValue("aidlist",aidlist);
      List l=getMainDAO().execQuery(
         "select distinct count(*) from Datatablecoldef f " + 
            "where f.datatabledef.dtdefid in (" + sb + ") and f.isidfield=true " +
               "group by f.datatabledef.dtdefid " +  
               "order by count(*) " 
            );
      ArrayList passlist=new ArrayList();      
      i=l.iterator();
      int ncombs=l.size();
      long maxkeys=0;
      while (i.hasNext()) {
         Long nfields=(Long)i.next();
         passlist.add(nfields);
         maxkeys=nfields>maxkeys ? nfields : maxkeys;
      }
      mav.addObject("list",passlist);
      mav.addObject("maxkeys",maxkeys);
      mav.addObject("ncombs",ncombs);
      mav.addObject("status","DISPLAYLIST");
      return mav;
   }


   // Display ECODE for ID Combination
   private ModelAndView getEcode(ModelAndView mav) throws FORMSException,FORMSKeyException,FORMSSecurityException {

      try {

         // Construct where clause using specified values
         Enumeration e=request.getParameterNames();
         StringBuilder seedsb=new StringBuilder();
         while (e.hasMoreElements()) {
            String k=(String)e.nextElement();
            if (!k.equalsIgnoreCase("_function_")) {
                seedsb.insert(0,"-"+request.getParameter(k));
            }
         }
         seedsb.deleteCharAt(seedsb.indexOf("-"));
         String ecode=EcodeUtil.getEcode(seedsb.toString());
         mav.addObject("status","SHOWECODE");
         mav.addObject("seedstring",seedsb.toString());
         mav.addObject("ecode",ecode);
         return mav;

       } catch (Exception eee) {

         mav.addObject("status","SHOWECODE");
         mav.addObject("ecode","-1");
         return mav;
       
       }

   }

}






