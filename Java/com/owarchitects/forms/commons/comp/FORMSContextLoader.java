 
/**
 *
 * This program is part of:
 *
 *    FORMS [Forms-Oriented Research Management System]
 *
 * Copyright (C) 2009 OpenWare Architects.  All Rights Reserved.
 *
 * FORMS is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * FORMS is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with FORMS.  If not, see <http://www.gnu.org/licenses/>.
 *
 */
 
/*
 * FORMSContextLoader.java
 * 
 * Extends spring framework ContextLoader.  Overrides createWebApplicationContext
 * method to allow resolving of application path within applicationContext.xml
 * 
 */
 
package com.owarchitects.forms.commons.comp;
 
 import java.io.*;
 import java.lang.*;
 import java.util.*;
 import javax.servlet.ServletContext;
 import org.springframework.beans.BeanUtils;
 import org.springframework.beans.BeansException;
 import org.springframework.beans.factory.xml.XmlBeanDefinitionReader;
 import org.springframework.core.io.ByteArrayResource;
 import org.springframework.context.ApplicationContext;
 import org.springframework.context.ApplicationContextException;
 import org.springframework.web.context.ContextLoader;
 import org.springframework.web.context.WebApplicationContext;
 import org.springframework.web.context.support.GenericWebApplicationContext;
 import org.springframework.beans.factory.config.BeanDefinition;
 import org.apache.commons.io.FileUtils;

 public class FORMSContextLoader extends ContextLoader {

     public static final Class DEFAULT_CONTEXT_CLASS = GenericWebApplicationContext.class;

     public WebApplicationContext createWebApplicationContext(
             ServletContext servletContext, ApplicationContext parent) {
         
         try {
            String contextClassName = servletContext.getInitParameter(CONTEXT_CLASS_PARAM);
            Class contextClass = DEFAULT_CONTEXT_CLASS;
   
            GenericWebApplicationContext gc=(GenericWebApplicationContext)BeanUtils.instantiateClass(contextClass);
            String appPath=servletContext.getRealPath("/");
            String filepath =appPath + "/WEB-INF/applicationContext.xml";
            XmlBeanDefinitionReader xmlReader = new XmlBeanDefinitionReader(gc);
            ByteArrayResource bar=new ByteArrayResource(
                       FileUtils.readFileToString(new File(filepath),"UTF-8") 
                       .replaceAll("\\$appPath",appPath.replaceAll("\\\\","\\\\\\\\")
                    ).getBytes("UTF-8"));
            xmlReader.loadBeanDefinitions(bar);
            gc.refresh();
            return gc;

         } catch (Exception e) {

            throw new ApplicationContextException("Context initialization error" + Stack2string.getString(e));

         }

     }
 
}

