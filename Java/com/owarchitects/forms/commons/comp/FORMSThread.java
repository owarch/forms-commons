 
/**
 *
 * This program is part of:
 *
 *    FORMS [Forms-Oriented Research Management System]
 *
 * Copyright (C) 2009 OpenWare Architects.  All Rights Reserved.
 *
 * FORMS is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * FORMS is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with FORMS.  If not, see <http://www.gnu.org/licenses/>.
 *
 */
 
/* 
 *
 * FORMSThread.java -  Abstract FORMS Thread Class, containing helper methods to access FORMS resources
 *
 */

package com.owarchitects.forms.commons.comp;

import com.owarchitects.forms.commons.db.*;
import java.util.*;
import java.io.*;
import java.beans.*;
import java.lang.reflect.*;
import javax.servlet.*;
import javax.servlet.http.*;

import org.springframework.context.*;
import org.springframework.context.support.*;
import org.springframework.orm.hibernate3.*;
import org.springframework.transaction.support.TransactionSynchronizationManager;
import org.springframework.web.servlet.view.velocity.*;

import org.hibernate.*;
import org.hibernate.transform.*;
import org.apache.commons.io.FileUtils;
import org.apache.velocity.Template;
import org.apache.velocity.app.VelocityEngine;

public abstract class FORMSThread extends Thread {

   protected HttpSession session;
   protected HttpServletRequest request;
   protected HttpServletResponse response;
   protected ServletContext sc;
   protected String syswork;
   protected String sysconn;
   protected SessionFactory sessionFactory;
   protected Session hibernateSession;
   protected FORMSAuth auth;
   // Check auth status automatically
   protected boolean autoAuthCheck=true;

   // Constructor
   // NOTE:  Threads had trouble accessing info when passing just request & response.  Passing this info seems to 
   // allow them full access to FORMS functionality
   public FORMSThread(HttpServletRequest request,HttpServletResponse response,HttpSession session,FORMSAuth auth,String syswork) {
      this.request=request;
      this.response=response;
      this.session=session;
      this.auth=auth;
      this.syswork=syswork;
   }

   public final ServletContext getServletContext() {
      sc=getSession().getServletContext();
      return sc;
   }

   public final HttpServletRequest getRequest() {
      return request;
   }

   public final HttpServletResponse getResponse() {
      return response;
   }

   public final HttpSession getSession() {
      return session;
   }

   protected final void safeRedirect(String loc) throws FORMSException {
      // Prevents trying to sendRedirect on a committed response
      try {
         if (!response.isCommitted()) response.sendRedirect(loc);
      } catch (IOException ioe) {
         throw new FORMSException("Redirection exception");
      }
   }

   // Return cookie specified by input string.  Returns empty string if no such cookie.
   protected final String getCookieValue(String cstr) {
      try {
         Cookie[] carray=this.getRequest().getCookies();
         Iterator i=Arrays.asList(carray).iterator();
         while (i.hasNext()) {
            Cookie c=(Cookie)i.next();
            if (c.getName().replaceAll("=","").equals(cstr.replaceAll("=",""))) {
               return c.getValue().replaceAll("=","");
            }
         }
      } catch (Exception ce) {}   
      return "";
   }

   // Method used by subclass instance initializer blocks to disable auto auth checking
   protected final void autoAuthCheckOff() {
      autoAuthCheck=false;
   }

   /*
    * NOTE HERE:  We are maintaining and using a separate application context
    * here from the root application context for the application that would
    * be accessed from the getApplicationContext() method.  For some reason
    * when using that context, hibernate write operations threw exceptions.
    * When fixes were implemented to try eliminate the error (setting
    * HibernateSession flushMode to >= COMMIT), the exceptions went away,
    * but the write operations simply did not occur.  The hack here is 
    * simply to create another ApplicationContext maintained in the 
    * httpsession for use with hibernate.
    * MRH 10/03/2007
    */
    
   protected final AbstractApplicationContext getContext() {
      return SessionObjectUtil.getApplicationContext(this.getSession(),getSysconn());
   }

   public Session getHibernateSession() {
      sessionFactory = (SessionFactory) this.getContext().getBean("sessionFactory"); 
      hibernateSession = SessionFactoryUtils.getSession(sessionFactory, true); 
      return hibernateSession;
   }

   public void bindHibernateSession() {
      sessionFactory = (SessionFactory) this.getContext().getBean("sessionFactory"); 
      hibernateSession = SessionFactoryUtils.getSession(sessionFactory, true); 
      TransactionSynchronizationManager.bindResource(sessionFactory, new SessionHolder(hibernateSession));
   }
   
   public void releaseHibernateSession() {
      TransactionSynchronizationManager.unbindResource(sessionFactory);
      SessionFactoryUtils.releaseSession(hibernateSession,sessionFactory);
   }
   
   public MainDAO getMainDAO() {
      return (MainDAO)this.getContext().getBean("mainDAO");
   }   
   
   public EmbeddedDAO getEmbeddedDAO() {
      return (EmbeddedDAO)this.getContext().getBean("embeddedDAO");
   }   

   protected final FORMSAuth getAuth(boolean setCallingClass) throws FORMSException,FORMSKeyException,FORMSSecurityException {

      if (setCallingClass) {
         auth.setValue("callingjclass",request.getServletPath().substring(1));
      }   
      return auth;
   }

   protected final FORMSAuth getAuth() throws FORMSException,FORMSKeyException,FORMSSecurityException {
      // Default setCallingClass to false for Dwr calls.
      return this.getAuth(false);
   }

   protected final void setCallingJClass(FORMSAuth auth) throws FORMSException {
      auth.setValue("callingjclass",this.getRequest().getServletPath().substring(1));
   } 

   protected final void setCallingJClass() throws FORMSException,FORMSKeyException,FORMSSecurityException {
      this.getAuth(true);
   } 

   protected final String getAppPath() {
      return getSession().getServletContext().getRealPath("/");
   } 

   protected final org.apache.velocity.Template getVelocityTemplate(String inString) throws FORMSException {

      try {
         VelocityEngine velocity=SessionObjectUtil.getVelocityEngine(session,this.getContext(),this.getAppPath());
         return velocity.getTemplate(inString);

      }  catch (Exception e) {

         throw new FORMSException("FORMS Service-side Velocity exception");

      }

   } 

   protected final File getSyswork() {
      return new File(syswork);
   }

   protected final String getSysconnString() {
      return sysconn;
   }

   private final Sysconn getSysconn() {
      try {
         return (Sysconn) ObjectXmlUtil.objectFromXmlString(
                   EncryptUtil.decryptString(sysconn,SessionObjectUtil.getInternalKeyObj(session))
                );
      } catch (Exception e) {
         // OK, no initialization first pass through login
         return null;
      }
   }

}


