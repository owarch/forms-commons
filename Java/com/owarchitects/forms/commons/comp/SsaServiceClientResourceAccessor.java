 
/**
 *
 * This program is part of:
 *
 *    FORMS [Forms-Oriented Research Management System]
 *
 * Copyright (C) 2009 OpenWare Architects.  All Rights Reserved.
 *
 * FORMS is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * FORMS is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with FORMS.  If not, see <http://www.gnu.org/licenses/>.
 *
 */
 
/* 
 *
 * FORMSSsaServiceClientResourceAccessor.java -  Extends ServiceClientResourceAccessor to include FORMSService helpers
 *
 */

package com.owarchitects.forms.commons.comp;

import com.owarchitects.forms.commons.db.*;

import java.util.*;
import java.io.*;
import java.beans.*;
import java.lang.reflect.*;

import org.apache.axis2.context.*;
import org.apache.axis2.client.*;
import org.apache.axis2.transport.http.HTTPConstants;
import org.apache.axis2.AxisFault;
import org.apache.axis2.addressing.EndpointReference;
import org.apache.axis2.rpc.client.RPCServiceClient;
import org.apache.velocity.Template;
import org.apache.velocity.app.VelocityEngine;
import javax.xml.namespace.QName;

import javax.net.ssl.*;
import javax.servlet.http.*;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

public abstract class SsaServiceClientResourceAccessor extends ServiceClientResourceAccessor {

   // This initializer must be called from subclasses
   public SsaServiceClientResourceAccessor(HttpServletRequest request,HttpServletResponse response) {
      super(request,response);
   }


   //////////////////////////
   //////////////////////////
   //////////////////////////
   // FORMSService Methods //
   //////////////////////////
   //////////////////////////
   //////////////////////////


   // Submit FORMSService Request
   protected FORMSServiceResult submitServiceRequest(Embeddedinst einst,FORMSServiceParms parms) throws FORMSException,FORMSKeyException,FORMSSecurityException {

      return SsaServiceClientUtil.submitServiceRequest(getSession(),getContext(),getAuth(),getSyswork(),einst,parms);

   }

   // Submit FORMSService Request
   protected FORMSServiceResult submitServiceRequest(Linkedinst linst,FORMSServiceParms parms) throws FORMSException,FORMSKeyException,FORMSSecurityException {

      return SsaServiceClientUtil.submitServiceRequest(getSession(),getContext(),getAuth(),getSyswork(),linst,parms);

   }

   // Submit FORMSService Request
   protected FORMSServiceResult submitServiceRequest(Embeddedinst einst,Linkedinst linst,FORMSServiceParms parms) throws FORMSException,FORMSKeyException,FORMSSecurityException {

      return SsaServiceClientUtil.submitServiceRequest(getSession(),getContext(),getAuth(),getSyswork(),einst,linst,parms);

   }


   // Submit joined study FORMSService-Based Request (
   protected List submitServiceStudyRequest(FORMSServiceParms parms) throws FORMSException,FORMSKeyException,FORMSSecurityException {

      return SsaServiceClientUtil.submitServiceStudyRequest(getSession(),getContext(),getAuth(),getSyswork(),parms);

   }


   /////////////////////////
   /////////////////////////
   /////////////////////////
   // SOAP Client Methods //
   /////////////////////////
   /////////////////////////
   /////////////////////////


   // Submit SOAP Request
   protected FORMSServiceResult submitSoapRequest(Embeddedinst einst,FORMSServiceParms parms) throws FORMSException,FORMSKeyException,FORMSSecurityException {

      return SsaServiceClientUtil.submitSoapRequest(getRequest(),getSession(),getContext(),getAuth(),getSyswork(),einst,parms);

   }

   // Submit SOAP Request
   protected FORMSServiceResult submitSoapRequest(Linkedinst linst,FORMSServiceParms parms) throws FORMSException,FORMSKeyException,FORMSSecurityException {

      return SsaServiceClientUtil.submitSoapRequest(getRequest(),getSession(),getContext(),getAuth(),getSyswork(),linst,parms);

   }

   // Submit SOAP Request
   protected FORMSServiceResult submitSoapRequest(Embeddedinst einst,Linkedinst linst,FORMSServiceParms parms) throws FORMSException,FORMSKeyException,FORMSSecurityException {

      return SsaServiceClientUtil.submitSoapRequest(getRequest(),getSession(),getContext(),getAuth(),getSyswork(),einst,linst,parms);

   }


   // Submit joined study SOAP-Based Request (
   protected List submitSoapStudyRequest(FORMSServiceParms parms) throws FORMSException,FORMSKeyException,FORMSSecurityException {

      return SsaServiceClientUtil.submitSoapStudyRequest(getRequest(),getSession(),getContext(),getAuth(),getSyswork(),parms);

   }

}



