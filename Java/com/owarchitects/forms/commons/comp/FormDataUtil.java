 
/**
 *
 * This program is part of:
 *
 *    FORMS [Forms-Oriented Research Management System]
 *
 * Copyright (C) 2009 OpenWare Architects.  All Rights Reserved.
 *
 * FORMS is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * FORMS is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with FORMS.  If not, see <http://www.gnu.org/licenses/>.
 *
 */
 
/**
* FormDataUtil.java -- Various utilities for transforming form output
* Original Creation -- 06/2007 (MRH)
*/

package com.owarchitects.forms.commons.comp;

import java.io.*;
import java.util.*;
import javax.servlet.http.*;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.io.FileUtils;
import org.apache.commons.lang.ArrayUtils.*;
import java.util.regex.*;
import org.apache.commons.httpclient.util.URIUtil;

public class FormDataUtil {
   
   // Instances should not be created
   public FormDataUtil() {
      super();
   }

   // Transforms XML output from forms to the format expected for processing programs
   public static String transformDataStream(String inString) throws org.apache.commons.httpclient.URIException {

         // READ EVERYTHING INTO STRINGBUFFER
         StringBuilder sb = new StringBuilder(URIUtil.decode(inString));
   
         // REMOVE COMMENTS & XML TAGS
         String sbString=new String(sb);
   
         String REGEX = "<!--[^\n]*-->\n";
         String REPLACE = "";
         Pattern p = Pattern.compile(REGEX);
         Matcher m = p.matcher(sbString); 
         sbString = m.replaceAll(REPLACE);
   
         REGEX = "<[^\n<>]*xfa:data[^\n<>]*>\n";
         REPLACE = "";
         p = Pattern.compile(REGEX);
         m = p.matcher(sbString); 
         sbString = m.replaceAll(REPLACE);
   
         REGEX = "<[?][^\n<>]*[?]>\n";
         REPLACE = "";
         p = Pattern.compile(REGEX);
         m = p.matcher(sbString); 
         sbString = m.replaceAll(REPLACE);
   
         // Starting with Adobe 7.0, the export return character was changed.  This character doesn't work
         // in a JavaScript assignment, however, as we're doing.  Therefore we convert this to a \n JavaScript
         // return character.  Four backslashes are required here to get the required value written to the
         // SAS dataset.
   
         REGEX = "&amp;";
         REPLACE = "&";
         p = Pattern.compile(REGEX);
         m = p.matcher(sbString); 
         sbString = m.replaceAll(REPLACE);
   
         REGEX = "&#xD;";
         REPLACE = "\\\\n";
         p = Pattern.compile(REGEX);
         m = p.matcher(sbString); 
         sbString = m.replaceAll(REPLACE);
         
         return sbString;

   }

   // return a field value from the transformed data string
   public static String getFieldValue(String trString,String fieldname) {

      int floc=trString.toLowerCase().indexOf("<" + fieldname.toLowerCase() + ">");
      int flen=("<" + fieldname + ">").length();
      String rvalue="";
      if (floc>=0) {
         rvalue=trString.substring(floc+flen);
         rvalue=rvalue.substring(0,rvalue.indexOf("<"));
      }
      return rvalue;

   }      

}


