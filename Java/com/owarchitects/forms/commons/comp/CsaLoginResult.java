 
/**
 *
 * This program is part of:
 *
 *    FORMS [Forms-Oriented Research Management System]
 *
 * Copyright (C) 2009 OpenWare Architects.  All Rights Reserved.
 *
 * FORMS is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * FORMS is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with FORMS.  If not, see <http://www.gnu.org/licenses/>.
 *
 */
 
/**
 * CsaLoginResult.java -- results object returned by Authenticator.login
 * Original Creation -- 03/2007 (MRH)
 */

package com.owarchitects.forms.commons.comp;

import java.io.*;
import java.util.*;
import java.lang.reflect.*;

public class CsaLoginResult extends FORMSServiceResult implements Serializable {

   // shared parameters

   public int loginstatus;

   // 

   public void setLoginstatus(int loginstatus) {
      this.loginstatus=loginstatus;
   }

   public int getLoginstatus() {
      return loginstatus;
   }

   // 

}

