 
/**
 *
 * This program is part of:
 *
 *    FORMS [Forms-Oriented Research Management System]
 *
 * Copyright (C) 2009 OpenWare Architects.  All Rights Reserved.
 *
 * FORMS is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * FORMS is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with FORMS.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

package com.owarchitects.forms.commons.comp;

import com.owarchitects.forms.commons.db.*;
import java.io.*;
import java.util.*;
import java.lang.*;
import javax.servlet.*;
import javax.servlet.http.*;
import javax.crypto.SecretKey;
import org.springframework.core.io.*;
import org.springframework.beans.factory.xml.*;
import org.springframework.web.context.support.*;
import org.springframework.context.support.*;
import org.springframework.context.*;
import org.apache.commons.io.FileUtils;
import org.apache.axis2.rpc.client.RPCServiceClient;
//import org.apache.axis2.context.*;
//import org.apache.axis2.transport.http.HTTPConstants;
import org.springframework.web.servlet.view.velocity.*;
import org.apache.velocity.Template;
import org.apache.velocity.app.VelocityEngine;
import org.springframework.beans.factory.BeanFactoryUtils;
import org.springframework.web.context.support.GenericWebApplicationContext;
import java.security.*;
import com.mchange.v2.c3p0.ComboPooledDataSource;
import org.springframework.ui.velocity.VelocityEngineFactory;

import org.apache.velocity.runtime.resource.loader.ClasspathResourceLoader;
import org.apache.commons.discovery.tools.DiscoverSingleton;
import org.springframework.beans.factory.support.*;
import org.springframework.beans.factory.config.*;

import org.apache.commons.httpclient.HttpClient;
 
/**
* SessionObjectUtil.java -- Returns RMS Session Objects (returns current object if one exists
*    otherwise returns new one).
*/

// NOTE:  Currently using public rather than protected methods to allow JWS access to
// class methods.  May want to look into setting up JWS files as part of the package. 
// Axis at one time didn't allow JWS files to be a package member and I'm not sure how
// well this is currently working.

public class SessionObjectUtil {

   private static final String SERVLET_CONTEXT_KEY = ServletContext.class.getName();
   
   // Instances should not be created
   public SessionObjectUtil() {
      super();
   }

   /////////////////////////////
   // CONTROLLER-SIDE OBJECTS //
   /////////////////////////////

   public static FORMSAuth getAuthObj(HttpServletRequest request,HttpServletResponse response,String syswork,String sysconn) throws FORMSException,FORMSKeyException,FORMSSecurityException {

      HttpSession session=request.getSession();
      FORMSAuth obj=(FORMSAuth)session.getAttribute("formsauth");
      if (obj==null) {
         obj=new FORMSAuth(request,response,syswork,sysconn);
         session.setAttribute("formsauth",obj);
      }
      return obj;

   }

   public static HttpClient getHttpClient(HttpSession session) {
      HttpClient obj=(HttpClient)session.getAttribute("httpclient");
      if (obj==null) {
         obj=new HttpClient();
         session.setAttribute("httpclient",obj);
      }
      return obj;
   }

   public static FORMSSasConnection getSasConnection(HttpServletRequest request,HttpServletResponse response) {
      FORMSSasConnection obj=(FORMSSasConnection)request.getSession().getAttribute("formssasconnection");
      if (obj==null) {
         obj=new FORMSSasConnection(request,response);
         request.getSession().setAttribute("formssasconnection",obj);
      }
      return obj;
   }

   public static RPCServiceClient getAxis2Client(HttpSession session) {
      RPCServiceClient obj=(RPCServiceClient)session.getAttribute("serviceclient");
      try {
         if (obj==null) {
            obj=new RPCServiceClient();
            session.setAttribute("serviceclient",obj);
         }
         return obj;
      } catch (Exception e) {
         return null;
      }
   }

   public static FORMSConfig getConfigObj(HttpSession session) {
      String pathVar=session.getServletContext().getRealPath("/");
      FORMSConfig obj=(FORMSConfig)session.getAttribute("formsconfig");
      if (obj==null) {
         obj=new FORMSConfig(pathVar);
         session.setAttribute("formsconfig",obj);
      }
      return obj;
   }

   public static WinProps getWinPropsObj(HttpServletRequest request,HttpServletResponse response) {
      HttpSession session=request.getSession();
      WinProps obj=(WinProps)session.getAttribute("winprops");
      if (obj==null) {
         obj=new WinProps(request,response);
         session.setAttribute("winprops",obj);
      }
      return obj;
   }

   public static SecretKey getEncryptionKeyObj(HttpSession session) throws FORMSKeyException {
      SecretKey obj=(SecretKey)session.getAttribute("encryptkey");
      if (obj==null) {
         throw new FORMSKeyException("Session encryption key doesn't exist");
      }
      return obj;
   }

   public static void setEncryptionKeyObj(HttpSession session,SecretKey sk) throws FORMSException {
      session.setAttribute("encryptkey",sk);
   }

   public static SecretKey getInternalKeyObj(HttpSession session) throws FORMSException,FORMSKeyException {
      SecretKey obj=(SecretKey)session.getAttribute("internalkey");
      if (obj==null) {
         obj=EncryptUtil.getSecretKey(session,"formsinternal",getConfigObj(session).getKeyStorePassword());
         session.setAttribute("internalkey",obj);
         if (obj==null) {
            throw new FORMSKeyException("Session internal key doesn't exist");
         }   
      }
      return obj;
   }

   public static VelocityEngine getVelocityEngine(HttpSession session,ApplicationContext ctx,String appPath) throws FORMSException {
      VelocityEngine obj=(VelocityEngine)session.getAttribute("VelocityEngine");
      if (obj==null) {
         try {
            String directory="/WEB-INF/templates";
            Properties p = new Properties();
            p.setProperty("resource.loader", "webapp");
            p.setProperty("webapp.resource.loader.path", directory );
            p.setProperty("webapp.resource.loader.class", org.apache.velocity.tools.view.servlet.WebappLoader.class.getName() );
            obj=new VelocityEngine();
            obj.setApplicationAttribute(SERVLET_CONTEXT_KEY, session.getServletContext());
            obj.init(p);
            //obj.init(appPath + "/WEB-INF/velocity.properties");
            session.setAttribute("VelocityEngine",obj);
         }  catch (Exception e) {
            throw new FORMSException("FORMS Service-side Velocity exception");
         }
      }
      return obj;
   }

   public static AbstractApplicationContext getApplicationContext(HttpSession session,Sysconn sysconn) {
      AbstractApplicationContext obj=null;
      try {
         obj=(AbstractApplicationContext)session.getAttribute("ApplicationContext");
      } catch (Exception gwce) { }
      if (obj==null) {
         return refreshApplicationContext(session,sysconn);
      }
      return obj;
   }

   public static AbstractApplicationContext getApplicationContext(HttpSession session) {
      return getApplicationContext(session,null);
   }

   public static AbstractApplicationContext refreshApplicationContext(HttpSession session,Sysconn sysconn) {

      GenericWebApplicationContext obj;
      try {
         // Do initial load from applicationContext.xml
         obj=refreshInitialContext(session);
         // If have sysconn info, do load from FORMSContext.xml
         if (!(sysconn==null || sysconn.getUsername()==null)) { 
            String u=sysconn.getUsername();
            String p=sysconn.getPassword();
            try {

               readMainDBIntoContext(session,u,p);
               
            } catch (Exception e2) {

               throw new FORMSException("Couldn't read MainDB into context");
            }
         }
         session.setAttribute("ApplicationContext",obj);
      } catch (Exception e) {
         obj=null;
         try {
            // try to proceed with initial context (necessary for CSA user initialization)
            obj=refreshInitialContext(session);
            session.setAttribute("ApplicationContext",obj);
         } catch (Exception ee) { }
      }
      return obj;
   }

   public static AbstractApplicationContext refreshApplicationContext(HttpSession session,String username,String password) {
      GenericWebApplicationContext obj;
      try {
         // Do initial load from applicationContext.xml
         obj=refreshInitialContext(session);
         // If have sysconn info, do load from FORMSContext.xml
         if (!(username==null || password==null)) { 
            String u=username;
            String p=password;
            try {

               readMainDBIntoContext(session,u,p);

            } catch (Exception e2) {

               throw new FORMSException("Couldn't read MainDB into context");
            }
         }
         session.setAttribute("ApplicationContext",obj);
      } catch (Exception e) {
         obj=null;
         try {
            // try to proceed with initial context (necessary for CSA user initialization)
            obj=refreshInitialContext(session);
            session.setAttribute("ApplicationContext",obj);
         } catch (Exception ee) { }
      }
      return obj;
   }

   public static AbstractApplicationContext refreshApplicationContext(HttpSession session,String dbpw) {
      GenericWebApplicationContext obj;
      try {
         // Do initial load from applicationContext.xml
         obj=refreshInitialContext(session);
         // If have sysconn info, do load from FORMSContext.xml
         if (!(dbpw==null)) { 
            readMainDBIntoContext(session,dbpw);
         }
         session.setAttribute("ApplicationContext",obj);
      } catch (Exception e) {
         obj=null;
         try {
            // try to proceed with initial context (necessary for CSA user initialization)
            obj=refreshInitialContext(session);
            session.setAttribute("ApplicationContext",obj);
         } catch (Exception ee) { }
      }
      return obj;
   }

   public static void readMainDBIntoContext(HttpSession session,String username,String password) throws FORMSException {
      //AbstractApplicationContext obj=(AbstractApplicationContext)getApplicationContext(session);
      AbstractApplicationContext obj=(AbstractApplicationContext)session.getAttribute("ApplicationContext");
      if (obj==null) {
         obj=refreshApplicationContext(session,username,password);
      }
      try {
         String u=username;
         String p=password;
         EmbeddedusersDAO embeddedusersDAO=(EmbeddedusersDAO)obj.getBean("embeddedusersDAO");
         Embeddedusers euser=embeddedusersDAO.getRecord(u);
         PrivateKey privkey=EncryptUtil.getPrivateKey(session,u,p);
         String dbpw=EncryptUtil.decryptString(euser.getDbpassword(),privkey);

         updateContext(obj,session,dbpw);

      } catch (Exception e) {

         throw new FORMSException("Couldn't read MainDB into context");
      }
   }

   public static void readMainDBIntoContext(HttpSession session,String dbpw) throws FORMSException {
      //AbstractApplicationContext obj=(AbstractApplicationContext)getApplicationContext(session);
      AbstractApplicationContext obj=(AbstractApplicationContext)session.getAttribute("ApplicationContext");
      if (obj==null) {
         obj=refreshApplicationContext(session,dbpw);
         return;
      }
      try {
         updateContext(obj,session,dbpw);
      } catch (Exception e) {
         throw new FORMSException("Couldn't read MainDB into context");
      }
   }

   public static void updateContextCreateDatabase(HttpSession session,String dbpw) throws FORMSException {
      //AbstractApplicationContext obj=(AbstractApplicationContext)getApplicationContext(session);
      AbstractApplicationContext obj=(AbstractApplicationContext)session.getAttribute("ApplicationContext");
      if (obj==null) {
         obj=refreshApplicationContext(session,dbpw);
         return;
      }
      try {
         updateContextCreateDatabase(obj,session,dbpw);
      } catch (Exception e) {
         throw new FORMSException("Couldn't read MainDB into context");
      }
   }

   // pull MainDB into forms context
   private static void updateContext(AbstractApplicationContext appctx,HttpSession session,String dbpw) throws FORMSException {

      try {

         String appPath=session.getServletContext().getRealPath("/");
         String filepath=appPath + "/WEB-INF/FORMSContext.xml";
         // temporary context to pull in updated context information
         GenericWebApplicationContext tmpctx=new GenericWebApplicationContext();
         XmlBeanDefinitionReader xmlReader = new XmlBeanDefinitionReader(tmpctx);
         xmlReader.loadBeanDefinitions(
            new ByteArrayResource(
                FileUtils.readFileToString(new File(filepath),"UTF-8")
                    .replaceAll("\\$appPath",appPath.replaceAll("\\\\","\\\\\\\\"))
                    .replaceFirst("Password=XXXX[^<][^<]*","XXXXYYYYXXXX").replace("XXXXYYYYXXXX","Password=" + new String(dbpw))
                    .replaceFirst("password=XXXX[^<][^<]*","XXXXYYYYXXXX").replace("XXXXYYYYXXXX","password=" + new String(dbpw))
                        .getBytes("UTF-8")
            )
         );
         // update application context with updated beans from temporary context
         String[] beannames=tmpctx.getBeanDefinitionNames();
         Iterator i=Arrays.asList(beannames).iterator();
         while (i.hasNext()) {
            String beanname=(String)i.next();
            if (!appctx.containsBean(beanname)) {
               BeanDefinition bd=tmpctx.getBeanDefinition(beanname);
               ((GenericWebApplicationContext)appctx).registerBeanDefinition(beanname,bd);
            }
         }
         tmpctx=null;

      } catch (Exception e) {
         throw new FORMSException("Couldn't read MainDB into context");
      }

   }

   // pull MainDB into forms context
   private static void updateContextCreateDatabase(AbstractApplicationContext appctx,HttpSession session,String dbpw) throws FORMSException {

      try {

         String appPath=session.getServletContext().getRealPath("/");
         String filepath=appPath + "/WEB-INF/FORMSContext.xml";
         // temporary context to pull in updated context information
         GenericWebApplicationContext tmpctx=new GenericWebApplicationContext();
         XmlBeanDefinitionReader xmlReader = new XmlBeanDefinitionReader(tmpctx);
         xmlReader.loadBeanDefinitions(
            new ByteArrayResource(
                FileUtils.readFileToString(new File(filepath),"UTF-8")
                    .replaceAll("create=false;","create=true;")
                    .replaceAll("\\$appPath",appPath.replaceAll("\\\\","\\\\\\\\"))
                    .replaceFirst("Password=XXXX[^<][^<]*","XXXXYYYYXXXX").replace("XXXXYYYYXXXX","Password=" + new String(dbpw))
                    .replaceFirst("password=XXXX[^<][^<]*","XXXXYYYYXXXX").replace("XXXXYYYYXXXX","password=" + new String(dbpw))
                        .getBytes("UTF-8")
            )
         );
         // update application context with updated beans from temporary context
         String[] beannames=tmpctx.getBeanDefinitionNames();
         Iterator i=Arrays.asList(beannames).iterator();
         while (i.hasNext()) {
            String beanname=(String)i.next();
            BeanDefinition bd=tmpctx.getBeanDefinition(beanname);
            ((GenericWebApplicationContext)appctx).registerBeanDefinition(beanname,bd);
         }
         tmpctx=null;

      } catch (Exception e) {
         throw new FORMSException("Couldn't read MainDB into context");
      }

   }

   private static GenericWebApplicationContext refreshInitialContext(HttpSession session) throws java.io.IOException {

      GenericWebApplicationContext obj=new GenericWebApplicationContext();
      obj.setServletContext(session.getServletContext());
      String appPath=session.getServletContext().getRealPath("/");
      XmlBeanDefinitionReader xmlReader = new XmlBeanDefinitionReader(obj);
      String filepath=" ";
      filepath =appPath + "/WEB-INF/applicationContext.xml";
      xmlReader.loadBeanDefinitions(
         new ByteArrayResource(
             FileUtils.readFileToString(new File(filepath),"UTF-8")
                 .replaceAll("\\$appPath",appPath.replaceAll("\\\\","\\\\\\\\")
                 )
                     .getBytes("UTF-8")
         )
      );
      obj.refresh();
      session.setAttribute("ApplicationContext",obj);
      return obj;

   }

}




