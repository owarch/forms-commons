 
/**
 *
 * This program is part of:
 *
 *    FORMS [Forms-Oriented Research Management System]
 *
 * Copyright (C) 2009 OpenWare Architects.  All Rights Reserved.
 *
 * FORMS is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * FORMS is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with FORMS.  If not, see <http://www.gnu.org/licenses/>.
 *
 */
 
/*
 * LockInfo.java - Auth object containing locking info for use by controllers - MRH 03/2007
 */

package com.owarchitects.forms.commons.comp;

import com.owarchitects.forms.commons.db.*;
import java.io.Serializable;

public class LockInfo implements Serializable {


   public static final int LOCALFORM=1;
   public static final int REMOTEFORM=2;

   protected boolean islocked;
   protected int formlocation;
   protected Linkedinst linkedinst;
   protected long datarecordid;

   //

   public void setIslocked(boolean islocked) {
      this.islocked=islocked;
   }

   public boolean getIslocked() {
      return islocked;
   }

   //

   public void setFormlocation(int formlocation) {
      this.formlocation=formlocation;
   }

   public int getFormlocation() {
      return formlocation;
   }

   //

   public void setLinkedinst(Linkedinst linkedinst) {
      this.linkedinst=linkedinst;
   }

   public Linkedinst getLinkedinst() {
      return linkedinst;
   }

   //

   public void setDatarecordid(long datarecordid) {
      this.datarecordid=datarecordid;
   }

   public long getDatarecordid() {
      return datarecordid;
   }

   //

}

