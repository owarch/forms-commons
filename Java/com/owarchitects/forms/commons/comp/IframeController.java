 
/**
 *
 * This program is part of:
 *
 *    FORMS [Forms-Oriented Research Management System]
 *
 * Copyright (C) 2009 OpenWare Architects.  All Rights Reserved.
 *
 * FORMS is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * FORMS is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with FORMS.  If not, see <http://www.gnu.org/licenses/>.
 *
 */
 
/*
 *
 * IframeController.java - Controller used to display IFRAME output.  This program
 *    grabs the results object from the session, populating the MAV with parameters
 *    from the results object.
 *                                                                  MRH 07/24/2007
 *
 */

package com.owarchitects.forms.commons.comp;

import com.owarchitects.forms.commons.db.*;
import java.util.*;
import java.io.*;
import java.beans.*;
import java.lang.reflect.*;
import org.springframework.web.servlet.*;
import org.springframework.web.servlet.mvc.*;
import org.springframework.web.context.support.*;
import javax.servlet.http.*;
import java.util.regex.*;

public class IframeController extends FORMSMinimalController {

   public ModelAndView submitRequest() throws FORMSException {

      try {
   
         ModelAndView mav=new ModelAndView();

         // Retrieve results object from session
         Object o;

         String ivalue="";
         try {
            Pattern p=Pattern.compile("[0-9]+[.]do");
            Matcher m=p.matcher(this.getRequest().getServletPath());
            if (m.find()) {
               ivalue=m.group().replaceAll(".do","");
            }
         } catch (Exception e2) { }
         if (!ivalue.equals("")) {
            // Delay response for multiple loads.  Otherwise there are sometimes mix-ups
            Thread.sleep(300);
         } else {
            // Very small delay for single loads
            Thread.sleep(20);
         }
         o=this.getSession().getAttribute("iframe" + ivalue + "result");
         if (o!=null) {

            // Retrieve beanname and method to be invoked from input object, create method object
            Class ic=o.getClass();
            Method im;
            // if results object, run addAllToMav method, otherwise add object itself as
            // "parm" object
            try {
               im=ic.getMethod("addAllToMAV",new Class[] { ModelAndView.class });
               // Merge parameter values into MAV & return MAV
               Object tmp=(String)im.invoke(o,new Object[] { mav });
            } catch (NoSuchMethodException e) {
               mav.addObject("parm",o);
            }

         } 
         return mav;
   
      } catch (Exception e) { 
         throw new FORMSException("FORMS Controller exception:  " + Stack2string.getString(e));
      }

   }

}

