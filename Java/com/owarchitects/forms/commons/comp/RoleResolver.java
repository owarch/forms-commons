 
/**
 *
 * This program is part of:
 *
 *    FORMS [Forms-Oriented Research Management System]
 *
 * Copyright (C) 2009 OpenWare Architects.  All Rights Reserved.
 *
 * FORMS is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * FORMS is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with FORMS.  If not, see <http://www.gnu.org/licenses/>.
 *
 */
 
/* 
 *
 * RoleResolver.java -  Determines user roles from database
 *
 */

package com.owarchitects.forms.commons.comp;

import com.owarchitects.forms.commons.db.*;
import java.util.*;
import java.io.*;
import javax.servlet.http.*;

public class RoleResolver extends FORMSResourceAccessor {

   // Constructor 
   public RoleResolver(HttpServletRequest request,HttpServletResponse response) {

      super(request,response);

   }

   // Write serialized role list to auth
   public void createAuthRroleidList() throws FORMSException,FORMSSecurityException,FORMSKeyException {

      getAuth().setValue("rroleidlist",getRroleidList().toString().replaceAll("[^0-9,]",""));

   }

   // Return string representation of list
   public String getRroleidListString() throws FORMSException,FORMSSecurityException,FORMSKeyException {

       return ObjectUtil.objectToString(getRroleidList());

   }

   // Return role list for current user
   public List getRroleidList() throws FORMSException,FORMSSecurityException,FORMSKeyException {

      FORMSAuth auth=getAuth();
      Long auserid=new Long(auth.getValue("auserid"));

      List currlist=getMainDAO().execQuery(
         "select rrua.role.rroleid from Rroleuserassign rrua where rrua.user.auserid=:auserid"
            ,new String[] { "auserid" }
            ,new Object[] { auserid }
            );

      // Iterate through rroleroleassign to find all roles the above roles include
      if (currlist.size()<=0) {
         return currlist;
      }

      for (;;) {

         boolean docontinue=false;
         List newlist=getMainDAO().execQuery(
            "select rrra.parentrole.rroleid from Rroleroleassign rrra where rrra.childrole.rroleid in (" + 
                 currlist.toString().replaceAll("[^0-9,]","") + ")"
         );   

         Iterator i=newlist.iterator();
         while (i.hasNext()) {
            Long rid=(Long)i.next();
            if (!currlist.contains(rid)) {
               currlist.add(rid);
               docontinue=true;
            }
         }
         if (!docontinue) {
            break;
         }

      }

      return currlist;

   }

}


