 
/**
 *
 * This program is part of:
 *
 *    FORMS [Forms-Oriented Research Management System]
 *
 * Copyright (C) 2009 OpenWare Architects.  All Rights Reserved.
 *
 * FORMS is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * FORMS is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with FORMS.  If not, see <http://www.gnu.org/licenses/>.
 *
 */
 
/* 
 *
 * FORMSController.java -  Abstract FORMS Controller Class, contains no FORMSService helpers
 *
 */

package com.owarchitects.forms.commons.comp;

import com.owarchitects.forms.commons.db.*;

import java.util.*;
import java.io.*;
import java.beans.*;
import java.lang.reflect.*;
import javax.net.ssl.*;
import javax.servlet.*;
import javax.servlet.http.*;
import java.security.*;
import javax.crypto.SecretKey;

import org.springframework.ui.ModelMap;
import org.springframework.beans.*;
import org.springframework.beans.factory.*;
import org.springframework.beans.factory.xml.*;
import org.springframework.beans.factory.config.*;
import org.springframework.context.*;
import org.springframework.context.support.*;
import org.springframework.core.io.*;
import org.springframework.orm.hibernate3.*;
import org.springframework.transaction.support.TransactionSynchronizationManager;
import org.springframework.web.context.support.*;
import org.springframework.web.servlet.*;
import org.springframework.web.servlet.mvc.*;
import org.springframework.web.context.support.*;
import org.springframework.web.servlet.view.velocity.*;

import org.hibernate.*;
import org.hibernate.transform.*;

import org.apache.commons.io.FileUtils;
import org.apache.commons.dbcp.BasicDataSource;
import org.apache.velocity.Template;
import org.apache.velocity.app.VelocityEngine;
import com.mchange.v2.c3p0.ComboPooledDataSource;

public abstract class FORMSController extends AbstractController implements AppResource {

   protected String bgcolor;
   protected String bgimage;
   protected String menubgcolor;
   protected PrintWriter out;
   protected HttpSession session;
   protected WinProps wp;
   protected ModelAndView mav;
   protected HttpServletRequest request;
   protected HttpServletResponse response;
   protected String syswork;
   protected String sysconn;
   protected SessionFactory sessionFactory;
   protected Session hibernateSession;
   // Check auth status automatically
   protected boolean autoAuthCheck=true;
   protected ComboPooledDataSource mds=null;
   protected PermHelper helper;

   // Abstract override method 
   protected abstract ModelAndView submitRequest() throws FORMSException,FORMSKeyException,FORMSSecurityException;

   // Main Request Handler Method, contains wrapped call to submitRequest method
   protected ModelAndView handleRequestInternal(HttpServletRequest request, HttpServletResponse response) throws FORMSException {

        try {

           // PrintWriter for diagnostic output
           out=response.getWriter();        

           this.request=request;
           this.response=response;
           this.syswork=null;
           this.sysconn=null;
           this.helper=null;

           // Pull syswork, sysconn from session attribute preferably, alternately from cookie.
           try {
              this.syswork=request.getSession().getAttribute("syswork").toString();
           } catch (Exception swe) {}
           if (syswork==null || syswork.length()<1) {
              this.syswork=Base64.decode(this.getCookieValue(Base64.encode("workdir")).replaceAll("=",""));
           }   

           response.addCookie(new Cookie(Base64.encode("workdir").replaceAll("=",""),
                                         Base64.encode(syswork).replaceAll("=","")));
           try {
              this.sysconn=request.getSession().getAttribute("sysconn").toString();
           } catch (Exception swe) {}
           if (sysconn==null || sysconn.length()<1) {
              this.sysconn=Base64.decode(this.getCookieValue(Base64.encode("sysconn")).replaceAll("=",""));
           }   
           // formstime used for sysconn and auth
           String formstime=new Long(System.currentTimeMillis()).toString();
           // Set initial sysconn value
           if (sysconn==null || sysconn.length()<1 || 
              (this.getRequest().getServletPath().indexOf("login.do")>=0 && request.getParameter("passwd")!=null)) {
              if (this.getRequest().getServletPath().indexOf("login.do")>=0) {
                 Sysconn sconn=new Sysconn();
                 sconn.setUsername(request.getParameter("user"));
                 sconn.setPassword(request.getParameter("passwd"));
                 sconn.setFormstime(formstime);
                 if (sconn.getUsername()!=null) {
                    sysconn=EncryptUtil.encryptString(ObjectXmlUtil.objectToXmlString(sconn),
                                        SessionObjectUtil.getInternalKeyObj(session));
                    request.getSession().setAttribute("sysconn",sysconn);
                    try {
                       SessionObjectUtil.readMainDBIntoContext(session,request.getParameter("user"),request.getParameter("passwd"));
                    } catch (Exception cex) {
                       this.refreshContext();
                    }
                 } 
              } else {
                 safeRedirect("login.do");
              }   
           }

           // Get context
           session=request.getSession();
           wp=SessionObjectUtil.getWinPropsObj(request,response);
   
           // Immediately expire headers
           response.setHeader("Cache-Control","no-cache"); //HTTP 1.1
           response.setHeader("Pragma","no-cache"); //HTTP 1.0
           response.setDateHeader ("Expires", -1); //prevents caching at the proxy server
           /*
           this.preventCaching(response);
           */

           try {
              // Initialize Main Datasource
              initDataSource();

              // Create mav used by override method
              this.mav=new ModelAndView();
              if (autoAuthCheck) {
                  try {
                     if (!this.getAuth(false).isAuth(request,response)) {
                        if (this.getAuth(false).getValue("timeout").equals("Y")) {
                           safeRedirect("timeout.do");
                        } else {
                           safeRedirect("logout.do");
                        }
                        return null;
                     }
                  } catch (Exception authex) {
                     safeRedirect("login.do");
                     return null;
                  }
              } 
              // username check
              if (this.getRequest().getServletPath().indexOf("login.do")<0 &&
                    (this.getAuth().getValue("username")==null || this.getAuth().getValue("username")=="") 
                 ) {
                 safeRedirect("login.do");
                 return null;
              }
              // Release any record lock held by this session
              releaseRecordLocks();
              // Submit override method
              mav=submitRequest();
              if (mav!=null) {
                 // add WinProps objects to view
                 wp.addViewObjects(mav);
                 // add ServletPath (spath) object to view
                 mav.addObject("spath",request.getServletPath().substring(1));
                 // add other important values
                 try {
                    mav.addObject("rightslevel",getAuth().getValue("rightslevel"));
                    mav.addObject("analystaccess",getAuth().getValue("analystaccess"));
                    mav.addObject("studyaccess",getAuth().getValue("studyaccess"));
                 } catch (Exception aex) { }
              }    
              // bump formstime & update sysconn
              try {
                 Sysconn sconn=(Sysconn)ObjectXmlUtil.objectFromXmlString(EncryptUtil.decryptString(sysconn,SessionObjectUtil.getInternalKeyObj(session)));
                 sconn.setFormstime(formstime);
                 this.sysconn=EncryptUtil.encryptString(ObjectXmlUtil.objectToXmlString(sconn),
                                 SessionObjectUtil.getInternalKeyObj(session));
                 this.getAuth().setValue("formstime",formstime);
                 response.addCookie(new Cookie(Base64.encode("sysconn").replaceAll("=",""),
                                               Base64.encode(sysconn).replaceAll("=","")));
                 request.getSession().setAttribute("sysconn",sysconn);
              } catch (Exception e2) {
              }   
              return mav;

           } catch (Exception ke) {
              throw new FORMSException("FORMSController exception (submitRequest exception) - <BR>",ke);
           }
          
        } catch (Exception e) {

           throw new FORMSException("FORMSController exception - <BR>",e);

        }

   }     

   private void releaseRecordLocks() {
      try {
         String apptype=getAuth().getValue("apptype");
         if (apptype.equals("SSA")) {
            SsaRecordLockReleaseUtil.releaseRecordLocks(getSession(),getContext(),getAuth(),getMainDAO(),getSyswork());
         } else if (apptype.equals("CSA")) {
            CsaRecordLockReleaseUtil.releaseRecordLocks(getSession(),getContext(),getAuth(),getSyswork());
         }
      } catch (Exception e) {
         // Do nothing
      }
   }

   // Method used by subclass instance initializer blocks to disable auto auth checking
   public final void autoAuthCheckOff() {
      autoAuthCheck=false;
   }

   // field getter methods

   public final PrintWriter getWriter() {
      return out;
   }

   public final HttpSession getSession() {
      return session;
   }

   public final WinProps getWinProps() {
      return wp;
   }

   public final HttpServletRequest getRequest() {
      return request;
   }

   public final HttpServletResponse getResponse() {
      return response;
   }

   /*
    * NOTE HERE:  We are maintaining and using a separate application context
    * here from the root application context for the application that would
    * be accessed from the getApplicationContext() method.  For some reason
    * when using that context, hibernate write operations threw exceptions.
    * When fixes were implemented to try eliminate the error (setting
    * HibernateSession flushMode to >= COMMIT), the exceptions went away,
    * but the write operations simply did not occur.  The hack here is 
    * simply to create another ApplicationContext maintained in the 
    * httpsession for use with hibernate.
    * MRH 10/03/2007
    */
    
   public final AbstractApplicationContext getContext() {
      if (request.getSession().getAttribute("ApplicationContext")==null) {
         this.refreshContext();
         return SessionObjectUtil.getApplicationContext(this.getSession(),getSysconn());
      } else {
         return SessionObjectUtil.getApplicationContext(this.getSession(),getSysconn());
      }
   }
    
   public final void refreshContext() {
      SessionObjectUtil.refreshApplicationContext(this.getSession(),getSysconn());
   }

   public final void safeRedirect(String loc) throws FORMSException {
      // Prevents trying to sendRedirect on a committed response
      try {
         if (!response.isCommitted()) response.sendRedirect(loc);
      } catch (IOException ioe) {
         throw new FORMSException("Redirection exception");
      }
   }

   // Return cookie specified by input string.  Returns empty string if no such cookie.
   public final String getCookieValue(String cstr) {
      try {
         Cookie[] carray=this.getRequest().getCookies();
         Iterator i=Arrays.asList(carray).iterator();
         while (i.hasNext()) {
            Cookie c=(Cookie)i.next();
            if (c.getName().replaceAll("=","").equals(cstr.replaceAll("=",""))) {
               return c.getValue().replaceAll("=","");
            }
         }
      } catch (Exception ce) {}   
      return "";
   }

   public Session getHibernateSession(String sf) {
      sessionFactory = (SessionFactory) this.getContext().getBean(sf); 
      hibernateSession = SessionFactoryUtils.getSession(sessionFactory, true); 
      return hibernateSession;
   }

   public Session getHibernateSession() {
      return getHibernateSession("sessionFactory");
   }

   public void bindHibernateSession(String sf) {
      sessionFactory = (SessionFactory) this.getContext().getBean(sf); 
      hibernateSession = SessionFactoryUtils.getSession(sessionFactory, true); 
      TransactionSynchronizationManager.bindResource(sessionFactory, new SessionHolder(hibernateSession));
   }

   public void bindHibernateSession() {
      bindHibernateSession("sessionFactory");
   }
   
   public void releaseHibernateSession() {
      TransactionSynchronizationManager.unbindResource(sessionFactory);
      SessionFactoryUtils.releaseSession(hibernateSession,sessionFactory);
   }
   
   public MainDAO getMainDAO() {

      return (MainDAO)this.getContext().getBean("mainDAO");
   }   
   
   public EmbeddedDAO getEmbeddedDAO() {
      return (EmbeddedDAO)this.getContext().getBean("embeddedDAO");
   }   

   // return PermHelper object
   public PermHelper getPermHelper() { 
      if (this.helper!=null) {
         return this.helper;
      } else {
         return new PermHelper(getRequest(),getResponse());
      }
   }

   // return specific global permission
   public final boolean hasGlobalPerm(int pcatvalue,String permacr) throws FORMSException,FORMSKeyException,FORMSSecurityException { 
      return getPermHelper().hasGlobalPerm(pcatvalue,permacr);
   }

   // check permission at ANY level
   public final boolean hasGlobalPerm(String permacr) throws FORMSException,FORMSKeyException,FORMSSecurityException { 
      return getPermHelper().hasGlobalPerm(permacr);
   }

   public final FORMSAuth getAuth(boolean setCallingClass) throws FORMSException,FORMSKeyException,FORMSSecurityException {

      HttpServletRequest request=this.getRequest();
      HttpServletResponse response=this.getResponse();
      
      FORMSAuth auth=SessionObjectUtil.getAuthObj(request,response,syswork,sysconn);

      // In case auth created syswork
      if (syswork==null) {
         this.syswork=auth.getSyswork();
      }

      if (setCallingClass) {
         auth.setValue("callingjclass",request.getServletPath().substring(1));
      }   

      return auth;
   }

   public final FORMSAuth getAuth() throws FORMSException,FORMSKeyException,FORMSSecurityException {
      return this.getAuth(true);
   }

   public final void setCallingJClass(FORMSAuth auth) throws FORMSException {
      auth.setValue("callingjclass",this.getRequest().getServletPath().substring(1));
   } 

   public final void setCallingJClass() throws FORMSException,FORMSKeyException,FORMSSecurityException {
      this.getAuth(true);
   } 

   public final String getAppPath() {
      return getSession().getServletContext().getRealPath("/");
   } 

   public final org.apache.velocity.Template getVelocityTemplate(String inString) throws FORMSException {

      try {
         VelocityEngine velocity=SessionObjectUtil.getVelocityEngine(session,this.getContext(),this.getAppPath());
         return velocity.getTemplate(inString);

      }  catch (Exception e) {

         throw new FORMSException("FORMS Service-side Velocity exception");

      }

   } 

   // Log system event
   public final void logEvent(String detail) {

      try {

         Systemlog sl=new Systemlog();
         sl.setIpaddr(this.getRequest().getRemoteAddr());
         sl.setUserdesc(this.getAuth().getValue("userdesc"));
         sl.setDetail(detail);
         sl.setServletpath(this.getRequest().getServletPath());
         sl.setTimestamp(new Date());
         getMainDAO().saveOrUpdate(sl);

      } catch (Exception loge) {
         // Do nothing
      }

   }

   public final File getSyswork() {
      return new File(syswork);
   }

   public final ModelAndView getMAV() {
      return mav;
   }

   public final void initDataSource() {
      if (mds==null) {
         try {
            String u=getSysconn().getUsername();
            String p=getSysconn().getPassword();

            EmbeddedusersDAO embeddedusersDAO=(EmbeddedusersDAO)this.getContext().getBean("embeddedusersDAO");
            Embeddedusers euser=embeddedusersDAO.getRecord(u);
            PrivateKey privkey=EncryptUtil.getPrivateKey(session,u,p);
            String dbpw=EncryptUtil.decryptString(euser.getDbpassword(),privkey);
            if (dbpw!=null && dbpw.length()>0) {
               // Set main data source password 
               mds=(ComboPooledDataSource)this.getContext().getBean("mainDataSource");
               // replaceFirst & replaceAll have problems with backslashes in replacement text.
               // here we do a replaceFirst with intermediate text then use replace to 
               // replace that text in the password.  replace uses "CharSequence" for the
               // replacement and handles backslashes and other characters correctly
               mds.setJdbcUrl(mds.getJdbcUrl().replaceFirst("bootPassword=.*","XXXXYYYYXXXX").replace("XXXXYYYYXXXX","bootPassword=" + new String(dbpw)));
               mds.setPassword(new String(dbpw));
               mds.setOverrideDefaultPassword(new String(dbpw));
            }
         } catch (Exception e) {
            // OK, no initialization first pass through login
         }   
      }
   }

   public final String getSysconnString() {
      return sysconn;
   }

   public final Sysconn getSysconn() {
      try {
         return (Sysconn) ObjectXmlUtil.objectFromXmlString(
                   EncryptUtil.decryptString(sysconn,SessionObjectUtil.getInternalKeyObj(session))
                );
      } catch (Exception e) {
         // OK, no initialization first pass through login
         return null;
      }
   }

}

