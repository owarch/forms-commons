 
/**
 *
 * This program is part of:
 *
 *    FORMS [Forms-Oriented Research Management System]
 *
 * Copyright (C) 2009 OpenWare Architects.  All Rights Reserved.
 *
 * FORMS is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * FORMS is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with FORMS.  If not, see <http://www.gnu.org/licenses/>.
 *
 */
 
/**
 * FORMSResultMethods.java -- Contains methods implemented by FORMSResult objects
 * Original Creation -- 03/2007 (MRH)
 */

package com.owarchitects.forms.commons.comp;

import java.io.*;
import java.util.*;
import java.lang.reflect.*;
import org.springframework.web.servlet.*;
import org.apache.commons.io.FileUtils;

public abstract class FORMSResultMethods implements Cloneable,Serializable {

   // Add results object fields to MAV (assumes like-named get function)
   private void addFunc(ModelAndView mav,String[] inarray,boolean isexcept) {
      Arrays.sort(inarray);
      Class c=getClass();
      Field f[]=c.getFields();
      Method m[]=c.getMethods();
      Iterator mi=Arrays.asList(m).iterator();
      while (mi.hasNext()) {
         Method thism=(Method)mi.next();
         String mname=thism.getName();
         if (mname.substring(0,3).equalsIgnoreCase("get")) {
            for (int i=0;i<f.length;i++) {
               String fname=f[i].getName();
               String ftype=f[i].getType().toString();
               if (mname.substring(3).equalsIgnoreCase(fname)) {
                  if (
                       (isexcept && (Arrays.binarySearch(inarray,fname)<0)) ||
                       (!isexcept && (Arrays.binarySearch(inarray,fname)>=0)) 
                     ) {
                     try {
                        Object val=f[i].get(this);
                        mav.addObject(fname,val);
                     } catch (Exception e) { }
                  }
                  break;
               }
            }
         }
      }
   }

   // Add all results object fields to MAV 
   public final void addAllToMAV(ModelAndView mav) {
      addFunc(mav,new String[0],true);
   }

   // Add listed results object fields to MAV 
   public final void addToMAV(ModelAndView mav,String[] addList) {
      addFunc(mav,addList,false);
   }

   // Add all results object fields to MAV except those listed
   public final void addAllExcept(ModelAndView mav,String[] exceptList) {
      addFunc(mav,exceptList,true);
   }

   //

   public Object clone() {
       try {
          return super.clone();
       }
       catch (CloneNotSupportedException e) {
           throw new InternalError(e.toString());
       }
   }

}

