 
/**
 *
 * This program is part of:
 *
 *    FORMS [Forms-Oriented Research Management System]
 *
 * Copyright (C) 2009 OpenWare Architects.  All Rights Reserved.
 *
 * FORMS is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * FORMS is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with FORMS.  If not, see <http://www.gnu.org/licenses/>.
 *
 */
 
/*
 *
 * SelectStudyController.java - Study Selection Interface
 *
 */

package com.owarchitects.forms.commons.comp;

import com.owarchitects.forms.commons.db.*;
import java.util.*;
import java.io.*;
import javax.servlet.http.*;
import org.hibernate.*;
import org.springframework.web.servlet.*;
import org.springframework.context.support.*;
import org.springframework.orm.hibernate3.*;
import org.springframework.orm.hibernate3.*;
import org.springframework.transaction.support.TransactionSynchronizationManager;

import org.hibernate.impl.*;
import org.hibernate.jdbc.*;

public class SelectStudyController extends FORMSController {

   public ModelAndView submitRequest() throws FORMSException {

      try {

         FORMSAuth auth=this.getAuth();

         ModelAndView mav=new ModelAndView();
         HttpServletResponse response=this.getResponse();
         PrintWriter out=this.getWriter();

         String sortcolumn=request.getParameter("SORTCOLUMN");
         String archiveform=request.getParameter("ARCHIVEFORM");
         boolean showarchivedstudies=new Boolean(auth.getValue("showarchivedstudies")).booleanValue();

         if (sortcolumn==null && archiveform==null) {

            try {

               // Process Data
               SelectStudyResult result=this.processData(showarchivedstudies);
      
               this.getSession().setAttribute("iframeresult",result);
   
               if (getPermHelper().hasAnalystSysPerm("ACCESS")) {
                  if (!hasGlobalPerm(PcatConstants.SYSTEM,"ACCESSCONTENT")) {
                     safeRedirect("analystcontent.do");
                  }
                  mav.addObject("analystaccess",true);
               } else {
                  if (!hasGlobalPerm(PcatConstants.SYSTEM,"ACCESSCONTENT")) {
                     mav.addObject("status","NOPERMISSION");
                     return mav;
                  }
                  mav.addObject("analystaccess",false);
               }
      
               // Direct output based on result
               if (result.getStatus().equalsIgnoreCase("OK")) {
      
                  result.addToMAV(mav,new String[] { "list","status","rightslevel" });
                  mav.addObject("showstudybutton",new Boolean(result.getIsadmin()).toString());
                  if (showarchivedstudies) {
                     auth.setValue("archivecheck","checked");
                  } else {   
                     auth.setValue("archivecheck"," ");
                  } 
   
                  return mav;
         
               } else if (result.getStatus().equalsIgnoreCase("REDIRECT")) {
   
                  result.addToMAV(mav,new String[] { "list","status","rightslevel" });
                  return mav;
   
               } else {
         
                  return null;
         
               }

            } catch (Exception e) {
               // This is a temporary workaround for a problem occuring in the CSA.  Failed logins followed
               // by successful ones are often followed by an exception thrown here.  Need to get to the
               // root cause of this.
               out.println("<script>alert(\"Error processing login information.  Please login again.\");" +
                             "location.replace(\"login.do\");</script>");
               return null;              
            }

         }
         else if (sortcolumn!=null) {

            String oldsortcol=auth.getValue("studysortcolumn");
            String oldsortdir=auth.getValue("studysortdir");
            if (oldsortcol.equalsIgnoreCase(sortcolumn)) {
               if (oldsortdir.equalsIgnoreCase("asc")) {
                  auth.setValue("studysortdir","desc");
               } else {
                  auth.setValue("studysortdir","asc");
               }
            } else {
               auth.setValue("studysortcolumn",sortcolumn);
               auth.setValue("studysortdir","asc");
            }

            mav.addObject("status","SORT");
            return mav;

         }
         else if (archiveform!=null) {

            if (showarchivedstudies) {
               auth.setValue("showarchivedstudies","false");
            } else {
               auth.setValue("showarchivedstudies","true");
            }   

            mav.addObject("status","OPTIONS");
            return mav;

         }
         return null;
   
      } catch (Exception e) { 
         throw new FORMSException("FORMS Controller exception:  ",e);
      }

   }

   private SelectStudyResult processData(boolean showarchivedstudies) throws FORMSException {

      SelectStudyResult result=new SelectStudyResult();

      try {

        FORMSAuth auth=this.getAuth();

        LocalusersDAO localusersDAO=(LocalusersDAO)this.getContext().getBean("localusersDAO");

        Localusers user=localusersDAO.getCurrentRecordByUsername(auth.getValue("username"));

        ArrayList<Map> passList=new ArrayList<Map>();
        List l=null;
        
        boolean isadmin=false;
        String rightslevel="NONE";
       
        // Retrieve studies based on permissions
        if (user.getIsadmin()) {
           rightslevel="ADMIN";
           auth.setValue("rightslevel",rightslevel);
           isadmin=true;
           if (showarchivedstudies) {
              l=getMainDAO().execQuery(" select sd from Studies as sd join fetch sd.sites order by " +
                    auth.getValue("studysortcolumn") + " " + auth.getValue("studysortdir") + ",siteno,studyno" );
           } else {      
              l=getMainDAO().execQuery(" select sd from Studies as sd join fetch sd.sites where sd.isarchived!=true order by " +
                    auth.getValue("studysortcolumn") + " " + auth.getValue("studysortdir") + ",siteno,studyno" );
           } 

        } 
        else {
           isadmin=false;
           // Set rightslevel (internal/external)
           if (hasGlobalPerm(PcatConstants.SYSTEM,"INTERNALVIEW")) {
              rightslevel="INTERNAL";
           } else {
              rightslevel="EXTERNAL";
           }
           auth.setValue("rightslevel",rightslevel);

           // Pull relevant study list
           if (showarchivedstudies) {
              l=getMainDAO().execQuery(" select sd from Studies as sd join fetch sd.sites order by " +
                    auth.getValue("studysortcolumn") + " " + auth.getValue("studysortdir") + ",siteno,studyno" );
           } else {      
              l=getMainDAO().execQuery(" select sd from Studies as sd join fetch sd.sites where sd.isarchived!=true order by " +
                    auth.getValue("studysortcolumn") + " " + auth.getValue("studysortdir") + ",siteno,studyno" );
           } 

           // Iterate through list.  Remove if user has no rights to study.
           List gplist=getPermHelper().getGlobalPermList();
           Iterator i=l.iterator();
           while (i.hasNext()) {
              Studies std=(Studies)i.next();
              // Check global perm list for permissions
              boolean havematch=false;
              Iterator gpiter=gplist.iterator();
              while (gpiter.hasNext()) {
                 PermDetail detail=(PermDetail)gpiter.next();
                 if (detail.getPcatvalue()==PcatConstants.STUDY &&
                     detail.getSiteid()==std.getSites().getSiteid() &&
                     detail.getStudyid()==std.getStudyid()) {
                    havematch=true;
                    break;
                 }
              }
              if (havematch) continue;
              // check forms for permissions

              // get list of forms for which user has permission
              List permlist=getPermHelper().getFormPermList();
              // get list of current studies' forms
              List formlist=getMainDAO().execQuery(
                 "select fd from Datatabledef fd, Formha fa " +
                    "where fa.formhaid=fd.pformhaid and fa.studies.studyid=" + std.getStudyid()
                 );
              Iterator fiter=formlist.iterator();
              while (fiter.hasNext()) {
                 Datatabledef fd=(Datatabledef)fiter.next();
                 boolean match=false;
                 Iterator piter=permlist.iterator();
                 while (piter.hasNext()) {
                    HashMap map=(HashMap)piter.next();
                    Long dtdefid;
                    try {
                       dtdefid=(Long)map.get("dtdefid");
                    } catch (ClassCastException cce) {
                       dtdefid=new Long((String)map.get("dtdefid"));
                    }
                    if (dtdefid.longValue()==new Long(fd.getDtdefid()).longValue()) {
                       match=true;
                       break;
                    }
                 }
                 if (match) {
                    havematch=true;
                    break;
                 }
              }
              if (!havematch) {
                 i.remove();
              }
           }
        }   

        // see if has access to analyst tools
        getAuth().setValue("analystaccess",new Boolean(getPermHelper().hasAnalystSysPerm("ACCESS")).toString());
        // see if has access to study part of system
        getAuth().setValue("studyaccess",new Boolean(getPermHelper().hasGlobalPerm(PcatConstants.SYSTEM,"ACCESSCONTENT")).toString());

        // If only access to one study, bypass this screen
        // TODO Currently not skipping if have permission to make changes.  That's the only place
        // to make some edit study changes.  This should be corrected.
        if (l.size()==1 && !hasGlobalPerm(PcatConstants.SYSTEM,"MODSTUDY")) {
           Studies sd=(Studies)l.get(0);
           ArrayList rlist=new ArrayList();
           HashMap map=new HashMap();
           map.put("site",sd.getSites().getSiteno());
           map.put("study",sd.getStudyno());
           map.put("studydesc",sd.getStudydesc());
           map.put("siteid",sd.getSites().getSiteid());
           map.put("studyid",sd.getStudyid());
           rlist.add(map);
           result.setStatus("REDIRECT");
           result.setList(rlist);
           result.setIsadmin(isadmin);
           result.setRightslevel(auth.getValue("rightslevel"));
           return result;
        }

        // show site fields initialization
        boolean showsite=false;
        Long lastsite=null;

        Iterator i=l.iterator();
        while (i.hasNext()) {
           Map<String,String> map = new HashMap<String,String>();
           // NOTE:  May retrieve either studies or studyrights object
           Object o=i.next();
           try {
              Studies sd=(Studies)o;

              // Show site field?
              if (lastsite!=null) {
                 if (lastsite.longValue()!=sd.getSites().getSiteid()) {
                    showsite=true;
                 }
              }
              lastsite=new Long(sd.getSites().getSiteid());

              // export values to map
              map.put("site",sd.getSites().getSiteno());
              map.put("study",sd.getStudyno());
              map.put("studydesc",sd.getStudydesc());
              if (sd.getIsarchived()) {
                 map.put("archivestatus","X");
                 map.put("isarchived","T");
              } else {
                 map.put("archivestatus"," ");
                 map.put("isarchived","F");
              }
              map.put("siteid",new Long(sd.getSites().getSiteid()).toString());
              map.put("studyid",new Long(sd.getStudyid()).toString());
              passList.add(map);

           } catch (Exception e) { }
        }   

        result.setStatus("OK");
        result.setShowsite(showsite);
        result.setShowarchivedstudies(showarchivedstudies);
        result.setList(passList);
        result.setIsadmin(isadmin);
        result.setRightslevel(auth.getValue("rightslevel"));

        return result;

      } catch (Exception e) {

        throw new FORMSException("FORMS Service exception:  " + Stack2string.getString(e));

      }

   }
   
}


