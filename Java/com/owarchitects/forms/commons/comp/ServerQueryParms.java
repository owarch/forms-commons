 
/**
 *
 * This program is part of:
 *
 *    FORMS [Forms-Oriented Research Management System]
 *
 * Copyright (C) 2009 OpenWare Architects.  All Rights Reserved.
 *
 * FORMS is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * FORMS is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with FORMS.  If not, see <http://www.gnu.org/licenses/>.
 *
 */
 
/**
 * ServerQueryParms.java -- 
 *                           involving Service data transfers.
 * Original Creation -- 01/2008 (MRH)
 */

package com.owarchitects.forms.commons.comp;

import com.owarchitects.forms.commons.db.*;
import java.io.*;
import java.util.*;

public class ServerQueryParms extends FORMSCsaServiceParms implements Serializable {
 
   // shared parameters

   public Linkedinst linkedinst;
   public String querystring;
   public String[] parmnames;
   public Object[] parmvalues;

   // 

   public void setLinkedinst(Linkedinst linkedinst) {
      this.linkedinst=linkedinst;
   }

   public Linkedinst getLinkedinst() {
      return linkedinst;
   }

   // 

   public void setQuerystring(String querystring) {
      this.querystring=querystring;
   }

   public String getQuerystring() {
      return querystring;
   }

   // 

   public void setParmnames(String[] parmnames) {
      this.parmnames=parmnames;
   }

   public String[] getParmnames() {
      return parmnames;
   }

   // 

   public void setParmvalues(Object[] parmvalues) {
      this.parmvalues=parmvalues;
   }

   public Object[] getParmvalues() {
      return parmvalues;
   }

   // 

}

