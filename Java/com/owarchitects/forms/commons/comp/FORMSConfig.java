 
/**
 *
 * This program is part of:
 *
 *    FORMS [Forms-Oriented Research Management System]
 *
 * Copyright (C) 2009 OpenWare Architects.  All Rights Reserved.
 *
 * FORMS is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * FORMS is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with FORMS.  If not, see <http://www.gnu.org/licenses/>.
 *
 */
 
package com.owarchitects.forms.commons.comp;
/**
* FORMSConfig.java -- Reads Configuration file and Assigns Parameters
* Original Creation -- 07/2004 (MRH)
*/

import java.io.*;
import java.util.*;
import java.net.*;
//import com.adobe.fdf.*;
//import com.adobe.fdf.exceptions.*;
//import com.sas.sasserver.sclfuncs.*;
//import com.sas.rmi.*;
//import com.sas.sasserver.submit.*;
import javax.servlet.*;
import javax.servlet.http.*;
//import javax.crypto.*;
//import javax.crypto.spec.*;
//import java.security.*;
//import java.security.spec.*;
//import com.oreilly.servlet.*;
import org.apache.commons.lang.StringUtils;

public class FORMSConfig {

   private String pathVar = null;
   // Default timeout (seconds)
   private int Timeout = 600;
   private int PasswordExpire = 9999;
   private int MaxInvalidLogins = 3;
   private int MaxInvalidIPLogins = 10;
   private int WorkDirKeepDays = 5;

   private String BgColor = " ";
   private String BgImage = " ";
   private String MenuBgColor = " ";

   private String KeyStore = " ";
   private String KeyStorePassword = " ";
   private String EncryptionKey = " ";

   private String SasServer = " ";
   private String SasRootDir = " ";
   private int    SasConnectPort = -1;
   private String SasUserName = " ";
   private String SasEncryptedPassword = " ";

   private String RServer = " ";
   private int    RConnectPort = -1;
   private String RUserName = " ";
   private String REncryptedPassword = " ";

   private String AuthAttributeStorageLocation = "D";

   public FORMSConfig(String pathVar) {
      this.pathVar=pathVar;
      ReadParameters();
   }

   public FORMSConfig(HttpSession session) {
      this.pathVar=session.getServletContext().getRealPath("/");
      ReadParameters();
   }

   // READ CONFIGURATION FILE
   private void ReadParameters() {
      try {
         String f = pathVar + File.separator + "Conf" + 
                    File.separator + "FORMS.conf";
         BufferedReader in = new BufferedReader(new FileReader(f));
         String line;
         while((line = in.readLine()) != null) {
            if (line==null || line.length()<1) continue;
            try {
               if (line.substring(0,1).equalsIgnoreCase("#")) {
                  line="##COMMENT##";
               }
               if (StringUtils.contains(line,"Timeout:")) {
                  try {
                     int val = new Integer(StringUtils.trim(StringUtils.substringAfter(line,"Timeout:"))).intValue();
                     Timeout=val;
                  } catch (Exception t) {
                     // If invalid, use default value
                  }
               }
               else if (StringUtils.contains(line,"PasswordExpire:")) {
                  try {
                     int val = new Integer(StringUtils.trim(StringUtils.substringAfter(line,"PasswordExpire:"))).intValue();
                     PasswordExpire=val;
                  } catch (Exception t) {
                     // If invalid, use default value
                  }
               }
               else if (StringUtils.contains(line,"MaxInvalidLogins:")) {
                  try {
                     int val = new Integer(StringUtils.trim(StringUtils.substringAfter(line,"MaxInvalidLogins:"))).intValue();
                     MaxInvalidLogins=val;
                  } catch (Exception t) {
                     // If invalid, use default value
                  }
               }
               else if (StringUtils.contains(line,"MaxInvalidIPLogins:")) {
                  try {
                     int val = new Integer(StringUtils.trim(StringUtils.substringAfter(line,"MaxInvalidIPLogins:"))).intValue();
                     MaxInvalidIPLogins=val;
                  } catch (Exception t) {
                     // If invalid, use default value
                  }
               }
               else if (StringUtils.contains(line,"WorkDirKeepDays:")) {
                  try {
                     int val = new Integer(StringUtils.trim(StringUtils.substringAfter(line,"WorkDirKeepDays:"))).intValue();
                     WorkDirKeepDays=val;
                  } catch (Exception t) {
                     // If invalid, use default value
                  }
               }
               else if (StringUtils.contains(line,"KeyStore:")) {
                  KeyStore = StringUtils.trim(StringUtils.substringAfter(line,"KeyStore:"));
               }
               else if (StringUtils.contains(line,"KeyStorePassword:")) {
                  KeyStorePassword = StringUtils.trim(StringUtils.substringAfter(line,"KeyStorePassword:"));
               }
               else if (StringUtils.contains(line,"EncryptionKey:")) {
                  EncryptionKey = StringUtils.trim(StringUtils.substringAfter(line,"EncryptionKey:"));
               }
               ///////
               ///////
               ///////
               else if (StringUtils.contains(line,"BgColor:")) {
                  BgColor = StringUtils.trim(StringUtils.substringAfter(line,"BgColor:"));
               }
               else if (StringUtils.contains(line,"BgImage:")) {
                  BgImage = StringUtils.trim(StringUtils.substringAfter(line,"BgImage:"));
               }
               else if (StringUtils.contains(line,"MenuBgColor:")) {
                  MenuBgColor = StringUtils.trim(StringUtils.substringAfter(line,"MenuBgColor:"));
               } 
               ///////
               ///////
               ///////
               else if (StringUtils.contains(line,"SasServer:")) {
                  SasServer = StringUtils.trim(StringUtils.substringAfter(line,"SasServer:"));
               }
               else if (StringUtils.contains(line,"SasRootDir:")) {
                  SasRootDir = StringUtils.trim(StringUtils.substringAfter(line,"SasRootDir:"));
               }
               else if (StringUtils.contains(line,"SasConnectPort:")) {
                  try {
                     int val = new Integer(StringUtils.trim(StringUtils.substringAfter(line,"SasConnectPort:"))).intValue();
                     SasConnectPort=val;
                  } catch (Exception t) {
                     // If invalid, use default value
                  }
               }
               else if (StringUtils.contains(line,"SasUserName:")) {
                  SasUserName = StringUtils.trim(StringUtils.substringAfter(line,"SasUserName:"));
               }
               else if (StringUtils.contains(line,"SasEncryptedPassword:")) {
                  SasEncryptedPassword = StringUtils.trim(StringUtils.substringAfter(line,"SasEncryptedPassword:"));
               }
               ///////
               ///////
               ///////
               else if (StringUtils.contains(line,"RServer:")) {
                  RServer = StringUtils.trim(StringUtils.substringAfter(line,"RServer:"));
               }
               else if (StringUtils.contains(line,"RConnectPort:")) {
                  try {
                     int val = new Integer(StringUtils.trim(StringUtils.substringAfter(line,"RConnectPort:"))).intValue();
                     RConnectPort=val;
                  } catch (Exception t) {
                     // If invalid, use default value
                  }
               }
               else if (StringUtils.contains(line,"RUserName:")) {
                  RUserName = StringUtils.trim(StringUtils.substringAfter(line,"RUserName:"));
               }
               else if (StringUtils.contains(line,"REncryptedPassword:")) {
                  REncryptedPassword = StringUtils.trim(StringUtils.substringAfter(line,"REncryptedPassword:"));
               }
               ///////
               ///////
               ///////
               else if (StringUtils.contains(line,"AuthAttributeStorageLocation:")) {
                  AuthAttributeStorageLocation = StringUtils.trim(StringUtils.substringAfter(line,"AuthAttributeStorageLocation:"));
               }
               ///////
               ///////
               ///////
            } catch (Exception ex2) { }
         }
         
      } catch(Exception ex) { }
   }

   /////
   /////
   /////

   // RETURN Timeout VALUE
   public int getTimeout () {
      return Timeout;
   }

   // RETURN PasswordExpire VALUE
   public int getPasswordExpire () {
      return PasswordExpire;
   }

   // RETURN MaxInvalidLogins VALUE
   public int getMaxInvalidLogins () {
      return MaxInvalidLogins;
   }

   // RETURN MaxInvalidIPLogins VALUE
   public int getMaxInvalidIPLogins () {
      return MaxInvalidIPLogins;
   }

   // RETURN WorkDirKeepDays VALUE
   public int getWorkDirKeepDays () {
      return WorkDirKeepDays;
   }

   // RETURN KeyStore VALUE
   public String getKeyStore () {
      return KeyStore;
   }

   // RETURN KeyStorePassword VALUE
   public String getKeyStorePassword () {
      return KeyStorePassword;
   }

   // RETURN EncryptionKey VALUE
   public String getEncryptionKey () {
      return EncryptionKey;
   }

   /////
   /////
   /////

   // RETURN BgColor VALUE
   public String getBgColor () {
      return BgColor;
   }

   // RETURN BgImage VALUE
   public String getBgImage () {
      return BgImage;
   }

   // RETURN MenuBgColor VALUE
   public String getMenuBgColor () {
      return MenuBgColor;
   }

   /////
   /////
   /////

   // RETURN SasServer VALUE
   public String getSasServer () {
      return SasServer;
   }
   // RETURN SasRootDir VALUE
   public String getSasRootDir () {
      return SasRootDir;
   }
   // RETURN SasConnectPort VALUE
   public int getSasConnectPort () {
      return SasConnectPort;
   }
   // RETURN SasUserName VALUE
   public String getSasUserName () {
      return SasUserName;
   }
   // RETURN SasEncryptedPassword VALUE
   public String getSasEncryptedPassword () {
      return SasEncryptedPassword;
   }

   /////
   /////
   /////

   // RETURN RServer VALUE
   public String getRServer () {
      return RServer;
   }
   // RETURN RConnectPort VALUE
   public int getRConnectPort () {
      return RConnectPort;
   }
   // RETURN RUserName VALUE
   public String getRUserName () {
      return RUserName;
   }
   // RETURN REncryptedPassword VALUE
   public String getREncryptedPassword () {
      return REncryptedPassword;
   }

   /////
   /////
   /////

   public String getAuthAttributeStorageLocation () {
      return AuthAttributeStorageLocation;
   }

   /////
   /////
   /////

}


