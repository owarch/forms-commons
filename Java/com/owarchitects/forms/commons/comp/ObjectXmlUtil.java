 
/**
 *
 * This program is part of:
 *
 *    FORMS [Forms-Oriented Research Management System]
 *
 * Copyright (C) 2009 OpenWare Architects.  All Rights Reserved.
 *
 * FORMS is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * FORMS is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with FORMS.  If not, see <http://www.gnu.org/licenses/>.
 *
 */
 
/*
 *
 * ObjectXmlUtil.java - Utility to encode objects to an XML string and decode XML
 *                      strings back to objects
 *
 */

package com.owarchitects.forms.commons.comp;

import java.util.*;
import java.io.*;
import java.beans.*;
import java.lang.reflect.*;

/*
 * IMPORTANT NOTE:  Careful here using this for object transfer.  Does not always
 * encode object other than standard String and numeric objects.  Dates, for example,
 * are often not sent.
 */

public class ObjectXmlUtil {

   // Instances should not be created
   public ObjectXmlUtil() {
      super();
   }

   // Encode object to XML String
   public static String objectToXmlString(Object o) {

      if (o==null) return null;

      ByteArrayOutputStream bos=new ByteArrayOutputStream();
      XMLEncoder enc=new XMLEncoder(new BufferedOutputStream(bos));

      enc.writeObject(o);
      enc.close();
      return bos.toString();

   }
   
   // Return object from XML String
   public static Object objectFromXmlString(String ins) {

      if (ins==null) return null;

      ByteArrayInputStream bais=new ByteArrayInputStream((ins).getBytes());
      BufferedInputStream bis=new BufferedInputStream(bais);
      XMLDecoder dcr=new XMLDecoder(bis);
      return dcr.readObject();

   }

}

