 
/**
 *
 * This program is part of:
 *
 *    FORMS [Forms-Oriented Research Management System]
 *
 * Copyright (C) 2009 OpenWare Architects.  All Rights Reserved.
 *
 * FORMS is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * FORMS is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with FORMS.  If not, see <http://www.gnu.org/licenses/>.
 *
 */
 
/*
 * SelectStudyResult.java - Output parameters for SelectStudyService - MRH 03/2007
 */

package com.owarchitects.forms.commons.comp;

import java.util.*;
import java.io.*;

public class SelectStudyResult extends FORMSResult implements Serializable {

   public ArrayList list;
   public boolean isadmin;
   public boolean showsite;
   public boolean showarchivedstudies;
   public String rightslevel;

   //

   public void setList(ArrayList list) {
      this.list=list;
   }

   public ArrayList getList() {
      return list;
   }

   //

   public void setIsadmin(boolean isadmin) {
      this.isadmin=isadmin;
   }

   public boolean getIsadmin() {
      return isadmin;
   }

   //

   public void setShowsite(boolean showsite) {
      this.showsite=showsite;
   }

   public boolean getShowsite() {
      return showsite;
   }

   //

   public void setShowarchivedstudies(boolean showarchivedstudies) {
      this.showarchivedstudies=showarchivedstudies;
   }

   public boolean getShowarchivedstudies() {
      return showarchivedstudies;
   }

   //

   public void setRightslevel(String rightslevel) {
      this.rightslevel=rightslevel;
   }

   public String getRightslevel() {
      return rightslevel;
   }

   //

}


