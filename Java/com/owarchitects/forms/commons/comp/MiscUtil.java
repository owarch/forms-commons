
/**
* MiscUtil.java -- Miscellaneous Utilities
* Original Creation -- 06/2007 (MRH)
*/

package com.owarchitects.forms.commons.comp;

import java.io.*;
import java.util.*;
import javax.servlet.http.*;
import org.apache.commons.io.FileUtils;

public class MiscUtil {
   
   // Instances should not be created
   public MiscUtil() {
      super();
   }

   // Return cookie specified by input string.  Returns empty string if no such cookie.
   public static String getCookieValue(HttpServletRequest request,String cstr) {
      try {
         Cookie[] carray=request.getCookies();
         Iterator i=Arrays.asList(carray).iterator();
         while (i.hasNext()) {
            Cookie c=(Cookie)i.next();
            if (c.getName().equals(cstr)) {
               return c.getValue();
            }
         }
      } catch (Exception ce) {}   
      return "";
   }

   // Temporary methods for development use
   public static void writeTemp(int num,Object obj) {
      try {
         if (obj==null) {
            writeStringToFile(new File(getTempDir() + "junkfil_" + System.currentTimeMillis() + "_" + num),"null");
         } else if ((String.class).isInstance(obj)) {
            writeStringToFile(new File(getTempDir() + "junkfil_" + System.currentTimeMillis() + "_" + num),(String)obj);
         } else if ((Throwable.class).isInstance(obj)) {
            writeStringToFile(new File(getTempDir() + "junkfil_" + System.currentTimeMillis() + "_" + num),Stack2string.getString((Throwable)obj));
         } else {
            writeStringToFile(new File(getTempDir() + "junkfil_" + System.currentTimeMillis() + "_" + num),ObjectXmlUtil.objectToXmlString(obj));
         }
      } catch (Exception ex) {
         writeStringToFile(new File(getTempDir() + "junkfil_" + System.currentTimeMillis() + "_" + num),"MiscUtil.writeTemp threw an exception \n" + Stack2string.getString(ex));
      }
      return;
   }
   public static void writeTemp(String str,Object obj) {
      try {
         if (obj==null) {
            writeStringToFile(new File(getTempDir() + "junkfil_" + System.currentTimeMillis() + "_" + str),"null");
         } else if ((String.class).isInstance(obj)) {
            writeStringToFile(new File(getTempDir() + "junkfil_" + System.currentTimeMillis() + "_" + str),(String)obj);
         } else if ((Throwable.class).isInstance(obj)) {
            writeStringToFile(new File(getTempDir() + "junkfil_" + System.currentTimeMillis() + "_" + str),Stack2string.getString((Throwable)obj));
         } else {
            writeStringToFile(new File(getTempDir() + "junkfil_" + System.currentTimeMillis() + "_" + str),ObjectXmlUtil.objectToXmlString(obj));
         }
      } catch (Exception ex) {
         writeStringToFile(new File(getTempDir() + "junkfil_" + System.currentTimeMillis() + "_" + str),"MiscUtil.writeTemp threw an exception \n" + Stack2string.getString(ex));
      }
      return;
   }

   private static String getTempDir() {
      String rs;
      rs=System.getenv("TEMP");
      if (rs!=null && rs.length()>0) {
         return rs + File.separator;
      }
      rs=System.getenv("TMP");
      return rs + File.separator;
   }

   // Transform username to ignore "_" characters
   public static void writeStringToFile(File tempfile,Object obj,String enc) {
      String str;
      if (obj.getClass().getName().equals("java.lang.String")) {
         str=(String)obj;
      } else {
         str=ObjectXmlUtil.objectToXmlString(obj);
      }
      try {
         FileUtils.writeStringToFile(tempfile,str,enc);
      } catch (Exception ex) { }
      return;
   }

   public static void writeStringToFile(File tempfile,Object obj) {
      try {
         writeStringToFile(tempfile,obj,"UTF-8");
      } catch (Exception ex) { }
      return;
   }

   public static void writeStringToFile(String fn,Object obj,String enc) {
      File tempfile=new File(fn);
      writeStringToFile(tempfile,obj,enc);
      return;
   }

   public static void writeStringToFile(String fn,Object obj) {
      File tempfile=new File(fn);
      writeStringToFile(tempfile,obj);
      return;
   }

   public static String readFileToString(File tempfile) {
      try {
         return FileUtils.readFileToString(tempfile,"UTF-8");
      } catch (Exception ex) { }
      return null;
   }

   public static String readFileToString(File tempfile,String enc) {
      try {
         return FileUtils.readFileToString(tempfile,enc);
      } catch (Exception ex) { }
      return null;
   }

   public static String readFileToString(String fn,String enc) {
      File tempfile=new File(fn);
      return readFileToString(tempfile,enc);
   }

   public static String readFileToString(String fn) {
      File tempfile=new File(fn);
      return readFileToString(tempfile);
   }

}

        


