 
/**
 *
 * This program is part of:
 *
 *    FORMS [Forms-Oriented Research Management System]
 *
 * Copyright (C) 2009 OpenWare Architects.  All Rights Reserved.
 *
 * FORMS is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * FORMS is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with FORMS.  If not, see <http://www.gnu.org/licenses/>.
 *
 */
 
/* 
 *
 * CsaRecordLockReleaseUtil.java - CSA Utility for releasing record locks 
 *
 */

package com.owarchitects.forms.commons.comp;

import com.owarchitects.forms.commons.db.*;

import java.util.*;
import java.io.*;
import java.beans.*;
import java.lang.reflect.*;

import org.apache.axis2.context.*;
import org.apache.axis2.client.*;
import org.apache.axis2.client.async.AxisCallback;
import org.apache.axis2.transport.http.HTTPConstants;
import org.apache.axis2.AxisFault;
import org.apache.axis2.addressing.EndpointReference;
import org.apache.axis2.rpc.client.RPCServiceClient;
import org.apache.velocity.Template;
import org.apache.velocity.app.VelocityEngine;
import javax.xml.namespace.QName;

import javax.net.ssl.*;
import javax.servlet.http.*;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.context.support.AbstractApplicationContext;

public class CsaRecordLockReleaseUtil { //extends FORMSServiceClientUtil {

   // Instances should not be created
   public CsaRecordLockReleaseUtil() {
      super();
   }

   public static void releaseRecordLocks(HttpSession session,AbstractApplicationContext context,FORMSAuth auth,File syswork) {
      try {

         // retrieve info about record
         LockInfo linfo=(LockInfo)auth.getObjectValue("lockinfo");
         if (linfo==null) return; 
         Long datarecordid=linfo.getDatarecordid();
         if (datarecordid==null) return;
         Boolean islocked=linfo.getIslocked();
         if (islocked.booleanValue()==false) return;
         int formloc=linfo.getFormlocation();
         Linkedinst forminst=linfo.getLinkedinst();

         // pull record
         String querystring="select f from Datatablerecords f where f.datarecordid=" + datarecordid;
         Datatablerecords rec=null;
         ServerQueryParms qparms=new ServerQueryParms();
         qparms.setQuerystring(querystring);
         if (formloc==LockInfo.LOCALFORM) {
            // Set parameters for local form
            qparms.setBeanname("csaServerQueryServiceTarget");
            qparms.setMethodname("execUniqueServerQuery");
         } else {
            // Set parameters for remote form
            qparms.setLinkedinst(forminst);
            qparms.setBeanname("csaServerQueryServiceTarget");
            qparms.setMethodname("execUniqueRemoteServerQuery");
         }
         CsaServiceObjectResult qresult=(CsaServiceObjectResult)CsaServiceClientUtil.submitServiceRequest(session,context,auth,syswork,qparms);
         Object[] oarray=(Object[])qresult.getObject();
         rec=(Datatablerecords)oarray[0];
         if (rec==null) return;

         // Information map to be passed to service target
         HashMap map=new HashMap();
         map.put("formloc",formloc);
         map.put("forminst",forminst);
         map.put("record",rec);
         map.put("auserid",auth.getValue("auserid"));

         // release lock for record
         CsaServicePassObjectParms lparms=new CsaServicePassObjectParms("csaRecordLockingServiceTarget","unlockRecord");
         lparms.setObject(map);
         CsaServiceStatusResult lresult=(CsaServiceStatusResult)CsaServiceClientUtil.submitServiceRequest(session,context,auth,syswork,lparms);
         if (lresult.getStatus()==FORMSServiceConstants.OK) {
            auth.setObjectValue("lockinfo",null);
         } 

      } catch (Exception lex) {
         // Do nothing
      }
   } 

}

