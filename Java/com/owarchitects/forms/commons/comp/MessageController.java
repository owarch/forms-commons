 
/**
 *
 * This program is part of:
 *
 *    FORMS [Forms-Oriented Research Management System]
 *
 * Copyright (C) 2009 OpenWare Architects.  All Rights Reserved.
 *
 * FORMS is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * FORMS is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with FORMS.  If not, see <http://www.gnu.org/licenses/>.
 *
 */
 
package com.owarchitects.forms.commons.comp;

import java.util.*;
import java.io.*;
import org.springframework.web.servlet.*;
import org.springframework.web.servlet.mvc.*;
import org.springframework.context.support.*;
import javax.servlet.http.*;

public class MessageController extends AbstractController {
    
    public ModelAndView handleRequestInternal(HttpServletRequest request, HttpServletResponse response) throws Exception {

        // Get context
        HttpSession session=request.getSession();
        String appPath=session.getServletContext().getRealPath("/");
        ModelAndView mav=new ModelAndView("Message");
        WinProps wp=SessionObjectUtil.getWinPropsObj(request,response);
        wp.setWinNameMissOk("N");

        wp.addViewObjects(mav);
        mav.addObject("status",request.getServletPath());
        return mav;
    }

}

