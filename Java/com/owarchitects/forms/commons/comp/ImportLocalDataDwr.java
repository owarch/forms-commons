 
/**
 *
 * This program is part of:
 *
 *    FORMS [Forms-Oriented Research Management System]
 *
 * Copyright (C) 2009 OpenWare Architects.  All Rights Reserved.
 *
 * FORMS is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * FORMS is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with FORMS.  If not, see <http://www.gnu.org/licenses/>.
 *
 */
 
/**
* ImportLocalDataDwr.java -- This program processes form data saved locally on
*     the client.  It creates JavaScript that is sent back to populate the client-side
*     form.  Dwr isn't really used in this process, but is used by the class name
*     to distinguish this as a class used by the forms.
* Original Creation:  05/2007 (MRH)
*/

package com.owarchitects.forms.commons.comp;

import java.io.*;
import java.util.*;
//import com.adobe.fdf.*;
//import com.adobe.fdf.exceptions.*;
//import com.sas.sasserver.sclfuncs.*;
//import com.sas.rmi.*;
//import com.sas.sasserver.submit.*;
import javax.servlet.*;
import javax.servlet.http.*;
import org.apache.velocity.app.*;
import org.apache.velocity.*;
import com.oreilly.servlet.*;
import com.oreilly.servlet.multipart.*;
import org.apache.commons.io.FileUtils;
import org.apache.commons.httpclient.util.URIUtil;
import org.apache.commons.lang.ArrayUtils;
import org.apache.commons.lang.StringUtils;
import org.hibernate.*;
import org.springframework.web.servlet.*;
import org.springframework.context.support.*;
import org.springframework.orm.hibernate3.*;
import org.springframework.orm.hibernate3.*;
import org.springframework.transaction.support.TransactionSynchronizationManager;
import org.json.simple.JSONObject;

import java.util.regex.*;

public class ImportLocalDataDwr extends FORMSController {

   public ModelAndView submitRequest() throws FORMSKeyException,FORMSSecurityException,FORMSException {
      try {

         // UPLOAD TASK SHOULD BE MULTIPART REQUEST
         if (getRequest().getContentType().toLowerCase().indexOf("multipart")>=0) {

            //String keyStr = "!@#$%^&*();<>[]{},.|\\-_'?~AbCdEfGhIjKlMnOpQrStUvWxYz0123456789+/=";
            String keyStr = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/=";
            MultipartParser mp = new MultipartParser(getRequest(), 100*1024*1024); // 100MB
            Part part;
            while ((part = mp.readNextPart()) != null) {
               StringBuilder sb = new StringBuilder();
               if (part.isFile()) {
                  FilePart filePart = (FilePart) part;
                  String fileName = filePart.getFileName();
                  String fileString=null;
                  if (fileName != null) {
                     fileName=fileName.trim();
                     ByteArrayOutputStream baos = new ByteArrayOutputStream();
                     long size = filePart.writeTo(baos);
                     // Transform filepart (remove surrounding html tags if any and extraneous bytes)
                     byte[] barray = baos.toByteArray();
                     byte[] karray = keyStr.getBytes("ISO-8859-1");
                     boolean nooutput=false;
                     for (int i=0;i<barray.length;i++) {
                         byte[] sbarray=new byte[1];
                         sbarray[0]=barray[i];
                         String t=new String(sbarray,"ISO-8859-1");
                         if (t.equals("<")) {
                            nooutput=true;
                         } else if (t.equals(">")) {
                            nooutput=false;
                            continue;
                         }   
                         if (nooutput) {
                            continue;
                         }
                         if (ArrayUtils.contains(karray,barray[i])) {
                            sb.append(t);
                         }
                     }

                     // final transformed file string
                     fileString=URIUtil.decode(Base64.decode(sb.toString(),keyStr));
                     // create JavaScript to assign values

                  }

                  // BEGIN PARSING FROM STRING BUFFER TO RETRIEVE VALUES

                  StringBuilder sb_f=new StringBuilder(fileString);
                  StringBuilder sb_vname = new StringBuilder();
                  StringBuilder sb_vvalue = new StringBuilder();
                  String formPathVar=new String();
                  char ch;
                  int level=0;
                  boolean captureName=false;
                  boolean captureValue=true;
                  ArrayList a_vnames = new ArrayList();
                  ArrayList a_vpath = new ArrayList();
                  ArrayList a_vvalues = new ArrayList();
                  for (int i=0;i<sb_f.length();i++) {
                     ch = sb_f.charAt(i);
                     if (ch=='<' && sb_f.charAt(i+1)!='/') {
                        level++;
                        sb_vname=new StringBuilder("");
                        sb_vvalue=new StringBuilder("");
                        captureName=true;
                        captureValue=false;
                     } else if (ch=='<') {
                        // if sb_vname is missing then this is the end of a path field, so delete a block
                        // from the path
                        if (sb_vname.toString().trim().length()<1) {
                           formPathVar=formPathVar.substring(0,formPathVar.lastIndexOf("."));
                        }
                        level--;
                        captureValue=false;
                        // The _rmsEnter... fields may have been added by the processform program
                        if (sb_vname.length()>=1 && !(sb_vname.length()>9 && sb_vname.substring(0,9).equals("_rmsEnter"))) {
                           a_vnames.add(sb_vname); 
                           try {
                              String _formTemp=formPathVar.substring(1);
                              _formTemp=_formTemp.substring(0,_formTemp.indexOf("."));
                              a_vpath.add(_formTemp);
                           } catch (Exception e3) {
                              try {
                                 a_vpath.add(formPathVar.substring(1));
                              } catch (Exception e4) { } 
                           }
                           if (sb_vvalue.length()<1) {
                              // NOTE:  Need the space here for SAS use later
                              sb_vvalue=new StringBuilder(" ");
                           }
                           // WORKAROUND FOR BUG IN ADOBE WHERE DROPDOWN LISTS, WHEN RETURNED TO BLANKS
                           // FILL THE EXPORT FILE WITH UNPRINTABLE CHARACTERS, UTF-8 WONT BE ABLE TO
                           // CONVERT ALL OF THEM, SO IT WONT BE EQUAL TO THE ISO-8859-1 ENCODING
                           if (!new String(sb_vvalue.toString().getBytes("UTF-8")).equals(
                                new String(sb_vvalue.toString().getBytes("ISO-8859-1")))
                               ) {
                              sb_vvalue=new StringBuilder(" ");
                           }
            	       // WE HAVE TO CONVERT ALL QUOTES TO BACKSLASH-QUOTE OR VALUES WILL NOT BE RETRIEVED
            	       // ALSO CONVERT ALL BACKSLASHES TO DOUBLE BACKSLASHES
                           sb_vvalue=new StringBuilder(StringUtils.replace(new String(sb_vvalue),"\\","\\\\"));
                           sb_vvalue=new StringBuilder(StringUtils.replace(new String(sb_vvalue),"\"","\\\""));
                           a_vvalues.add(sb_vvalue);
                           sb_vname=new StringBuilder("");
                           sb_vvalue=new StringBuilder("");
                        } else {
                           break;
                        }
                     } else if (ch=='>') {
                        if (sb_f.charAt(i-1)!='/') {
                           // LOCATE PATH TO VARIABLES 
                           try {
                              if (sb_f.charAt(i+1)=='\n' && captureName && !captureValue) {
                                 formPathVar=formPathVar + "." + sb_vname; 
                              }
                              captureName=false;
                              captureValue=true;
                           } catch (Exception e4) { } 
                        } else {
                           captureName=false;
                           level--;
                           // The _rmsEnter... fields may have been added by the processform program
                           if (sb_vname.length()>=1 && !(sb_vname.length()>9 && sb_vname.substring(0,9).equals("_rmsEnter"))) {
                              a_vnames.add(sb_vname);
                              try {
                                 String _formTemp=formPathVar.substring(1);
                                 _formTemp=_formTemp.substring(0,_formTemp.indexOf("."));
                                 a_vpath.add(_formTemp);
                              } catch (Exception e3) {
                                 try {
                                    a_vpath.add(formPathVar.substring(1));
                                 } catch (Exception e4) { } 
                              }
                              if (sb_vvalue.length()<1) {
                                 // NOTE:  Need the space here for SAS use later
                                 sb_vvalue=new StringBuilder(" ");
                              }
                              a_vvalues.add(sb_vvalue);
                              sb_vname=new StringBuilder("");
                              sb_vvalue=new StringBuilder("");
                           } else {
                              break;
                           }
                        }
                     } else if (captureName && ch!='/') {
                        sb_vname.append(ch);
                     } else if (captureValue) {
                        sb_vvalue.append(ch);
                     } 
                  }
         
                  // CREATE JAVASCRIPT ASSIGNMENTS
    
                  // NOTE:  The resetData method call here is important.  When you go "BACK", it returns
                  // you to the state when the form was first loaded.  If a value was entered and you
                  // cleared it out, that would not be reflected when going back and the JavaScript only
                  // contains code to set fields that have values (keeps it smaller).
                  //StringBuilder sb_sas=new StringBuilder("\"resetForm(); \" +\n");
                  //sb_sas.append("\"resetForm(); \\n\" +");
                  StringBuilder sb_sas=new StringBuilder();
                  String _fnvar_="", _fnval_="",_fvvar_="",_fvval_="";
                  for (int i=0; i<a_vnames.size(); i++) {
                     if (a_vnames.get(i).toString().equalsIgnoreCase("dtdefid_")) {
                        _fnvar_=a_vnames.get(i).toString();
                        _fnval_=a_vvalues.get(i).toString();
                        break;
                     } 
                  }
                  
                  sb_sas.append("\"if (getFormElementById(\\\"" + _fnvar_ + "\\\").value==\\\"" + _fnval_ + "\\\") {\\n\" + \n"); 

                  sb_sas.append("\"   resetForm();\\n\" + \n");

                  for (int i=0; i<a_vnames.size(); i++) {
                     if (a_vvalues.get(i).toString().trim().length()>=1) {
                        // Want to use current sessions rmsPassValueField_
                        
                        // NOTE:  getFormElementArray used here to set all instances of global fields
                        // We change focus to element so validation is run.  This seems to be the only reliable
                        // way to get it to run.  Kicking off script didn't work
                        sb_sas.append( 
                              "\"   try { setFormValue(getFormElementArrayById(\\\"" + a_vnames.get(i).toString() +
                              "\\\"),decodeURI(\\\"" +
                              URIUtil.encodePathQuery(a_vvalues.get(i).toString()).replaceAll("&gt;",">").replaceAll("&lt;","<").replaceAll("%5Cn","%0D%0A") +  
                              "\\\")); getFormElementById(\\\"" + a_vnames.get(i).toString() + "\\\").focus() } catch (e) {} \\n\" + \n" 
                        ); 

                     } 
                  }
                  // THIS MUST BE LAST, SO WE ARE NOT SETTING PASSVALUE FIELD FROM FILE THAT WAS READ IN
                  // Contains server information passed to form
                  JSONObject passv=new JSONObject();
                  passv.put("submittype","temprecord");
                  passv.put("rightslevel",getAuth().getValue("rightslevel"));
                  passv.put("servername",getRequest().getServerName());
                  passv.put("continueservlet","selectform.do");
                  sb_sas.append("\"try { _rmsPassValueField='" + passv.toString().replaceAll("\\\"","\\\\\"") + "';\\n\" + \n");
                  sb_sas.append("\"_rmsPassValueObject=jsonParse(_rmsPassValueField); } catch (e) {}\\n\" + \n");
            
                  sb_sas.append("\"_docReady=0;\\n\" + \n");
                  sb_sas.append("\"try { execValidate(); } catch (e) {}\\n\" + \n");
                  sb_sas.append("\"_saveStatus=\\\"OK\\\";\\n\" + \n");
                  sb_sas.append("\"_docReady=1;\\n\" + \n");
                  // Return focus back to ID
                  sb_sas.append("\"   getFormElementById('LocalButton').focus();\\n\" + \n");
                  sb_sas.append("\"   execValidate();\\n\" + \n");
                  sb_sas.append("\"} else {\\n\" + \n");
                  sb_sas.append("\"   alert(\\\"ERROR:  The dtdefid value in the saved file do not \\\" +\" +\n" +
                                "\"\\\"match those in the form.  These values must match to ensure values are pulled \\\" +\" +\n" +
                                "\"\\\"into the correct form.  Please check these values and use the corresponding form:\\\\n\\\\n \\\" +\" +\n" +
                                "\"\\\"   dtdefid_=" + _fnval_ + "\\\\n\\\");\" + \n");
                  sb_sas.append("\"}\\n\" + \n");

          
                  // it's a file part
                  String outscript=sb_sas.toString();
                  out.println("<script>");
                  out.println(
                     "var s = parent.parent.opener.document.createElement('script'); \n" +
                     "s.type = 'text/javascript'; \n" +
                     "s.text = " + outscript + 
                     "\" \";\n" +  
                     "parent.parent.opener.document.appendChild(s); \n" +
                     "parent.window.close();\n" 
                  );   
                  out.println("</script>");

               }
            } 
         } 
         return null;
      } catch(Exception ex) {
         return null;
      }
   }

}


