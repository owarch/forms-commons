 
/**
 *
 * This program is part of:
 *
 *    FORMS [Forms-Oriented Research Management System]
 *
 * Copyright (C) 2009 OpenWare Architects.  All Rights Reserved.
 *
 * FORMS is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * FORMS is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with FORMS.  If not, see <http://www.gnu.org/licenses/>.
 *
 */
 
/* 
 *
 * BasicResource.java -  Interface for basic resources used in the FORMS
 *   system
 *
 */

package com.owarchitects.forms.commons.comp;

import com.owarchitects.forms.commons.db.*;

import java.util.*;
import java.io.*;
import javax.servlet.*;
import javax.servlet.http.*;
import java.security.*;

import org.springframework.context.*;
import org.springframework.context.support.*;
import org.springframework.orm.hibernate3.*;
import org.hibernate.*;
import org.hibernate.transform.*;
import org.apache.velocity.Template;
import org.apache.velocity.app.VelocityEngine;
import org.apache.commons.dbcp.BasicDataSource;
import com.mchange.v2.c3p0.ComboPooledDataSource;
import org.springframework.transaction.support.TransactionSynchronizationManager;


public interface BasicResource {

   public PrintWriter getWriter();

   public HttpSession getSession();

   public HttpServletRequest getRequest();

   public HttpServletResponse getResponse();

   public AbstractApplicationContext getContext();

   public String getCookieValue(String cstr);

   public Session getHibernateSession(String sessionfactory);

   public Session getHibernateSession();

   public void bindHibernateSession(String sessionfactory);

   public void bindHibernateSession();
   
   public void releaseHibernateSession();

   public String getAppPath();

   public MainDAO getMainDAO();
   
   public EmbeddedDAO getEmbeddedDAO();

   public void initDataSource();

}

