 
/**
 *
 * This program is part of:
 *
 *    FORMS [Forms-Oriented Research Management System]
 *
 * Copyright (C) 2009 OpenWare Architects.  All Rights Reserved.
 *
 * FORMS is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * FORMS is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with FORMS.  If not, see <http://www.gnu.org/licenses/>.
 *
 */
 
/* 
 *
 * FORMSSsaAllServiceClientController.java -  Extends FORMSController to include SOAP helpers
 *
 */

package com.owarchitects.forms.commons.comp;

import com.owarchitects.forms.commons.db.*;

import java.util.*;
import java.io.*;
import java.beans.*;
import java.lang.reflect.*;

import org.apache.axis2.context.*;
import org.apache.axis2.client.*;
import org.apache.axis2.client.async.AxisCallback;
import org.apache.axis2.transport.http.HTTPConstants;
import org.apache.axis2.AxisFault;
import org.apache.axis2.addressing.EndpointReference;
import org.apache.axis2.rpc.client.RPCServiceClient;
import org.apache.velocity.Template;
import org.apache.velocity.app.VelocityEngine;
import javax.xml.namespace.QName;

import javax.net.ssl.*;
import javax.servlet.http.*;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.context.support.AbstractApplicationContext;

public class SsaServiceClientUtil extends FORMSServiceClientUtil {

   // Instances should not be created
   public SsaServiceClientUtil() {
      super();
   }


   //////////////////////////
   //////////////////////////
   //////////////////////////
   // FORMSService Methods //
   //////////////////////////
   //////////////////////////
   //////////////////////////


   // Submit FORMSService Request
   public static FORMSServiceResult submitServiceRequest(HttpSession session,AbstractApplicationContext context,
         FORMSAuth auth,File syswork,Embeddedinst einst,FORMSServiceParms parms) throws FORMSException {

      LinkedinstDAO linkedinstDAO=(LinkedinstDAO)context.getBean("linkedinstDAO");
      Linkedinst linst=linkedinstDAO.getRecord(einst.getAcronym());

      if (linst==null) {
         throw new FORMSException("Couldn't pull linkedinst record");
      }

      return submitServiceRequest(session,context,auth,syswork,einst,linst,parms);

   }

   // Submit FORMSService Request
   public static FORMSServiceResult submitServiceRequest(HttpSession session,AbstractApplicationContext context,
         FORMSAuth auth,File syswork,Linkedinst linst,FORMSServiceParms parms) throws FORMSException {

      EmbeddedinstDAO embeddedinstDAO=(EmbeddedinstDAO)context.getBean("embeddedinstDAO");
      Embeddedinst einst=embeddedinstDAO.getRecordByAcronym(linst.getAcronym());

      if (einst==null) {
         throw new FORMSException("Couldn't pull embeddedinst record");
      }

      return submitServiceRequest(session,context,auth,syswork,einst,linst,parms);

   }

   // Submit FORMSService Request
   public static FORMSServiceResult submitServiceRequest(HttpSession session,AbstractApplicationContext context,
         FORMSAuth auth,File syswork,Embeddedinst einst,Linkedinst linst,FORMSServiceParms parms) throws FORMSException {

      Method im;
      Object oj;

      try {
         // Write codeword, checkstring parameters
         im=parms.getClass().getMethod("setCodeword",new Class[] { String.class });
         oj=im.invoke(parms,new Object[] { einst.getCodeword() });
         im=parms.getClass().getMethod("setLuserid",new Class[] { String.class });
         oj=im.invoke(parms,new Object[] { auth.getValue("luserid") });
         im=parms.getClass().getMethod("setConnectstring",new Class[] { String.class });
         oj=im.invoke(parms,new Object[] { linst.getConnectstring() });
         im=parms.getClass().getMethod("setCheckstring",new Class[] { String.class });
         oj=im.invoke(parms,new Object[] { EncryptUtil.serviceCheckEncrypt(session,linst) });
      } catch (Exception oke1) { 
         FORMSLog.append(syswork,Stack2string.getString(oke1));
         try {
            return getErrorObject(context,parms,FORMSServiceConstants.SERVICECLIENT_EXCEPTION);
         } catch (Exception oke1b) {
            FORMSLog.append(syswork,Stack2string.getString(oke1b));
            throw new FORMSException(oke1b,"submitServiceRequest exception");
         }
      }

      // invoke superclass method
      return submitServiceRequest(session,parms,linst.getUrl() + "/formsserviceinterceptor.do");

   }

   // Submit SOAP Request (localhost SOAP server)
   public static FORMSServiceResult submitLocalServiceRequest(HttpServletRequest request,HttpSession session,AbstractApplicationContext context,
         FORMSAuth auth,String sysconn,File syswork,FORMSServiceParms parms) throws FORMSException {

      Method im;
      Object oj;

      try {
         // Write codeword, checkstring parameters
         im=parms.getClass().getMethod("setLuserid",new Class[] { String.class });
         oj=im.invoke(parms,new Object[] { auth.getValue("luserid") });
         im=parms.getClass().getMethod("setConnectstring",new Class[] { String.class });
         oj=im.invoke(parms,new Object[] { sysconn });
         im=parms.getClass().getMethod("setCheckstring",new Class[] { String.class });
         oj=im.invoke(parms,new Object[] { EncryptUtil.serviceCheckEncrypt(session,null) });
      } catch (Exception oke1) { 
         FORMSLog.append(syswork,Stack2string.getString(oke1));
         try {
            return getErrorObject(context,parms,FORMSServiceConstants.SERVICECLIENT_EXCEPTION);
         } catch (Exception oke1b) {
            FORMSLog.append(syswork,Stack2string.getString(oke1b));
            throw new FORMSException(oke1b,"submitServiceRequest exception");
         }
      }

      String url=request.getRequestURL().toString();

      // invoke superclass method
      return submitServiceRequest(session,parms,url.substring(0,url.lastIndexOf("/")) + "/formsserviceinterceptor.do");

   }

   // Submit SOAP Request (localhost SOAP server)
   public static FORMSServiceResult submitLocalServiceRequest(String url,HttpSession session,AbstractApplicationContext context,
         FORMSAuth auth,String sysconn,File syswork,FORMSServiceParms parms) throws FORMSException {

      Method im;
      Object oj;

      try {
         // Write codeword, checkstring parameters
         im=parms.getClass().getMethod("setLuserid",new Class[] { String.class });
         oj=im.invoke(parms,new Object[] { auth.getValue("luserid") });
         im=parms.getClass().getMethod("setConnectstring",new Class[] { String.class });
         oj=im.invoke(parms,new Object[] { sysconn });
         im=parms.getClass().getMethod("setCheckstring",new Class[] { String.class });
         oj=im.invoke(parms,new Object[] { EncryptUtil.serviceCheckEncrypt(session,null) });
      } catch (Exception oke1) { 
         FORMSLog.append(syswork,Stack2string.getString(oke1));
         try {
            return getErrorObject(context,parms,FORMSServiceConstants.SERVICECLIENT_EXCEPTION);
         } catch (Exception oke1b) {
            FORMSLog.append(syswork,Stack2string.getString(oke1b));
            throw new FORMSException(oke1b,"submitServiceRequest exception");
         }
      }

      // invoke superclass method
      return submitServiceRequest(session,parms,url.substring(0,url.lastIndexOf("/")) + "/formsserviceinterceptor.do");

   }

   // Submit joined study FORMSService-Based Request (
   public static List submitServiceStudyRequest(HttpSession session,AbstractApplicationContext context,
         FORMSAuth auth,File syswork,FORMSServiceParms parms) throws FORMSException,FORMSKeyException,FORMSSecurityException {

      Method im;
      Object oj;

      List lslist=getMainDAO(context).execQuery(
         "select l from Linkedstudies l join fetch l.linkedinst where l.studies.studyid=" + auth.getValue("studyid")
         );
      
      List resultlist=(List)new ArrayList();
      Iterator i=lslist.iterator();
      while (i.hasNext()) {

         Linkedstudies lstudy=(Linkedstudies)i.next();
         Linkedinst linst=lstudy.getLinkedinst();
   
         if (linst==null) {
            throw new FORMSException("Couldn't pull linkedinst record");
         }

         EmbeddedinstDAO embeddedinstDAO=(EmbeddedinstDAO)context.getBean("embeddedinstDAO");
         Embeddedinst einst=embeddedinstDAO.getRecordByAcronym(linst.getAcronym());
   
         if (einst==null) {
            throw new FORMSException("Couldn't pull embeddedinst record");
         }
   
         try {
            // Write codeword, checkstring parameters
            im=parms.getClass().getMethod("setCodeword",new Class[] { String.class });
            oj=im.invoke(parms,new Object[] { einst.getCodeword() });
            im=parms.getClass().getMethod("setLuserid",new Class[] { String.class });
            oj=im.invoke(parms,new Object[] { auth.getValue("luserid") });
            im=parms.getClass().getMethod("setConnectstring",new Class[] { String.class });
            oj=im.invoke(parms,new Object[] { linst.getConnectstring() });
            im=parms.getClass().getMethod("setCheckstring",new Class[] { String.class });
            oj=im.invoke(parms,new Object[] { EncryptUtil.serviceCheckEncrypt(session,linst) });
            im=parms.getClass().getMethod("setOristudyid",new Class[] { String.class });
            oj=im.invoke(parms,new Object[] { auth.getValue("studyid") });
            im=parms.getClass().getMethod("setDeststudyid",new Class[] { String.class });
            oj=im.invoke(parms,new Object[] { new Long(lstudy.getRemotestudyid()).toString() });
         } catch (Exception oke1) { 
            FORMSLog.append(syswork,Stack2string.getString(oke1));
            throw new FORMSException(oke1,"submitServiceRequest exception");
         }
   
         // invoke superclass method
         FORMSServiceResult returnobj=submitServiceRequest(session,parms,linst.getUrl() + "/formsserviceinterceptor.do");
         HashMap returnmap=new HashMap();
         returnmap.put("result",returnobj);
         returnmap.put("inst",linst);
         resultlist.add(returnmap);

      }

      return resultlist;

   }


   /////////////////////////
   /////////////////////////
   /////////////////////////
   // SOAP Client Methods //
   /////////////////////////
   /////////////////////////
   /////////////////////////


   // Submit SOAP Request (localhost SOAP server)
   public static void submitLocalNonBlockingSoapRequest(HttpServletRequest request,HttpSession session,AbstractApplicationContext context,
         FORMSAuth auth,String sysconn,File syswork,FORMSServiceParms parms,AxisCallback cb) throws FORMSException {

      Method im;
      Object oj;

      try {

         // Write codeword, checkstring parameters
         im=parms.getClass().getMethod("setLuserid",new Class[] { String.class });
         oj=im.invoke(parms,new Object[] { auth.getValue("luserid") });
         im=parms.getClass().getMethod("setConnectstring",new Class[] { String.class });
         oj=im.invoke(parms,new Object[] { sysconn });
         im=parms.getClass().getMethod("setCheckstring",new Class[] { String.class });
         oj=im.invoke(parms,new Object[] { EncryptUtil.serviceCheckEncrypt(session,null) });
      } catch (Exception oke1) { 
         FORMSLog.append(syswork,Stack2string.getString(oke1));
         throw new FORMSException(oke1,"submitNonBlockingSoapRequest exception");
      }

      String url=request.getRequestURL().toString();

      // invoke superclass method
      submitNonBlockingSoapRequest(session, parms, url.substring(0,url.lastIndexOf("/")) + "/services/FORMSSoapInterceptor", cb);

   }

   // constructor for submitting request when there is no need of callback notification
   public static void submitLocalNonBlockingSoapRequest(HttpServletRequest request,HttpSession session,AbstractApplicationContext context,
         FORMSAuth auth,String sysconn,File syswork,FORMSServiceParms parms) throws FORMSException {
      submitLocalNonBlockingSoapRequest(request,session,context,auth,sysconn,syswork,parms,new FORMSBasicCallback());
   }

   // Submit SOAP Request (localhost SOAP server)
   public static FORMSServiceResult submitLocalSoapRequest(HttpServletRequest request,HttpSession session,AbstractApplicationContext context,
         FORMSAuth auth,String sysconn,File syswork,FORMSServiceParms parms) throws FORMSException {

      Method im;
      Object oj;

      try {
         // Write codeword, checkstring parameters
         im=parms.getClass().getMethod("setLuserid",new Class[] { String.class });
         oj=im.invoke(parms,new Object[] { auth.getValue("luserid") });
         im=parms.getClass().getMethod("setConnectstring",new Class[] { String.class });
         oj=im.invoke(parms,new Object[] { sysconn });
         im=parms.getClass().getMethod("setCheckstring",new Class[] { String.class });
         oj=im.invoke(parms,new Object[] { EncryptUtil.serviceCheckEncrypt(session,null) });
      } catch (Exception oke1) { 
         FORMSLog.append(syswork,Stack2string.getString(oke1));
         try {
            return getErrorObject(context,parms,FORMSServiceConstants.SERVICECLIENT_EXCEPTION);
         } catch (Exception oke1b) {
            FORMSLog.append(syswork,Stack2string.getString(oke1b));
            throw new FORMSException(oke1b,"submitSoapRequest exception");
         }
      }

      String url=request.getRequestURL().toString();

      // invoke superclass method
      return submitSoapRequest(session, parms, url.substring(0,url.lastIndexOf("/")) + "/services/FORMSSoapInterceptor");

   }

   // Submit SOAP Request
   public static FORMSServiceResult submitSoapRequest(HttpServletRequest request,HttpSession session,AbstractApplicationContext context,
         FORMSAuth auth,File syswork,Embeddedinst einst,FORMSServiceParms parms) throws FORMSException {

      LinkedinstDAO linkedinstDAO=(LinkedinstDAO)context.getBean("linkedinstDAO");
      Linkedinst linst=linkedinstDAO.getRecord(einst.getAcronym());

      if (linst==null) {
         throw new FORMSException("Couldn't pull linkedinst record");
      }

      return submitSoapRequest(request,session,context,auth,syswork,einst,linst,parms);

   }

   // Submit SOAP Request
   public static FORMSServiceResult submitSoapRequest(HttpServletRequest request,HttpSession session,AbstractApplicationContext context,
         FORMSAuth auth,File syswork,Linkedinst linst,FORMSServiceParms parms) throws FORMSException {

      EmbeddedinstDAO embeddedinstDAO=(EmbeddedinstDAO)context.getBean("embeddedinstDAO");
      Embeddedinst einst=embeddedinstDAO.getRecordByAcronym(linst.getAcronym());

      if (einst==null) {
         throw new FORMSException("Couldn't pull embeddedinst record");
      }

      return submitSoapRequest(request,session,context,auth,syswork,einst,linst,parms);

   }

   // Submit SOAP Request
   public static FORMSServiceResult submitSoapRequest(HttpServletRequest request,HttpSession session,AbstractApplicationContext context,
         FORMSAuth auth,File syswork,Embeddedinst einst,Linkedinst linst,FORMSServiceParms parms) throws FORMSException {

      Method im;
      Object oj;

      try {
         // Write codeword, checkstring parameters
         im=parms.getClass().getMethod("setCodeword",new Class[] { String.class });
         oj=im.invoke(parms,new Object[] { einst.getCodeword() });
         im=parms.getClass().getMethod("setLuserid",new Class[] { String.class });
         oj=im.invoke(parms,new Object[] { auth.getValue("luserid") });
         im=parms.getClass().getMethod("setConnectstring",new Class[] { String.class });
         oj=im.invoke(parms,new Object[] { linst.getConnectstring() });
         im=parms.getClass().getMethod("setCheckstring",new Class[] { String.class });
         oj=im.invoke(parms,new Object[] { EncryptUtil.serviceCheckEncrypt(session,linst) });
      } catch (Exception oke1) { 
         FORMSLog.append(syswork,Stack2string.getString(oke1));
         try {
            return getErrorObject(context,parms,FORMSServiceConstants.SERVICECLIENT_EXCEPTION);
         } catch (Exception oke1b) {
            FORMSLog.append(syswork,Stack2string.getString(oke1b));
            throw new FORMSException(oke1b,"submitSoapRequest exception");
         }
      }

      // invoke superclass method
      return submitSoapRequest(session, parms,linst.getUrl() + "/services/FORMSSoapInterceptor");

   }


   // Submit joined study SOAP-Based Request (
   public static List submitSoapStudyRequest(HttpServletRequest request,HttpSession session,AbstractApplicationContext context,
         FORMSAuth auth,File syswork,FORMSServiceParms parms) throws FORMSException,FORMSKeyException,FORMSSecurityException {

      Method im;
      Object oj;

      List lslist=getMainDAO(context).execQuery(
         "select l from Linkedstudies l join fetch l.linkedinst where l.studies.studyid=" + auth.getValue("studyid")
         );
      
      List resultlist=(List)new ArrayList();
      Iterator i=lslist.iterator();
      while (i.hasNext()) {

         Linkedstudies lstudy=(Linkedstudies)i.next();
         Linkedinst linst=lstudy.getLinkedinst();
   
         if (linst==null) {
            throw new FORMSException("Couldn't pull linkedinst record");
         }

         EmbeddedinstDAO embeddedinstDAO=(EmbeddedinstDAO)context.getBean("embeddedinstDAO");
         Embeddedinst einst=embeddedinstDAO.getRecordByAcronym(linst.getAcronym());
   
         if (einst==null) {
            throw new FORMSException("Couldn't pull embeddedinst record");
         }
   
         try {
            // Write codeword, checkstring parameters
            im=parms.getClass().getMethod("setCodeword",new Class[] { String.class });
            oj=im.invoke(parms,new Object[] { einst.getCodeword() });
            im=parms.getClass().getMethod("setLuserid",new Class[] { String.class });
            oj=im.invoke(parms,new Object[] { auth.getValue("luserid") });
            im=parms.getClass().getMethod("setConnectstring",new Class[] { String.class });
            oj=im.invoke(parms,new Object[] { linst.getConnectstring() });
            im=parms.getClass().getMethod("setCheckstring",new Class[] { String.class });
            oj=im.invoke(parms,new Object[] { EncryptUtil.serviceCheckEncrypt(session,linst) });
            im=parms.getClass().getMethod("setOristudyid",new Class[] { String.class });
            oj=im.invoke(parms,new Object[] { auth.getValue("studyid") });
            im=parms.getClass().getMethod("setDeststudyid",new Class[] { String.class });
            oj=im.invoke(parms,new Object[] { new Long(lstudy.getRemotestudyid()).toString() });
         } catch (Exception oke1) { 
            FORMSLog.append(syswork,Stack2string.getString(oke1));
            throw new FORMSException(oke1,"submitSoapRequest exception");
         }
   
         // invoke superclass method
         FORMSServiceResult returnobj=submitSoapRequest(session, parms,linst.getUrl() + "/services/FORMSSoapInterceptor");
         HashMap returnmap=new HashMap();
         returnmap.put("result",returnobj);
         returnmap.put("inst",linst);
         resultlist.add(returnmap);

      }

      return resultlist;

   }
   
   private static MainDAO getMainDAO(AbstractApplicationContext context) {

      return (MainDAO)context.getBean("mainDAO");
   }

}


