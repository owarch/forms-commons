 
/**
 *
 * This program is part of:
 *
 *    FORMS [Forms-Oriented Research Management System]
 *
 * Copyright (C) 2009 OpenWare Architects.  All Rights Reserved.
 *
 * FORMS is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * FORMS is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with FORMS.  If not, see <http://www.gnu.org/licenses/>.
 *
 */
 
/*
 *
 * ForminfoUtilController.java - Form utilities controller
 *
 */

package com.owarchitects.forms.commons.comp;

import com.owarchitects.forms.commons.db.*;
import java.util.*;
import java.io.*;
import javax.servlet.http.*;
import org.hibernate.*;
import org.springframework.web.servlet.*;
import org.springframework.context.support.*;
import org.springframework.orm.hibernate3.*;
import org.springframework.orm.hibernate3.*;
import org.mla.html.table.*;

public class ForminfoUtilController extends FORMSController {
 
   String spath="";

   public ModelAndView submitRequest() throws FORMSException,FORMSKeyException,FORMSSecurityException {

      // Use class-name based view
      mav=new ModelAndView("ForminfoUtil");

      try {

         HttpServletRequest request=this.getRequest();
         spath=request.getServletPath();

         if (spath.indexOf("showinfo.")>=0) {
          
            return showInfo(mav);

         }
         // add new site interface
         else {
            out.println("BAD REQUEST! - " + spath);
            return null;
         }
   
      } catch (Exception e) { 
         throw new FORMSException("FORMS Controller exception:  ",e);
      }

   }

   private ModelAndView showInfo(ModelAndView mav) {
      boolean islocal=new Boolean(getRequest().getParameter("islocal")).booleanValue();
      String dtdefid=getRequest().getParameter("dtdefid");
      String dttype=getRequest().getParameter("dttype");
      if (dttype==null || !dttype.equalsIgnoreCase("DATATABLE")) {
         dttype="FORM";
      }
      Datatabledef form=(Datatabledef)getMainDAO().execUniqueQuery(
         "select f from Datatabledef f left join fetch f.allinst left join fetch f.allinst.linkedinst where f.dtdefid=" + dtdefid
         );
      if (form==null) {
         mav.addObject("status","FORMPULLERROR");
         return mav;
      }
      if (form.getAllinst().getIslocal()) {
         mav.addObject("instacr","LOCAL");
      } else {
         try {
            mav.addObject("instacr",form.getAllinst().getLinkedinst().getAcronym());
         } catch (Exception iae) {
            mav.addObject("instacr","UNKNOWN");
         }
      } 
      mav.addObject("status","SHOWINFO");
      mav.addObject("dttype",dttype);
      mav.addObject("formacr",form.getFormacr());
      mav.addObject("formdesc",form.getFormdesc());
      mav.addObject("dtacr",form.getDtacr());
      mav.addObject("dtdesc",form.getDtdesc());
      mav.addObject("archived",form.getIsarchived());
      mav.addObject("enrollform",form.getIsenrollform());
      mav.addObject("notes",form.getNotes());
      // List key fields
      try {
         List l=getMainDAO().execQuery(
            "select f.columnname from Datatablecoldef f where f.datatabledef.dtdefid=" + dtdefid + " and iskeyfield=true");
         Iterator i=l.iterator();
         StringBuilder keysb=new StringBuilder();
         while (i.hasNext()) {
            keysb.append((String)i.next());
            if (i.hasNext()) keysb.append(", ");
         }
         mav.addObject("keyvars",keysb.toString());
      } catch (Exception iae) {
         mav.addObject("keyvars","");
      }
      // get SAS view name
      try {
         Formattrs fa=(Formattrs)getMainDAO().execUniqueQuery("select f from Formattrs f where f.datatabledef.dtdefid=" +
            dtdefid + " and f.attr='DSN_'");
         if (fa==null) mav.addObject("viewname","");
         else mav.addObject("viewname",fa.getValue());
      } catch (Exception iae) {
         mav.addObject("viewname","");
      }
      // Check against Enrollment form
      try {
         Formattrs fa=(Formattrs)getMainDAO().execUniqueQuery("select f from Formattrs f where f.datatabledef.dtdefid=" +
            dtdefid + " and f.attr='CHECKENROLL_'");
         if (fa==null) mav.addObject("checkenroll","");
         else mav.addObject("checkenroll",fa.getValue());
      } catch (Exception iae) {
         mav.addObject("checkenroll","");
      }
      return mav;
   }

}

