
/* 
 *
 * PermHelper.java -  Role/Permissions Helper Class
 *
 */

package com.owarchitects.forms.commons.comp;

import com.owarchitects.forms.commons.db.*;
import java.util.*;
import java.io.*;
import java.security.*;
import javax.crypto.SecretKey;
import javax.servlet.http.*;
import javax.servlet.http.HttpSession;
import org.springframework.web.servlet.*;
import org.springframework.web.context.support.*;
//import org.apache.axis2.context.*;
//import org.apache.axis2.client.*;
//import org.apache.axis2.transport.http.HTTPConstants;
import org.apache.commons.io.FileUtils;
import org.apache.commons.lang.time.DateUtils;

/////////////////////////////////////////////////////////////////////////////////////
// NOTE:  This object was originally used as a SessionObject in SessionObjectUtil. //
//        It had a high failure rate, however.  It is likely that ResourceAccessor //
//        objects cannot be stored as session objects because they or originated   //
//        by request and response objects, which are transient.  Because of this,  //
//        PermHelper objects are newly created as needed by FORMSController.       //
//        They should not be stored and re-used as session objects                 //
/////////////////////////////////////////////////////////////////////////////////////

public class PermHelper extends FORMSResourceAccessor {

   List studylist;
   boolean isservice;

   // For some reason getSession() statements were causing Tomcat exceptions to be
   // thrown when executed from ServiceTargets.  This happened even when the statement
   // was in a try/catch/finally block.  The only way to keep this from happening is
   // to completely bypass these statements for service targets

   public PermHelper(HttpServletRequest request,HttpServletResponse response,boolean isservice) {

      super(request,response);
      this.isservice=isservice;

   }

   public PermHelper(HttpServletRequest request,HttpServletResponse response) {

      super(request,response);
      this.isservice=false;

   }

   //////
   //////
   //////

   // Initialize permissions lists
   public void initPermLists() throws FORMSException,FORMSKeyException,FORMSSecurityException {

      getRoleList(true);
      getFormPermList(true);
      getMediaPermList(true);
      getReportPermList(true);
      getGlobalPermList(true);
      // NOTE:  getFormtypesPermList cannot be initialized here.  It initializes incorrectly
      // as it needs rightslevel set in FORMSAuth to run correctly

   }
   
   //////
   //////
   //////

   // Get complete list of roles user belongs to
   public List getRoleList(String auserid,boolean forceupdate) throws FORMSException,FORMSKeyException,FORMSSecurityException {
   
      List l=null;

      if ((auserid.equals(getAuth().getValue("auserid")) && (!isservice))) {
         try {
            l=(List)getSession().getAttribute("rrolelist");
         } catch (ClassCastException cce) {
            // Do nothing, create list
         }
      }

      if (forceupdate==false && l!=null) {
         return l;
      }

      // get roles to which user is specifically assigned
      l=getMainDAO().execQuery(
         "select r.role from Rroleuserassign r where r.user.auserid=" + auserid
         );

      boolean go=false;
      if (l.size()>0) {
         go=true;
      }   

      List qlist=l;
      // get parent roles of current role list
      while (go) {
         List l2=getMainDAO().execQuery(
            "select r.parentrole from Rroleroleassign r where r.childrole.rroleid in (" + getIdString(qlist) + ")"
            );
         Iterator i=l2.iterator();
         go=false;
         qlist=(List)new ArrayList();
         while (i.hasNext()) {
            Resourceroles r=(Resourceroles)i.next();
            // See if already exists in list (contains didn't work here)
            Iterator i2=l.iterator();
            boolean already=false;
            while (i2.hasNext()) {
               Resourceroles r2=(Resourceroles)i2.next();
               if (r2.getRroleid()==r.getRroleid()) {
                  already=true;
               }
            }
            if (!already) {
               l.add(r);
               qlist.add(r);
               go=true;
            }
         }
      }

      if ((auserid.equals(getAuth().getValue("auserid")) && (!isservice))) {
         getSession().setAttribute("rrolelist",l);
      } 

      return l;
      
   }

   // Get complete list of roles user belongs to
   public List getRoleList(String auserid) throws FORMSException,FORMSKeyException,FORMSSecurityException {
      return getRoleList(auserid,false);
   }

   // Get complete list of roles user belongs to
   public List getRoleList(boolean forceupdate) throws FORMSException,FORMSKeyException,FORMSSecurityException {
      return getRoleList(getAuth().getValue("auserid"),false);
   }

   // Get complete list of roles user belongs to
   public List getRoleList() throws FORMSException,FORMSKeyException,FORMSSecurityException {
      return getRoleList(getAuth().getValue("auserid"),false);
   }

   // Get list of ID's from role list
   public List getRoleIdList(String auserid,boolean forceupdate) throws FORMSException,FORMSKeyException,FORMSSecurityException {

      List nl=null;

      if ((auserid.equals(getAuth().getValue("auserid")) && (!isservice))) {
         try {
            nl=(List)getSession().getAttribute("rroleidlist");
         } catch (ClassCastException cce) {
            // Do nothing, create list
         }
      }

      if (forceupdate==false && nl!=null) {
         return nl;
      }

      List ol=getRoleList(auserid,forceupdate);
   
      nl=(List)new ArrayList();
      Iterator i=ol.iterator();
      while (i.hasNext()) {
         Resourceroles r=(Resourceroles)i.next();
         nl.add(new Long(r.getRroleid()));
      }

      if ((auserid.equals(getAuth().getValue("auserid")) && (!isservice))) {
         getSession().setAttribute("rroleidlist",nl);
      }
   
      return nl;

   }

   // Get list of ID's from role list
   public List getRoleIdList(String auserid) throws FORMSException,FORMSKeyException,FORMSSecurityException {
      return getRoleIdList(auserid,false);
   }

   // Get list of ID's from role list
   public List getRoleIdList(boolean forceupdate) throws FORMSException,FORMSKeyException,FORMSSecurityException {
      return getRoleIdList(getAuth().getValue("auserid"),forceupdate);
   }

   // Get list of ID's from role list
   public List getRoleIdList() throws FORMSException,FORMSKeyException,FORMSSecurityException {
      return getRoleIdList(getAuth().getValue("auserid"),false);
   }
   
   //////
   //////
   //////

   private String getIdString(List l) {
      StringBuilder sb=new StringBuilder();
      Iterator i=l.iterator();
      while (i.hasNext()) {
         Resourceroles r=(Resourceroles)i.next();
         sb.append(new Long(r.getRroleid()).toString());
         if (i.hasNext()) sb.append(",");
      }
      return sb.toString();
   }   
   
   //////
   //////
   //////

   // NOTE:  Calculation of studylist is done within 
   public List getStudyList(boolean forceupdate) {
      // NOTE:  Calculation of studylist is done 
      return studylist;
   }
   
   //////
   //////
   //////

   // Get complete list of forms user has access along with permissions
   public List getFormPermList(String auserid,boolean forceupdate) throws FORMSException,FORMSKeyException,FORMSSecurityException {
      return getFormPermList(auserid,null,forceupdate);
   }

   // Get complete list of forms user has access along with permissions
   public List getFormPermList(String auserid,String dtdefid_,boolean forceupdate) throws FORMSException,FORMSKeyException,FORMSSecurityException {

      try {

         List fplist=null;
   
         if (dtdefid_==null) {
   
            if ((auserid.equals(getAuth().getValue("auserid")) && (!isservice))) {
               try {
                  fplist=(List)getSession().getAttribute("fpermlist");
               } catch (ClassCastException cce) {
                  // Do nothing, create list
               }
            }
      
            if (forceupdate==false && fplist!=null) {
               return fplist;
            }
   
         }
   
         Iterator i;
         // create dtdefid list
         List fidlist;
         if (dtdefid_==null) {
            fidlist=getMainDAO().execQuery("select fd.dtdefid from Datatabledef fd");
         } else {
            fidlist=getMainDAO().execQuery("select fd.dtdefid from Datatabledef fd where fd.dtdefid=" + dtdefid_);
         }
   
         // get role list
         List rolelist=getRoleIdList(auserid,forceupdate);
   
         // Precedence is given to user-based perms, then role-based perms, then profile-based perms
         fplist=(List)new ArrayList();
   
         // Pull user defined permissions
         List upl=getMainDAO().execQuery(
            "select upa from Dtuserpermassign upa where upa.allusers.auserid=" + auserid
            );
   
         // Write perms to fplist
         i=upl.iterator();
         while (i.hasNext()) {
            Dtuserpermassign upa=(Dtuserpermassign)i.next();
            HashMap map=new HashMap();
            map.put("dtdefid",new Long(upa.getDtdefid()).toString());
            map.put("permlevel",(BitSet)upa.getPermlevel());
            fplist.add(map);
            // remove id from fidlist (if user permission exists, that's what we use)
            Iterator idi=fidlist.iterator();
            while (idi.hasNext()) {
               Long ckid=(Long)idi.next();
               if (ckid.longValue()==upa.getDtdefid()) {
                  idi.remove();
               }
            }
         }
   
         // Iterate through remaining forms, constructing BitSet from each from roles
         Iterator idi=fidlist.iterator();
         while (idi.hasNext()) {
            String dtdefid=((Long)idi.next()).toString();
            // User that uploaded a form can always see it
            boolean alwayssee=false;
            List asl=getMainDAO().execQuery(
               "select f from Formtypes f where f.uploaduser=" + auserid + " and f.datatabledef.remotedtdefid=null and f.datatabledef.dtdefid=" + dtdefid
               );
            if (asl.size()>0) {
               alwayssee=true;
            }
            // create list of profiles used by for for later
            List proflist=getMainDAO().execQuery(
                "select f.dtpermprofiles from Dtpermprofassign f where f.dtdefid=" + dtdefid
                );
            // role-/profile- based permission holder for form
            BitSet roleset=null;
            // Iterate through roles
            Iterator rdi=rolelist.iterator();
            while (rdi.hasNext()) {
               String rroleid=((Long)rdi.next()).toString();
               // First check for role specific permission
               Dtrolepermassign frp=(Dtrolepermassign)getMainDAO().execUniqueQuery(
                  "select frp from Dtrolepermassign frp where frp.dtdefid=" + dtdefid +
                      " and frp.resourceroles.rroleid=" + rroleid
                  );
               // if role specific permission, update roleset with that
               if (frp!=null) {
                  if (roleset==null) {
                     roleset=(BitSet)frp.getPermlevel();
                  } else {
                     // Modify roleset with values from frp
                     roleset.or((BitSet)frp.getPermlevel());
                  }
               // if no role-specific permission, use profile-based perm if available
               } else {
                  // Iterate through profile list
                  Iterator piter=proflist.iterator();
                  while (piter.hasNext()) {
                     Dtpermprofiles profile=(Dtpermprofiles)piter.next();
                     // See if profile has permissions assignment for role
                     Dtrolepermprofassign frppa=(Dtrolepermprofassign)getMainDAO().execUniqueQuery(
                        "select f from Dtrolepermprofassign f where f.dtpermprofiles.dtprofileid=" + profile.getDtprofileid() +
                           " and f.resourceroles.rroleid=" + rroleid
                        );
                     if (frppa!=null) {
                        // Update roleset with profile assignment
                        if (roleset==null) {
                           roleset=(BitSet)frppa.getPermlevel();
                        } else {
                           roleset.or((BitSet)frppa.getPermlevel());
                        }
                     }
                  }
               }
            }
            if (alwayssee || roleset!=null) {
               HashMap map=new HashMap();
               map.put("dtdefid",dtdefid);
               map.put("permlevel",roleset);
               map.put("alwayssee",alwayssee);
               fplist.add(map);
            }
         }
   
         // As a last step, iterate through list, eliminating rows for empty BitSets.  No perms
         // will be deterined by presence or absence from list
         Iterator fi=fplist.iterator();
         while (fi.hasNext()) {
            HashMap map=(HashMap)fi.next();
            BitSet bs=(BitSet)map.get("permlevel");
            Boolean alwayssee=(Boolean)map.get("alwayssee");
            if (bs==null || bs.isEmpty()) {
               if (alwayssee==null || (!alwayssee.booleanValue())) {
                  fi.remove();
               }
            }   
         }
   
         if (dtdefid_==null) {
   
            if ((auserid.equals(getAuth().getValue("auserid")) && (!isservice))) {
               getSession().setAttribute("fpermlist",fplist);
            }
   
         }
   
         return fplist;

      } catch (Exception permex) {

         List rlist=new ArrayList();
         getSession().setAttribute("fpermlist",rlist);
         return rlist;
      }
      
   }   

   // Get complete list of forms user has access along with permissions
   public List getFormPermList(String auserid) throws FORMSException,FORMSKeyException,FORMSSecurityException {
      return getFormPermList(auserid,false);
   }

   // Get complete list of forms user has access along with permissions
   public List getFormPermList(boolean forceupdate) throws FORMSException,FORMSKeyException,FORMSSecurityException {
      return getFormPermList(getAuth().getValue("auserid"),forceupdate);
   }

   // Get complete list of forms user has access along with permissions
   public List getFormPermList() throws FORMSException,FORMSKeyException,FORMSSecurityException {
      return getFormPermList(getAuth().getValue("auserid"),false);
   }
   
   //////
   //////
   //////

   // Get Permission Level BitSet for a specified Form
   public BitSet getFormPermLevel(String dtdefid) throws FORMSException,FORMSKeyException,FORMSSecurityException {

      Iterator i=getFormPermList().iterator();
      BitSet returnset=null;
      while (i.hasNext()) {
         HashMap m=(HashMap)i.next();
         if (m.containsKey("dtdefid") && dtdefid.equals(m.get("dtdefid"))) {
            returnset=(BitSet)m.get("permlevel");
         }
      }
      return returnset;

   }
   
   //////
   //////
   //////

   // Check specific permission for specified form
   public boolean hasFormPerm(String dtdefid,String permacr) throws FORMSException,FORMSKeyException,FORMSSecurityException {

      // NOTE:  Implicit trust for ADMIN users, even for forms in other studies.  We may want
      // to change this later
      if (getAuth().getValue("rightslevel").equalsIgnoreCase("ADMIN")) return true;

      boolean rval=false;
      BitSet bset=getFormPermLevel(dtdefid);
      if (bset==null) return false;
      com.owarchitects.forms.commons.db.Permissions p=(com.owarchitects.forms.commons.db.Permissions)getMainDAO().execUniqueQuery(
         "select p from Permissions p where p.permissioncats.pcatvalue=" + PcatConstants.FORM_DATATABLE +
            " and p.permacr='" + permacr + "'"
         );
      if (p!=null) {
         if (bset.get(p.getPermpos()-1)) {
            rval=true;
         }
      }

      return rval;

   }

   //////
   //////
   //////

   // Check permissions for a specific form & user

   public List getFormPermList(String auserid,String dtdefid_) throws FORMSException,FORMSKeyException,FORMSSecurityException {
      return getFormPermList(auserid,dtdefid_,false);
   }

   public BitSet getFormPermLevel(String auserid,String dtdefid) throws FORMSException,FORMSKeyException,FORMSSecurityException {

      Iterator i=getFormPermList(auserid,dtdefid).iterator();
      BitSet returnset=null;
      while (i.hasNext()) {
         HashMap m=(HashMap)i.next();
         if (m.containsKey("dtdefid") && dtdefid.equals(m.get("dtdefid"))) {
            returnset=(BitSet)m.get("permlevel");
         }
      }
      return returnset;

   }

   public boolean hasFormPerm(String auserid,String dtdefid,String permacr) throws FORMSException,FORMSKeyException,FORMSSecurityException {

      boolean rval=false;
      BitSet bset=getFormPermLevel(auserid,dtdefid);
      if (bset==null) return false;
      com.owarchitects.forms.commons.db.Permissions p=(com.owarchitects.forms.commons.db.Permissions)getMainDAO().execUniqueQuery(
         "select p from Permissions p where p.permissioncats.pcatvalue=" + PcatConstants.FORM_DATATABLE +
            " and p.permacr='" + permacr + "'"
         );
      if (p!=null) {
         if (bset.get(p.getPermpos()-1)) {
            rval=true;
         }
      }

      return rval;

   }
   
   //////
   //////
   //////

   // Get complete list of forms user has access along with permissions
   public List getFormtypesPermList(String auserid,boolean forceupdate) throws FORMSException,FORMSKeyException,FORMSSecurityException {

      List fplist=null;

      if ((auserid.equals(getAuth().getValue("auserid")) && (!isservice))) {
         try {
            fplist=(List)getSession().getAttribute("ftypespermlist");
         } catch (ClassCastException cce) {
            // Do nothing, create list
         }
      }

      if (forceupdate==false && fplist!=null) {
         return fplist;
      }

      Iterator i;
      // create formtypelistid list
      List fidlist=getMainDAO().execQuery("select fd.formtypelistid from Formtypelist fd");

      if (getAuth().getValue("rightslevel").equalsIgnoreCase("ADMIN")) {

         // grant full access
         Integer maxpos=(Integer)getMainDAO().execUniqueQuery(
              "select max(p.permpos) from Permissions p " +
                 "where p.permissioncats.pcatvalue=" + PcatConstants.FORMTYPE + " and " +
                       "p.permissioncats.pcatscope=" + PcatscopeConstants.RESOURCE 
                 );
         BitSet permset=new BitSet(maxpos);
         permset.set(0,maxpos,true);
         i=fidlist.iterator();
         fplist=(List)new ArrayList();
         while (i.hasNext()) {
            HashMap map=new HashMap();
            map.put("permlevel",permset);
            map.put("formtypelistid",((Long)i.next()).toString());
            fplist.add(map);
         }
         if ((auserid.equals(getAuth().getValue("auserid")) && (!isservice))) {
            getSession().setAttribute("ftypespermlist",fplist);
         }

         return fplist;
      }

      // get role list
      List rolelist=getRoleIdList(auserid,forceupdate);

      // Precedence is given to user-based perms, then role-based perms, then profile-based perms
      fplist=(List)new ArrayList();

      // Pull user defined permissions
      List upl=getMainDAO().execQuery(
         "select upa from Formtypeuserpermassign upa where upa.allusers.auserid=" + auserid
         );
      // Write perms to fplist
      i=upl.iterator();
      while (i.hasNext()) {
         Formtypeuserpermassign upa=(Formtypeuserpermassign)i.next();
         HashMap map=new HashMap();
         map.put("formtypelistid",new Long(upa.getFormtypelistid()).toString());
         map.put("permlevel",(BitSet)upa.getPermlevel());
         fplist.add(map);
         // remove id from fidlist (if user permission exists, that's what we use)
         Iterator idi=fidlist.iterator();
         while (idi.hasNext()) {
            Long ckid=(Long)idi.next();
            if (ckid.longValue()==upa.getFormtypelistid()) {
               idi.remove();
            }
         }
      }

      // Iterate through remaining forms, constructing BitSet from each from roles
      Iterator idi=fidlist.iterator();
      while (idi.hasNext()) {
         String formtypelistid=((Long)idi.next()).toString();
         // role-/profile- based permission holder for form
         BitSet roleset=null;
         // Iterate through roles
         Iterator rdi=rolelist.iterator();
         while (rdi.hasNext()) {
            String rroleid=((Long)rdi.next()).toString();
            // First check for role specific permission
            Formtyperolepermassign frp=(Formtyperolepermassign)getMainDAO().execUniqueQuery(
               "select frp from Formtyperolepermassign frp where frp.formtypelistid=" + formtypelistid +
                   " and frp.resourceroles.rroleid=" + rroleid
               );
            // if role specific permission, update roleset with that
            if (frp!=null) {
               if (roleset==null) {
                  roleset=(BitSet)frp.getPermlevel();
               } else {
                  // Modify roleset with values from frp
                  roleset.or((BitSet)frp.getPermlevel());
               }
            }
         }
         if (roleset!=null) {
            HashMap map=new HashMap();
            map.put("formtypelistid",formtypelistid);
            map.put("permlevel",roleset);
            fplist.add(map);
         }
      }
      // As a last step, iterate through list, eliminating rows for empty BitSets.  No perms
      // will be deterined by presence or absence from list
      Iterator fi=fplist.iterator();
      while (fi.hasNext()) {
         HashMap map=(HashMap)fi.next();
         BitSet bs=(BitSet)map.get("permlevel");
         if (bs.isEmpty()) fi.remove();
      }

      if ((auserid.equals(getAuth().getValue("auserid")) && (!isservice))) {
         getSession().setAttribute("ftypespermlist",fplist);
      }

      return fplist;
   }   

   // Get complete list of forms user has access along with permissions
   public List getFormtypesPermList(String auserid) throws FORMSException,FORMSKeyException,FORMSSecurityException {
      return getFormtypesPermList(auserid,false);
   }

   // Get complete list of forms user has access along with permissions
   public List getFormtypesPermList(boolean forceupdate) throws FORMSException,FORMSKeyException,FORMSSecurityException {
      return getFormtypesPermList(getAuth().getValue("auserid"),forceupdate);
   }

   // Get complete list of forms user has access along with permissions
   public List getFormtypesPermList() throws FORMSException,FORMSKeyException,FORMSSecurityException {
      return getFormtypesPermList(getAuth().getValue("auserid"),false);
   }
   
   //////
   //////
   //////

   // Get system permissions for a specified user
   public List getGlobalPermList(String auserid,boolean forceupdate) throws FORMSException,FORMSKeyException,FORMSSecurityException {

      List gplist=null;

      if ((auserid.equals(getAuth().getValue("auserid")) && (!isservice))) {
         try {
            gplist=(List)getSession().getAttribute("gpermlist");
         } catch (ClassCastException cce) {
            // Do nothing, create list
         }
      }

      if (forceupdate==false && gplist!=null) {
         return gplist;
      }

      gplist=(List)new ArrayList();

      // Give priority to user-based permissions, if exist, otherwise use role-based permissions

      // HOWEVER:  For these assignments, we'll assign role perms first, then overwrite them with 
      //           user assignments

      // cycle through user roles, making relevant assignments
      Iterator roleiter=getRoleIdList().iterator();
      while (roleiter.hasNext()) {
         Long rroleid=(Long)roleiter.next();

         List ralist=getMainDAO().execQuery(
            "select a from Sysrolepermassign a join fetch a.role join fetch a.permissioncats " +
               "where a.role.rroleid=" + rroleid.toString()
            );
         Iterator raiter=ralist.iterator();
         while (raiter.hasNext()) {
            Sysrolepermassign srpa=(Sysrolepermassign)raiter.next();
            // create new detail record
            PermDetail detail=new PermDetail();
            int pcatvalue=srpa.getPermissioncats().getPcatvalue();
            detail.setPcatvalue(pcatvalue);
            detail.setSiteid(srpa.getRole().getSiteid());
            detail.setStudyid(srpa.getRole().getStudyid());
            detail.setPermlevel((BitSet)srpa.getPermlevel());
            // see if matching record already exists in permissions
            Iterator gpiter=gplist.iterator();
            boolean found=false;
            while (gpiter.hasNext()) {
               PermDetail cdetail=(PermDetail)gpiter.next();
               if (detail.getPcatvalue()==cdetail.getPcatvalue() &&
                   (detail.getPcatvalue()==PcatConstants.SYSTEM || detail.getSiteid()==cdetail.getSiteid()) &&
                   (detail.getPcatvalue()==PcatConstants.SYSTEM || detail.getPcatvalue()==PcatConstants.SITE ||
                      detail.getStudyid()==cdetail.getStudyid())) {
                   BitSet cset=cdetail.getPermlevel();
                   BitSet dset=detail.getPermlevel();
                   cset.or(dset);
                   cdetail.setPermlevel(cset);
                   gpiter.remove();
                   gplist.add(cdetail);
                   found=true;
                   break;
                }
            }
            if (!found) {
               gplist.add(detail);
            }

         }
         
      }

      // User assignment overrides
      List uassn=getMainDAO().execQuery(
         "Select s from Sysuserpermassign s join fetch s.user join fetch s.permissioncats " +
            "where s.user.auserid=" + auserid
         );
         
      Iterator i=uassn.iterator();
      while (i.hasNext()) {
         Sysuserpermassign s=(Sysuserpermassign)i.next();
         PermDetail detail=new PermDetail();
         int pcatvalue=s.getPermissioncats().getPcatvalue();
         detail.setPcatvalue(pcatvalue);
         if (pcatvalue==PcatConstants.SITE || pcatvalue==PcatConstants.STUDY) {
            detail.setSiteid(s.getSiteid());
         } 
         if (pcatvalue==PcatConstants.STUDY) {
            detail.setStudyid(s.getStudyid());
         }
         detail.setPermlevel((BitSet)s.getPermlevel());
         // see if matching record already exists in permissions
         Iterator gpiter=gplist.iterator();
         boolean found=false;
         while (gpiter.hasNext()) {
            PermDetail cdetail=(PermDetail)gpiter.next();
            if (detail.getPcatvalue()==cdetail.getPcatvalue() &&
               (detail.getPcatvalue()==PcatConstants.SYSTEM || detail.getSiteid()==cdetail.getSiteid()) &&
               (detail.getPcatvalue()==PcatConstants.SYSTEM || detail.getPcatvalue()==PcatConstants.SITE ||
                 detail.getStudyid()==cdetail.getStudyid())) {
                gpiter.remove();
                gplist.add(cdetail);
                found=true;
                break;
             }
         }
         if (!found) {
            gplist.add(detail);
         }
      }
      
      if ((auserid.equals(getAuth().getValue("auserid")) && (!isservice))) {
         getSession().setAttribute("gpermlist",gplist);
      }
      return gplist;

   }

   // Get complete list of forms user has access along with permissions
   public List getGlobalPermList(String auserid) throws FORMSException,FORMSKeyException,FORMSSecurityException {
      return getGlobalPermList(auserid,false);
   }

   // Get complete list of forms user has access along with permissions
   public List getGlobalPermList(boolean forceupdate) throws FORMSException,FORMSKeyException,FORMSSecurityException {
      return getGlobalPermList(getAuth().getValue("auserid"),forceupdate);
   }

   // Get complete list of forms user has access along with permissions
   public List getGlobalPermList() throws FORMSException,FORMSKeyException,FORMSSecurityException {
      return getGlobalPermList(getAuth().getValue("auserid"),false);
   }
   
   //////
   //////
   //////

   // return specific global permission
   public boolean hasGlobalPerm(String auserid,int pcatvalue,String permacr) { 

      try {
         if ((auserid.equals(getAuth().getValue("auserid")) && (!isservice))) {
            if (getAuth().getValue("rightslevel").equalsIgnoreCase("ADMIN")) return true;
         }

         // Retrieve BitSet
         List plist=getGlobalPermList(auserid);
         Iterator i=plist.iterator();
         // get permission set
         BitSet set=null;
         while (i.hasNext()) {
            PermDetail detail=(PermDetail)i.next();
            if (
                (detail.getPcatvalue()==pcatvalue) &&
                (pcatvalue==PcatConstants.SYSTEM || detail.getSiteid()==new Long(getAuth().getValue("siteid")).longValue()) &&
                (pcatvalue==PcatConstants.SYSTEM || pcatvalue==PcatConstants.SITE || 
                   detail.getStudyid()==new Long(getAuth().getValue("studyid")).longValue())
               ) {
               set=detail.getPermlevel();
               break;
            }
         }
   
         if (set==null) {
            return false;
         }
   
         // get permission position
         com.owarchitects.forms.commons.db.Permissions p=(com.owarchitects.forms.commons.db.Permissions)getMainDAO().execUniqueQuery(
            "select p from Permissions p where upper(p.permacr)=upper('" + permacr + "') and " +
               "p.permissioncats.pcatvalue=" + pcatvalue
            );
         if (p==null) {
            return false;
         }
         
         // subtract 1 because permpos>=1, while bitset>=0
         return set.get(p.getPermpos()-1);
        
      } catch (Exception e) {
         return false;
      }

   }

   public boolean hasGlobalPerm(int pcatvalue,String permacr) throws FORMSException,FORMSKeyException,FORMSSecurityException { 
      return hasGlobalPerm(getAuth().getValue("auserid"),pcatvalue,permacr);
   }

   /**
    *  See if has global permission at any level
    */
   public boolean hasGlobalPerm(String permacr) throws FORMSException,FORMSKeyException,FORMSSecurityException { 
      if (hasGlobalPerm(PcatConstants.STUDY,permacr)) return true;
      else if (hasGlobalPerm(PcatConstants.SITE,permacr)) return true;
      else if (hasGlobalPerm(PcatConstants.SYSTEM,permacr)) return true;
      else return false;
   }


   //////////////////////////////
   // MEDIA FILE PERMISSIONS ////
   //////////////////////////////


   // Get complete list of forms user has access along with permissions
   public List getMediaPermList(String auserid,boolean forceupdate) throws FORMSException,FORMSKeyException,FORMSSecurityException {

      List mplist=null;

      if ((auserid.equals(getAuth().getValue("auserid")) && (!isservice))) {
         try {
            mplist=(List)getSession().getAttribute("mpermlist");
         } catch (ClassCastException cce) {
            // Do nothing, create list
         }
      }

      if (forceupdate==false && mplist!=null) {
         return mplist;
      }

      Iterator i;
      // create mediainfoid list
      List fidlist=getMainDAO().execQuery("select fd.mediainfoid from Mediainfo fd");

      // get role list
      List rolelist=getRoleIdList(auserid,forceupdate);

      // Precedence is given to user-based perms, then role-based perms, then profile-based perms
      mplist=(List)new ArrayList();

      // Pull user defined permissions
      List upl=getMainDAO().execQuery(
         "select upa from Mediauserpermassign upa where upa.allusers.auserid=" + auserid
         );

      // Write perms to mplist
      i=upl.iterator();
      while (i.hasNext()) {
         Mediauserpermassign upa=(Mediauserpermassign)i.next();
         HashMap map=new HashMap();
         map.put("mediainfoid",new Long(upa.getMediainfoid()).toString());
         map.put("permlevel",(BitSet)upa.getPermlevel());
         mplist.add(map);
         // remove id from fidlist (if user permission exists, that's what we use)
         Iterator idi=fidlist.iterator();
         while (idi.hasNext()) {
            Long ckid=(Long)idi.next();
            if (ckid.longValue()==upa.getMediainfoid()) {
               idi.remove();
            }
         }
      }

      // Iterate through remaining files, constructing BitSet from each from roles
      Iterator idi=fidlist.iterator();
      while (idi.hasNext()) {
         String mediainfoid=((Long)idi.next()).toString();
         // create list of profiles used by for for later
         List proflist=getMainDAO().execQuery(
             "select f.mediapermprofiles from Mediapermprofassign f where f.mediainfoid=" + mediainfoid
             );
         // role-/profile- based permission holder for file
         BitSet roleset=null;
         // Iterate through roles
         Iterator rdi=rolelist.iterator();
         while (rdi.hasNext()) {
            String rroleid=((Long)rdi.next()).toString();
            // First check for role specific permission
            Mediarolepermassign frp=(Mediarolepermassign)getMainDAO().execUniqueQuery(
               "select frp from Mediarolepermassign frp where frp.mediainfoid=" + mediainfoid +
                   " and frp.resourceroles.rroleid=" + rroleid
               );
            // if role specific permission, update roleset with that
            if (frp!=null) {
               if (roleset==null) {
                  roleset=(BitSet)frp.getPermlevel();
               } else {
                  // Modify roleset with values from frp
                  roleset.or((BitSet)frp.getPermlevel());
               }
            // if no role-specific permission, use profile-based perm if available
            } else {
               // Iterate through profile list
               Iterator piter=proflist.iterator();
               while (piter.hasNext()) {
                  Mediapermprofiles profile=(Mediapermprofiles)piter.next();
                  // See if profile has permissions assignment for role
                  Mediarolepermprofassign frppa=(Mediarolepermprofassign)getMainDAO().execUniqueQuery(
                     "select f from Mediarolepermprofassign f where f.mediapermprofiles.mprofileid=" + profile.getMprofileid() +
                        " and f.resourceroles.rroleid=" + rroleid
                     );
                  if (frppa!=null) {
                     // Update roleset with profile assignment
                     if (roleset==null) {
                        roleset=(BitSet)frppa.getPermlevel();
                     } else {
                        roleset.or((BitSet)frppa.getPermlevel());
                     }
                  }
               }
            }
         }
         if (roleset!=null) {
            HashMap map=new HashMap();
            map.put("mediainfoid",mediainfoid);
            map.put("permlevel",roleset);
            mplist.add(map);
         }
      }

      // As a last step, iterate through list, eliminating rows for empty BitSets.  No perms
      // will be deterined by presence or absence from list
      Iterator fi=mplist.iterator();
      while (fi.hasNext()) {
         HashMap map=(HashMap)fi.next();
         BitSet bs=(BitSet)map.get("permlevel");
         if (bs.isEmpty()) fi.remove();
      }

      if ((auserid.equals(getAuth().getValue("auserid")) && (!isservice))) {
         getSession().setAttribute("mpermlist",mplist);
      }

      return mplist;
   }   

   // Get complete list of files user has access along with permissions
   public List getMediaPermList(String auserid) throws FORMSException,FORMSKeyException,FORMSSecurityException {
      return getMediaPermList(auserid,false);
   }

   // Get complete list of files user has access along with permissions
   public List getMediaPermList(boolean forceupdate) throws FORMSException,FORMSKeyException,FORMSSecurityException {
      return getMediaPermList(getAuth().getValue("auserid"),forceupdate);
   }

   // Get complete list of files user has access along with permissions
   public List getMediaPermList() throws FORMSException,FORMSKeyException,FORMSSecurityException {
      return getMediaPermList(getAuth().getValue("auserid"),false);
   }
   
   //////
   //////
   //////

   // Get Permission Level BitSet for a specified file
   public BitSet getMediaPermLevel(String mediainfoid) throws FORMSException,FORMSKeyException,FORMSSecurityException {

      Iterator i=getMediaPermList().iterator();
      BitSet returnset=null;
      while (i.hasNext()) {
         HashMap m=(HashMap)i.next();
         if (m.containsKey("mediainfoid") && mediainfoid.equals(m.get("mediainfoid"))) {
            returnset=(BitSet)m.get("permlevel");
         }
      }
      return returnset;

   }
   
   // Get Permission Level BitSet for a specified file (specify user)
   public BitSet getMediaPermLevel(String auserid,String mediainfoid) throws FORMSException,FORMSKeyException,FORMSSecurityException {

      Iterator i=getMediaPermList(auserid).iterator();
      BitSet returnset=null;
      while (i.hasNext()) {
         HashMap m=(HashMap)i.next();
         if (m.containsKey("mediainfoid") && mediainfoid.equals(m.get("mediainfoid"))) {
            returnset=(BitSet)m.get("permlevel");
         }
      }
      return returnset;

   }
   
   //////
   //////
   //////

   // Check specific permission for specified file
   public boolean hasMediaPerm(String mediainfoid,String permacr) throws FORMSException,FORMSKeyException,FORMSSecurityException {

      // NOTE:  Implicit trust for ADMIN users, even for forms in other studies.  We may want
      // to change this later
      if (getAuth().getValue("rightslevel").equalsIgnoreCase("ADMIN")) return true;

      boolean rval=false;
      BitSet bset=getMediaPermLevel(mediainfoid);
      if (bset==null) return false;
      com.owarchitects.forms.commons.db.Permissions p=(com.owarchitects.forms.commons.db.Permissions)getMainDAO().execUniqueQuery(
         "select p from Permissions p where p.permissioncats.pcatvalue=" + PcatConstants.MEDIA +
            " and p.permacr='" + permacr + "'"
         );
      if (p!=null) {
         if (bset.get(p.getPermpos()-1)) {
            rval=true;
         }
      }

      return rval;

   }

   // Check specific permission for specified file
   public boolean hasMediaPerm(String auserid,String mediainfoid,String permacr) throws FORMSException,FORMSKeyException,FORMSSecurityException {

      boolean rval=false;
      BitSet bset=getMediaPermLevel(auserid,mediainfoid);
      if (bset==null) return false;
      com.owarchitects.forms.commons.db.Permissions p=(com.owarchitects.forms.commons.db.Permissions)getMainDAO().execUniqueQuery(
         "select p from Permissions p where p.permissioncats.pcatvalue=" + PcatConstants.MEDIA +
            " and p.permacr='" + permacr + "'"
         );
      if (p!=null) {
         if (bset.get(p.getPermpos()-1)) {
            rval=true;
         }
      }

      return rval;

   }


   ////////////////////////////////////////
   // RESEARCHER/ANALYST CMS PERMISSIONS //
   ////////////////////////////////////////


   // Get complete list of roles user belongs to
   public List getAnalystRoleList(String auserid,boolean forceupdate) throws FORMSException,FORMSKeyException,FORMSSecurityException {
   
      List l=null;

      if ((auserid.equals(getAuth().getValue("auserid")) && (!isservice))) {
         try {
            l=(List)getSession().getAttribute("arolelist");
         } catch (ClassCastException cce) {
            // Do nothing, create list
         }
      }

      if (forceupdate==false && l!=null) {
         return l;
      }

      // get roles to which user is specifically assigned
      l=getMainDAO().execQuery(
         "select r.role from Analystroleuserassign r where r.user.auserid=" + auserid
         );

      if ((auserid.equals(getAuth().getValue("auserid")) && (!isservice))) {
         getSession().setAttribute("arolelist",l);
      } 

      return l;
      
   }

   // Get complete list of roles user belongs to
   public List getAnalystRoleList(String auserid) throws FORMSException,FORMSKeyException,FORMSSecurityException {
      return getAnalystRoleList(auserid,false);
   }

   // Get complete list of roles user belongs to
   public List getAnalystRoleList(boolean forceupdate) throws FORMSException,FORMSKeyException,FORMSSecurityException {
      return getAnalystRoleList(getAuth().getValue("auserid"),false);
   }

   // Get complete list of roles user belongs to
   public List getAnalystRoleList() throws FORMSException,FORMSKeyException,FORMSSecurityException {
      return getAnalystRoleList(getAuth().getValue("auserid"),false);
   }

   // Get list of ID's from role list
   public List getAnalystRoleIdList(String auserid,boolean forceupdate) throws FORMSException,FORMSKeyException,FORMSSecurityException {

      List nl=null;

      if ((auserid.equals(getAuth().getValue("auserid")) && (!isservice))) {
         try {
            nl=(List)getSession().getAttribute("aroleidlist");
         } catch (ClassCastException cce) {
            // Do nothing, create list
         }
      }

      if (forceupdate==false && nl!=null) {
         return nl;
      }

      List ol=getAnalystRoleList(auserid,forceupdate);
   
      nl=(List)new ArrayList();
      Iterator i=ol.iterator();
      while (i.hasNext()) {
         Analystroles r=(Analystroles)i.next();
         nl.add(new Long(r.getAroleid()));
      }

      if ((auserid.equals(getAuth().getValue("auserid")) && (!isservice))) {
         getSession().setAttribute("aroleidlist",nl);
      }

      return nl;

   }

   // Get list of ID's from role list
   public List getAnalystRoleIdList(String auserid) throws FORMSException,FORMSKeyException,FORMSSecurityException {
      return getAnalystRoleIdList(auserid,false);
   }

   // Get list of ID's from role list
   public List getAnalystRoleIdList(boolean forceupdate) throws FORMSException,FORMSKeyException,FORMSSecurityException {
      return getAnalystRoleIdList(getAuth().getValue("auserid"),forceupdate);
   }

   // Get list of ID's from role list
   public List getAnalystRoleIdList() throws FORMSException,FORMSKeyException,FORMSSecurityException {
      return getAnalystRoleIdList(getAuth().getValue("auserid"),false);
   }

   //////
   //////
   //////

   // Get complete list of groups user belongs to
   public List getAnalystGroupList(String auserid,boolean forceupdate) throws FORMSException,FORMSKeyException,FORMSSecurityException {
   
      List l=null;

      if ((auserid.equals(getAuth().getValue("auserid")) && (!isservice))) {
         try {
            l=(List)getSession().getAttribute("agrouplist");
         } catch (ClassCastException cce) {
            // Do nothing, create list
         }
      }

      if (forceupdate==false && l!=null) {
         return l;
      }

      // get groups to which user is specifically assigned
      l=getMainDAO().execQuery(
         "select r.group from Analystgroupuserassign r where r.user.auserid=" + auserid
         );

      if ((auserid.equals(getAuth().getValue("auserid")) && (!isservice))) {
         getSession().setAttribute("agrouplist",l);
      } 

      return l;
      
   }

   // Get complete list of groups user belongs to
   public List getAnalystGroupList(String auserid) throws FORMSException,FORMSKeyException,FORMSSecurityException {
      return getAnalystGroupList(auserid,false);
   }

   // Get complete list of groups user belongs to
   public List getAnalystGroupList(boolean forceupdate) throws FORMSException,FORMSKeyException,FORMSSecurityException {
      return getAnalystGroupList(getAuth().getValue("auserid"),false);
   }

   // Get complete list of groups user belongs to
   public List getAnalystGroupList() throws FORMSException,FORMSKeyException,FORMSSecurityException {
      return getAnalystGroupList(getAuth().getValue("auserid"),false);
   }

   // Get list of ID's from group list
   public List getAnalystGroupIdList(String auserid,boolean forceupdate) throws FORMSException,FORMSKeyException,FORMSSecurityException {

      List nl=null;

      if ((auserid.equals(getAuth().getValue("auserid")) && (!isservice))) {
         try {
            nl=(List)getSession().getAttribute("agroupidlist");
         } catch (ClassCastException cce) {
            // Do nothing, create list
         }
      }

      if (forceupdate==false && nl!=null) {
         return nl;
      }

      List ol=getAnalystGroupList(auserid,forceupdate);
   
      nl=(List)new ArrayList();
      Iterator i=ol.iterator();
      while (i.hasNext()) {
         Analystgroups r=(Analystgroups)i.next();
         nl.add(new Long(r.getAgroupid()));
      }

      if ((auserid.equals(getAuth().getValue("auserid")) && (!isservice))) {
         getSession().setAttribute("agroupidlist",nl);
      }

      return nl;

   }

   // Get list of ID's from group list
   public List getAnalystGroupIdList(String auserid) throws FORMSException,FORMSKeyException,FORMSSecurityException {
      return getAnalystGroupIdList(auserid,false);
   }

   // Get list of ID's from group list
   public List getAnalystGroupIdList(boolean forceupdate) throws FORMSException,FORMSKeyException,FORMSSecurityException {
      return getAnalystGroupIdList(getAuth().getValue("auserid"),forceupdate);
   }

   // Get list of ID's from group list
   public List getAnalystGroupIdList() throws FORMSException,FORMSKeyException,FORMSSecurityException {
      return getAnalystGroupIdList(getAuth().getValue("auserid"),false);
   }
   
   //////
   //////
   //////

   private String getAnalystIdString(List l) {
      StringBuilder sb=new StringBuilder();
      Iterator i=l.iterator();
      while (i.hasNext()) {
         Analystroles r=(Analystroles)i.next();
         sb.append(new Long(r.getAroleid()).toString());
         if (i.hasNext()) sb.append(",");
      }
      return sb.toString();
   }   
   
   //////
   //////
   //////

   // Get system permissions for a specified user
   public List getAnalystSysPermList(String auserid,boolean forceupdate) throws FORMSException,FORMSKeyException,FORMSSecurityException {

      List splist=null;

      if ((auserid.equals(getAuth().getValue("auserid")) && (!isservice))) {
         try {
            splist=(List)getSession().getAttribute("anasyspermlist");
         } catch (ClassCastException cce) {
            // Do nothing, create list
         }
      }

      if (forceupdate==false && splist!=null) {
         return splist;
      }

      splist=(List)new ArrayList();

      // Give priority to user-based permissions, if exist, otherwise use role-based permissions

      // User assignment overrides
      List uassn=getMainDAO().execQuery(
         "Select s from Analystusersyspermassign s join fetch s.allusers " +
            "where s.allusers.auserid=" + auserid
         );
         
      Iterator i=uassn.iterator();
      while (i.hasNext()) {
         Analystusersyspermassign s=(Analystusersyspermassign)i.next();
         PermDetail detail=new PermDetail();
         detail.setPermlevel((BitSet)s.getPermlevel());
         // see if matching record already exists in permissions
         Iterator gpiter=splist.iterator();
         boolean found=false;
         while (gpiter.hasNext()) {
            PermDetail cdetail=(PermDetail)gpiter.next();
            BitSet cset=cdetail.getPermlevel();
            BitSet dset=detail.getPermlevel();
            cset.or(dset);
            cdetail.setPermlevel(cset);
            gpiter.remove();
            splist.add(cdetail);
            found=true;
            break;
         }
         if (!found) {
            splist.add(detail);
         }
      }

      if (splist.size()<1) {

         // cycle through user roles, making relevant assignments
         Iterator roleiter=getAnalystRoleIdList(auserid).iterator();
         while (roleiter.hasNext()) {
            Long aroleid=(Long)roleiter.next();
   
            List ralist=getMainDAO().execQuery(
               "select a from Analystrolepermassign a join fetch a.analystroles " +
                  "where a.analystroles.aroleid=" + aroleid.toString()
               );
            Iterator raiter=ralist.iterator();
            while (raiter.hasNext()) {
               Analystrolepermassign srpa=(Analystrolepermassign)raiter.next();
               // create new detail record
               PermDetail detail=new PermDetail();
               detail.setPermlevel((BitSet)srpa.getPermlevel());
               // see if matching record already exists in permissions
               Iterator gpiter=splist.iterator();
               boolean found=false;
               while (gpiter.hasNext()) {
                  PermDetail cdetail=(PermDetail)gpiter.next();
                  BitSet cset=cdetail.getPermlevel();
                  BitSet dset=detail.getPermlevel();
                  cset.or(dset);
                  cdetail.setPermlevel(cset);
                  gpiter.remove();
                  splist.add(cdetail);
                  found=true;
                  break;
               }
               if (!found) {
                  splist.add(detail);
               }
   
            }

         }

      }

      if ((auserid.equals(getAuth().getValue("auserid")) && (!isservice))) {
         getSession().setAttribute("anasyspermlist",splist);
      }
      return splist;

   }

   // Get complete list of forms user has access along with permissions
   public List getAnalystSysPermList(String auserid) throws FORMSException,FORMSKeyException,FORMSSecurityException {
      return getAnalystSysPermList(auserid,false);
   }

   // Get complete list of forms user has access along with permissions
   public List getAnalystSysPermList(boolean forceupdate) throws FORMSException,FORMSKeyException,FORMSSecurityException {
      return getAnalystSysPermList(getAuth().getValue("auserid"),forceupdate);
   }

   // Get complete list of forms user has access along with permissions
   public List getAnalystSysPermList() throws FORMSException,FORMSKeyException,FORMSSecurityException {
      return getAnalystSysPermList(getAuth().getValue("auserid"),false);
   }
   
   //////
   //////
   //////

   // return specific global permission
   public boolean hasAnalystSysPerm(String auserid,String permacr) { 

      try {
         if ((auserid.equals(getAuth().getValue("auserid")) && (!isservice))) {
            if (getAuth().getValue("rightslevel").equalsIgnoreCase("ADMIN")) return true;
         }   

         // Retrieve BitSet
         List plist=getAnalystSysPermList(auserid);
         Iterator i=plist.iterator();
         // get permission set
         BitSet set=null;
         while (i.hasNext()) {
            PermDetail detail=(PermDetail)i.next();
            set=detail.getPermlevel();
            break;
         }
   
         if (set==null) {
            return false;
         }
   
         // get permission position
         com.owarchitects.forms.commons.db.Permissions p=(com.owarchitects.forms.commons.db.Permissions)getMainDAO().execUniqueQuery(
            "select p from Permissions p where upper(p.permacr)=upper('" + permacr + "') and " +
               "p.permissioncats.pcatvalue=" + PcatConstants.ANALYST_SYSTEM
            );
         if (p==null) {
            return false;
         }
         
         // subtract 1 because permpos>=1, while bitset>=0
         return set.get(p.getPermpos()-1);
        
      } catch (Exception e) {
         return false;
      }

   }

   // return specific global permission
   public boolean hasAnalystSysPerm(String permacr) throws FORMSException,FORMSKeyException,FORMSSecurityException { 
      return hasAnalystSysPerm(getAuth().getValue("auserid"),permacr);
   }

   /////////
   /////////
   /////////
   /////////
   /////////

   public List getAnalysthaPermList(String auserid,boolean forceupdate) throws FORMSException,FORMSKeyException,FORMSSecurityException {

      List aplist=null;

      if ((auserid.equals(getAuth().getValue("auserid")) && (!isservice))) {
         try {
            aplist=(List)getSession().getAttribute("ahapermlist");
         } catch (ClassCastException cce) {
            // Do nothing, create list
         }
      }

      if (forceupdate==false && aplist!=null) {
         return aplist;
      }

      Iterator i;
      // create analysthaid list
      List ahalist=getMainDAO().execQuery("select fd from Analystha fd");

      // get group list
      List grouplist=getAnalystGroupIdList(auserid,forceupdate);

      // Precedence is given to user-based perms, then group-based perms, then profile-based perms
      aplist=(List)new ArrayList();

      // Pull user defined permissions
      List upl=getMainDAO().execQuery(
         "select upa from Analysthauserpermassign upa where upa.allusers.auserid=" + auserid
         );

      //
      // Creating user has full rights to category   
      //

      // construct bitset
      Long nperm=(Long)getMainDAO().execUniqueQuery(
         "select count(p) from Permissions p where p.permissioncats.pcatvalue=" + PcatConstants.ANALYST_RESOURCE
         );
      BitSet bsall=new BitSet(nperm.intValue());
      bsall.set(0,nperm.intValue(),true);

      // iterate through, assiging bsall to user if is createuser and root category
      Iterator idi=ahalist.iterator();
      while (idi.hasNext()) {
         Analystha ha=(Analystha)idi.next();
         // assign bsall to creating user
         if (ha.getCreateuser()==(new Long(auserid).longValue())) {
            HashMap map=new HashMap();
            map.put("analysthaid",new Long(ha.getAnalysthaid()).toString());
            map.put("permlevel",bsall);
            aplist.add(map);
            idi.remove();
         // assign bsall to heirarchy root
         } else if (ha.getPanalysthaid()==null) {
            HashMap map=new HashMap();
            map.put("analysthaid",new Long(ha.getAnalysthaid()).toString());
            map.put("permlevel",bsall);
            aplist.add(map);
            idi.remove();
         }
      }

      // Write perms to aplist
      i=upl.iterator();
      while (i.hasNext()) {
         Analysthauserpermassign upa=(Analysthauserpermassign)i.next();
         HashMap map=new HashMap();
         map.put("analysthaid",new Long(upa.getAnalysthaid()).toString());
         map.put("permlevel",(BitSet)upa.getPermlevel());
         aplist.add(map);
         // remove id from ahalist (if user permission exists, that's what we use)
         idi=ahalist.iterator();
         while (idi.hasNext()) {
            Long ckid=((Analystha)idi.next()).getAnalysthaid();
            if (ckid.longValue()==upa.getAnalysthaid()) {
               idi.remove();
            }
         }
      }

      // Iterate through remaining files, constructing BitSet from each from groups
      idi=ahalist.iterator();
      while (idi.hasNext()) {
         String analysthaid=new Long(((Analystha)idi.next()).getAnalysthaid()).toString();
         // group-/profile- based permission holder for file
         BitSet groupset=null;
         // Iterate through groups
         Iterator rdi=grouplist.iterator();
         while (rdi.hasNext()) {
            String agroupid=((Long)rdi.next()).toString();
            // First check for group specific permission
            Analysthagrouppermassign frp=(Analysthagrouppermassign)getMainDAO().execUniqueQuery(
               "select frp from Analysthagrouppermassign frp where frp.analysthaid=" + analysthaid +
                   " and frp.analystgroups.agroupid=" + agroupid
               );
            // if group specific permission, update groupset with that
            if (frp!=null) {
               if (groupset==null) {
                  groupset=(BitSet)frp.getPermlevel();
               } else {
                  // Modify groupset with values from frp
                  groupset.or((BitSet)frp.getPermlevel());
               }
            } 
         }
         if (groupset!=null) {
            HashMap map=new HashMap();
            map.put("analysthaid",analysthaid);
            map.put("permlevel",groupset);
            aplist.add(map);
         }
      }

      // Now iterate through list, eliminating rows for empty BitSets.  No perms
      // will be deterined by presence or absence from list
      Iterator fi=aplist.iterator();
      while (fi.hasNext()) {
         HashMap map=(HashMap)fi.next();
         BitSet bs=(BitSet)map.get("permlevel");
         if (bs.isEmpty()) fi.remove();
      }

      // Where permissons are unavailable for the specific category, they are inherited from the next higher 
      // that contains a permission

      // continue until no more permission values are assigned
      while (1==1) {
         int stsize=ahalist.size();
         if (stsize<1) break;
         idi=ahalist.iterator();
         while (idi.hasNext()) {
            Analystha ha=(Analystha)idi.next();
            Long panalysthaid=ha.getPanalysthaid();
            // root heirarchy has null permission
            if (panalysthaid!=null) {
               // check permissions list for permission
               // Create temporary appendlist to avoid concurrentmodificationexception
               Iterator api=aplist.iterator();
               ArrayList applist=new ArrayList();
               while (api.hasNext()) {
                  HashMap map=(HashMap)api.next();
                  String canalysthaid=(String)map.get("analysthaid");
                  if (panalysthaid.longValue()==(new Long(canalysthaid).longValue())) {
                     HashMap newmap=new HashMap();
                     map.put("analysthaid",canalysthaid);
                     map.put("permlevel",(BitSet)map.get("permlevel"));
                     applist.add(map);
                     idi.remove();
                     break;
                  }
               }
               aplist.addAll(applist);
            }
         }
         // Stop if no new permissions have been found
         if (ahalist.size()==stsize || ahalist.size()<1) break;
      }
      
      // append applist to aplist

      if ((auserid.equals(getAuth().getValue("auserid")) && (!isservice))) {
         getSession().setAttribute("ahapermlist",aplist);
      }

      return aplist;

   }

   // Get complete list of files user has access along with permissions
   public List getAnalysthaPermList(String auserid) throws FORMSException,FORMSKeyException,FORMSSecurityException {
      return getAnalysthaPermList(auserid,false);
   }

   // Get complete list of files user has access along with permissions
   public List getAnalysthaPermList(boolean forceupdate) throws FORMSException,FORMSKeyException,FORMSSecurityException {
      return getAnalysthaPermList(getAuth().getValue("auserid"),forceupdate);
   }

   // Get complete list of files user has access along with permissions
   public List getAnalysthaPermList() throws FORMSException,FORMSKeyException,FORMSSecurityException {
      return getAnalysthaPermList(getAuth().getValue("auserid"),false);
   }
   
   //////
   //////
   //////

   // Get Permission Level BitSet for a specified file
   public BitSet getAnalysthaPermLevel(String analysthaid) throws FORMSException,FORMSKeyException,FORMSSecurityException {

      Iterator i=getAnalysthaPermList().iterator();
      BitSet returnset=null;
      while (i.hasNext()) {
         HashMap m=(HashMap)i.next();
         if (m.containsKey("analysthaid") && analysthaid.equals(m.get("analysthaid"))) {
            returnset=(BitSet)m.get("permlevel");
         }
      }
      return returnset;

   }
   
   // Get Permission Level BitSet for a specified file (specify user)
   public BitSet getAnalysthaPermLevel(String auserid,String analysthaid) throws FORMSException,FORMSKeyException,FORMSSecurityException {

      Iterator i=getAnalysthaPermList(auserid).iterator();
      BitSet returnset=null;
      while (i.hasNext()) {
         HashMap m=(HashMap)i.next();
         if (m.containsKey("analysthaid") && analysthaid.equals(m.get("analysthaid"))) {
            returnset=(BitSet)m.get("permlevel");
         }
      }
      return returnset;

   }
   
   //////
   //////
   //////


   // Check specific permission for specified file
   public boolean hasAnalysthaPerm(String analysthaid,String permacr) throws FORMSException,FORMSKeyException,FORMSSecurityException {

      // NOTE:  Implicit trust for ADMIN users, even for forms in other studies.  We may want
      // to change this later
      if (getAuth().getValue("rightslevel").equalsIgnoreCase("ADMIN")) return true;

      boolean rval=false;
      BitSet bset=getAnalysthaPermLevel(analysthaid);
      if (bset==null) return false;
      com.owarchitects.forms.commons.db.Permissions p=(com.owarchitects.forms.commons.db.Permissions)getMainDAO().execUniqueQuery(
         "select p from Permissions p where p.permissioncats.pcatvalue=" + PcatConstants.ANALYST_RESOURCE +
            " and p.permacr='" + permacr + "'"
         );
      if (p!=null) {
         if (bset.get(p.getPermpos()-1)) {
            rval=true;
         }
      }

      return rval;

   }

   // Check specific permission for specified file
   public boolean hasAnalysthaPerm(String auserid,String analysthaid,String permacr) throws FORMSException,FORMSKeyException,FORMSSecurityException {

      boolean rval=false;
      BitSet bset=getAnalysthaPermLevel(auserid,analysthaid);
      if (bset==null) return false;
      com.owarchitects.forms.commons.db.Permissions p=(com.owarchitects.forms.commons.db.Permissions)getMainDAO().execUniqueQuery(
         "select p from Permissions p where p.permissioncats.pcatvalue=" + PcatConstants.ANALYST_RESOURCE +
            " and p.permacr='" + permacr + "'"
         );
      if (p!=null) {
         if (bset.get(p.getPermpos()-1)) {
            rval=true;
         }
      }

      return rval;

   }
   
   //////
   //////
   //////

   // Get complete list of forms user has access along with permissions
   public List getAnalystPermList(String auserid,boolean forceupdate) throws FORMSException,FORMSKeyException,FORMSSecurityException {

      List aplist=null;

      if ((auserid.equals(getAuth().getValue("auserid")) && (!isservice))) {
         try {
            aplist=(List)getSession().getAttribute("apermlist");
         } catch (ClassCastException cce) {
            // Do nothing, create list
         }
      }

      if (forceupdate==false && aplist!=null) {
         return aplist;
      }

      Iterator i;
      // create analystinfoid list
      List ainfolist=getMainDAO().execQuery("select fd from Analystinfo fd");

      // get group list
      List grouplist=getAnalystGroupIdList(auserid,forceupdate);

      // Precedence is given to user-based perms, then group-based perms, then profile-based perms
      aplist=(List)new ArrayList();

      // Pull user defined permissions
      List upl=getMainDAO().execQuery(
         "select upa from Analystuserpermassign upa where upa.allusers.auserid=" + auserid
         );

      //
      // Creating user has full rights to category   
      //

      // construct bitset
      Long nperm=(Long)getMainDAO().execUniqueQuery(
         "select count(p) from Permissions p where p.permissioncats.pcatvalue=" + PcatConstants.ANALYST_RESOURCE
         );
      BitSet bsall=new BitSet(nperm.intValue());
      bsall.set(0,nperm.intValue(),true);

      // iterate through, assiging bsall to user if is uploaduser
      Iterator idi=ainfolist.iterator();
      while (idi.hasNext()) {
         Analystinfo info=(Analystinfo)idi.next();
         if (info.getUploaduser()==(new Long(auserid).longValue())) {
            HashMap map=new HashMap();
            map.put("analystinfoid",new Long(info.getAnalystinfoid()).toString());
            map.put("permlevel",bsall);
            aplist.add(map);
            idi.remove();
         }
      }


      // Write perms to aplist
      i=upl.iterator();
      while (i.hasNext()) {
         Analystuserpermassign upa=(Analystuserpermassign)i.next();
         HashMap map=new HashMap();
         map.put("analystinfoid",new Long(upa.getAnalystinfoid()).toString());
         map.put("permlevel",(BitSet)upa.getPermlevel());
         aplist.add(map);
         // remove id from ainfolist (if user permission exists, that's what we use)
         idi=ainfolist.iterator();
         while (idi.hasNext()) {
            Long ckid=((Analystinfo)idi.next()).getAnalystinfoid();
            if (ckid.longValue()==upa.getAnalystinfoid()) {
               idi.remove();
            }
         }
      }

      // Iterate through remaining files, constructing BitSet from each from groups
      idi=ainfolist.iterator();
      while (idi.hasNext()) {
         String analystinfoid=new Long(((Analystinfo)idi.next()).getAnalystinfoid()).toString();
         // group- based permission holder for file
         BitSet groupset=null;
         // Iterate through groups
         Iterator rdi=grouplist.iterator();
         while (rdi.hasNext()) {
            String agroupid=((Long)rdi.next()).toString();
            // First check for group specific permission
            Analystgrouppermassign frp=(Analystgrouppermassign)getMainDAO().execUniqueQuery(
               "select frp from Analystgrouppermassign frp where frp.analystinfoid=" + analystinfoid +
                   " and frp.analystgroups.agroupid=" + agroupid
               );
            // if group specific permission, update groupset with that
            if (frp!=null) {
               if (groupset==null) {
                  groupset=(BitSet)frp.getPermlevel();
               } else {
                  // Modify groupset with values from frp
                  groupset.or((BitSet)frp.getPermlevel());
               }
            } 
         }
         if (groupset!=null) {
            HashMap map=new HashMap();
            map.put("analystinfoid",analystinfoid);
            map.put("permlevel",groupset);
            aplist.add(map);
         }
      }

      // Now iterate through list, eliminating rows for empty BitSets.  No perms
      // will be deterined by presence or absence from list
      Iterator fi=aplist.iterator();
      while (fi.hasNext()) {
         HashMap map=(HashMap)fi.next();
         BitSet bs=(BitSet)map.get("permlevel");
         if (bs.isEmpty()) fi.remove();
      }

      // Where permissons are unavailable for the specific category, inherit them from the file's parent
      // category

      if (ainfolist.size()>0) {
         List catlist=getAnalysthaPermList(auserid);
         idi=ainfolist.iterator();
         while (idi.hasNext()) {
            Analystinfo info=(Analystinfo)idi.next();
            Iterator cdi=catlist.iterator();
            while (cdi.hasNext()) {
               HashMap map=(HashMap)cdi.next();
               Long canalysthaid=new Long((String)map.get("analysthaid"));
               if (canalysthaid.longValue()==info.getPanalysthaid()) {
                  HashMap newmap=new HashMap();
                  newmap.put("analystinfoid",new Long(info.getAnalystinfoid()).toString());
                  newmap.put("permlevel",(BitSet)map.get("permlevel"));
                  aplist.add(newmap);
                  idi.remove();
                  break;
               }
            }
         }

      }
      
      if ((auserid.equals(getAuth().getValue("auserid")) && (!isservice))) {
         getSession().setAttribute("apermlist",aplist);
      }

      return aplist;
   }   

   // Get complete list of files user has access along with permissions
   public List getAnalystPermList(String auserid) throws FORMSException,FORMSKeyException,FORMSSecurityException {
      return getAnalystPermList(auserid,false);
   }

   // Get complete list of files user has access along with permissions
   public List getAnalystPermList(boolean forceupdate) throws FORMSException,FORMSKeyException,FORMSSecurityException {
      return getAnalystPermList(getAuth().getValue("auserid"),forceupdate);
   }

   // Get complete list of files user has access along with permissions
   public List getAnalystPermList() throws FORMSException,FORMSKeyException,FORMSSecurityException {
      return getAnalystPermList(getAuth().getValue("auserid"),false);
   }
   
   //////
   //////
   //////

   // Get Permission Level BitSet for a specified file
   public BitSet getAnalystPermLevel(String analystinfoid) throws FORMSException,FORMSKeyException,FORMSSecurityException {

      Iterator i=getAnalystPermList().iterator();
      BitSet returnset=null;
      while (i.hasNext()) {
         HashMap m=(HashMap)i.next();
         if (m.containsKey("analystinfoid") && analystinfoid.equals(m.get("analystinfoid"))) {
            returnset=(BitSet)m.get("permlevel");
         }
      }
      return returnset;

   }
   
   // Get Permission Level BitSet for a specified file (specify user)
   public BitSet getAnalystPermLevel(String auserid,String analystinfoid) throws FORMSException,FORMSKeyException,FORMSSecurityException {

      Iterator i=getAnalystPermList(auserid).iterator();
      BitSet returnset=null;
      while (i.hasNext()) {
         HashMap m=(HashMap)i.next();
         if (m.containsKey("analystinfoid") && analystinfoid.equals(m.get("analystinfoid"))) {
            returnset=(BitSet)m.get("permlevel");
         }
      }
      return returnset;

   }
   
   //////
   //////
   //////


   // Check specific permission for specified file
   public boolean hasAnalystPerm(String analystinfoid,String permacr) throws FORMSException,FORMSKeyException,FORMSSecurityException {

      // NOTE:  Implicit trust for ADMIN users, even for forms in other studies.  We may want
      // to change this later
      if (getAuth().getValue("rightslevel").equalsIgnoreCase("ADMIN")) return true;

      boolean rval=false;
      BitSet bset=getAnalystPermLevel(analystinfoid);
      if (bset==null) return false;
      com.owarchitects.forms.commons.db.Permissions p=(com.owarchitects.forms.commons.db.Permissions)getMainDAO().execUniqueQuery(
         "select p from Permissions p where p.permissioncats.pcatvalue=" + PcatConstants.ANALYST_RESOURCE +
            " and p.permacr='" + permacr + "'"
         );
      if (p!=null) {
         if (bset.get(p.getPermpos()-1)) {
            rval=true;
         }
      }

      return rval;

   }

   // Check specific permission for specified file
   public boolean hasAnalystPerm(String auserid,String analystinfoid,String permacr) throws FORMSException,FORMSKeyException,FORMSSecurityException {

      boolean rval=false;
      BitSet bset=getAnalystPermLevel(auserid,analystinfoid);
      if (bset==null) return false;
      com.owarchitects.forms.commons.db.Permissions p=(com.owarchitects.forms.commons.db.Permissions)getMainDAO().execUniqueQuery(
         "select p from Permissions p where p.permissioncats.pcatvalue=" + PcatConstants.ANALYST_RESOURCE +
            " and p.permacr='" + permacr + "'"
         );
      if (p!=null) {
         if (bset.get(p.getPermpos()-1)) {
            rval=true;
         }
      }

      return rval;

   }


   /////////////////////////////////////
   // REPORTING PROGRAM PERMISSIONS ////
   /////////////////////////////////////


   // Get complete list of forms user has access along with permissions
   public List getReportPermList(String auserid,boolean forceupdate) throws FORMSException,FORMSKeyException,FORMSSecurityException {

      List mplist=null;

      if ((auserid.equals(getAuth().getValue("auserid")) && (!isservice))) {
         try {
            mplist=(List)getSession().getAttribute("rpermlist");
         } catch (ClassCastException cce) {
            // Do nothing, create list
         }
      }

      if (forceupdate==false && mplist!=null) {
         return mplist;
      }

      Iterator i;
      // create reportinfoid list
      List fidlist=getMainDAO().execQuery("select fd.reportinfoid from Reportinfo fd");

      // get role list
      List rolelist=getRoleIdList(auserid,forceupdate);

      // Precedence is given to user-based perms, then role-based perms, then profile-based perms
      mplist=(List)new ArrayList();

      // Pull user defined permissions
      List upl=getMainDAO().execQuery(
         "select upa from Reportuserpermassign upa where upa.allusers.auserid=" + auserid
         );

      // Write perms to mplist
      i=upl.iterator();
      while (i.hasNext()) {
         Reportuserpermassign upa=(Reportuserpermassign)i.next();
         HashMap map=new HashMap();
         map.put("reportinfoid",new Long(upa.getReportinfoid()).toString());
         map.put("permlevel",(BitSet)upa.getPermlevel());
         mplist.add(map);
         // remove id from fidlist (if user permission exists, that's what we use)
         Iterator idi=fidlist.iterator();
         while (idi.hasNext()) {
            Long ckid=(Long)idi.next();
            if (ckid.longValue()==upa.getReportinfoid()) {
               idi.remove();
            }
         }
      }

      // Iterate through remaining files, constructing BitSet from each from roles
      Iterator idi=fidlist.iterator();
      while (idi.hasNext()) {
         String reportinfoid=((Long)idi.next()).toString();
         // create list of profiles used by for for later
         List proflist=getMainDAO().execQuery(
             "select f.reportpermprofiles from Reportpermprofassign f where f.reportinfoid=" + reportinfoid
             );
         // role-/profile- based permission holder for file
         BitSet roleset=null;
         // Iterate through roles
         Iterator rdi=rolelist.iterator();
         while (rdi.hasNext()) {
            String rroleid=((Long)rdi.next()).toString();
            // First check for role specific permission
            Reportrolepermassign frp=(Reportrolepermassign)getMainDAO().execUniqueQuery(
               "select frp from Reportrolepermassign frp where frp.reportinfoid=" + reportinfoid +
                   " and frp.resourceroles.rroleid=" + rroleid
               );
            // if role specific permission, update roleset with that
            if (frp!=null) {
               if (roleset==null) {
                  roleset=(BitSet)frp.getPermlevel();
               } else {
                  // Modify roleset with values from frp
                  roleset.or((BitSet)frp.getPermlevel());
               }
            // if no role-specific permission, use profile-based perm if available
            } else {
               // Iterate through profile list
               Iterator piter=proflist.iterator();
               while (piter.hasNext()) {
                  Reportpermprofiles profile=(Reportpermprofiles)piter.next();
                  // See if profile has permissions assignment for role
                  Reportrolepermprofassign frppa=(Reportrolepermprofassign)getMainDAO().execUniqueQuery(
                     "select f from Reportrolepermprofassign f where f.reportpermprofiles.rprofileid=" + profile.getRprofileid() +
                        " and f.resourceroles.rroleid=" + rroleid
                     );
                  if (frppa!=null) {
                     // Update roleset with profile assignment
                     if (roleset==null) {
                        roleset=(BitSet)frppa.getPermlevel();
                     } else {
                        roleset.or((BitSet)frppa.getPermlevel());
                     }
                  }
               }
            }
         }
         if (roleset!=null) {
            HashMap map=new HashMap();
            map.put("reportinfoid",reportinfoid);
            map.put("permlevel",roleset);
            mplist.add(map);
         }
      }

      // As a last step, iterate through list, eliminating rows for empty BitSets.  No perms
      // will be deterined by presence or absence from list
      Iterator fi=mplist.iterator();
      while (fi.hasNext()) {
         HashMap map=(HashMap)fi.next();
         BitSet bs=(BitSet)map.get("permlevel");
         if (bs.isEmpty()) fi.remove();
      }

      if ((auserid.equals(getAuth().getValue("auserid")) && (!isservice))) {
         getSession().setAttribute("rpermlist",mplist);
      }

      return mplist;
   }   

   // Get complete list of files user has access along with permissions
   public List getReportPermList(String auserid) throws FORMSException,FORMSKeyException,FORMSSecurityException {
      return getReportPermList(auserid,false);
   }

   // Get complete list of files user has access along with permissions
   public List getReportPermList(boolean forceupdate) throws FORMSException,FORMSKeyException,FORMSSecurityException {
      return getReportPermList(getAuth().getValue("auserid"),forceupdate);
   }

   // Get complete list of files user has access along with permissions
   public List getReportPermList() throws FORMSException,FORMSKeyException,FORMSSecurityException {
      return getReportPermList(getAuth().getValue("auserid"),false);
   }
   
   //////
   //////
   //////

   // Get Permission Level BitSet for a specified file
   public BitSet getReportPermLevel(String reportinfoid) throws FORMSException,FORMSKeyException,FORMSSecurityException {

      Iterator i=getReportPermList().iterator();
      BitSet returnset=null;
      while (i.hasNext()) {
         HashMap m=(HashMap)i.next();
         if (m.containsKey("reportinfoid") && reportinfoid.equals(m.get("reportinfoid"))) {
            returnset=(BitSet)m.get("permlevel");
         }
      }
      return returnset;

   }
   
   // Get Permission Level BitSet for a specified file (specify user)
   public BitSet getReportPermLevel(String auserid,String reportinfoid) throws FORMSException,FORMSKeyException,FORMSSecurityException {

      Iterator i=getReportPermList(auserid).iterator();
      BitSet returnset=null;
      while (i.hasNext()) {
         HashMap m=(HashMap)i.next();
         if (m.containsKey("reportinfoid") && reportinfoid.equals(m.get("reportinfoid"))) {
            returnset=(BitSet)m.get("permlevel");
         }
      }
      return returnset;

   }
   
   //////
   //////
   //////

   // Check specific permission for specified file
   public boolean hasReportPerm(String reportinfoid,String permacr) throws FORMSException,FORMSKeyException,FORMSSecurityException {

      // NOTE:  Implicit trust for ADMIN users, even for forms in other studies.  We may want
      // to change this later
      if (getAuth().getValue("rightslevel").equalsIgnoreCase("ADMIN")) return true;

      boolean rval=false;
      BitSet bset=getReportPermLevel(reportinfoid);
      if (bset==null) return false;
      com.owarchitects.forms.commons.db.Permissions p=(com.owarchitects.forms.commons.db.Permissions)getMainDAO().execUniqueQuery(
         "select p from Permissions p where p.permissioncats.pcatvalue=" + PcatConstants.REPORT +
            " and p.permacr='" + permacr + "'"
         );
      if (p!=null) {
         if (bset.get(p.getPermpos()-1)) {
            rval=true;
         }
      }

      return rval;

   }

   // Check specific permission for specified file
   public boolean hasReportPerm(String auserid,String reportinfoid,String permacr) throws FORMSException,FORMSKeyException,FORMSSecurityException {

      boolean rval=false;
      BitSet bset=getReportPermLevel(auserid,reportinfoid);
      if (bset==null) return false;
      com.owarchitects.forms.commons.db.Permissions p=(com.owarchitects.forms.commons.db.Permissions)getMainDAO().execUniqueQuery(
         "select p from Permissions p where p.permissioncats.pcatvalue=" + PcatConstants.REPORT +
            " and p.permacr='" + permacr + "'"
         );
      if (p!=null) {
         if (bset.get(p.getPermpos()-1)) {
            rval=true;
         }
      }

      return rval;

   }
   
}

