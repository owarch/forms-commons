 
/**
 *
 * This program is part of:
 *
 *    FORMS [Forms-Oriented Research Management System]
 *
 * Copyright (C) 2009 OpenWare Architects.  All Rights Reserved.
 *
 * FORMS is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * FORMS is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with FORMS.  If not, see <http://www.gnu.org/licenses/>.
 *
 */
 
package com.owarchitects.forms.commons.comp;

///////////////////////////////////
// Web-Service side auth program //
///////////////////////////////////

import com.owarchitects.forms.commons.db.*;
import java.io.*;
import java.util.*;
import java.net.*;
//import com.adobe.fdf.*;
//import com.adobe.fdf.exceptions.*;
//import com.sas.sasserver.sclfuncs.*;
//import com.sas.rmi.*;
//import com.sas.sasserver.submit.*;
import javax.servlet.*;
import javax.servlet.http.*;
import com.oreilly.servlet.*;
import org.apache.commons.io.FileUtils;
import java.io.UnsupportedEncodingException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import javax.crypto.*;
import javax.crypto.spec.*;
import java.security.*;
import java.security.spec.*;
import java.util.regex.*;
//import org.apache.regexp.RE;
import org.apache.commons.lang.StringUtils;
//import org.apache.axis2.context.*;
//import org.apache.axis2.transport.http.HTTPConstants;
import org.springframework.web.servlet.*;
import org.springframework.web.context.support.*;
import org.springframework.context.support.AbstractApplicationContext;

// NOTE:  Currently using public rather than protected methods to allow JWS access to
// class methods.  May want to look into setting up JWS files as part of the package. 
// Axis at one time didn't allow JWS files to be a package member and I'm not sure how
// well this is currently working.

// NOTE:  Using cookies for session tracking because using HttpSessions and session
// parameters was occasionally causing "IllegalStateException - can't create session
// after response has been committed errors".  Seemed weird since the session had
// already been created, but things seem to be a little more trouble free managing
// state with cookies rather than with session parameters.  Session parameters 
// usually use cookies anyway.

public class FORMSAuth {

   private HttpServletRequest request = null;
   private HttpServletResponse response = null;
   private PrintWriter out;
   private HttpSession httpsession;
   private String[] ckarray={ "session","user","site","study","formname","formtable" };
   private int timeoutvalue;
   private String pathvar;
   private String workdir;
   private String ckvar;
   private String fileNameAdd;
   private Sysconn sconn;

   SecretKey sk;

   // Can't use class-level file statement.  This creates pretty severe problems on
   // multi-processor servers because many pages have multiple servlets loading
   // simultaneously.  There are definite problems when multiple sessions are
   // checking and modifying auth values.  One servlet may set the file value to 
   // one file while the other servlet sets it to something else before the first
   // one reads from it.
   //File tempfile;

   // CONSTRUCTORS

   // constructor with supplied work directory
   protected FORMSAuth(HttpServletRequest request,HttpServletResponse response,String inworkdir,String sysconn) throws FORMSException,FORMSKeyException,FORMSSecurityException {

      if (inworkdir!=null && inworkdir.length()>1) {
         this.workdir=inworkdir;
      }

      this.request=request;
      this.response=response;

      httpsession=request.getSession(true);
      pathvar=httpsession.getServletContext().getRealPath("/");

      // SET UP ENCRYPTION KEY 
      boolean resetkey=false;
      String resettime="";
      try {
         sk=SessionObjectUtil.getEncryptionKeyObj(httpsession);
      } catch (Exception ioe) {
         // create new session key
         try {
            Sysconn sconn=(Sysconn)ObjectXmlUtil.objectFromXmlString(
                              EncryptUtil.decryptString(sysconn,SessionObjectUtil.getInternalKeyObj(httpsession)));
            this.sconn=sconn;
            AbstractApplicationContext ctx=SessionObjectUtil.getApplicationContext(httpsession,sconn);
            EmbeddedusersDAO embeddedusersDAO=(EmbeddedusersDAO)ctx.getBean("embeddedusersDAO");
            Embeddedusers einfo=embeddedusersDAO.getRecord(sconn.getUsername());
            PrivateKey pk=EncryptUtil.getPrivateKey(httpsession,sconn.getUsername(),sconn.getPassword());
            String encpw=EncryptUtil.decryptString(einfo.getFkpassword(),pk);
            sk=EncryptUtil.getEncryptionKey(httpsession,encpw);
            // Write to session 
            SessionObjectUtil.setEncryptionKeyObj(httpsession,sk);
            resetkey=true;
            resettime=sconn.getFormstime();
         } catch (Exception ioe2) {
            throw new FORMSException(ioe2,"Encryption key error");
         }
      }

      // CREATE WORKING DIRECTORY 
      //   This directory is needed by the syncing and transfer programs, so it must
      //   be created prior to them being called.                                    
      if (workdir==null || workdir.length()<1) {
         FORMSWorkDir createWorkdir=new FORMSWorkDir(pathvar.trim() + "TempFiles" + File.separator,SessionObjectUtil.getConfigObj(httpsession).getWorkDirKeepDays());
         this.workdir=createWorkdir.getWorkdir();
         httpsession.setAttribute("syswork",workdir);

      }	

      // WORKDIR PORTION TO USE WITH FILE NAMES 
      if (workdir!=null && workdir.length()>10) {
         fileNameAdd=workdir.substring(workdir.length()-11,workdir.length()-1);
      } else {
         fileNameAdd="";
      }

      // Set workdir value (Must occur after filenameadd is created)
      this.setValue("workdir",workdir);

      if (pathvar!=null && pathvar.length()>0 && workdir!=null && workdir.length()>0) {
         this.setValue("pathvar",pathvar);
      }

      // GET REMOTE HOST INFO (LONGER TIMEOUTS FOR PCF MACHINES)
      timeoutvalue= new Integer(new Integer(SessionObjectUtil.getConfigObj(httpsession).getTimeout()).intValue()).intValue();
      byte[] b=new byte[4]; 
      InetAddress clientByteAddr=null;
      String[] ra=StringUtils.split(request.getRemoteAddr(),".");
      if (ra.length==4) {
         b[0]=new Integer(ra[0]).byteValue(); 
         b[1]=new Integer(ra[1]).byteValue(); 
         b[2]=new Integer(ra[2]).byteValue(); 
         b[3]=new Integer(ra[3]).byteValue(); 
         try {
         clientByteAddr=InetAddress.getByAddress(b); 
         } catch (Exception e) {}
      }

      String ckHostName=clientByteAddr.getHostName();
      String ckCanonicalHostName=clientByteAddr.getCanonicalHostName();
      if (ckHostName.substring(0,6).equalsIgnoreCase("pcf20-") &&
          (ckCanonicalHostName.toLowerCase().equals(ckHostName.toLowerCase()) ||
           ckCanonicalHostName.toLowerCase().indexOf("pcf.wustl.edu")>0)
         ) {
         timeoutvalue= new Integer(new Integer(SessionObjectUtil.getConfigObj(httpsession).getTimeout()).intValue()*10).intValue();
      }

      if (resetkey) {
         // for security, check reset time against last timestamp
         if ((!resettime.equals(getValue("formstime"))) && 
              /* SOAP CALLS WILL HAVE MISSING FORMSTIME */
              !(getValue("formstime")==null || getValue("formstime").length()<1)) {
            throw new FORMSSecurityException("Session FORMSTIME Error");
         }
      }

      // Set session timeout to match RMS Timeout*2

      // COMMENT THIS OUT FOR GOOD (short timeouts should be fine)?????
      //httpsession.setMaxInactiveInterval(timeoutvalue*2);

      // Set initial time field (first try to get it since value should persist across sessions)
      if (getValue("formstime")==null || getValue("formstime").length()<1) {
         setValue("formstime",new Long(System.currentTimeMillis()).toString());
         setValue("session","1");
      }   
	    
      // set ckvar based on above read-in values
      setCkvarValue();
   }

   /*
   // constructor with NO supplied work directory
   protected FORMSAuth(HttpServletRequest request,HttpServletResponse response) throws FORMSException,FORMSKeyException {
      this(request,response,"");
   }   
   */

   /////////////
   // METHODS //
   /////////////

   // RETURN user VALUE
   public String getValue (String parm) throws FORMSException {
      try {
         File tempfile=new File(workdir,encryptString(parm + "_" + fileNameAdd));
         if (tempfile.exists()) return decryptString(FileUtils.readFileToString(tempfile,"UTF-8"));
      } catch (Exception e) { }
      return "";
   }
   
   // Retrieve object (deserialized) value 
   public Object getObjectValue (String parm) throws FORMSException {
      return ObjectUtil.objectFromString(getValue(parm));
   }

   // SET user VALUE
   public void setValue (String parm,String value) throws FORMSException {
      File tempfile=new File(workdir,encryptString(parm + "_" + fileNameAdd));
      try {
         FileUtils.writeStringToFile(tempfile,encryptString(value),"UTF-8");
      } catch (Exception ex) { }

      if (parm.matches("(session)|(user)|(site)|(study)|(formname)|(formtable)")) {
         setCkvarValue();
      }   
   }

   // SET user VALUE
   public void setObjectValue (String parm,Object value) throws FORMSException {
      setValue(parm,ObjectUtil.objectToString(value));
   }
   
   // Retrieve session value (first check session, then auth string)
   public String getSessionValue (String parm) throws FORMSException {
      try {
         String evalue=(String)httpsession.getAttribute(parm);
         if (evalue!=null) {
            return decryptString(evalue);
         } else {   
            return getValue(parm);
         } 
      } catch (Exception e) { }
      return "";
   }


   // SET user session (save to auth string AND to session) VALUE
   public void setSessionValue (String parm,String value) throws FORMSException {
      File tempfile=new File(workdir,encryptString(parm + "_" + fileNameAdd));
      try {
         String evalue=encryptString(value);
         FileUtils.writeStringToFile(tempfile,evalue,"UTF-8");
         httpsession.setAttribute(parm,evalue);
      } catch (Exception ex) { 
         httpsession.setAttribute(parm,null);
      }

      if (parm.matches("(session)|(user)|(site)|(study)|(formname)|(formtable)")) {
         setCkvarValue();
      }   
   }
   
   // Retrieve session object (deserialized) value (first check session, then auth string)
   public Object getSessionObjectValue (String parm) throws FORMSException {
      return ObjectUtil.objectFromString(getSessionValue(parm));
   }

   // SET user session object (serialized to string) (save to auth string AND to session) VALUE
   public void setSessionObjectValue (String parm,Object value) throws FORMSException {
      setSessionValue(parm,ObjectUtil.objectToString(value));
   }

   // Initialize Value (If no current value)
   public void initValue(String parm,String value) throws FORMSException {

      if (getValue(parm).length()<1) {
         setValue(parm,value);
      }

   }   

   //////////////////////////////////
   // INPUT-OUTPUT STREAMS METHODS //
   //////////////////////////////////

   // write unencrypted, unencoded stream to auth file
   public void writeAuthStream (String parm,ByteArrayOutputStream baos) throws FORMSException {
      File tempfile=new File(workdir,parm + "_" + fileNameAdd);
      try {
         FileOutputStream fos=new FileOutputStream(tempfile);
         baos.writeTo(fos);
      } catch (Exception ex) { }
   }

   // stream unencrypted, unencoded serialized object to auth file
   public void objectToAuthStream (String parm,Object o) throws FORMSException {
      if (o==null) {
         File tempfile=new File(workdir,parm + "_" + fileNameAdd);
         if (tempfile.exists()) {
            tempfile.delete();
         }
      }
      try {
         ByteArrayOutputStream baos=ObjectUtil.objectToStream(o);
         writeAuthStream(parm,baos);
      } catch (Exception ex) { }
   }

   // write unencrypted, unencoded stream to auth file
   public InputStream getAuthStream (String parm) throws FORMSException {
      File tempfile=new File(workdir,parm + "_" + fileNameAdd);
      if (!tempfile.exists()) {
         return null;
      }
      try {
         FileInputStream fis=new FileInputStream(tempfile);
         return fis;
      } catch (Exception ex) { }
      return null;
   }

   // retrieve object from auth file stream
   public Object objectFromAuthStream (String parm) throws FORMSException {
      try {
         return ObjectUtil.objectFromStream(getAuthStream(parm));
      } catch (Exception ex) { }
      return null;
   }

   // Set auth session attribute (memory or disk depending on configuration)
   public void setSessionAttribute (String name,Object value) {
      try {
         String loc=getAttrstorage();
         if (loc.equals("M")) {
            httpsession.setAttribute(name,value);
         } else {
            objectToAuthStream(name,value);
         }
      } catch (Exception e) { }   
   }

   // Set auth session attribute (memory or disk depending on configuration)
   public Object getSessionAttribute (String name) {
      try {
         String loc=getAttrstorage();
         if (loc.equals("M")) {
            return httpsession.getAttribute(name);
         } else {
            return objectFromAuthStream(name);
         }
      } catch (Exception e) {
         return null;
      }   
   }

   // return attribute storage location
   private String getAttrstorage() {   
      try {
         String loc=getValue("attrstorage");
         if (loc==null) {
            loc=SessionObjectUtil.getConfigObj(httpsession).getAuthAttributeStorageLocation();
            if (!loc.equalsIgnoreCase("M")) loc="D";
            setValue("attrstorage",loc);
         }
         return loc;
      } catch (Exception e) {
         return "D";
      }
   }   

   // SET Option Value
   public void setOption(String parm,String value) throws FORMSException {

      setValue(parm,value);
      try {
         Long luserid=new Long(getValue("luserid"));
         Useroptions uo=(Useroptions)getMainDAO().execUniqueQuery(
            "select uo from Useroptions uo where uo.luserid=" + luserid + " and uo.optname='" + parm + "'"
            );
         if (uo==null) {
            uo=new Useroptions();
            uo.setOptname(parm);
            uo.setLuserid(luserid);
         }
         uo.setOptval(value);
         getMainDAO().saveOrUpdate(uo);
      } catch (Exception de) {
         // Do nothing.  At times, auth has trouble returing the context.  Not sure why
         // at this point, but the effects of not writing to/pulling from Useroptions
         // is minimal
      }

   }   

   // SET Option Value
   public String getOption(String parm) throws FORMSException {

      String value=getValue(parm);
      if (value.length()>0) {
         return value;
      }
      try {
         Long luserid=new Long(getValue("luserid"));
         Useroptions uo=(Useroptions)getMainDAO().execUniqueQuery(
            "select uo from Useroptions uo where uo.luserid=" + luserid + " and uo.optname='" + parm + "'"
            );
         if (uo!=null) {
            return uo.getOptval();
         } 
      } catch (Exception de) {
         // Do nothing.  At times, auth has trouble returing the context.  Not sure why
         // at this point, but the effects of not writing to/pulling from Useroptions
         // is minimal
      }
      return "";

   }   

   // Initialize Option Value (If no current value)
   public void initOption(String parm,String value) throws FORMSException {

      if (getOption(parm).length()<1) {
         setOption(parm,value);
      }

   }   
         
   // RETURN ckvar VALUE
   public String getCkvarValue () throws FORMSException {
      return getValue("ckvar");
   }

   public void setCkvarValue () throws FORMSException {
      setValue("ckvar",CkvarUtil.getCkvarValue(getValue("session"),getValue("user"),getValue("site"),
                       getValue("study"),getValue("formname"),getValue("formtable")));
   }

   // SET ckvar VALUE (USED BY TIMEOUT PROGRAM)
   public void setTimeoutCkvarValue () throws FORMSException {
      setValue("ckvar",CkvarUtil.getCkvarValue("1","1","1","1","1","1"));
   }

   // Override method used to generate bad ckvar value used by logout program
   public void setCkvarValue (String inString) throws FORMSException {
      setValue("ckvar",inString);
   }


   // REMOVE WORKING DIRECTORY
   protected void deleteWorkDir () throws FORMSException {
      File f=new File(workdir);
      if (f.getName().toLowerCase().startsWith("_work")) {
         try {
           // commons utility does recursive delete
           org.apache.commons.io.FileUtils.deleteDirectory(f);
         } catch(Exception ex) { }
      }
      workdir="";
   }

   // Retrieve encrypted string
   public String getEncryptedString (String inString) throws FORMSException {
      return encryptString(inString);
   }  

   // Retrieve Decrypted string
   public String getDecryptedString (String inString) throws FORMSException {
      return decryptString(inString);
   }  

   private HttpServletRequest getRequest() {
      return request;
   }

   private HttpServletResponse getResponse() {
      return response;
   }

   private HttpSession getSession() {
      return getRequest().getSession();
   }

   /*
    * NOTE HERE:  We are maintaining and using a separate application context
    * here from the root application context for the application that would
    * be accessed from the getApplicationContext() method.  For some reason
    * when using that context, hibernate write operations threw exceptions.
    * When fixes were implemented to try eliminate the error (setting
    * HibernateSession flushMode to >= COMMIT), the exceptions went away,
    * but the write operations simply did not occur.  The hack here is 
    * simply to create another ApplicationContext maintained in the 
    * httpsession for use with hibernate.
    * MRH 10/03/2007
    */
    
   private AbstractApplicationContext getContext() {
      try {
         return SessionObjectUtil.getApplicationContext(this.getSession(),sconn);
      } catch (Exception e) {
         return null;
      }
   }

   
   private MainDAO getMainDAO() {
      return (MainDAO)this.getContext().getBean("mainDAO");
   }   
   
   private EmbeddedDAO getEmbeddedDAO() {
      return (EmbeddedDAO)this.getContext().getBean("embeddedDAO");
   }   

   public String getSyswork() {
      return workdir;
   }

   // CHECK FOR VALID AUTHORIZATION
   public boolean isAuth(HttpServletRequest request,HttpServletResponse response) {
      String ck2="";
      try {
         ck2=CkvarUtil.getCkvarValue(getValue("session"),getValue("user"),getValue("site"),
                                     getValue("study"),getValue("formname"),getValue("formtable"));

         setValue("timeout","N");
         if (getValue("session").equalsIgnoreCase("1") && ck2.equalsIgnoreCase(getValue("ckvar"))) {
            // MAKE SURE FORMS SESSION HAS NOT TIMED OUT
            long currtime=System.currentTimeMillis();

            // NOTE:  Cannot timeout when backing up into form, hence the getformbackup exception
            if ((currtime-new Long(getValue("formstime")).longValue())>
                  (timeoutvalue*1000)
                  //&& !(disable.equalsIgnoreCase("NOTIMEOUT")) 
               ) {

               setValue("timeout","Y");
               return false;
               
            } 
            else if (getValue("study").equalsIgnoreCase("TIMEOUT")) {

               setValue("timeout","Y");
               return false;
            }
            else {
               // DO NOTHING (CONTINUE IN PROGRAM)
               return true;
            }
         }
         return false;

      } catch(Exception ex) {
         // NOTE: Some pages with multiple IFRAMES (which are have multiple servlets running
         // more or less at the same time), are occasionally generating exceptions.  if the 
         // ckvars match, goahead and return true, otherwise we'll return false.  I think 
         // this should be satisfactory.

         try {
            if (ck2.equalsIgnoreCase(getValue("ckvar"))) {
               return true;
            } else {
               return false;
            } 
         } catch (Exception ex2) {
            return false;
         }
      }
   }


  private String encryptString(String ins) throws FORMSException {

    if (ins==null || ins.length()<1) {
       return "";
    }

    try {

       byte[] b=ins.getBytes("ISO-8859-1");  
       Cipher cipher = Cipher.getInstance("DESede");
       cipher.init(Cipher.ENCRYPT_MODE, sk);
       return new String(Base64.encode(cipher.doFinal(b)),"ISO-8859-1");

    } catch (Exception e) {
       throw new FORMSException(e,"Encryption error");
    }

  }

  private String decryptString(String ins) throws FORMSException {

    if (ins==null || ins.length()<1) {
       return "";
    }

    try {
       byte[] b=Base64.decode(ins.getBytes("ISO-8859-1"));  
       Cipher cipher = Cipher.getInstance("DESede");
       cipher.init(Cipher.DECRYPT_MODE, sk);
       return new String(cipher.doFinal(b),"ISO-8859-1");
    
    } catch (Exception e) {
       throw new FORMSException(e,"Decryption error");
    }

  }

}


