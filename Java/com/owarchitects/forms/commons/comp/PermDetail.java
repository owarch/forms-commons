 
/**
 *
 * This program is part of:
 *
 *    FORMS [Forms-Oriented Research Management System]
 *
 * Copyright (C) 2009 OpenWare Architects.  All Rights Reserved.
 *
 * FORMS is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * FORMS is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with FORMS.  If not, see <http://www.gnu.org/licenses/>.
 *
 */
 
/* 
 *
 * PermDetail.java -  Class used by PermHelper to store perms info in session list
 *
 */

package com.owarchitects.forms.commons.comp;

import java.util.*;
import java.io.*;


public class PermDetail implements Serializable {
   
   //

   private com.owarchitects.forms.commons.db.Permissioncats permissioncats;
   private int pcatvalue;
   private long siteid;
   private long studyid;
   private BitSet permlevel;
   
   //

   public com.owarchitects.forms.commons.db.Permissioncats getPermissioncats() {
      return permissioncats;
   }

   public void setPermissioncats(com.owarchitects.forms.commons.db.Permissioncats permissioncats) {
      this.permissioncats=permissioncats;
   }
   
   //

   public int getPcatvalue() {
      return pcatvalue;
   }

   public void setPcatvalue(int pcatvalue) {
      this.pcatvalue=pcatvalue;
   }
   
   //

   public long getSiteid() {
      return siteid;
   }

   public void setSiteid(long siteid) {
      this.siteid=siteid;
   }
   
   //

   public long getStudyid() {
      return studyid;
   }

   public void setStudyid(long studyid) {
      this.studyid=studyid;
   }
   
   //

   public BitSet getPermlevel() {
      return permlevel;
   }

   public void setPermlevel(BitSet permlevel) {
      this.permlevel=permlevel;
   }
   
   //

}

