 
/**
 *
 * This program is part of:
 *
 *    FORMS [Forms-Oriented Research Management System]
 *
 * Copyright (C) 2009 OpenWare Architects.  All Rights Reserved.
 *
 * FORMS is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * FORMS is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with FORMS.  If not, see <http://www.gnu.org/licenses/>.
 *
 */
 
/**
* FORMSLog.java -- FORMS Logging Facility
* Original Creation -- 10/2007 (MRH)
*/

package com.owarchitects.forms.commons.comp;

import java.util.*;
import java.math.*;
import java.io.*;
import org.apache.commons.io.FileUtils;

public class FORMSLog {

   // Instances should not be created
   public FORMSLog() {
      super();
   }

   // Append message to FORMSLog
   public static void append (File syswork,String message) {

      try {
   
         // Append output to log file
         if (syswork.exists() && syswork.isDirectory()) {
            File logfile=new File(syswork,"FORMSLog.txt");
            // create file if it doesn't exist
            logfile.createNewFile();
            logfile.setWritable(true);
            // append message to file
            FileWriter fw=new FileWriter(logfile,true);
            fw.write("\n--- Log Entry:  " + new Date().toString() + " ---\n" + message);
            fw.close();
         }   

      } catch (Exception e) { 
         // On exception, just skip logging
      }   

   }

}



