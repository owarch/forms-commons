 
/**
 *
 * This program is part of:
 *
 *    FORMS [Forms-Oriented Research Management System]
 *
 * Copyright (C) 2009 OpenWare Architects.  All Rights Reserved.
 *
 * FORMS is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * FORMS is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with FORMS.  If not, see <http://www.gnu.org/licenses/>.
 *
 */
 
package com.owarchitects.forms.commons.comp;

/*
 * FORMSServiceConstants.java - Constant values used by service client controllers
 *   and services.
 */

import java.lang.reflect.*;

public class FORMSServiceConstants extends FORMSConstants {

   public static final int OK = 1;
   public static final int NO_PERMISSION = 90;
   public static final int NO_MATCHING_INSTALLATION = 91;
   public static final int CHECKSTRING_ERROR = 92;
   public static final int SERVICETARGET_EXCEPTION = 93;
   public static final int SERVICECLIENT_EXCEPTION = 94;
   public static final int SERVICEINTERCEPTOR_EXCEPTION = 95;
   public static final int NO_DATA_ACCESS = 96;
   public static final int CSA_NOT_OK = 97;
   public static final int INVALID_CREDENTIALS = 98;
   public static final int OTHER_ERROR = 99;
   //public static final String CHECK_STRING = "^1234567890ABCDEFGHIJKLMNOPQRSTUVWXYZ0987654321$";
   public static final String CHECK_STRING = "123";

}


