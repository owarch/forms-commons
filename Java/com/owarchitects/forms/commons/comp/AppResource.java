 
/**
 *
 * This program is part of:
 *
 *    FORMS [Forms-Oriented Research Management System]
 *
 * Copyright (C) 2009 OpenWare Architects.  All Rights Reserved.
 *
 * FORMS is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * FORMS is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with FORMS.  If not, see <http://www.gnu.org/licenses/>.
 *
 */
 
/* 
 *
 * ResourceAccessor.java -  When extended, gives subclasses access to resources
 *   available to controllers.  Subclasses should call super(request,response).
 *
 * NOTE:  Objects extending ResourceAccessor should not be made available via
 *        SessionObjectUtil.  They have a high failure rate, probably due to
 *        their construction via transient request & response objects
 *
 */

package com.owarchitects.forms.commons.comp;

import com.owarchitects.forms.commons.db.*;

import java.util.*;
import java.io.*;
import javax.servlet.*;
import javax.servlet.http.*;
import java.security.*;

import org.springframework.context.*;
import org.springframework.context.support.*;
import org.springframework.orm.hibernate3.*;
import org.hibernate.*;
import org.hibernate.transform.*;
import org.apache.velocity.Template;
import org.apache.velocity.app.VelocityEngine;
import org.apache.commons.dbcp.BasicDataSource;
import com.mchange.v2.c3p0.ComboPooledDataSource;
import org.springframework.transaction.support.TransactionSynchronizationManager;


public interface AppResource extends BasicResource {

   public FORMSAuth getAuth(boolean setCallingClass) throws FORMSException,FORMSKeyException,FORMSSecurityException;

   public FORMSAuth getAuth() throws FORMSException,FORMSKeyException,FORMSSecurityException;

   public org.apache.velocity.Template getVelocityTemplate(String inString) throws FORMSException;

   public void logEvent(String detail);

   public File getSyswork();

   public Sysconn getSysconn() throws Exception;

}

