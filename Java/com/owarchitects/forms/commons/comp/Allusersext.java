 
/**
 *
 * This program is part of:
 *
 *    FORMS [Forms-Oriented Research Management System]
 *
 * Copyright (C) 2009 OpenWare Architects.  All Rights Reserved.
 *
 * FORMS is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * FORMS is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with FORMS.  If not, see <http://www.gnu.org/licenses/>.
 *
 */
 
package com.owarchitects.forms.commons.comp;

/**
 *  Allusersext.java  - used by role utilities
 */

import com.owarchitects.forms.commons.db.*;
import java.io.Serializable;

public class Allusersext implements Serializable {

    private boolean ischild;
    private Allusers allusers;

    //

    public boolean getIschild() {
        return this.ischild;
    }

    public void setIschild(boolean ischild) {
        this.ischild = ischild;
    }

    //

    public Allusers getAllusers() {
        return this.allusers;
    }

    public void setAllusers(Allusers allusers) {
        this.allusers = allusers;
    }

    //

}

