 
/**
 *
 * This program is part of:
 *
 *    FORMS [Forms-Oriented Research Management System]
 *
 * Copyright (C) 2009 OpenWare Architects.  All Rights Reserved.
 *
 * FORMS is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * FORMS is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with FORMS.  If not, see <http://www.gnu.org/licenses/>.
 *
 */
 
package com.owarchitects.forms.commons.comp;

import java.io.*;
import java.util.*;
import java.text.*;
import edu.cornell.lassp.houle.RngPack.*;
import edu.cornell.lassp.houle.RngPack.*;
import cern.colt.list.*;
import cern.jet.stat.*;
import org.apache.regexp.RE;
import org.apache.commons.lang.StringUtils;


public class EcodeUtil {

   // SET THIS PARAMETER TO STRING POSITION TO BEGIN PULL
   private static int startAtChar=4;
   // SET ECODE LENGTH
   private static int ecodeLength=6;

   // Method is public (needs to be accessible by FORMSService)
   public static String getEcode(String seedString) {
      String ecode="";

      // Delete any leading zeros from study and append to the end of ID 
      // DASHES AND UNDERSCORES ARE ALLOWED BUT IGNORED
      seedString=seedString.toUpperCase().replaceAll("_","").replaceAll("-","");
      try {
         long seedStringInt=new Long(seedString).longValue();
         if (seedStringInt>0) {
            seedString=new Long(seedString).toString();
         }
      } catch (Exception e) {
         // do nothing
      }

      // Will go through up to three passes to produce ECODE, 
      for (int j=1;j<=3;j++) {
         try {
            //System.out.println("HELLO - " + seedString);
            // LETTERS ARE CONVERTED TO UPPERCASE
            // DASHES AND UNDERSCORES ARE ALLOWED BUT IGNORED
            // DECIDE ON MATCH TYPE
            RE r1 = new RE("[^0-9]");
            RE r2 = new RE("[^A-Z0-9]");
            if (!r1.match(seedString)) {
               // RETURN ECODE FOR NUMERIC STRING
               ecode=getFromRan(new Long(seedString).longValue());
            } else if (!r2.match(seedString)) {
               String indexString="0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ";
               long finalSeed=0;
               // DOUBLESEED USED TO CHECK IF INPUT BECOMING TOO LONG FOR A LONG VALUE
               double doubleSeed=0;
               for (int i=0;i<seedString.length();i++) {
                  char thisChar=seedString.charAt(i);
                  String thisString=new Character(thisChar).toString();
                  int indexVar=indexString.indexOf(thisString)+1;
   	       // FORMULA CREATED TO INSURE NO TWO STRINGS CAN PRODUCE THE SAME FINALSEED.  
   	       // THIS PROGRAM LEAVES A LOT OF GAPS (UNUSED SEED VALUES) AND LIMITS THE
   	       // LENGTH OF ALPHANUMERIC SEEDS TO ABOUT 12 CHARACTERS.  THIS SHOULDNT
   	       // BE A PROBLEM, HOWEVER                                              
                  finalSeed=new Double(finalSeed + 
   	           (java.lang.Math.pow(new Integer(indexString.length()).doubleValue(),i))*indexVar).longValue();
                  doubleSeed=new Double(doubleSeed + 
   	           (java.lang.Math.pow(new Integer(indexString.length()).doubleValue(),i))*indexVar).doubleValue();
   	       //System.out.println("seedString=" + seedString + "   " + "finalSeed=" + finalSeed);
   
               }
   	    //System.out.println("seedString=" + seedString + "   " + "finalSeed=" + finalSeed);
               // SEE IF VALUE HAS BECOME TOO LONG TO STORE IN LONG.  JAVA DOESNT PRODUCE AN
   	    // EXCEPTION IF THIS HAPPENS, WE WOULD JUST START GETTING IDENTICAL ECODES
               if (new Long(finalSeed).doubleValue()==doubleSeed) {
                  ecode=getFromRan(finalSeed);
               } else {
                  ecode="-1";
               }
            } else {
               // RETURN "INVALID MATCH" ECODE
               ecode="-1";
            } 
         } catch (Exception e) {
            ecode="-1";
         }
         if (ecode.equals("-1")) {
            // For next pass, remove alphabetic characters
            if (j<=1) {
               seedString.replaceAll("[A-Za-z]","");
            } 
         // leave loop as soon as a valid ecode is returned
         } else {
            break;
         }
      }
      return ecode;
   }

   public static String getFromRan(long seed) {
      // Create new Ranlux instance
      Ranlux ran=new Ranlux(seed);
      // Pull first random number from seed
      double addVal=ran.raw();
      // Format resulting output to StringBuffer
      DecimalFormat df=new DecimalFormat("0.00000000000000000000");
      StringBuffer sb=new StringBuffer();
      df.format(addVal,sb,new FieldPosition(0));
      // Delete initial 1/0 and decimal point
      String sString=sb.substring(2);
      // Pull in six-character substring starting at "startAtChar" position
      String ecodeString=sString.substring(startAtChar,startAtChar+ecodeLength);
      // Return result as ecode
      return ecodeString;
   }

}


