 
/**
 *
 * This program is part of:
 *
 *    FORMS [Forms-Oriented Research Management System]
 *
 * Copyright (C) 2009 OpenWare Architects.  All Rights Reserved.
 *
 * FORMS is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * FORMS is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with FORMS.  If not, see <http://www.gnu.org/licenses/>.
 *
 */
 
/*
 *
 * AdminMenuController.java - Admin Menu controller
 *
 */

package com.owarchitects.forms.commons.comp;

import java.util.*;
import java.io.*;
import java.lang.Thread;
import javax.servlet.http.*;
import org.springframework.web.servlet.*;
import org.springframework.context.support.*;

public class AdminMenuController extends FORMSController {

   public ModelAndView submitRequest() throws FORMSException {

      // Use class-name based view
      mav=new ModelAndView("AdminMenu");
      try {

         mav.addObject("auth",this.getAuth());
         return mav;
   
      } catch (Exception e) { 
         throw new FORMSException("FORMS Controller exception:  ",e);
      }

   }

}

