 
/**
 *
 * This program is part of:
 *
 *    FORMS [Forms-Oriented Research Management System]
 *
 * Copyright (C) 2009 OpenWare Architects.  All Rights Reserved.
 *
 * FORMS is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * FORMS is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with FORMS.  If not, see <http://www.gnu.org/licenses/>.
 *
 */
 
/*
 * SelectFormResult.java - Output parameters for SelectFormService - MRH 03/2007
 */

package com.owarchitects.forms.commons.comp;

import java.util.*;
import java.io.*;

public class SelectFormResult extends FORMSResult implements Serializable {

   public org.mla.html.table.Table table;
   public boolean showtype;
   public String typeopt;
   public String veropt;
   public String fnumopt;
   public String sinstopt;
   public String archopt;
   public String rightslevel;

   //

   public void setTable(org.mla.html.table.Table table) {
      this.table=table;
   }

   public org.mla.html.table.Table getTable() {
      return table;
   }

   //

   public void setShowtype(boolean showtype) {
      this.showtype=showtype;
   }

   public boolean getShowtype() {
      return showtype;
   }

   //

   public void setTypeopt(String typeopt) {
      this.typeopt=typeopt;
   }

   public String getTypeopt() {
      return typeopt;
   }

   //

   public void setVeropt(String veropt) {
      this.veropt=veropt;
   }

   public String getVeropt() {
      return veropt;
   }

   //

   public void setFnumopt(String fnumopt) {
      this.fnumopt=fnumopt;
   }

   public String getFnumopt() {
      return fnumopt;
   }

   //

   public void setSinstopt(String sinstopt) {
      this.sinstopt=sinstopt;
   }

   public String getSinstopt() {
      return sinstopt;
   }

   //

   public void setArchopt(String archopt) {
      this.archopt=archopt;
   }

   public String getArchopt() {
      return archopt;
   }

   //

   public void setRightslevel(String rightslevel) {
      this.rightslevel=rightslevel;
   }

   public String getRightslevel() {
      return rightslevel;
   }

}



