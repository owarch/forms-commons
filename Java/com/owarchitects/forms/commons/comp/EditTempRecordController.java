 
/**
 *
 * This program is part of:
 *
 *    FORMS [Forms-Oriented Research Management System]
 *
 * Copyright (C) 2009 OpenWare Architects.  All Rights Reserved.
 *
 * FORMS is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * FORMS is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with FORMS.  If not, see <http://www.gnu.org/licenses/>.
 *
 */
 
/*
 *
 * EditTempRecordController.java - Display/Open FORMS Temporary Record
 *
 */

package com.owarchitects.forms.commons.comp;

import com.owarchitects.forms.commons.db.*;
import java.util.*;
import java.io.*;
//import java.sql.Clob;
import javax.servlet.http.*;
import org.hibernate.*;
import org.springframework.web.servlet.*;
import org.springframework.context.support.*;
import org.springframework.orm.hibernate3.*;
import org.apache.commons.io.FileUtils;
import org.apache.commons.lang.StringUtils;
//import org.springframework.transaction.support.TransactionSynchronizationManager;
//import org.mla.html.table.*;
import org.json.simple.JSONObject;

public class EditTempRecordController extends FORMSController {

   private int keycount;

   public ModelAndView submitRequest() throws FORMSException {

      // User class-name based view
      mav=new ModelAndView("EditTempRecord");

      try {

         String dtdefid=getRequest().getParameter("dtdefid");
         String formtypeid=getRequest().getParameter("formtypeid");
         String recordpath=getRequest().getParameter("recordpath");

         FORMSAuth auth=this.getAuth();
   
         if (dtdefid!=null && formtypeid!=null) {
   
            // Initialize options if necessary
            auth.initOption("trprioropt","N");
            auth.initOption("trinterimopt","N");
   
            // 1) Initial call
            auth.setValue("dtdefid",dtdefid);
            auth.setValue("formtypeid",formtypeid);
            safeRedirect("edittemprecord.do");
   
         } else if (recordpath!=null) {

            // 3) open selected record
            openRecord(recordpath);
            return null;

         } else {
   
            // 2) display records
   
            // Create Options Menu Map
            HashMap optionsmap=new HashMap();
            optionsmap.put("trprioropt",auth.getOption("trprioropt"));
            optionsmap.put("trinterimopt",auth.getOption("trinterimopt"));
            auth.setSessionAttribute("optionsobject",optionsmap);
   
            mav.addObject("status","SHOWRECORDS");   
            getTempRecords();
            ListResult result=new ListResult();
            result.setList(getTempRecords());
            this.getSession().setAttribute("iframeresult",result);
            return mav;

         } 

         return null;

      } catch (Exception e) { 
         throw new FORMSException("FORMS Controller exception:  ",e);
      }

   }

   // Return list of relevant FORMS Temporary Records
   public List getTempRecords() throws FORMSException, FORMSKeyException, FORMSSecurityException {

      FORMSAuth auth=this.getAuth();
      String dtdefid=auth.getValue("dtdefid");
      String formtypeid=auth.getValue("formtypeid");

      // get form key fields
      List keylist=getMainDAO().execQuery(
         "select f from Datatablecoldef f where f.datatabledef.dtdefid=" + dtdefid + 
             " and f.iskeyfield=true " +
             "order by f.columnorder"
         );

      // Get collection of files
      File workdir=this.getSyswork();
      File workparent=workdir.getParentFile();
      File searchdir;

      if (auth.getOption("trprioropt").equals("N")) {
         searchdir=workdir;
      } else {
         searchdir=workparent;
      }

      Collection fc=FileUtils.listFiles(searchdir,new String[] { "xml","encrypt","xml.encrypt" },true);
      Iterator i=fc.iterator();
      ArrayList filelist=new ArrayList();
      // Pull user info to add username to list
      List allusers=getMainDAO().getTable("Allusers");
      // Iterate through files
      while (i.hasNext()) {
         File tfile=(File)i.next();
         String fname=tfile.getName();
         if (fname.substring(0,2).equalsIgnoreCase("t_") || 
              (fname.substring(0,2).equalsIgnoreCase("i_") && auth.getOption("trinterimopt").equalsIgnoreCase("Y"))
            ) {
            String[] fnameinfo=fname.split("[_.]");
            String f_dtdefid=fnameinfo[1];
            // continue only if dtdefid matches current form selection
            if (dtdefid.equals(f_dtdefid)) {
               LinkedHashMap map=new LinkedHashMap();
               String f_auserid=fnameinfo[2];
               String f_time=fnameinfo[3];
               // add full path to file
               map.put("_path",tfile.getPath());
               // add prior session indicator
               String _prior="X";
               if (tfile.getParentFile().equals(workdir)) {
                  _prior=" ";
               } 
               map.put("_prior",_prior);
               map.put("__showprior",auth.getValue("trprioropt"));
               // add interim record indicator
               String _interim=" ";
               if (fname.substring(0,2).equalsIgnoreCase("i_")) {
                  _interim="X";
               } 
               map.put("_interim",_interim);
               map.put("__showinterim",auth.getValue("trinterimopt"));
               // add keyfields
               try {
                  String fcontent=EncryptUtil.decryptString(FileUtils.readFileToString(tfile,"ISO-8859-1"),
                              SessionObjectUtil.getInternalKeyObj(getSession()));
                  // get key field values from content            
                  Iterator kiter=keylist.iterator();
                  while (kiter.hasNext()) {
                     Datatablecoldef f=(Datatablecoldef)kiter.next();
                     String keyname=f.getColumnname();
                     String keyvalue=FormDataUtil.getFieldValue(fcontent,keyname.toLowerCase());
                     map.put(keyname,keyvalue);
                  }
               } catch (Exception e) {
                  // Do nothing
               }
               // add time to map
               try {
               Date _date=new Date(new Long(f_time).longValue());
                  map.put("_date",_date);
               } catch (Exception de) {
                  map.put("_date","");
               }
               // add username to map
               Iterator uiter=allusers.iterator();
               String _user="";
               while (uiter.hasNext()) {
                  Allusers auser=(Allusers)uiter.next();
                  if (new Long(f_auserid).longValue()==auser.getAuserid()) {
                     _user=auser.getUserdesc();
                  }
               }
               map.put("_user",_user);
               filelist.add(map);
            }
         }
      }
      return filelist;
   }   

   // Displays requested record in form
   public void openRecord(String filepath) throws Exception {

      // construct form javascript from values    
      FORMSAuth auth=this.getAuth();
      String dtdefid=auth.getValue("dtdefid");
      String formtypeid=auth.getValue("formtypeid");

/*
      // NOTE:  There is implicit trust for admin users here.  Even for forms that don't
      // exist or for forms from another study.  We may want to check that the form is
      // under the current study later 
      if (!getPermHelper().hasFormPerm(dtdefid,"NEWRECORD")) {

         mav.addObject("status","NOPERMISSION");
         return mav;

      }
*/

      // See who saved record and if user has permission to open it
      File tfile=new File(filepath);
      String fname=tfile.getName();
      String f_auserid="";
      try {
          f_auserid=(tfile.getName().split("[_.]"))[2];
      } catch (Exception ee) {
         // do nothing
      }
      boolean ownfile=false;
      if (f_auserid.equalsIgnoreCase(auth.getValue("auserid"))) {
         ownfile=true;
      }
      if (ownfile) {
         if (!getPermHelper().hasFormPerm(dtdefid,"READOWN")) {
            safeRedirect("permission.err");
            return;
         }
      } else {
         if (!getPermHelper().hasFormPerm(dtdefid,"READOTHER")) {
            safeRedirect("permission.err");
            return;
         }
      }

      // Display form
      Formstore fstore=(Formstore)getMainDAO().execUniqueQuery(
         "select f from Formstore f where f.formtypes.formtypeid=" + formtypeid
         );
      //StringBuilder fcontent=new StringBuilder(fstore.getFormcontent());   
      // TEMPORARY TO ALLOW COMPATIBILITY WITH FORMS STORED PRE-VERSION 0.5.0
      StringBuilder fcontent=new StringBuilder(fstore.getFormcontent().replaceFirst("\"formdefid_\"","\"dtdefid_\""));   


      response.setHeader("Cache-Control","no-cache"); //HTTP 1.1
      response.setHeader("Pragma","no-cache"); //HTTP 1.0
      response.setDateHeader ("Expires", -1); //prevents caching at the proxy server

      /*************************************************************/
      /** RETRIEVE VALUES INTO JAVASCRIPT VARIABLES AND OPEN FILE **/
      /*************************************************************/

      ///////////////////////////////////
      // ADD JAVASCRIPT TO FILE STREAM //
      ///////////////////////////////////
   
      // Create LoadScript
      StringBuilder ls=new StringBuilder();    

      ls.append("_numErrors=0;\n");
      ls.append("try { setReadOnly(getFormElementById(\"FormDataOK\")); } catch (e) {}\n");

      String _webProtocol=request.getRequestURL().toString();
      _webProtocol=_webProtocol.substring(0,_webProtocol.indexOf(":"));

      // Retrieve form data from file
      String trString=EncryptUtil.decryptString(FileUtils.readFileToString(tfile,"ISO-8859-1"),
                         SessionObjectUtil.getInternalKeyObj(getSession()));
      // Create new formdataparser, retrieving javascript used to populate the form
      FormDataParser parser=new FormDataParser(trString);
      ls.append(parser.getBackupJs());

      // THIS MUST BE LAST, SO WE ARE NOT SETTING PASSVALUE FIELD FROM FILE THAT WAS READ IN
      // Contains server information passed to form
      JSONObject passv=new JSONObject();
      passv.put("submittype","temprecord");
      passv.put("rightslevel",auth.getValue("rightslevel"));
      passv.put("servername",request.getServerName());
      passv.put("continueservlet","selectform.do");
      ls.append("try { _rmsPassValueField='" + passv.toString() + "'; _rmsPassValueObject=jsonParse(_rmsPassValueField); } catch (e) {}\n");

      ls.append("getFormElementById(\"dtdefid_\").value='" + dtdefid + "';\n");
      ls.append("_docReady=0;\n");
      ls.append("try { execValidate(); } catch (e) {}\n");
      ls.append("_saveStatus=\"OK\";\n");
      ls.append("_docReady=1;\n");

      //auth.setValue("WinNameMissOk","Y");

      String rstr="_insertLoadScriptHere=\"XXXHEREXXX\";";
      int inx=fcontent.indexOf(rstr);
      int inxt =inx+rstr.length();
      String outstring=fcontent.replace(inx,inxt,ls.toString()).toString();
      out.println(outstring);
      out.flush();

   }

}




