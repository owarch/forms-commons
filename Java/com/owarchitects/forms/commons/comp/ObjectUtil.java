 
/**
 *
 * This program is part of:
 *
 *    FORMS [Forms-Oriented Research Management System]
 *
 * Copyright (C) 2009 OpenWare Architects.  All Rights Reserved.
 *
 * FORMS is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * FORMS is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with FORMS.  If not, see <http://www.gnu.org/licenses/>.
 *
 */
 
/*
 *
 * ObjectUtil.java - Utility to encode objects to an XML string and decode XML
 *                      strings back to objects
 *
 */

package com.owarchitects.forms.commons.comp;

import java.util.*;
import java.io.*;
//import java.beans.*;
//import java.lang.reflect.*;
import java.net.URLEncoder;
import java.net.URLDecoder;

public class ObjectUtil {
   
   // Instances should not be created
   public ObjectUtil() {
      super();
   }

   // save (serializable) object to String
   //public static String objectToString(Object o) throws IOException,UnsupportedEncodingException {
   public static String objectToString(Object o) {

      if (o==null) return null;

      try {

         ByteArrayOutputStream bos=new ByteArrayOutputStream();
         ObjectOutputStream oos=new ObjectOutputStream(bos);
         oos.writeObject(o);
         oos.close();
         return bos.toString("ISO-8859-1");

      } catch (Exception e) {

         return null;

      }

   }

   // Return encoded string
   public static String objectToEncodedString(Object obj) {

      try {

         if (obj==null) return null;

         return URLEncoder.encode(objectToString(obj),"UTF-8");

      } catch (Exception e) {

         return null;

      }


   }

   // save (serializable) object to String
   //public static Object objectFromString(String s) throws IOException,ClassNotFoundException {
   public static Object objectFromString(String s) {

      if (s==null) return null;

      try {

         ByteArrayInputStream bis=new ByteArrayInputStream(s.getBytes("ISO-8859-1"));
         ObjectInputStream ois=new ObjectInputStream(bis);
         Object o=ois.readObject();
         ois.close();
         return o;

      } catch (Exception e) {

         return null;

      }
   
   }

   // Return object from encoded string
   public static Object objectFromEncodedString(String enc) {

      try {

      if (enc==null) return null;

         return objectFromString(URLDecoder.decode(enc,"UTF-8"));

      } catch (Exception e) {

         return null;

      }

   }

   public static ByteArrayOutputStream objectToStream(Object o) {

      if (o==null) return null;

      try {

         ByteArrayOutputStream bos=new ByteArrayOutputStream();
         ObjectOutputStream oos=new ObjectOutputStream(bos);
         oos.writeObject(o);
         oos.close();
         return bos;

      } catch (Exception e) {

         return null;

      }

   }

   // save (serializable) object to String
   //public static Object objectFromString(String s) throws IOException,ClassNotFoundException {
   public static Object objectFromStream(InputStream is) {

      if (is==null) return null;

      try {

         ObjectInputStream ois=new ObjectInputStream(is);
         Object o=ois.readObject();
         ois.close();
         return o;

      } catch (Exception e) {

         return null;

      }
   
   }


}

