 
/**
 *
 * This program is part of:
 *
 *    FORMS [Forms-Oriented Research Management System]
 *
 * Copyright (C) 2009 OpenWare Architects.  All Rights Reserved.
 *
 * FORMS is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * FORMS is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with FORMS.  If not, see <http://www.gnu.org/licenses/>.
 *
 */
 
package com.owarchitects.forms.commons.comp;

import java.util.*;
import java.io.*;
import org.springframework.web.servlet.*;
import org.springframework.web.context.support.*;
import javax.servlet.http.*;

public class LogoutController extends FORMSController {

   private static GenericWebApplicationContext ctx = null;
   private String bgcolor;
   private String bgimage;
   private String menubgcolor;
   
   // Don't perform initial auth checking
   {
      autoAuthCheckOff();
   }

   public ModelAndView submitRequest() throws FORMSException {

      try {

         ModelAndView mav=new ModelAndView();
         HttpServletResponse response=this.getResponse();
         PrintWriter out=this.getWriter();
         WinProps wp=this.getWinProps();
         wp.setWinNameMissOk("N");

         FORMSAuth auth=this.getAuth();

         logEvent("User logout");

         // This will invalidate auth ckvar

         auth.setValue("username","");
         auth.setValue("site","XXXXXX");
         auth.setValue("study","XXXXXX");
         auth.setValue("formname","XXXXXX");
         auth.setValue("ckvar","XXXXXX");
         
         // Reset timeout value
         auth.setValue("timeout","N");
 
         wp.addViewObjects(mav);
         mav.addObject("status","OK");
         return mav;

      } catch (Exception e) { 
         throw new FORMSException("FORMS Controller exception:  " + Stack2string.getString(e));
      }

   }

}

