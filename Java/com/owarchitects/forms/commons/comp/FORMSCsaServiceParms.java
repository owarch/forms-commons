 
/**
 *
 * This program is part of:
 *
 *    FORMS [Forms-Oriented Research Management System]
 *
 * Copyright (C) 2009 OpenWare Architects.  All Rights Reserved.
 *
 * FORMS is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * FORMS is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with FORMS.  If not, see <http://www.gnu.org/licenses/>.
 *
 */
 
/**
 * FORMSCsaServiceParms.java -- Implements parameters and methods used by Parms classes in FORMS
 *                           involving Service data transfers.
 * Original Creation -- 03/2007 (MRH)
 */

package com.owarchitects.forms.commons.comp;

import java.io.*;
import java.util.*;
import java.lang.reflect.*;
import org.springframework.web.servlet.*;
import org.apache.commons.io.FileUtils;

public abstract class FORMSCsaServiceParms extends FORMSServiceParms implements Serializable {

   // shared parameters

   public String username;
   public String password;
   public long csaid;

   // 

   public void setUsername(String username) {
      this.username=username;
   }

   public String getUsername() {
      return username;
   }

   // 

   public void setPassword(String password) {
      this.password=password;
   }

   public String getPassword() {
      return password;
   }
   
   // 

   public void setCsaid(long csaid) {
      this.csaid=csaid;
   }

   public long getCsaid() {
      return csaid;
   }

   // 

}

