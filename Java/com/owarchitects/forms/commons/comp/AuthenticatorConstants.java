 
/**
 *
 * This program is part of:
 *
 *    FORMS [Forms-Oriented Research Management System]
 *
 * Copyright (C) 2009 OpenWare Architects.  All Rights Reserved.
 *
 * FORMS is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * FORMS is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with FORMS.  If not, see <http://www.gnu.org/licenses/>.
 *
 */
 
package com.owarchitects.forms.commons.comp;

/*
 * AuthenticatorConstants.java - Constant values used by Authenticator
 *   and services.
 */

import java.lang.reflect.*;

public class AuthenticatorConstants extends FORMSConstants {

   public static final int LOGGED_IN = 1;
   public static final int HOST_ALLOWED = 9;
   public static final int HOST_DENIED = 70;
   public static final int ACCOUNT_LOCKED = 71;
   public static final int KEY_ERROR = 72;
   public static final int NOENCKEY = 73;
   public static final int BADENCKEYPASS = 74;
   public static final int TEMPPWCHANGE = 75;
   public static final int PWEXPIRED = 76;
   public static final int PWEXPIRESSOON = 77;
   public static final int ACCT_EXPIRED = 78;
   public static final int FAILED = 79;
   public static final int WARNING = 80;
   public static final int EXCESSIVE_LOGINS = 81;

}


