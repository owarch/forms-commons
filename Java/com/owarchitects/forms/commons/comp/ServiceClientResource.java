 
/**
 *
 * This program is part of:
 *
 *    FORMS [Forms-Oriented Research Management System]
 *
 * Copyright (C) 2009 OpenWare Architects.  All Rights Reserved.
 *
 * FORMS is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * FORMS is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with FORMS.  If not, see <http://www.gnu.org/licenses/>.
 *
 */
 
/* 
 *
 * ServiceClientResource.java -  Interface for Service accessors
 *                    
 *
 */

package com.owarchitects.forms.commons.comp;

import java.lang.reflect.InvocationTargetException;

public interface ServiceClientResource {

   // Submit Service Request
   public FORMSServiceResult submitServiceRequest(FORMSServiceParms parms,String endpoint) throws FORMSException;

   public FORMSServiceResult getErrorObject(FORMSServiceParms parms,int i) 
      throws NoSuchMethodException,IllegalAccessException,InvocationTargetException,java.lang.InstantiationException;

}

