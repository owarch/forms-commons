 
/**
 *
 * This program is part of:
 *
 *    FORMS [Forms-Oriented Research Management System]
 *
 * Copyright (C) 2009 OpenWare Architects.  All Rights Reserved.
 *
 * FORMS is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * FORMS is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with FORMS.  If not, see <http://www.gnu.org/licenses/>.
 *
 */
 
/*
 *
 * NewFormRecordController.java - Enter New Form Record
 *
 */

package com.owarchitects.forms.commons.comp;

import com.owarchitects.forms.commons.db.*;
import java.util.*;
import java.io.*;
import java.text.SimpleDateFormat;
//import java.sql.Clob;
import javax.servlet.http.*;
import org.hibernate.*;
import org.springframework.web.servlet.*;
import org.springframework.context.support.*;
import org.springframework.orm.hibernate3.*;
//import org.apache.commons.io.FileUtils;
import org.apache.commons.lang.StringUtils;
//import org.springframework.transaction.support.TransactionSynchronizationManager;
//import org.mla.html.table.*;
import org.json.simple.JSONObject;

public class NewFormRecordController extends FORMSController {

   public ModelAndView submitRequest() throws FORMSException {

      try {

         FORMSAuth auth=this.getAuth();

         String dtdefid=getRequest().getParameter("dtdefid");
         String formtypeid=getRequest().getParameter("formtypeid");

         if (dtdefid!=null && formtypeid!=null) {

            auth.setValue("dtdefid",dtdefid);
            auth.setValue("formtypeid",formtypeid);
            safeRedirect("newformrecord.do");

         } else {

            dtdefid=auth.getValue("dtdefid");
            formtypeid=auth.getValue("formtypeid");

            // NOTE:  There is implicit trust for admin users here.  Even for forms that don't
            // exist or for forms from another study.  We may want to check that the form is
            // under the current study later 
            if (!getPermHelper().hasFormPerm(dtdefid,"NEWRECORD")) {

               safeRedirect("permission.err");
               return null;

            }

            // Display form
            Formstore fstore=(Formstore)getMainDAO().execUniqueQuery(
               "select f from Formstore f where f.formtypes.formtypeid=" + formtypeid
               );
            //StringBuilder fcontent=new StringBuilder(fstore.getFormcontent());   
            // TEMPORARY TO ALLOW COMPATIBILITY WITH FORMS STORED PRE-VERSION 0.5.0
            StringBuilder fcontent=new StringBuilder(fstore.getFormcontent().replaceFirst("\"formdefid_\"","\"dtdefid_\""));   

            response.setHeader("Cache-Control","no-cache"); //HTTP 1.1
            response.setHeader("Pragma","no-cache"); //HTTP 1.0
            response.setDateHeader ("Expires", -1); //prevents caching at the proxy server
   
            /*************************************************************/
            /** RETRIEVE VALUES INTO JAVASCRIPT VARIABLES AND OPEN FILE **/
            /*************************************************************/
   
            ///////////////////////////////////
            // ADD JAVASCRIPT TO FILE STREAM //
            ///////////////////////////////////
         
            // Create LoadScript
            StringBuilder ls=new StringBuilder();    

            ls.append("_numErrors=0;\n");
            ls.append("try { setReadOnly(getFormElementById(\"FormDataOK\")); } catch (e) {}\n");
   
            JSONObject passv=new JSONObject();
            passv.put("submittype","newrecord");
            passv.put("rightslevel",auth.getValue("rightslevel"));
            passv.put("servername",request.getServerName());
            if (getRequest().getServletPath().indexOf("datatable")>=0) {
               passv.put("continueservlet","viewdatatable.do");
            } else {
               passv.put("continueservlet","selectform.do");
            }
            ls.append("try { _rmsPassValueField='" + passv.toString() + "'; _rmsPassValueObject=jsonParse(_rmsPassValueField); } catch (e) {}\n");

            ls.append("getFormElementById(\"dtdefid_\").value='" + dtdefid + "';\n");
            ls.append("_docReady=0;\n");
            ls.append("try { execValidate(); } catch (e) {}\n");
            ls.append("_saveStatus=\"OK\";\n");
            ls.append("_docReady=1;\n");

            // for patients (external users), see if ID fields should be set 
            if (auth.getValue("rightslevel").equalsIgnoreCase("EXTERNAL")) {

               ls.append(externalIdAssign(dtdefid));

            }

            //auth.setValue("WinNameMissOk","Y");
   
            String rstr="_insertLoadScriptHere=\"XXXHEREXXX\";";
            int inx=fcontent.indexOf(rstr);
            int inxt =inx+rstr.length();
            String outstring=fcontent.replace(inx,inxt,ls.toString()).toString();
            out.println(outstring);
            out.flush();
            return null;

         }

         return null;
   
      } catch (Exception e) { 
         throw new FORMSException("FORMS Controller exception:  ",e);
      }

   }


   // Create JavaScript ID/ECODE assignments for external users if appropriate
   private String externalIdAssign(String dtdefid) throws FORMSException,FORMSKeyException,FORMSSecurityException {

      StringBuilder sb=new StringBuilder();

      FORMSAuth auth=getAuth();

      // See if an ID assignment exists for user
      List ulist=getMainDAO().execQuery( "select u from Useridassign u where u.studies.studyid=" +  auth.getValue("studyid") +
         " and u.localusers.luserid=" + auth.getValue("luserid")
         );

      if (ulist.size()<1) {
         return "";
      }

      // See if there's an ECODE field and get value for that   
      List ecl=getMainDAO().execQuery(
         "select f from Datatablecoldef f join fetch f.datatabledef where f.datatabledef.dtdefid=" + dtdefid +
             " and f.isecode=true " +
             "order by f.columnorder"
         );

      if (ecl.size()>0) {

         List fkl=getMainDAO().execQuery(
            "select f from Datatablecoldef f join fetch f.datatabledef where f.datatabledef.dtdefid=" + dtdefid +
                " and f.iskeyfield=true " +
                "order by f.columnorder"
            );

         // Proceed only if we have value assignments for all ID fields
         StringBuilder ecodesb=new StringBuilder();
         boolean allvalues=true;
         Iterator fki=fkl.iterator();
         while (fki.hasNext()) {
            Datatablecoldef f=(Datatablecoldef)fki.next();
            boolean hasmatch=false;
            Iterator ui=ulist.iterator();
            while (ui.hasNext()) {
               Useridassign u=(Useridassign)ui.next();
               String value=u.getVarvalue();
               if (u.getVarname().equalsIgnoreCase(f.getColumnname()) && value!=null && value.trim().length()>0) {
                  hasmatch=true;
                  // append value to stringbuffer for computing ecode
                  ecodesb.append(value);
               }
            }
            if (!hasmatch) {
               if (f.getIsidfield()) {
                  allvalues=false;
               } else {
                  // If there's a keyfield and it's a date with a recognized pattern, fill in the date with the current date
                  try {
                     List ffl=getMainDAO().execQuery(
                        "select f from Dtfieldattrs f where f.datatabledef.dtdefid=" + dtdefid + " and " +
                           "f.fieldname='" + f.getColumnname() + "' and f.attr='PAT'"
                         );
                     Iterator ffi=ffl.iterator();
                     while (ffi.hasNext()) {
                        Dtfieldattrs attr=(Dtfieldattrs)ffi.next();
                        if (attr.getValue().equalsIgnoreCase("MM/DD/YYYY")) {

                           SimpleDateFormat df=new SimpleDateFormat("MM/dd/yyyy");
                           String currdate=df.format(System.currentTimeMillis());
                           sb.append("getFormElementById(\"" + f.getColumnname() + "\").value='" + currdate + "';\n");

                        } else if (attr.getValue().equalsIgnoreCase("MM/DD/YYYY")) {

                           SimpleDateFormat df=new SimpleDateFormat("MM/dd/yy");
                           String currdate=df.format(System.currentTimeMillis());
                           // Don't set field to read-only, just set value
                           sb.append("getFormElementById(\"" + f.getColumnname() + "\").value='" + currdate + "';\n");

                        }
                     }
                  } catch (Exception skipex) {
                     // Do nothing, don't set date
                  }

               }
            }
         }

         if (allvalues) {

            String ecode=EcodeUtil.getEcode(ecodesb.toString());

            // Create temporary useridassign object for ecode field, so assignment is done
            Iterator eci=ecl.iterator();
            while (eci.hasNext()) {
               Datatablecoldef f=(Datatablecoldef)eci.next();
               Useridassign u=new Useridassign();
               u.setVarname(f.getColumnname());
               u.setVarvalue(ecode);

               ulist.add(u);
            }

         }   

      }   
      // create javascript assignments
      Iterator i=ulist.iterator();   
      while (i.hasNext()) {
         Useridassign u=(Useridassign)i.next();
         String varname=u.getVarname();
         String varvalue=u.getVarvalue();
         if (varvalue!=null && varvalue.length()>0) {
            sb.append("getFormElementById(\"" + varname + "\").value='" + varvalue + "';\n");
            sb.append("getFormElementById(\"" + varname + "\").readOnly='true';\n");

         }
      }

      return sb.toString();
  
   }

}

