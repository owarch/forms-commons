
/*
 *
 * SelectFormController.java - Form Selection Interface
 *
 */

package com.owarchitects.forms.commons.comp;

import com.owarchitects.forms.commons.db.*;
import java.util.*;
import java.io.*;
import javax.servlet.http.*;
import org.hibernate.*;
import org.springframework.web.servlet.*;
import org.springframework.context.support.*;
import org.springframework.orm.hibernate3.*;
import org.springframework.orm.hibernate3.*;
import org.springframework.transaction.support.TransactionSynchronizationManager;
import org.mla.html.table.*;

public class SelectFormController extends FORMSController {

   public ModelAndView submitRequest() throws FORMSException {

      try {

         FORMSAuth auth=this.getAuth();

         ModelAndView mav=new ModelAndView();
         HttpServletResponse response=this.getResponse();
         PrintWriter out=this.getWriter();

         // Initialize Options (If necessary)
         auth.initOption("typeopt","ALL");
         auth.initOption("veropt","REC");
         auth.initOption("archopt","N");
         auth.initOption("fnumopt","");
         auth.initOption("sinstopt","");

         // clear stored values
         auth.setValue("dtdefid","");
         auth.setValue("formtypeid","");

         SelectFormResult result=processData(false);
         result.setTypeopt(auth.getOption("typeopt"));
         result.setVeropt(auth.getOption("veropt"));
         result.setFnumopt(auth.getOption("fnumopt"));
         result.setArchopt(auth.getOption("archopt"));
         result.setSinstopt(auth.getOption("sinstopt"));
         result.setRightslevel(auth.getOption("rightslevel"));
         this.getSession().setAttribute("iframeresult",result);
         mav.addObject("status","OK");
         return mav;
   
      } catch (Exception e) { 
         throw new FORMSException("FORMS Controller exception:  ",e);
      }

   }

   // Create root hierarchy record if none exists (these are not created at database initialization)
   private void createHaRecord(int vis) throws FORMSException, FORMSSecurityException, FORMSKeyException {
      Studies s=(Studies)getMainDAO().execUniqueQuery(
         "select s from Studies s where studyid=" + getAuth().getValue("studyid")
         );
      if (s==null) return;   
      Formha newha=new Formha();
      newha.setVisibility(vis);
      newha.setStudies(s);
      newha.setHaordr(1);
      if (vis==VisibilityConstants.STUDY) {
         newha.setHadesc("Study-Level Forms");
      } else if (vis==VisibilityConstants.SITE) {
         newha.setHadesc("Site-Level Forms");
      } else if (vis==VisibilityConstants.SYSTEM) {
         newha.setHadesc("System-Level Forms");
      } else {
         return;
      }
      getMainDAO().saveOrUpdate(newha);
      return;

   }

   //private SelectFormResult processData(boolean showarchivedforms) throws FORMSException {
   private SelectFormResult processData(boolean showarchivedforms) throws FORMSException {

      //SelectFormResult result=new SelectFormResult();

      try {

        FORMSAuth auth=this.getAuth();

        // Use session-stored lists if available to avoid frequent hits to database for pulling form list
        List fhatemp=(List)auth.getSessionAttribute("formhalist");
        List fttemp=(List)auth.getSessionAttribute("formtypeslist");
        HashMap iinfotemp=(HashMap)auth.getSessionAttribute("iinfomap");
        Iterator i;

        // If no/empty list available, pull from database
        if (fhatemp==null || fttemp==null || fhatemp.size()<1 || fttemp.size()<1) {

           // Retrive installation information for later
           List ilist=getMainDAO().execQuery(
              "select a from Allinst a join fetch a.linkedinst where a.islocal!=true"
              );
           i=ilist.iterator();
           HashMap iinfomap=new HashMap();
           while (i.hasNext()) {
             Allinst ainst=(Allinst)i.next();
             iinfomap.put(new Long(ainst.getAinstid()).toString(),ainst.getLinkedinst().getAcronym());
           }

           fhatemp=getMainDAO().execQuery(
              "select fha from Formha fha " +
                    "where ((" +
                              "fha.visibility=" + VisibilityConstants.SYSTEM + 
                              ") or (" +
                              "fha.visibility=" + VisibilityConstants.SITE + " and fha.studies.sites.siteid=" + auth.getValue("siteid") + 
                              ") or (" +
                              "fha.visibility=" + VisibilityConstants.STUDY + " and fha.studies.studyid=" + auth.getValue("studyid")  + 
                           ")) " +
                    "order by haordr"
              );

           i=fhatemp.iterator();
           // 1) Write formhaid to StringBuilder for use in formtypes pull
           // 2) Verify that root system, site & study records exist.  Otherwise create them
           StringBuilder sb=new StringBuilder();
           boolean hassystem=false, hassite=false, hasstudy=false;
           while (i.hasNext()) {
              Formha ha=(Formha)i.next();
              // write id to stringbuffer
              sb.append(new Long(ha.getFormhaid()).toString());
              if (i.hasNext()) sb.append(",");
              // check that necessary root hierarchy records exist, else create them
              if (ha.getVisibility()==VisibilityConstants.SYSTEM && ha.getPformhaid()==null) {
                 hassystem=true;
              } else if (ha.getVisibility()==VisibilityConstants.SITE && ha.getPformhaid()==null) {
                 hassite=true;
              } else if (ha.getVisibility()==VisibilityConstants.STUDY && ha.getPformhaid()==null) {
                 hasstudy=true;
              } 
           }
           if (!hassystem) createHaRecord(VisibilityConstants.SYSTEM);
           if (!hassite) createHaRecord(VisibilityConstants.SITE);
           if (!hasstudy) createHaRecord(VisibilityConstants.STUDY);
     
           // Only include formtypes user has permission to
           StringBuilder ftsb=new StringBuilder();
           List ftypespermlist=getPermHelper().getFormtypesPermList();
     
           i=ftypespermlist.iterator();
           // pull system/site/study permissions 
           Permissions p=(Permissions)getMainDAO().execUniqueQuery(
              "select p from Permissions p join fetch p.permissioncats " +
                 "where p.permissioncats.pcatvalue=" + PcatConstants.FORMTYPE + " and " +
                       "p.permissioncats.pcatscope=" + PcatscopeConstants.RESOURCE + " and " + 
                       "p.permacr='VIEWTYPE'"
                 );

           while (i.hasNext()) {
              HashMap map=(HashMap)i.next();
              BitSet b=(BitSet)map.get("permlevel");
              if (b.get(p.getPermpos()-1)) { 
                 String idstring=(String)map.get("formtypelistid");
                 ftsb.append(idstring + ",");
              }
           }   
           int ftsbl=ftsb.length();
           if (ftsbl>0) {
              ftsb.deleteCharAt(ftsbl-1);
           }
     
           // Pull formtypes, datatabledef data
           if (sb.length()>0 && ftsb.length()>0) {
              fttemp=getMainDAO().execQuery(
                 "select ft from Formtypes ft join fetch ft.datatabledef join fetch ft.datatabledef.allinst join fetch ft.formtypelist " +
                       "join fetch ft.formformats join fetch ft.formtypelist join fetch ft.formtypelist.allinst " +
                    "where ft.datatabledef.pformhaid in (" 
                       + sb.toString() + ") and ft.formtypelist.formtypelistid in (" + ftsb.toString() + ") "
                           + " and ft.datatabledef.displayas in (" +
                               DisplayAsConstants.FORMONLY + "," + DisplayAsConstants.FORMANDDATATABLE 
                           + ") " +
                    "order by ft.datatabledef.formno,ft.datatabledef.formver desc"
              );
           } else {
              fttemp=(List)new ArrayList();
           }

           auth.setSessionAttribute("formhalist",fhatemp);
           auth.setSessionAttribute("formtypeslist",fttemp);
           auth.setSessionAttribute("iinfomap",iinfomap);

           iinfotemp=(HashMap)auth.getSessionAttribute("iinfomap");

        }


        // Create new list based on stored list
        List fha=(List)new ArrayList(fhatemp);

        // add forms and installation info to final form list
        Iterator fti=fttemp.iterator();
        List allformlist=(List)new ArrayList();
        auth.setValue("showsinstopt","N");
        boolean already=false;
        // Add forms to list
        while (fti.hasNext()) {
           Formtypes ft=(Formtypes)fti.next();
           HashMap ftimap=new HashMap();
           if (ft.getRemoteformtypeid()==null) {
              ftimap.put("linstid",null);
              ftimap.put("lstdid",null);
              ftimap.put("instacr","LOCAL");
           } else {
              if (!already) {
                 auth.setValue("showsinstopt","Y");
                 already=true;
              }
              ftimap.put("linstid",ft.getAinstid());
              ftimap.put("lstdid",new Long(getAuth().getValue("studyid")));
              ftimap.put("instacr",iinfotemp.get(new Long(ft.getAinstid()).toString()));
           }
           ftimap.put("form",ft);
           // check archive status and option and add to list
           String archoptval=auth.getOption("archopt");
           if (archoptval!=null && archoptval.equalsIgnoreCase("N")) { 
              if (!ft.getDatatabledef().getIsarchived()) {
                 allformlist.add(ftimap);
              }
           } else {
              allformlist.add(ftimap);
           }
        }

        List fpermlist=getPermHelper().getFormPermList();

        // Refine list based on options
        // 1) Drop formtypes depending on typeopt value && 
        // 2) create typelist for options menu
        // 3) Check permissions
        // 4) Compute recency (is most recent version)
        HashMap checkver=new HashMap();
        ArrayList typelist=new ArrayList();
        typelist.add("ALL");
        String typeopt=auth.getOption("typeopt");
        i=allformlist.iterator();
        while (i.hasNext()) {
           HashMap imap=(HashMap)i.next();
           Formtypes ftype=(Formtypes)imap.get("form");
           String instacr=(String)imap.get("instacr");
           String formtype=ftype.getFormtypelist().getFormtype();
           boolean isremoved=false;
           // Append formtype to typelist if not already there
           if (typelist.indexOf(formtype)<=0) {
              typelist.add(formtype);
           }
           // remove formtypes based on typeopt value
           if (!typeopt.equals("ALL")) {
              if (!ftype.getFormtypelist().getFormtype().equals(typeopt)) {
                 i.remove();
                 isremoved=true;
              }
           }
           // Check permissions - (based on form permission list list)
           if (!(isremoved || auth.getValue("rightslevel").equalsIgnoreCase("ADMIN"))) {
              Long dtdefid=new Long(ftype.getDatatabledef().getDtdefid());
              Iterator piter=fpermlist.iterator();
              boolean found=false;
              while (piter.hasNext()) {
                 HashMap map=(HashMap)piter.next();
                 Long compid;
                 try {
                    compid=new Long((String)map.get("dtdefid"));
                 } catch (ClassCastException cce) {   
                    compid=(Long)map.get("dtdefid");
                 }   
                 if (compid.equals(dtdefid)) {
                    found=true;
                    break;
                 }
              }
              if (!found) {
                 i.remove();
                 isremoved=true;
              }
           }
           // Compute recency (is most recent version) - drop if desired
           if (!isremoved && auth.getOption("veropt").equals("REC")) {
              String formno=ftype.getDatatabledef().getFormno();
              String formver=ftype.getDatatabledef().getFormver();
              if (!checkver.containsKey(formno)) {
                 checkver.put(formno,formver);
              } else if (!formver.equals(checkver.get(formno))) {
                 i.remove();
              }
           }
        }

        // Reset option to all if type is not contained in form list (can happen
        // when switching studies if studies don't share all form types)
        if (typelist.indexOf(typeopt)<0) {
           typeopt="ALL";
           auth.setOption("typeopt",typeopt);
        }

        ////////////////////////////////////////////////////////////////////
        // Organize data into heirarchical format for building HTML table //
        ////////////////////////////////////////////////////////////////////

        // ft holds number of rows in table.  Keep this info for later
        int tablerows=allformlist.size();

        // tlist holds table columns
        ArrayList tlist=new ArrayList();
        // alist holds items within columns
        ArrayList alist=new ArrayList();

        // pull first table column
        i=fha.iterator();
        while (i.hasNext()) {
           Formha ha=(Formha)i.next();
           if (ha.getPformhaid()==null) {
              HashMap map=new HashMap();
              map.put("isnull","N");
              map.put("obj",ha);
              map.put("nrows",0);
              map.put("ncols",1);
              // remove element from collection so we don't have to compare it again
              i.remove();
              alist.add(map);
           }
        }   
        tlist.add(alist);
        // pull subsequent columns by finding children of elements in previous columns
        for (int ii=1;ii>-1;ii++) {
           // pull elements in prior column
           ArrayList plist=(ArrayList)tlist.get(ii-1);
           alist=new ArrayList();
           Iterator i2=plist.iterator();
           boolean again=false;
           //iterate through prior column elements
           while (i2.hasNext()) {
              // find children
              HashMap pmap=(HashMap)i2.next();
              Formha pha=(Formha)pmap.get("obj");
              i=fha.iterator();
              boolean havechild=false;
              int prevsize=alist.size();
              while (i.hasNext()) {
                 Formha cha=(Formha)i.next();
                 // write child out to alist
                 if (cha.getPformhaid().equals(pha.getFormhaid())) {
                    HashMap cmap=new HashMap();
                    cmap.put("isnull","N");
                    cmap.put("obj",cha);
                    cmap.put("nrows",0);
                    cmap.put("ncols",1);
                    // remove element from collection so we don't have to compare it again
                    i.remove();
                    alist.add(cmap);
                    again=true;
                    havechild=true;
                 }
                 
              }
              // create null row to hold records belonging to item when child
              // categories are present
              if (havechild) {
                 HashMap nmap=new HashMap();
                 Formha nha=new Formha();
                 nha.setFormhaid(pha.getFormhaid());
                 nha.setPformhaid(pha.getPformhaid());
                 nha.setHadesc("&nbsp;");
                 nmap.put("isnull","Y");
                 nmap.put("obj",nha);
                 nmap.put("nrows",0);
                 nmap.put("ncols",1);
                 alist.add(prevsize,nmap);
              }
           }
           if (!again) break;
           tlist.add(alist);
        }
        // add forms to appropriate category (must iterate backwards through tlist
        // since forms are attached as the right most element)
        for (int ii=(tlist.size()-1);ii>=0;ii--) {
           // pull column arraylist
           ArrayList plist=(ArrayList)tlist.get(ii);
           Iterator i2=plist.iterator();
           while (i2.hasNext()) {
              HashMap pmap=(HashMap) i2.next();
              Formha pha=(Formha)pmap.get("obj");
              // formlist holds forms belonging to an element
              ArrayList formlist=new ArrayList();
              Iterator i3=allformlist.iterator();
              while (i3.hasNext()) {
                 HashMap i3map=(HashMap)i3.next();
                 Formtypes ctype=(Formtypes)i3map.get("form");
                 Long linstid=(Long)i3map.get("linstid");
                 Long lstdid=(Long)i3map.get("lstdid");
                 String instacr=(String)i3map.get("instacr");
                 // add form to formlist where there is a match
                 if (ctype.getDatatabledef().getPformhaid()==pha.getFormhaid()) {
                    HashMap flmap=new HashMap();
                    flmap.put("form",ctype);
                    flmap.put("linstid",linstid);
                    flmap.put("lstdid",lstdid);
                    flmap.put("instacr",instacr);
                    formlist.add(flmap);
                    // remove element from collection so we don't have to compare it again
                    i3.remove();
                 }
              }
              // add formlist to element's hashmap
              if (formlist.size()>0) {
                 pmap.put("formlist",formlist);
                 pmap.put("nrows",formlist.size());
                 pmap.put("ncols",tlist.size()-ii);
              }
              // add nrows to parent cells from prior columns
              Long compid=null;
              try {
                 compid=new Long(pha.getPformhaid());
              } catch (Exception cie) { }
              // loop through prior columns looking for parent elements
              for (int jj=(ii-1);jj>=0;jj--) {
                 ArrayList pplist=(ArrayList)tlist.get(jj);
                 Iterator i4=pplist.iterator();
                 while (i4.hasNext()) {
                    HashMap ppmap=(HashMap) i4.next();
                    Formha ppha=(Formha)ppmap.get("obj");
                    String isnull=(String)ppmap.get("isnull");
                    // if find a parent element, add rows to that element.  Then update
                    // compid to that element's parent for next loop
                    if ((
                           (!(isnull.equals("Y") || compid==null)) && ppha.getFormhaid()==compid.longValue()
                        ) ||
                        (
                           ppha.getFormhaid()==pha.getFormhaid() && ppha.getPformhaid()==pha.getPformhaid()
                       )) {
                       int nrows=((Integer)ppmap.get("nrows")).intValue();
                       nrows=nrows+formlist.size();
                       ppmap.put("nrows",nrows);
                       try {
                          compid=new Long(ppha.getPformhaid());
                       } catch (Exception cie2) {}
                    }
                 }
              }
           }
        }
        
        /////////////////////////////////
        // Create HTML Table of result //
        /////////////////////////////////

        int tablecols=tlist.size()+1;
        if (tablerows<1 || tablecols<1) {
           throw new FORMSException("Zero length table");
        }
        Table table=new Table(tablerows,tablecols);
        // iterate through table columns;
        for (int currcol=0;currcol<tlist.size();currcol++) {
           ArrayList rowlist=(ArrayList)tlist.get(currcol);
           int prevrow=0;
           Iterator rowiter=rowlist.iterator();
           while (rowiter.hasNext()) {
              HashMap map2=(HashMap)rowiter.next();
              Formha ha2=(Formha)map2.get("obj");
              int nrows=((Integer)map2.get("nrows")).intValue();
              int ncols=((Integer)map2.get("ncols")).intValue();
              if (nrows<=0 || ncols<=0) continue;
              Cell cell=new Cell(new Long(ha2.getFormhaid()).toString(),nrows,ncols);
              cell.setContent("type","formha");
              cell.setContent("obj",ha2);
              boolean okvar=false;
              int currrow=prevrow;
              // Use previous row & exceptions to calculate cell row placement.  This could probably
              // be made more efficient by running through a calculation loop, but the loss here 
              // is quite minimal
              int firstrow=0;
              while (!okvar) {
                 try {
                    table.setCell(cell,currrow,currcol);
                    firstrow=currrow;
                    prevrow=currrow+1;
                    okvar=true;
                 } catch (Exception te) { 
                    currrow++;
                    if (currrow>500000) throw new FORMSException("Too many rows returned");
                 }
              }   
              // see if contains any forms (final column)
              ArrayList formlist=(ArrayList)map2.get("formlist");
              if (formlist!=null) {
                 // Rows for items must be placed based on row location of containing element
                 // (first row number of containing element is held in "firstrow")
                 int prevrow2=firstrow;
                 Iterator formiter=formlist.iterator();
                 while (formiter.hasNext()) {
                    HashMap ftmap=(HashMap)formiter.next();
                    Formtypes formtype=(Formtypes)ftmap.get("form");
                    Long linstid=(Long)ftmap.get("linstid");
                    Long lstdid=(Long)ftmap.get("lstdid");
                    String instacr=(String)ftmap.get("instacr");
                    cell=new Cell(new Long(formtype.getFormtypeid()).toString(),1,1);
                    cell.setContent("type","formtype");
                    cell.setContent("obj",formtype);
                    cell.setContent("instacr",instacr);
                    if (linstid==null) {
                       cell.setContent("linstid",-99999999);
                       cell.setContent("lstdid",-99999999);
                       cell.setContent("islocal",true);

                    } else {
                       cell.setContent("linstid",linstid);
                       cell.setContent("lstdid",lstdid);
                       cell.setContent("islocal",false);
                    }
                    okvar=false;
                    currrow=prevrow2;
                    while (!okvar) {
                       try {
                          table.setCell(cell,currrow,tablecols-1);
                          prevrow2=currrow+1;
                          okvar=true;
                       } catch (Exception te) {
                          currrow++;
                          if (currrow>500000) throw new FORMSException("Too many rows returned");
                       }
                    }
                 }
              }
           }
        }
        // Create Options Menu Map
        HashMap optionsmap=new HashMap();
        optionsmap.put("typelist",typelist);
        optionsmap.put("typeopt",auth.getOption("typeopt"));
        optionsmap.put("veropt",auth.getOption("veropt"));
        optionsmap.put("archopt",auth.getOption("archopt"));
        optionsmap.put("fnumopt",auth.getOption("fnumopt"));
        optionsmap.put("sinstopt",auth.getOption("sinstopt"));
        auth.setSessionAttribute("optionsobject",optionsmap);

        // Create & return SelectFormResult Object
        SelectFormResult result=new SelectFormResult();

        if (typelist.size()>2) {
           result.setShowtype(true);
        } else {   
           result.setShowtype(false);
        }   
        result.setTable(table);

        return result;

      } catch (Exception e) {

        SelectFormResult result=new SelectFormResult();
        return result;

      }

   }


}

