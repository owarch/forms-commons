 
/**
 *
 * This program is part of:
 *
 *    FORMS [Forms-Oriented Research Management System]
 *
 * Copyright (C) 2009 OpenWare Architects.  All Rights Reserved.
 *
 * FORMS is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * FORMS is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with FORMS.  If not, see <http://www.gnu.org/licenses/>.
 *
 */
 
/* 
 *
 * AppResourceAccessor.java - Resource accessor for non-service-side resources.
 *
 * NOTE:  Objects extending ResourceAccessor should not be made available via
 *        SessionObjectUtil.  They have a high failure rate, probably due to
 *        their construction via transient request & response objects
 *
 * IMPORTANT:  11/12/2008  This IS being used as the superclass for FORMSServiceTarget
 *        objects, to enable them to save and retrieve using FORMSAuth.  This should
 *        definitely perform well for forms.services, however there could be some issues
 *        with SOAP.  I assume that was the reason for not using it before.  This
 *        should be checked and verified against the current SOAP processing.
 *
 */

package com.owarchitects.forms.commons.comp;

import com.owarchitects.forms.commons.db.*;

import java.util.*;
import java.io.*;
import javax.servlet.*;
import javax.servlet.http.*;
import java.security.*;

import org.springframework.context.*;
import org.springframework.context.support.*;
import org.springframework.orm.hibernate3.*;
import org.hibernate.*;
import org.hibernate.transform.*;
import org.apache.velocity.Template;
import org.apache.velocity.app.VelocityEngine;
import org.apache.commons.dbcp.BasicDataSource;
import com.mchange.v2.c3p0.ComboPooledDataSource;
import org.springframework.transaction.support.TransactionSynchronizationManager;


public abstract class AppResourceAccessor extends BasicResourceAccessor implements AppResource {

   protected String syswork;
   protected String sysconn;
   protected FORMSAuth thisauth;

   public AppResourceAccessor() {
   }

   public AppResourceAccessor(HttpServletRequest request,HttpServletResponse response) {
      super(request,response);
   }

   public final org.apache.velocity.Template getVelocityTemplate(String inString) throws FORMSException {

      try {
         VelocityEngine velocity=SessionObjectUtil.getVelocityEngine(session,this.getContext(),this.getAppPath());
         return velocity.getTemplate(inString);

      }  catch (Exception e) {

         throw new FORMSException("FORMS Service-side Velocity exception");

      }

   } 

   public final FORMSAuth getAuth(boolean setCallingClass) throws FORMSException,FORMSKeyException,FORMSSecurityException {

      HttpServletRequest request=this.getRequest();
      HttpServletResponse response=this.getResponse();
      if (thisauth==null) {
         thisauth=SessionObjectUtil.getAuthObj(request,response,syswork,sysconn);
      }   
      if (setCallingClass) {
         thisauth.setValue("callingjclass",request.getServletPath().substring(1));
      }   
      if (syswork==null) {
         this.syswork=thisauth.getSyswork();
      }
      return thisauth;

   }

   public final FORMSAuth getAuth() throws FORMSException,FORMSKeyException,FORMSSecurityException {
      return this.getAuth(true);
   }

   // Log system event
   public final void logEvent(String detail) {

      try {

         Systemlog sl=new Systemlog();
         sl.setIpaddr(this.getRequest().getRemoteAddr());
         sl.setUserdesc(this.getAuth().getValue("userdesc"));
         sl.setDetail(detail);
         sl.setServletpath(this.getRequest().getServletPath());
         sl.setTimestamp(new Date());
         getMainDAO().saveOrUpdate(sl);

      } catch (Exception loge) {
         // Do nothing
      }

   }

   public final void initDataSource(boolean force) {
      if (force || mds==null) {
         try {
            String u=getSysconn().getUsername();
            String p=getSysconn().getPassword();
            EmbeddedusersDAO embeddedusersDAO=(EmbeddedusersDAO)this.getContext().getBean("embeddedusersDAO");
            Embeddedusers euser=embeddedusersDAO.getRecord(u);
            PrivateKey privkey=EncryptUtil.getPrivateKey(session,u,p);
            String dbpw=EncryptUtil.decryptString(euser.getDbpassword(),privkey);
            // Set main data source password 
            mds=(ComboPooledDataSource)this.getContext().getBean("mainDataSource");
            // replaceFirst & replaceAll have problems with backslashes in replacement text.
            // here we do a replaceFirst with intermediate text then use replace to 
            // replace that text in the password.  replace uses "CharSequence" for the
            // replacement and handles backslashes and other characters correctly
            mds.setJdbcUrl(mds.getJdbcUrl().replaceFirst("bootPassword=.*","XXXXYYYYXXXX").replace("XXXXYYYYXXXX","bootPassword=" + new String(dbpw)));
            mds.setPassword(new String(dbpw));
            mds.setOverrideDefaultPassword(new String(dbpw));
         } catch (Exception e) {
            // OK, no initialization first pass through login
         }   
      }
   }

   public final void initDataSource() {
      initDataSource(false);
   }

   public final File getSyswork() {
      return new File(syswork);
   }

   public final void setSysconnString(String sysconn) throws Exception {
      this.sysconn=sysconn;
   }

   public final String getSysconnString() throws Exception {
      return sysconn;
   }

   public final Sysconn getSysconn() throws Exception {
      return (Sysconn) ObjectXmlUtil.objectFromXmlString(
                EncryptUtil.decryptString(sysconn,SessionObjectUtil.getInternalKeyObj(session))
             );
   }

}

