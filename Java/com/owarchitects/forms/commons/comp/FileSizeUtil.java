 
/**
 *
 * This program is part of:
 *
 *    FORMS [Forms-Oriented Research Management System]
 *
 * Copyright (C) 2009 OpenWare Architects.  All Rights Reserved.
 *
 * FORMS is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * FORMS is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with FORMS.  If not, see <http://www.gnu.org/licenses/>.
 *
 */
 
package com.owarchitects.forms.commons.comp;

/*
 * VisibilityConstants.java 
 */

import java.lang.reflect.*;

public class FileSizeUtil extends FORMSConstants {

    public static final long KILOBYTE = 1024;
    public static final long MEGABYTE = 1024 * 1024;
    public static final long GIGABYTE = 1024 * 1024 * 1024;

    public static String displayFileSize(long bytes) {

       bytes=bytes*10;
       if ((bytes/GIGABYTE)>10) {
          return new Long(Math.round(bytes/GIGABYTE)).doubleValue()/10 + " GB";
       } else if ((bytes/MEGABYTE)>10) {
          return new Long(Math.round(bytes/MEGABYTE)).doubleValue()/10 + " MB";
       } else if ((bytes/KILOBYTE)>10) {
          return new Long(Math.round(bytes/KILOBYTE)).doubleValue()/10 + " KB";
       } else {
          return bytes +  " bytes";
       }
       
    }

}

