 
/**
 *
 * This program is part of:
 *
 *    FORMS [Forms-Oriented Research Management System]
 *
 * Copyright (C) 2009 OpenWare Architects.  All Rights Reserved.
 *
 * FORMS is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * FORMS is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with FORMS.  If not, see <http://www.gnu.org/licenses/>.
 *
 */
 
/* 
 *
 * ResourceAccessor.java -  When extended, gives subclasses access to resources
 *   available to controllers.  Subclasses should call super(request,response).
 *
 * NOTE:  Objects extending ResourceAccessor should not be made available via
 *        SessionObjectUtil.  They have a high failure rate, probably due to
 *        their construction via transient request & response objects
 *
 */

package com.owarchitects.forms.commons.comp;

import com.owarchitects.forms.commons.db.*;

import java.util.*;
import java.io.*;
import javax.servlet.*;
import javax.servlet.http.*;
import java.security.*;

import org.springframework.context.*;
import org.springframework.context.support.*;
import org.springframework.orm.hibernate3.*;
import org.hibernate.*;
import org.hibernate.transform.*;
import org.apache.velocity.Template;
import org.apache.velocity.app.VelocityEngine;
import org.apache.commons.dbcp.BasicDataSource;
import com.mchange.v2.c3p0.ComboPooledDataSource;
import org.springframework.transaction.support.TransactionSynchronizationManager;


public abstract class BasicResourceAccessor implements BasicResource {

   protected PrintWriter out;
   protected HttpSession session;
   protected HttpServletRequest request;
   protected HttpServletResponse response;
   protected SessionFactory sessionFactory;
   protected Session hibernateSession;
   protected ComboPooledDataSource mds=null;

   // Empty onstructor used by ServiceTargets.  They initialize request,response & session 
   // in FORMSServiceTarget.
   public BasicResourceAccessor() {
   }

   // Constructor used by most Accessor classes to initialize request, response & session
   public BasicResourceAccessor(HttpServletRequest request,HttpServletResponse response) {
      this.request=request;
      this.response=response;
      this.session=request.getSession();
      try {
         this.out=response.getWriter();
      } catch (java.io.IOException ioe) {
         this.out=null;
      }
   }

   // field getter methods

   public HttpServletRequest getRequest() {
      return request;
   }

   public HttpServletResponse getResponse() {
      return response;
   }

   public HttpSession getSession() {
      return session;
   }

   public PrintWriter getWriter() {
      return out;
   }

   /*
    * NOTE HERE:  We are maintaining and using a separate application context
    * here from the root application context for the application that would
    * be accessed from the getApplicationContext() method.  For some reason
    * when using that context, hibernate write operations threw exceptions.
    * When fixes were implemented to try eliminate the error (setting
    * HibernateSession flushMode to >= COMMIT), the exceptions went away,
    * but the write operations simply did not occur.  The hack here is 
    * simply to create another ApplicationContext maintained in the 
    * httpsession for use with hibernate.
    * MRH 10/03/2007
    */
    
   public final AbstractApplicationContext getContext() {
      try {
         return SessionObjectUtil.getApplicationContext(this.getSession());
      } catch (Exception e) {
         return null;
      }
   }

   // Return cookie specified by input string.  Returns empty string if no such cookie.
   public final String getCookieValue(String cstr) {
      try {
         Cookie[] carray=this.getRequest().getCookies();
         Iterator i=Arrays.asList(carray).iterator();
         while (i.hasNext()) {
            Cookie c=(Cookie)i.next();
            if (c.getName().replaceAll("=","").equals(cstr.replaceAll("=",""))) {
               return c.getValue().replaceAll("=","");
            }
         }
      } catch (Exception ce) {}   
      return "";
   }

   public Session getHibernateSession(String sf) {
      sessionFactory = (SessionFactory) this.getContext().getBean(sf); 
      hibernateSession = SessionFactoryUtils.getSession(sessionFactory, true); 
      return hibernateSession;
   }

   public Session getHibernateSession() {
      return getHibernateSession("sessionFactory"); 
   }

   public void bindHibernateSession(String sf) {
      sessionFactory = (SessionFactory) this.getContext().getBean(sf); 
      hibernateSession = SessionFactoryUtils.getSession(sessionFactory, true); 
      TransactionSynchronizationManager.bindResource(sessionFactory, new SessionHolder(hibernateSession));
   }

   public void bindHibernateSession() {
      bindHibernateSession("sessionFactory"); 
   }
   
   public void releaseHibernateSession() {
      TransactionSynchronizationManager.unbindResource(sessionFactory);
      SessionFactoryUtils.releaseSession(hibernateSession,sessionFactory);
   }

   public final String getAppPath() {
      return getSession().getServletContext().getRealPath("/");
   } 

   public MainDAO getMainDAO() {
      if (getSession().getAttribute("maindao")==null) {
         getSession().setAttribute("maindao",this.getContext().getBean("mainDAO"));
      } 
      return (MainDAO)getSession().getAttribute("maindao");
   }   
   
   public EmbeddedDAO getEmbeddedDAO() {
      if (getSession().getAttribute("embeddeddao")==null) {
         getSession().setAttribute("embeddeddao",this.getContext().getBean("embeddedDAO"));
      } 
      return (EmbeddedDAO)getSession().getAttribute("embeddeddao");
   }   
   
   public abstract void initDataSource();

}



