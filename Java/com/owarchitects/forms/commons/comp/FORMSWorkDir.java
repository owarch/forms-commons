 
/**
 *
 * This program is part of:
 *
 *    FORMS [Forms-Oriented Research Management System]
 *
 * Copyright (C) 2009 OpenWare Architects.  All Rights Reserved.
 *
 * FORMS is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * FORMS is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with FORMS.  If not, see <http://www.gnu.org/licenses/>.
 *
 */
 
/**
* FORMSWorkDir.java -- create FORMS working directory/eliminate old directories
* Original Creation -- 09/2004 (MRH)
*/

// NOTE:  Currently using public rather than protected methods to allow JWS access to
// class methods.  May want to look into setting up JWS files as part of the package. 
// Axis at one time didn't allow JWS files to be a package member and I'm not sure how
// well this is currently working.

package com.owarchitects.forms.commons.comp;

import java.io.*;
import java.util.*;
//import com.adobe.fdf.*;
//import com.adobe.fdf.exceptions.*;
//import com.sas.sasserver.sclfuncs.*;
//import com.sas.rmi.*;
//import com.sas.sasserver.submit.*;
//import javax.servlet.*;
//import javax.servlet.http.*;
//import com.oreilly.servlet.*;
import org.apache.commons.io.FileUtils.*;


public class FORMSWorkDir {

   private String workdir = null;
   long _ms;

   // CONSTRUCTOR

   public FORMSWorkDir(String sasdir,int days) {
      // TRIM OFF TRAILING FILE.SEPARATOR
      int endchar=sasdir.lastIndexOf(File.separator);
      if (endchar>0) {
         sasdir=sasdir.substring(0,endchar);
      }
      _ms = System.currentTimeMillis();
      // CONSTRUCT PATH TO FILENAME 
      workdir = sasdir.trim() + File.separator + "_FORMSWork" + _ms;
      File newdir = new File(workdir);
      // CREATE DIRECTORY
      if (!(newdir.mkdir())) {
         // Couldn't create directory
         workdir="ERROR";
      }

      // DELETE OLD SUBDIRECTORIES (Greater than N days old) -- DONE SILENTLY
      // be careful here.  if you do not use the ndays (long) field in the multiplication
      // expression, the default type will be used and may return the wrong value to
      // lengthTime.  The resulting integer may be too large for the default.
      // This process is run as a thread so it doesn't slow down servlet loading.
      final String sasdir2=sasdir;
      final long ndays=new Integer(days).longValue();
      try {
            Thread t1=new Thread(new Runnable() {
               public void run () {
                  try {
                       final long lengthTime = 1000*60*60*24*ndays;
                       File sasworkroot = new File(sasdir2);
                       File[] files = sasworkroot.listFiles(new FileFilter() {
                           public boolean accept(File pathname) {
                               return (pathname.getName().toLowerCase().startsWith("_formswork") &&
                                       ((_ms-pathname.lastModified())>lengthTime));
                           }
                       });
                       for (int i = 0; i < files.length; i++) {
                         try {
                           // commons utility does recursive delete
                           org.apache.commons.io.FileUtils.deleteDirectory(files[i]);
                         } catch(Exception ex) { }
                       }
                  } catch (Exception e) { }
               }
            });
            t1.start();
          } catch (Exception e) { }

   }

   // RETURN workdir VALUE

   public String getWorkdir () {
      return workdir;
   }

}

   
