 
/**
 *
 * This program is part of:
 *
 *    FORMS [Forms-Oriented Research Management System]
 *
 * Copyright (C) 2009 OpenWare Architects.  All Rights Reserved.
 *
 * FORMS is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * FORMS is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with FORMS.  If not, see <http://www.gnu.org/licenses/>.
 *
 */
 
package com.owarchitects.forms.commons.comp;

// This provides empty callback methods when we don't need to know operation status
// can be extended to provide additional functionality when knowing status is important

import org.apache.axis2.client.async.AxisCallback;
import org.apache.axis2.context.MessageContext;

public class FORMSBasicCallback implements AxisCallback {

   public void onComplete() { }

   public void onError(java.lang.Exception e) { }

   public void onFault(MessageContext mc) { }

   public void onMessage(MessageContext mc) { }
   
}

