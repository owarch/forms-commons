 
/**
 *
 * This program is part of:
 *
 *    FORMS [Forms-Oriented Research Management System]
 *
 * Copyright (C) 2009 OpenWare Architects.  All Rights Reserved.
 *
 * FORMS is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * FORMS is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with FORMS.  If not, see <http://www.gnu.org/licenses/>.
 *
 */
 
package com.owarchitects.forms.commons.comp;

/*
 * PcatConstants.java 
 */

import java.lang.reflect.*;

public class PcatConstants extends FORMSConstants {

   public static final int SYSTEM = 1;
   public static final int SITE = 2;
   public static final int STUDY = 3;
   public static final int FORM_DATATABLE = 4;
   public static final int FORMTYPE = 5;
   public static final int REPORT = 6;
   public static final int LETTER = 7;
   public static final int MEDIA = 8;
   public static final int ANALYST_SYSTEM = 11;
   public static final int ANALYST_RESOURCE = 12;
   // Wrap-up value used by permsmgtcontroller
   public static final int ALL = 99;

}

