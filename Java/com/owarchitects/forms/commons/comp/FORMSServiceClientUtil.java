 
/**
 *
 * This program is part of:
 *
 *    FORMS [Forms-Oriented Research Management System]
 *
 * Copyright (C) 2009 OpenWare Architects.  All Rights Reserved.
 *
 * FORMS is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * FORMS is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with FORMS.  If not, see <http://www.gnu.org/licenses/>.
 *
 */
 
/* 
 *
 * FORMSServiceClientController.java -  Extends FORMSController to include both SOAP and FORMSService helpers
 *
 */

package com.owarchitects.forms.commons.comp;

import com.owarchitects.forms.commons.db.*;

import java.util.*;
import java.io.*;
import java.beans.*;
import java.lang.reflect.*;

import org.apache.axis2.context.*;
import org.apache.axis2.client.*;
import org.apache.axis2.client.async.AxisCallback;
import org.apache.axis2.transport.http.HTTPConstants;
import org.apache.axis2.AxisFault;
import org.apache.axis2.addressing.EndpointReference;
import org.apache.axis2.rpc.client.RPCServiceClient;
import org.apache.axis2.client.ServiceClient;
import org.apache.velocity.Template;
import org.apache.velocity.app.VelocityEngine;
import javax.xml.namespace.QName;

import javax.net.ssl.*;
import javax.servlet.http.*;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.axiom.om.*;
import org.apache.axiom.om.impl.llom.*;

import org.apache.commons.httpclient.HttpClient;
import org.apache.commons.httpclient.HttpStatus;
import org.apache.commons.httpclient.methods.InputStreamRequestEntity;
import org.apache.commons.httpclient.methods.PostMethod;

import org.springframework.context.support.AbstractApplicationContext;


public class FORMSServiceClientUtil {

   // Instances should not be created
   public FORMSServiceClientUtil() {
      super();
   }

   //////////////////////////
   //////////////////////////
   //////////////////////////
   // FORMSService Methods //
   //////////////////////////
   //////////////////////////
   //////////////////////////


   // Submit Service Request
   public static FORMSServiceResult submitServiceRequest(HttpSession session,FORMSServiceParms parms,String endpoint) throws FORMSException {

      try {

         // Encode parameters object
         String parmstr=ObjectUtil.objectToEncodedString(parms);

         HttpClient client=SessionObjectUtil.getHttpClient(session);
         PostMethod post=new PostMethod(endpoint);
         post.addParameter("parm",parmstr);
         client.executeMethod(post);
         if (post.getStatusCode()==HttpStatus.SC_OK) {
            Object rtnobj=(Object)ObjectUtil.objectFromEncodedString(post.getResponseBodyAsString());
            post.releaseConnection();
            return (FORMSServiceResult)rtnobj;
         } else {
            // service failure, return null 
            post.releaseConnection();
            return null;
         }

      } catch (java.io.IOException ioe) {

         throw new FORMSException("FORMSService object retrieval exception - " + Stack2string.getString(ioe));

      }
      
   }

   /////////////////////////
   /////////////////////////
   /////////////////////////
   // SOAP Client Methods //
   /////////////////////////
   /////////////////////////
   /////////////////////////

   // Submit SOAP Request
   public static FORMSServiceResult submitSoapRequest(HttpSession session,FORMSServiceParms parms,String endpoint) throws FORMSException {

      // Encode parameters object
      String s=ObjectUtil.objectToEncodedString(parms);

      // Establish SOAP connection 
      RPCServiceClient sclient=null;
      Options opt=null;
      try {
         sclient = SessionObjectUtil.getAxis2Client(session);
         sclient.engageModule(new QName("addressing").toString());
         opt=sclient.getOptions();
         opt.setManageSession(true);
         EndpointReference epr=new EndpointReference( 
            endpoint 
         );
         opt.setTo(epr);
      } catch (Exception e) {
         throw new FORMSException("FORMSController SOAP Endpoint exception - <BR>",e);
      }
      
      Object rtn[];
      try {
         // Submit request
         rtn=invokeBlocking(sclient, opt, "invokeMethod", 
            new Object [] { s } , new Class[] { String.class } );

      } catch (Exception inve) {

         throw new FORMSException(inve,"submitSoapRequest exception");
      }
      
      // Return result object
      Object rtnobj=null;
      try {

         rtnobj=ObjectUtil.objectFromEncodedString((String)rtn[0]);

      } catch (Exception objex) {

         rtnobj=rtn[0];
      }
      return (FORMSServiceResult)rtnobj;

   }

   // Submit SOAP Request
   public static void submitNonBlockingSoapRequest(HttpSession session,FORMSServiceParms parms,String endpoint,AxisCallback cb) throws FORMSException {

      // Encode parameters object
      String s=ObjectUtil.objectToEncodedString(parms);

      // Establish SOAP connection 
      RPCServiceClient sclient=null;
      Options opt=null;
      try {
         sclient = SessionObjectUtil.getAxis2Client(session);
         sclient.engageModule(new QName("addressing").toString());
         opt=sclient.getOptions();
         opt.setManageSession(true);
         EndpointReference epr=new EndpointReference( 
            endpoint 
         );
         opt.setTo(epr);
      } catch (Exception e) {
         throw new FORMSException("FORMSController SOAP Endpoint exception - <BR>",e);
      }

      try {
         // Submit request
         invokeNonBlocking(sclient, opt, "invokeMethod", new Object [] { s } , cb );

      } catch (Exception inve) {
         throw new FORMSException(inve,"submitSoapRequest exception");
      }
      
   }

   // execute invokeBlocking method call against serviceclient
   public static final Object[] invokeBlocking(RPCServiceClient sclient,Options opt,String method,Object[] inArray,Class[] outArray) throws FORMSException {
      try {
         opt.setAction("urn:invokeMethod");
         return sclient.invokeBlocking(new QName("http://services.ssa.forms.owarchitects.com","invokeMethod"),inArray,outArray);
      } catch (Exception e) {
         throw new FORMSException("FORMSController SOAP Invocation exception - <BR>",e);
      }

   }

   // execute invokeNonBlocking method call against serviceclient
   public static final void invokeNonBlocking(RPCServiceClient sclient,Options opt,String method,Object[] inArray,AxisCallback cb) throws FORMSException {
      try {
         opt.setAction("urn:invokeMethod");
         sclient.invokeNonBlocking(new QName("http://services.ssa.forms.owarchitects.com","invokeMethod"),inArray,cb);
      } catch (Exception e) {
         throw new FORMSException("FORMSController SOAP Invocation exception - <BR>",e);
      }

   }

   ////////////////////
   ////////////////////
   ////////////////////
   // Shared Methods //
   ////////////////////
   ////////////////////
   ////////////////////


   public static FORMSServiceResult getErrorObject(AbstractApplicationContext context,FORMSServiceParms parms,int i) 
          throws NoSuchMethodException,IllegalAccessException,InvocationTargetException,java.lang.InstantiationException {

      Class ic=parms.getClass();

      Method im;
      im=ic.getMethod("getMethodname");
      String methodname=(String)im.invoke(parms,new Object[]{ });
      im=ic.getMethod("getBeanname");
      String beanname=(String)im.invoke(parms,new Object[]{ });
      Object beanobj=context.getBean(beanname);
      Method cm=null;
      try {
         // Try calling method passing parameters object
         cm=beanobj.getClass().getMethod(methodname,new Class[] { ic });
      } catch (NoSuchMethodException nsme) {
         // Try calling method with no passed parameters
         cm=beanobj.getClass().getMethod(methodname,new Class[] { });
      }   
      Class rc=cm.getReturnType();
      Object o=rc.newInstance();
      Method rm=o.getClass().getMethod("setStatus",new Class[] { Integer.class });
      FORMSServiceResult outobj=(FORMSServiceResult)rm.invoke(o,new Object[] { new Integer(i) } );
      return outobj;

   }

}




