 
/**
 *
 * This program is part of:
 *
 *    FORMS [Forms-Oriented Research Management System]
 *
 * Copyright (C) 2009 OpenWare Architects.  All Rights Reserved.
 *
 * FORMS is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * FORMS is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with FORMS.  If not, see <http://www.gnu.org/licenses/>.
 *
 */
 
/*
 *
 * StudyMainController.java - Main Study Menu Interface
 *
 */

package com.owarchitects.forms.commons.comp;

import com.owarchitects.forms.commons.db.*;
import java.util.*;
import java.io.*;
import javax.servlet.http.*;
import org.hibernate.*;
import org.springframework.web.servlet.*;
import org.springframework.web.servlet.mvc.*;
import org.springframework.web.context.support.*;
import org.springframework.orm.hibernate3.*;
import org.apache.commons.io.FileUtils;


public class StudyMainController extends FORMSController {

   public ModelAndView submitRequest() throws FORMSException {

      try {

         HttpServletResponse response=this.getResponse();
         PrintWriter out=this.getWriter();
         this.getWinProps().setWinNameMissOk("N");

         // Invoke FORMSService
         // site,study parameters may get passed in from input parameters or retrieved from 
         // auth on web service side
         StudyMainParms parms=new StudyMainParms();
         parms.setSite(request.getParameter("site"));
         parms.setSiteid(request.getParameter("siteid"));
         parms.setStudy(request.getParameter("study"));
         parms.setStudydesc(request.getParameter("studydesc"));
         parms.setStudyid(request.getParameter("studyid"));
         StudyMainResult result=this.processData(parms);
         this.getSession().setAttribute("iframeresult",result);
         String status=result.getStatus();

         if (status.equalsIgnoreCase("OK")) {

            if (getAuth().getValue("apptype").equals("CSA")) {
               mav.addObject("worklocally",getAuth().getValue("worklocally"));
            }
            result.addAllToMAV(mav);
            return mav;

         } else if (status.equalsIgnoreCase("RELOAD")) {

            safeRedirect(request.getRequestURL().toString());
            return null;


         } else if (status.equalsIgnoreCase("REDIRECT")) {

            safeRedirect("selectform.do");
            return null;

         } else {

            return null;

         }
   
      } catch (Exception e) { 
         throw new FORMSException("FORMS Controller exception:  " + Stack2string.getString(e));
      }

   }

   private StudyMainResult processData(StudyMainParms parms) throws com.owarchitects.forms.commons.comp.FORMSException {

      try {

        FORMSAuth auth=this.getAuth(false);

        StudyMainResult result=new StudyMainResult();

        // retrieve site,study,studydescription either from controller or auth object, write to view
        String site=parms.getSite();
        String siteid=parms.getSiteid();
        String study=parms.getStudy();
        String studydesc=parms.getStudydesc();
        String studyid=parms.getStudyid();
        if (site==null) {
           site=auth.getValue("site");
           siteid=auth.getValue("siteid");
           study=auth.getValue("study");
           studydesc=auth.getValue("studydesc");
           studyid=auth.getValue("studyid");
           result.setStatus("OK");
           if (auth.getValue("rightslevel").equalsIgnoreCase("EXTERNAL")) { 
              result.setStatus("REDIRECT");
           }
        } else {
           auth.setValue("site",site);
           auth.setValue("siteid",siteid);
           auth.setValue("study",study);
           auth.setValue("studydesc",studydesc);
           auth.setValue("studyid",studyid);
           // Empty session-stored forms lists if hitting this after select study program
           auth.setSessionAttribute("datatablehalist",null);
           auth.setSessionAttribute("formtypeslist",null);
           auth.setSessionAttribute("mediahalist",null);
           auth.setSessionAttribute("mediainfolist",null);
           // retrieve and set rightslevel
           // Force reload to eliminate (expired) message
           result.setStatus("RELOAD");
           if (auth.getValue("rightslevel").equalsIgnoreCase("EXTERNAL")) { 
              result.setStatus("REDIRECT");
           }
        }   
        result.setSite(site);
        result.setStudy(study);
        result.setStudydesc(studydesc);
        result.setStudyid(studyid);
        return result;

      } catch (Exception e) {

        throw new FORMSException("FORMS Service exception:  " + Stack2string.getString(e));

      }

    }

}


