 
/**
 *
 * This program is part of:
 *
 *    FORMS [Forms-Oriented Research Management System]
 *
 * Copyright (C) 2009 OpenWare Architects.  All Rights Reserved.
 *
 * FORMS is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * FORMS is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with FORMS.  If not, see <http://www.gnu.org/licenses/>.
 *
 */
 
/* 
 *
 * FORMSCsaServiceClientController.java -  Extends FORMSController to include Service helpers
 *
 */

package com.owarchitects.forms.commons.comp;

import com.owarchitects.forms.commons.db.*;

import java.util.*;
import java.io.*;
import java.beans.*;
import java.lang.reflect.*;

//import org.apache.axis2.context.*;
//import org.apache.axis2.client.*;
//import org.apache.axis2.client.async.AxisCallback;
//import org.apache.axis2.transport.http.HTTPConstants;
//import org.apache.axis2.AxisFault;
//import org.apache.axis2.addressing.EndpointReference;
//import org.apache.axis2.rpc.client.RPCServiceClient;
//import org.apache.velocity.Template;
import org.apache.velocity.app.VelocityEngine;
import javax.xml.namespace.QName;

import javax.net.ssl.*;
import javax.servlet.http.*;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.context.support.AbstractApplicationContext;

public abstract class CsaServiceClientUtil extends FORMSServiceClientUtil {

   // Instances should not be created
   public CsaServiceClientUtil() {
      super();
   }

   private static long csaid;

   //////////////////////////
   //////////////////////////
   //////////////////////////
   //////////////////////////
   // FORMSService Methods //
   //////////////////////////
   //////////////////////////
   //////////////////////////
   //////////////////////////

   // Submit Service Request (Override method)
   public static FORMSServiceResult submitServiceRequest(HttpSession session,AbstractApplicationContext context,FORMSAuth auth,File syswork,FORMSServiceParms parms) throws FORMSException {

      return submitServiceRequest(session,context,auth,syswork,parms,getSsaUrl(session,context,auth) + "/formsserviceinterceptor.do");

   }   

   public static FORMSServiceResult submitServiceRequest(HttpSession session,AbstractApplicationContext context,FORMSAuth auth,
         File syswork,FORMSServiceParms parms,String epref) throws FORMSException {

      Method im;
      Object oj;

      try {

         // Write username, password, csaid values.  Use supplied user info if available.
         im=parms.getClass().getMethod("getUsername",new Class[] { });
         String username=(String)im.invoke(parms,new Object[] { });
         if (username==null || username.length()<1) {
            im=parms.getClass().getMethod("setUsername",new Class[] { String.class });
            oj=im.invoke(parms,new Object[] { auth.getValue("username") });
            im=parms.getClass().getMethod("setPassword",new Class[] { String.class });
            oj=im.invoke(parms,new Object[] { EncryptUtil.decryptString(auth.getValue("password"),
               SessionObjectUtil.getInternalKeyObj(session)) });
         }   
         im=parms.getClass().getMethod("setCsaid",new Class[] { Long.TYPE });
         oj=im.invoke(parms,new Object[] { new Long(csaid) });
      } catch (Exception oke1) { 
         FORMSLog.append(syswork,Stack2string.getString(oke1));
         try {
            return getErrorObject(context,parms,FORMSServiceConstants.SERVICECLIENT_EXCEPTION);
         } catch (Exception oke1b) {
            FORMSLog.append(syswork,Stack2string.getString(oke1b));
            throw new FORMSException(oke1b,"submitServiceRequest exception");
         }
      }

      // invoke superclass method
      return submitServiceRequest(session,parms,epref);

   }

   // Submit Service Request (Override method, password supplied)
   public static FORMSServiceResult submitServiceRequest(HttpSession session,AbstractApplicationContext context,File syswork,
         FORMSServiceParms parms,String uname,String passwd,String epref) throws FORMSException {

      Method im;
      Object oj;

      try {

         // Write username, password, csaid values.  Use supplied user info if available.
         im=parms.getClass().getMethod("getUsername",new Class[] { });
         String username=(String)im.invoke(parms,new Object[] { });
         if (username==null || username.length()<1) {
            im=parms.getClass().getMethod("setUsername",new Class[] { String.class });
            oj=im.invoke(parms,new Object[] { uname });
            im=parms.getClass().getMethod("setPassword",new Class[] { String.class });
            oj=im.invoke(parms,new Object[] { passwd });
         }   
         im=parms.getClass().getMethod("setCsaid",new Class[] { Long.TYPE });
         oj=im.invoke(parms,new Object[] { new Long(csaid) });
      } catch (Exception oke1) { 
         FORMSLog.append(syswork,Stack2string.getString(oke1));
         try {
            return getErrorObject(context,parms,FORMSServiceConstants.SERVICECLIENT_EXCEPTION);
         } catch (Exception oke1b) {
            FORMSLog.append(syswork,Stack2string.getString(oke1b));
            throw new FORMSException(oke1b,"submitServiceRequest exception");
         }
      }

      // invoke superclass method
      return submitServiceRequest(session,parms,epref);

   }

   //////////////////
   //////////////////
   //////////////////
   //////////////////
   // SOAP Methods //
   //////////////////
   //////////////////
   //////////////////
   //////////////////

   // Submit SOAP Request (Override method)
   public static FORMSServiceResult submitSoapRequest(HttpSession session,AbstractApplicationContext context,FORMSAuth auth,File syswork,FORMSServiceParms parms) throws FORMSException {

      return submitSoapRequest(session,context,auth,syswork,parms,getSsaUrl(session,context,auth) + "/services/FORMSServiceInterceptor");

   }   

   public static  FORMSServiceResult submitSoapRequest(HttpSession session,AbstractApplicationContext context,FORMSAuth auth,
         File syswork,FORMSServiceParms parms,String epref) throws FORMSException {
   
      Method im;
      Object oj;

      try {

         // Write username, password, csaid values.  Use supplied user info if available.
         im=parms.getClass().getMethod("getUsername",new Class[] { });
         String username=(String)im.invoke(parms,new Object[] { });
         if (username==null || username.length()<1) {
            im=parms.getClass().getMethod("setUsername",new Class[] { String.class });
            oj=im.invoke(parms,new Object[] { auth.getValue("username") });
            im=parms.getClass().getMethod("setPassword",new Class[] { String.class });
            oj=im.invoke(parms,new Object[] { EncryptUtil.decryptString(auth.getValue("password"),
               SessionObjectUtil.getInternalKeyObj(session)) });
         }   
         im=parms.getClass().getMethod("setCsaid",new Class[] { Long.TYPE });
         oj=im.invoke(parms,new Object[] { new Long(csaid) });
      } catch (Exception oke1) { 
         FORMSLog.append(syswork,Stack2string.getString(oke1));
         try {
            return getErrorObject(context,parms,FORMSServiceConstants.SERVICECLIENT_EXCEPTION);
         } catch (Exception oke1b) {
            FORMSLog.append(syswork,Stack2string.getString(oke1b));
            throw new FORMSException(oke1b,"submitSoapRequest exception");
         }
      }

      // invoke superclass method
      return submitSoapRequest(session,parms,epref);

   }

   ////////////////////
   ////////////////////
   ////////////////////
   ////////////////////
   // Shared Methods //
   ////////////////////
   ////////////////////
   ////////////////////
   ////////////////////

   public static String getSsaUrl(HttpSession session,AbstractApplicationContext context,FORMSAuth auth) throws FORMSException {
      try {
         String ssaurl="";
         try {
            ssaurl=auth.getValue("ssaurl");
            csaid=new Long(auth.getValue("csaid")).longValue();
         } catch (Exception ce) {
            ssaurl=(String)session.getAttribute("ssaurl");
            if (ssaurl!=null && ssaurl.length()>0) {
               csaid=new Long((String)session.getAttribute("csaid")).longValue();
               try {
                  // try writing to auth
                  auth.setValue("ssaurl",ssaurl);
                  auth.setValue("csaid",new Long(csaid).toString());
               } catch (Exception dne) {
                  // Do nothing for now
               }
            } else {
               csaid=-1;
            }
         }
         try {
            if (ssaurl.length()>0 && csaid>0) {
               return ssaurl;
            } 
         } catch (Exception ee) {
            // Do nothing, continue
         }
         // get SSA url
         SsainfoDAO ssainfoDAO=(SsainfoDAO)context.getBean("ssainfoDAO");
         Ssainfo si=ssainfoDAO.getRecord();
         ssaurl=si.getSsaurl();
         csaid=si.getCsaid();
         try {
            auth.setValue("ssaurl",ssaurl);
            auth.setValue("csaid",new Long(csaid).toString());
         } catch (Exception fe) {
            // Initial CSA user setup has no auth access, so save to session
            session.setAttribute("ssaurl",ssaurl);
            session.setAttribute("csaid",new Long(csaid).toString());
         }
         return ssaurl;
      } catch (Exception e) {
         throw new FORMSException("Unable to retrieve SSA information \n\n" + Stack2string.getString(e));
      }
   }

   // return SSA URL Auth unavailable
   public static String getSsaUrl(HttpSession session,AbstractApplicationContext context) throws FORMSException {

      return getSsaUrl(session,context,null);

   }

}

