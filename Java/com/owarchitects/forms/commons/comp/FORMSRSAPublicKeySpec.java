 
/**
 *
 * This program is part of:
 *
 *    FORMS [Forms-Oriented Research Management System]
 *
 * Copyright (C) 2009 OpenWare Architects.  All Rights Reserved.
 *
 * FORMS is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * FORMS is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with FORMS.  If not, see <http://www.gnu.org/licenses/>.
 *
 */
 
package com.owarchitects.forms.commons.comp;

/*
 * Creates an Public Key Specification from FORMS Exported public key string
 */

import java.security.spec.*;
import java.math.*;
import java.util.regex.*;
import java.security.spec.*;

public class FORMSRSAPublicKeySpec implements KeySpec {

   private BigInteger modulus;
   private BigInteger publicExponent;

   public FORMSRSAPublicKeySpec(String ins) throws FORMSException {

      String REGEX = "";
      String REPLACE = "";
      Pattern p;
      Matcher m;

      String mstring=ins;
      REGEX="^.*MODULUS: *";
      p = Pattern.compile(REGEX,Pattern.DOTALL|Pattern.MULTILINE|Pattern.UNIX_LINES);
      m = p.matcher(mstring); 
      mstring = m.replaceAll(REPLACE);
      REGEX="\\D.*\\z";
      p = Pattern.compile(REGEX,Pattern.DOTALL|Pattern.MULTILINE|Pattern.UNIX_LINES);
      m = p.matcher(mstring); 
      mstring = m.replaceAll(REPLACE);

      String pstring=ins;
      REGEX="^.*PUBLIC EXPONENT: *";
      p = Pattern.compile(REGEX,Pattern.DOTALL|Pattern.MULTILINE|Pattern.UNIX_LINES);
      m = p.matcher(pstring); 
      pstring = m.replaceAll(REPLACE);
      REGEX="\\D.*\\z";
      p = Pattern.compile(REGEX,Pattern.DOTALL|Pattern.MULTILINE|Pattern.UNIX_LINES);
      m = p.matcher(pstring); 
      pstring = m.replaceAll(REPLACE);

      BigInteger mint=new BigInteger(mstring);
      BigInteger pint=new BigInteger(pstring);
      if (mint==null || pint==null || mint.compareTo(BigInteger.ZERO)!=1 || pint.compareTo(BigInteger.ZERO)!=1) {
         throw new FORMSException("Invalid KeySpec String");
      }

      this.modulus=mint;
      this.publicExponent=pint;
   }

   public BigInteger getModulus() {
      return modulus;
   }

   public BigInteger getPublicExponent() {
      return publicExponent;
   }

}
