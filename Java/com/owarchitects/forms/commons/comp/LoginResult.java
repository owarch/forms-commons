 
/**
 *
 * This program is part of:
 *
 *    FORMS [Forms-Oriented Research Management System]
 *
 * Copyright (C) 2009 OpenWare Architects.  All Rights Reserved.
 *
 * FORMS is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * FORMS is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with FORMS.  If not, see <http://www.gnu.org/licenses/>.
 *
 */
 
/**
 * LoginResult.java -- results object returned by Authenticator.login
 * Original Creation -- 03/2007 (MRH)
 */

package com.owarchitects.forms.commons.comp;

import java.io.*;
import java.util.*;
import java.lang.reflect.*;

public class LoginResult implements Serializable {

   // shared parameters

   public int status;
   public long badlogincount;
   public long daystoexpire;

   // 

   public void setStatus(int status) {
      this.status=status;
   }

   public int getStatus() {
      return status;
   }

   // 

   public void setBadlogincount(long badlogincount) {
      this.badlogincount=badlogincount;
   }

   public long getBadlogincount() {
      return badlogincount;
   }

   //

   public void setDaystoexpire(long daystoexpire) {
      this.daystoexpire=daystoexpire;
   }

   public long getDaystoexpire() {
      return daystoexpire;
   }

   // 

}

