 
/**
 *
 * This program is part of:
 *
 *    FORMS [Forms-Oriented Research Management System]
 *
 * Copyright (C) 2009 OpenWare Architects.  All Rights Reserved.
 *
 * FORMS is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * FORMS is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with FORMS.  If not, see <http://www.gnu.org/licenses/>.
 *
 */
 
/**
* FORMSSasConnection.java -- returns sas connection
* Original Creation -- 07/2005 (MRH)
*/

package com.owarchitects.forms.commons.comp;

import java.io.*;
import java.util.*;
import java.net.*;
import javax.servlet.*;
import javax.servlet.http.*;
import com.sas.sasserver.sclfuncs.*;
import com.sas.rmi.*;
import com.sas.sasserver.submit.*;
import com.sas.sasserver.dataset.*;
import java.lang.Thread;
import org.apache.velocity.app.*;
import org.apache.velocity.*;

public class FORMSSasConnection extends FORMSResourceAccessor {

   private com.sas.rmi.Connection connection = null;
   private com.sas.rmi.Rocf rocf = null;
   private SclfuncsV3Interface iscl = null;
   private SubmitInterface submit = null;
   private boolean reused=false;
      
   // CONSTRUCTORS
   public FORMSSasConnection(HttpServletRequest request,HttpServletResponse response) {

      super(request,response);
   
   }

   // RETURN Username VALUE
   public com.sas.rmi.Connection getConnection () {

         try {

            // make multiple attempts at connection
            for (int i=1;i<=5;i++) {

               reused=true;

               try {

                  if (connection==null) {

                     boolean haveinet=false;
                     for (int j=1; j<=5; j++) {
                        String myipaddress;
                        try {
                           myipaddress=InetAddress.getLocalHost().getHostAddress();
                           if (!myipaddress.equals("127.0.0.1")) {
                              haveinet=true;
                              break;
                           } 
                           Thread.sleep(100); 
                        } catch (Exception ex) {}
                     }

                     if (haveinet) {

                        establishConn();
         
                     }

                  }
         
                  // Start new connection if in Syntax Check Mode
                  if (iscl.optgetn("OBS")==0) {

                     establishConn();

                  }

                  // verify connection is usable
                  submit.setProgramText("data _null_; run;");

                  // run initial autoexec statements
                  org.apache.velocity.Template template = getVelocityTemplate("autoexec.sas");
                  StringWriter sout=new StringWriter();
                  VelocityContext vcontext=new VelocityContext();
                  vcontext.put("pathvar",getSession().getServletContext().getRealPath("/"));
                  template.merge(vcontext, sout);
                  submit.setProgramText(sout.toString());
                  break;

               } catch (Exception e2) {

                  try { Thread.sleep(100); } catch (Exception e) {}

                  if (i==4) {
                     connection=null;
                     session.setAttribute("sasconnection",connection);
                  }

               }
            } 
            return connection;
         } catch (Exception e) {
            connection=null;
            session.setAttribute("sasconnection",connection);
            return connection;
         } 

   }

   private void establishConn() throws Exception {

       FORMSConfig config=SessionObjectUtil.getConfigObj(getSession());

       String sasserver=null, sasrootdir=null, sasusername=null, sasencryptedpassword=null;
       int sasconnectport=-1;

       sasserver=config.getSasServer();
       sasrootdir=config.getSasRootDir();
       sasconnectport=config.getSasConnectPort();
       sasusername=config.getSasUserName();
       sasencryptedpassword=config.getSasEncryptedPassword();

       FORMSAuth auth=this.getAuth();

       reused=false;
       connection = new com.sas.rmi.Connection();
       com.sas.rmi.Connection.setServerArchitecture ( connection, "PC" );
       connection.setAccessMethod(connection.ACCESS_METHOD_JCONNECT);
       connection.setPort(sasconnectport);
       connection.setHost(sasserver);
       connection.setUsernamePrompt("Username:");
       connection.setPasswordPrompt("Password:");
       connection.setUsername(sasusername);
       connection.setPassword(EncryptUtil.decryptString(sasencryptedpassword,SessionObjectUtil.getInternalKeyObj(getSession())));

       session.setAttribute("sasconnection",connection);
       rocf = new Rocf();
       rocf.connect(connection);
       iscl = (SclfuncsV3Interface)rocf.newInstance(SclfuncsV3Interface.class,connection);
       submit = (SubmitInterface)rocf.newInstance(SubmitInterface.class,connection);

   }                     

   public com.sas.rmi.Rocf getRocf () {
      if (rocf==null) getConnection();
      return rocf;
   }

   public SclfuncsV3Interface getIscl () {
      if (iscl==null) getConnection();
      return iscl;
   }

   public SubmitInterface getSubmit () {
      if (submit==null) getConnection();
      return submit;
   }

   public DataSetInterface getDataSetInterface () {
      if (connection==null) getConnection();
      // programs might have more than one datasetinterface, so return a new one.  Should only
      // have one iscl & submit interface, so return the session one there.
      try {
         return (DataSetInterface)rocf.newInstance(DataSetInterface.class,connection);
      } catch (Exception e) {
         return null;
      }
   }

   public boolean isReused () {
      return reused;
   }

   public void finalize () {
      // CLOSE CONNECTIONS
      rocf.removeConnection(connection);
      rocf.reset();
      rocf.stop();
      connection.stop();
   }

}

