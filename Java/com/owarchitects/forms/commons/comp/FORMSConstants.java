 
/**
 *
 * This program is part of:
 *
 *    FORMS [Forms-Oriented Research Management System]
 *
 * Copyright (C) 2009 OpenWare Architects.  All Rights Reserved.
 *
 * FORMS is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * FORMS is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with FORMS.  If not, see <http://www.gnu.org/licenses/>.
 *
 */
 
package com.owarchitects.forms.commons.comp;

import java.util.*;
import java.lang.reflect.*;
import java.lang.SecurityManager;

/*
 * FORMSConstants.java - Abstract class implementing method used by 
 *                       various other constants classes
 */

public abstract  class FORMSConstants {

   public String getFieldName(int v) {
      try {
         Field[] f=this.getClass().getFields();
         for (int i=0;i<f.length;i++) {
            if (f[i].getInt(this)==v) {
               return f[i].getName();
            }
         }
         return "OTHER_ERROR";
      } catch (Exception e) {
         return "OTHER_ERROR";
      }
   }

   public int getFieldValue(String fieldname) {
      try {
         Field[] f=this.getClass().getFields();
         for (int i=0;i<f.length;i++) {
            if (f[i].getName().equalsIgnoreCase(fieldname)) {
               return f[i].getInt(this);
            }
         }
         return Integer.MIN_VALUE;
      } catch (Exception e) {
         return Integer.MIN_VALUE;
      }
   }

   public List getFieldList(int j) {
      try {
         Field[] f=this.getClass().getFields();
         List l=new ArrayList();
         for (int i=0;i<f.length;i++) {
            HashMap map=new HashMap();
            if (j!=Integer.MIN_VALUE && f[i].getInt(this)!=j) continue;
            map.put("fieldname",f[i].getName());
            map.put("fieldvalue",f[i].get(this));
            l.add(map);
         }
         return l;
      } catch (Exception e) {
         return null;
      }
   }

   public List getFieldList() {
      return getFieldList(Integer.MIN_VALUE);
   }

}



