 
/**
 *
 * This program is part of:
 *
 *    FORMS [Forms-Oriented Research Management System]
 *
 * Copyright (C) 2009 OpenWare Architects.  All Rights Reserved.
 *
 * FORMS is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * FORMS is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with FORMS.  If not, see <http://www.gnu.org/licenses/>.
 *
 */
 
/*
 * SelectReportResult.java - Output parameters for SelectReportService - MRH 03/2007
 */

package com.owarchitects.forms.commons.comp;

import java.util.*;
import java.io.*;

public class SelectReportResult extends FORMSResult implements Serializable {

   public org.mla.html.table.Table table;
   public String sinstopt;
   public String rightslevel;
   public String resourcetype;

   //

   public void setTable(org.mla.html.table.Table table) {
      this.table=table;
   }

   public org.mla.html.table.Table getTable() {
      return table;
   }

   //

   public void setSinstopt(String sinstopt) {
      this.sinstopt=sinstopt;
   }

   public String getSinstopt() {
      return sinstopt;
   }

   //

   public void setRightslevel(String rightslevel) {
      this.rightslevel=rightslevel;
   }

   public String getRightslevel() {
      return rightslevel;
   }

   //

   public void setResourcetype(String resourcetype) {
      this.resourcetype=resourcetype;
   }

   public String getResourcetype() {
      return resourcetype;
   }

}



