 
/**
 *
 * This program is part of:
 *
 *    FORMS [Forms-Oriented Research Management System]
 *
 * Copyright (C) 2009 OpenWare Architects.  All Rights Reserved.
 *
 * FORMS is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * FORMS is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with FORMS.  If not, see <http://www.gnu.org/licenses/>.
 *
 */
 
/* 
 *
 * ResourceAccessor.java -  When extended, gives subclasses access to resources
 *   available to controllers.  Subclasses should call super(request,response).
 *
 */

package com.owarchitects.forms.commons.comp;

import com.owarchitects.forms.commons.db.*;

import java.util.*;
import java.io.*;
import javax.servlet.*;
import javax.servlet.http.*;
import java.security.*;

import org.springframework.context.*;
import org.springframework.context.support.*;
import org.springframework.orm.hibernate3.*;
import org.hibernate.*;
import org.hibernate.transform.*;
import org.apache.velocity.Template;
import org.apache.velocity.app.VelocityEngine;
import org.apache.commons.dbcp.BasicDataSource;
import com.mchange.v2.c3p0.ComboPooledDataSource;
import org.springframework.transaction.support.TransactionSynchronizationManager;


public abstract class ResourceAccessor {

   protected PrintWriter out;
   protected HttpSession session;
   protected HttpServletRequest request;
   protected HttpServletResponse response;
   protected String syswork;
   protected SessionFactory sessionFactory;
   protected Session hibernateSession;
   protected String sysconn;
   protected ComboPooledDataSource mds=null;

   // This initializer must be called from subclasses
   public ResourceAccessor(HttpServletRequest request,HttpServletResponse response) {
      
       this.request=request;
       this.response=response;
       try {
          this.out=response.getWriter();        
       } catch (Exception e) {
          this.out=null;
       }
       this.syswork=null;
       this.sysconn=null;
       // Pull syswork, sysconn from session attribute preferably, alternately from cookie.
       try {
          this.syswork=request.getSession().getAttribute("syswork").toString();
       } catch (Exception swe) {}
       if (syswork==null || syswork.length()<1) {
          this.syswork=Base64.decode(this.getCookieValue(Base64.encode("workdir")).replaceAll("=",""));
       }   
       try {
          this.sysconn=request.getSession().getAttribute("sysconn").toString();
       } catch (Exception swe) {}
       if (sysconn==null || sysconn.length()<1) {
          this.sysconn=Base64.decode(this.getCookieValue(Base64.encode("sysconn")).replaceAll("=",""));
       }   

       // Get context
       session=request.getSession();

       // Initialize main data source
       try {
          initDataSource();
       } catch (Exception e) { }   
   
   }     

   // field getter methods
   protected final PrintWriter getWriter() {
      return out;
   }

   protected final HttpSession getSession() {
      return session;
   }

   protected final HttpServletRequest getRequest() {
      return request;
   }

   protected final HttpServletResponse getResponse() {
      return response;
   }

   /*
    * NOTE HERE:  We are maintaining and using a separate application context
    * here from the root application context for the application that would
    * be accessed from the getApplicationContext() method.  For some reason
    * when using that context, hibernate write operations threw exceptions.
    * When fixes were implemented to try eliminate the error (setting
    * HibernateSession flushMode to >= COMMIT), the exceptions went away,
    * but the write operations simply did not occur.  The hack here is 
    * simply to create another ApplicationContext maintained in the 
    * httpsession for use with hibernate.
    * MRH 10/03/2007
    */
    
   protected final AbstractApplicationContext getContext() {
      try {
         return SessionObjectUtil.getApplicationContext(this.getSession());
      } catch (Exception e) {
         return null;
      }
   }

   // Return cookie specified by input string.  Returns empty string if no such cookie.
   protected final String getCookieValue(String cstr) {
      try {
         Cookie[] carray=this.getRequest().getCookies();
         Iterator i=Arrays.asList(carray).iterator();
         while (i.hasNext()) {
            Cookie c=(Cookie)i.next();
            if (c.getName().replaceAll("=","").equals(cstr.replaceAll("=",""))) {
               return c.getValue().replaceAll("=","");
            }
         }
      } catch (Exception ce) {}   
      return "";
   }

   public Session getHibernateSession() {
      sessionFactory = (SessionFactory) this.getContext().getBean("sessionFactory"); 
      hibernateSession = SessionFactoryUtils.getSession(sessionFactory, true); 
      return hibernateSession;
   }

   public void bindHibernateSession() {
      sessionFactory = (SessionFactory) this.getContext().getBean("sessionFactory"); 
      hibernateSession = SessionFactoryUtils.getSession(sessionFactory, true); 
      TransactionSynchronizationManager.bindResource(sessionFactory, new SessionHolder(hibernateSession));
   }
   
   public void releaseHibernateSession() {
      TransactionSynchronizationManager.unbindResource(sessionFactory);
      SessionFactoryUtils.releaseSession(hibernateSession,sessionFactory);
   }

   protected final FORMSAuth getAuth(boolean setCallingClass) throws FORMSException,FORMSKeyException,FORMSSecurityException {

      HttpServletRequest request=this.getRequest();
      HttpServletResponse response=this.getResponse();
      
      FORMSAuth auth=SessionObjectUtil.getAuthObj(request,response,syswork,sysconn);

      // In case auth created syswork
      if (syswork==null) {
         this.syswork=auth.getSyswork();
      }

      if (setCallingClass) {
         auth.setValue("callingjclass",request.getServletPath().substring(1));
      }   

      return auth;
   }

   protected final FORMSAuth getAuth() throws FORMSException,FORMSKeyException,FORMSSecurityException {
      return this.getAuth(true);
   }

   protected final String getAppPath() {
      return getSession().getServletContext().getRealPath("/");
   } 

   protected final org.apache.velocity.Template getVelocityTemplate(String inString) throws FORMSException {

      try {
         VelocityEngine velocity=SessionObjectUtil.getVelocityEngine(session,this.getContext(),this.getAppPath());
         return velocity.getTemplate(inString);

      }  catch (Exception e) {

         throw new FORMSException("FORMS Service-side Velocity exception");

      }

   } 
   
   public MainDAO getMainDAO() {
      return (MainDAO)this.getContext().getBean("mainDAO");
   }   
   
   public EmbeddedDAO getEmbeddedDAO() {
      return (EmbeddedDAO)this.getContext().getBean("embeddedDAO");
   }   


   // Log system event
   protected final void logEvent(String detail) {

      try {

         Systemlog sl=new Systemlog();
         sl.setIpaddr(this.getRequest().getRemoteAddr());
         sl.setUserdesc(this.getAuth().getValue("userdesc"));
         sl.setDetail(detail);
         sl.setServletpath(this.getRequest().getServletPath());
         sl.setTimestamp(new Date());
         getMainDAO().saveOrUpdate(sl);

      } catch (Exception loge) {
         // Do nothing
      }

   }

   protected final void initDataSource() {
      if (mds==null) {
         try {
            String u=getSysconn().getUsername();
            String p=getSysconn().getPassword();
            EmbeddedusersDAO embeddedusersDAO=(EmbeddedusersDAO)this.getContext().getBean("embeddedusersDAO");
            Embeddedusers euser=embeddedusersDAO.getRecord(u);
            PrivateKey privkey=EncryptUtil.getPrivateKey(session,u,p);
            String dbpw=EncryptUtil.decryptString(euser.getDbpassword(),privkey);
            // Set main data source password 
            mds=(ComboPooledDataSource)this.getContext().getBean("mainDataSource");
            // replaceFirst & replaceAll have problems with backslashes in replacement text.
            // here we do a replaceFirst with intermediate text then use replace to 
            // replace that text in the password.  replace uses "CharSequence" for the
            // replacement and handles backslashes and other characters correctly
            mds.setJdbcUrl(mds.getJdbcUrl().replaceFirst("bootPassword=.*","XXXXYYYYXXXX").replace("XXXXYYYYXXXX","bootPassword=" + new String(dbpw)));
            mds.setPassword(new String(dbpw));
            mds.setOverrideDefaultPassword(new String(dbpw));

         } catch (Exception e) {
            // OK, no initialization first pass through login
         }   
      }
   }

   protected final File getSyswork() {
      return new File(syswork);
   }

   private final Sysconn getSysconn() throws Exception {
      return (Sysconn) ObjectXmlUtil.objectFromXmlString(
                EncryptUtil.decryptString(sysconn,SessionObjectUtil.getInternalKeyObj(session))
             );
   }

}

