 
/**
 *
 * This program is part of:
 *
 *    FORMS [Forms-Oriented Research Management System]
 *
 * Copyright (C) 2009 OpenWare Architects.  All Rights Reserved.
 *
 * FORMS is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * FORMS is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with FORMS.  If not, see <http://www.gnu.org/licenses/>.
 *
 */
 
/* 
 *
 * ServiceClientResourceAccessor.java -  Abstract ResourceAccessor class containing Service access methods
 *                    
 *
 */

package com.owarchitects.forms.commons.comp;

import java.util.*;
import java.beans.*;
import java.lang.reflect.*;
import org.apache.axis2.context.*;
import org.apache.axis2.client.*;
import org.apache.axis2.client.async.AxisCallback;
import org.apache.axis2.rpc.client.RPCServiceClient;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public abstract class ServiceClientResourceAccessor extends FORMSResourceAccessor implements ServiceClientResource,SoapClientResource  {

   // This initializer must be called from subclasses
   public ServiceClientResourceAccessor(HttpServletRequest request,HttpServletResponse response) {
      super(request,response);
   }

   //////////////////////////
   // FORMSService Methods //
   //////////////////////////


   // Submit Service Request
   public FORMSServiceResult submitServiceRequest(FORMSServiceParms parms,String endpoint) throws FORMSException {

      return FORMSServiceClientUtil.submitServiceRequest(getSession(),parms,endpoint);

   }

   /////////////////////////
   // SOAP Client Methods //
   /////////////////////////

   // Submit SOAP Request
   public FORMSServiceResult submitSoapRequest(FORMSServiceParms parms,String endpoint) throws FORMSException {

      return FORMSServiceClientUtil.submitSoapRequest(getSession(),parms,endpoint);

   }

   // Submit SOAP Request
   public void submitNonBlockingSoapRequest(FORMSServiceParms parms,String endpoint,AxisCallback cb) throws FORMSException {

      FORMSServiceClientUtil.submitNonBlockingSoapRequest(getSession(),parms,endpoint,cb);

   }

   // execute invokeBlocking method call against serviceclient
   public final Object[] invokeBlocking(RPCServiceClient sclient,Options opt,String method,Object[] inArray,Class[] outArray) throws FORMSException {

      return FORMSServiceClientUtil.invokeBlocking(sclient,opt,method,inArray,outArray);

   }

   // execute invokeNonBlocking method call against serviceclient
   public final void invokeNonBlocking(RPCServiceClient sclient,Options opt,String method,Object[] inArray,AxisCallback cb) throws FORMSException {

      FORMSServiceClientUtil.invokeNonBlocking(sclient,opt,method,inArray,cb);

   }

   ////////////////////
   // Shared Methods //
   ////////////////////


   public FORMSServiceResult getErrorObject(FORMSServiceParms parms,int i) 
          throws NoSuchMethodException,IllegalAccessException,InvocationTargetException,java.lang.InstantiationException {

      return FORMSServiceClientUtil.getErrorObject(getContext(),parms,i);

   }

}

