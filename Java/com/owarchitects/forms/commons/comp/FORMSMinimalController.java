 
/**
 *
 * This program is part of:
 *
 *    FORMS [Forms-Oriented Research Management System]
 *
 * Copyright (C) 2009 OpenWare Architects.  All Rights Reserved.
 *
 * FORMS is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * FORMS is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with FORMS.  If not, see <http://www.gnu.org/licenses/>.
 *
 */
 
/* 
 *
 * FORMSMinimalController.java -  Overrides FORMSController handleRequestInternal method to
 *    perform minimal checking and verification.  Descendents of this class are not able to 
 *    provide FORMS persistence across sessions or provide security validation.
 *
 */

package com.owarchitects.forms.commons.comp;

import com.owarchitects.forms.commons.db.*;
import java.util.*;
import java.util.regex.Pattern;
import java.util.regex.Matcher;
import java.io.*;
import java.lang.*;
import java.security.*;
import javax.crypto.SecretKey;
import javax.servlet.http.*;
import javax.servlet.http.HttpSession;
import org.springframework.web.servlet.*;
import org.springframework.web.context.support.*;
//import org.apache.axis2.context.*;
//import org.apache.axis2.client.*;
//import org.apache.axis2.transport.http.HTTPConstants;
import org.apache.commons.io.FileUtils;
import org.apache.commons.dbcp.BasicDataSource;
import org.apache.commons.lang.time.DateUtils;
import org.springframework.orm.hibernate3.HibernateJdbcException;



public abstract class FORMSMinimalController extends FORMSController {

   // Override superclass handleRequestInternal method
   protected ModelAndView handleRequestInternal(HttpServletRequest request, HttpServletResponse response) throws FORMSException {

        try {

           // PrintWriter for diagnostic output
           out=response.getWriter();        

           this.request=request;
           this.response=response;
           this.syswork=null;
           this.sysconn=null;
           // Pull syswork, sysconn from session attribute preferably, alternately from cookie.
           try {
              this.syswork=request.getSession().getAttribute("syswork").toString();
           } catch (Exception swe) {}
           if (syswork==null || syswork.length()<1) {
              this.syswork=Base64.decode(this.getCookieValue(Base64.encode("workdir")).replaceAll("=",""));
           }   

           response.addCookie(new Cookie(Base64.encode("workdir").replaceAll("=",""),
                                         Base64.encode(syswork).replaceAll("=","")));
           try {
              this.sysconn=request.getSession().getAttribute("sysconn").toString();
           } catch (Exception swe) {}
           if (sysconn==null || sysconn.length()<1) {
              this.sysconn=Base64.decode(this.getCookieValue(Base64.encode("sysconn")).replaceAll("=",""));
           }   
           // formstime used for sysconn and auth
           String formstime=new Long(System.currentTimeMillis()).toString();

           //// Ignore sysconn for setup program, no need to persist this across sessions
           //if (sysconn==null || sysconn.length()<1) {
           //   safeRedirect("login.do");
           //}

           // Get context
           session=request.getSession();
           wp=SessionObjectUtil.getWinPropsObj(request,response);
   
           // Immediately expire headers
           response.setHeader("Cache-Control","no-cache"); //HTTP 1.1
           response.setHeader("Pragma","no-cache"); //HTTP 1.0
           response.setDateHeader ("Expires", -1); //prevents caching at the proxy server

           try {

              // Initialize Main Data Source
              initDataSource();

              // Create mav used by override method
              this.mav=new ModelAndView();
              mav=submitRequest();

              if (mav!=null) {
                 // add WinProps objects to view
                 wp.addViewObjects(mav);
                 // add ServletPath (spath) object to view
                 mav.addObject("spath",request.getServletPath().substring(1));
                 // add other important values
                 try {
                    mav.addObject("rightslevel",getAuth().getValue("rightslevel"));
                    mav.addObject("analystaccess",getAuth().getValue("analystaccess"));
                    mav.addObject("studyaccess",getAuth().getValue("studyaccess"));
                 } catch (Exception aex) { }
              }    

              return mav;

           } catch (Exception ke) {
                 
              throw new FORMSException("FORMSController exception (submitRequest exception) - <BR>",ke);
           }
          
        } catch (Exception e) {

           throw new FORMSException("FORMSController exception - <BR>",e);

        }

   }     


}

