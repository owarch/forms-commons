 
/**
 *
 * This program is part of:
 *
 *    FORMS [Forms-Oriented Research Management System]
 *
 * Copyright (C) 2009 OpenWare Architects.  All Rights Reserved.
 *
 * FORMS is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * FORMS is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with FORMS.  If not, see <http://www.gnu.org/licenses/>.
 *
 */
 

package com.owarchitects.forms.commons.comp;

import java.io.*;
import java.util.*;
import javax.servlet.http.*;
import org.springframework.web.servlet.ModelAndView;
import org.apache.commons.io.FileUtils;

/**
* WinProps.java -- Keeps track of window naming state/properties
*/

public class WinProps {

   private String bgcolor;
   private String bgimage;
   private String menubgcolor;
   private String windowname = null;
   private String winnamemissok = null;
   private HttpServletRequest request;
   private HttpServletResponse response;
   private HttpSession session;

   public WinProps(HttpServletRequest request,HttpServletResponse response) {
      this.request=request;
      this.response=response;
      this.session=request.getSession();
      // Get window color/image info from config object
      String appName=" ";
      try {
         appName=session.getServletContext().getResource("/").toString();
      } catch (Exception j) {}
      bgcolor=SessionObjectUtil.getConfigObj(session).getBgColor();
      bgimage=SessionObjectUtil.getConfigObj(session).getBgImage();
      menubgcolor=SessionObjectUtil.getConfigObj(session).getMenuBgColor();
      setWindowName();
   }

   public String getWindowName () {
      return windowname;
   }

   public String getWinNameMissOk () {
      return winnamemissok;
   }

   public String getBgColor () {
      return bgcolor;
   }

   public String getBgImage () {
      return bgimage;
   }

   public String getMenuBgColor () {
      return menubgcolor;
   }

   public String setWindowName () {
      if (getWindowName()==null) {
         windowname="RMS" + System.currentTimeMillis();
         try {
            PrintWriter out=response.getWriter();
            // Send window naming script to HTTP Response
            out.println(
               "<script>\n" +
               "   try { window.name='" + windowname + "'; } catch (e) {}\n" +
               "   try { parent.window.name='" + windowname + "'; } catch (e) {}\n" +
               "</script>\n" 
               );
         } catch (Exception e) {
         }   
         return windowname;
      } else {
         return "already";
      }
   }

   public void setWinNameMissOk (String s) {
      this.winnamemissok=s;
   }

   // Add Objects to View
   public ModelAndView addViewObjects (ModelAndView mav) {
      mav.addObject("bgcolor",getBgColor());
      mav.addObject("bgimage",getBgImage());
      mav.addObject("menubgcolor",getMenuBgColor());
      mav.addObject("windowname",getWindowName());
      mav.addObject("winnamemissok",getWinNameMissOk());
      return mav;
   }

}


