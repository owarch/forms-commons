 
/**
 *
 * This program is part of:
 *
 *    FORMS [Forms-Oriented Research Management System]
 *
 * Copyright (C) 2009 OpenWare Architects.  All Rights Reserved.
 *
 * FORMS is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * FORMS is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with FORMS.  If not, see <http://www.gnu.org/licenses/>.
 *
 */
 
package com.owarchitects.forms.commons.comp;

import java.io.*;
import java.util.*;

public class ColorConvertUtil {

   ////////////////////////////////////////////////////////////////////////////////////////////
   //                                                                                        //
   // ColorConvertUtil.java - (03/2007 MRH) - Converts 255,255,255 color used by PDF files   //
   //                 to #FFFFFF format used by HTML forms                                   //
   //                                                                                        //
   //                 NOTE:  Currently no checking here for invalid colors                   //
   //                                                                                        //
   ////////////////////////////////////////////////////////////////////////////////////////////

   public static String convert(String instring) {
      // return values already in #FFFFFF format
      if (instring.substring(0,1).equals("#")) {
         return instring;
      }
      // add # to FFFFFF format values
      if (!(instring.indexOf(",")>=0)) {
         return "#" + instring;
      }
      String[] inarray=instring.split(",");
      Iterator i=Arrays.asList(inarray).iterator();
      StringBuilder outsb=new StringBuilder("#");
      while (i.hasNext()) {
         String hex=Integer.toString(new Integer((String)i.next()).intValue(),16);
         if (hex.length()==1) hex="0"+hex;
         outsb.append(hex);
      }
      return outsb.toString();
   }

   // testing code
   public static void main(String[] args) {

      Iterator i=Arrays.asList(args).iterator();
      while (i.hasNext()) {
         System.out.println(convert((String)i.next()));
      }

   }

}


