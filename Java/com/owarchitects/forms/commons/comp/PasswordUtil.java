 
/**
 *
 * This program is part of:
 *
 *    FORMS [Forms-Oriented Research Management System]
 *
 * Copyright (C) 2009 OpenWare Architects.  All Rights Reserved.
 *
 * FORMS is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * FORMS is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with FORMS.  If not, see <http://www.gnu.org/licenses/>.
 *
 */
 
/**
* PasswordUtil.java -- Password Utilities
* Original Creation -- 12/2006 (MRH)
*/

package com.owarchitects.forms.commons.comp;

import java.io.*;
import java.util.*;
//import sun.misc.BASE64Encoder;
import java.security.*;
import java.security.spec.*;

public class PasswordUtil {
   
   // Instances should not be created
   public PasswordUtil() {
      super();
   }

   // PASSWORD ENCRYPTION
   public static String pwEncrypt(String plaintext) {

     MessageDigest md = null;

     try {
         md = MessageDigest.getInstance("SHA1"); 
     } catch(NoSuchAlgorithmException e) {
         // DO NOTHING
     }

     try {
         md.update(stringMod(plaintext,true).getBytes("UTF-8")); 
     } catch(UnsupportedEncodingException e) {
         // DO NOTHING
     }

     byte raw[] = md.digest();
     md.update(raw); 
     raw = md.digest(); 
     //String hash = (new BASE64Encoder()).encode(raw); 
     String hash;
     try {
        hash = new String(Base64.encode(raw),"UTF-8"); 
     } catch (java.io.UnsupportedEncodingException ue) {
        return null;
     }
     return stringMod(hash,false); 

   }
  
   // MODIFY STRING
   protected static String stringMod(String instring,boolean reverse) {
     char[] c=instring.toCharArray();
     if (reverse) {
        char[] d=instring.toCharArray();
        long total=0;
        for (int i = 0; i <= (c.length)-1; i++) {
           c[i]=d[(c.length-1)-i];
        }
     }
     for (int i = 0; i <= (c.length)-1; i++) {
        if (Character.isLowerCase(c[i])) c[i]=Character.toUpperCase(c[i]);
        else if (Character.isUpperCase(c[i])) c[i]=Character.toLowerCase(c[i]);
        if (Character.isDigit(c[i])) {
           int ii=(9-Character.getNumericValue(c[i]));
           String jj=new String(new Integer(ii).toString());
           c[i]=jj.charAt(0);
        }
     }
     return new String(c).trim();
   }

   // Return temporary password
   public static String getTemporaryPassword() {
      String letters="ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz";
      long seed1=Math.round(Math.random()*99999999);
      long seed2=Math.round(Math.random()*99999999);
      return new Character(letters.charAt(new Long(seed1 % 52).intValue())).toString() +
             EcodeUtil.getFromRan(seed1) +
             new Character(letters.charAt(new Long(seed2 % 52).intValue())).toString()
             ;
   }

   // Return temporary password
   public static String getRandomPassword() {
      String letters="ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789_";
      long seed1=Math.round(Math.random()*99999999);
      long seed2=Math.round(Math.random()*99999999);
      long seed3=Math.round(Math.random()*99999999);
      long seed4=Math.round(Math.random()*99999999);
      long seed5=Math.round(Math.random()*99999999);
      long seed6=Math.round(Math.random()*99999999);
      long seed7=Math.round(Math.random()*99999999);
      long seed8=Math.round(Math.random()*99999999);
      long seed9=Math.round(Math.random()*99999999);
      return new Character(letters.charAt(new Long(seed1 % 63).intValue())).toString() +
             new Character(letters.charAt(new Long(seed2 % 63).intValue())).toString() +
             new Character(letters.charAt(new Long(seed3 % 63).intValue())).toString() +
             new Character(letters.charAt(new Long(seed4 % 63).intValue())).toString() +
             new Character(letters.charAt(new Long(seed5 % 63).intValue())).toString() +
             new Character(letters.charAt(new Long(seed6 % 63).intValue())).toString() +
             new Character(letters.charAt(new Long(seed7 % 63).intValue())).toString() +
             new Character(letters.charAt(new Long(seed8 % 63).intValue())).toString() +
             new Character(letters.charAt(new Long(seed9 % 63).intValue())).toString()
             ;
   }

}

