 
/**
 *
 * This program is part of:
 *
 *    FORMS [Forms-Oriented Research Management System]
 *
 * Copyright (C) 2009 OpenWare Architects.  All Rights Reserved.
 *
 * FORMS is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * FORMS is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with FORMS.  If not, see <http://www.gnu.org/licenses/>.
 *
 */
 
package com.owarchitects.forms.commons.comp;

import java.io.*;
import java.util.*;
import java.lang.reflect.*;
import org.hibernate.*;
import org.hibernate.cfg.*;
import org.apache.commons.io.FileUtils;

public class HibernateUtil {

    // Prints table output to Writer

    public static void deleteAllRecords(Object dao,java.lang.Class ic) throws NoSuchMethodException,IllegalAccessException,InvocationTargetException {

       Class daoc=dao.getClass();
       Method m1=daoc.getMethod("getTable",new Class[] {});
       List l=(List)m1.invoke(dao,new Object[] {} );
       Method m2=daoc.getMethod("deleteRecord",new Class[] { ic } );
       Iterator i=l.iterator();
       while (i.hasNext()) {
          Object obj=i.next();
          m2.invoke(dao,new Object[] { ic.cast(obj) });
       }

    }

    // Prints table output to PrintWriter
    public static String tableToString(java.util.List l,java.lang.Class c) {
       try {
          return getOutput(l,c).toString();
       } catch (Exception e) {
          return Stack2string.getString(e);
       }
    }

    // Prints table output to PrintWriter
    public static void printTable(java.util.List l,java.lang.Class c,java.io.PrintWriter o) {
       try {
          o.println(getOutput(l,c));
       } catch (Exception e) {
          o.println(Stack2string.getString(e));
       }
    }

    // Prints table output to Writer
    public static void printTable(java.util.List l,java.lang.Class c,java.io.Writer o) {
       try {
          o.write(getOutput(l,c).toString());
       } catch (Exception e) {
          try {
             o.write(Stack2string.getString(e));
          } catch (Exception e2) {}
       }
    }

    // Prints table output to PrintStream
    public static void printTable(java.util.List l,java.lang.Class c,java.io.PrintStream o) {
       try {
          o.println(getOutput(l,c));
       } catch (Exception e) {
          o.println(Stack2string.getString(e));
       }
    }
    
    // Formats table output for printTable methods
    private static StringBuilder getOutput(java.util.List l,java.lang.Class c) {
       try {
          if (l.size()>=1) {
             Iterator li=l.iterator();
             StringBuilder sb=new StringBuilder();
             while (li.hasNext()) {
                Object lio=li.next();
                sb.append("\n\nBegin Record:  \n");
                Field f[]=c.getFields();
                Method m[]=c.getMethods();
                Iterator mi=Arrays.asList(m).iterator();
                while (mi.hasNext()) {
                   Method thism=(Method)mi.next();
                   String mname=thism.getName();
                   if (mname.substring(0,3).equalsIgnoreCase("get")) {
                      Object outobj=thism.invoke(c.cast(lio),new Object[] {} );
                      try {
                         sb.append("\n   " + mname.substring(3).toUpperCase() + " - " + outobj.toString());
                      } catch (Exception e) {
                         sb.append("\n   " + mname.substring(3).toUpperCase() + " - " + "NULL" );
                      }
                   }
                }

             }   
             return sb;
          } else {
             return new StringBuilder("No Records Found");
          }   
       } catch (Exception e) {
          return new StringBuilder(Stack2string.getString(e));
       }
    }

}

