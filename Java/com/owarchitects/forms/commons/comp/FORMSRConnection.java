 
/**
 *
 * This program is part of:
 *
 *    FORMS [Forms-Oriented Research Management System]
 *
 * Copyright (C) 2009 OpenWare Architects.  All Rights Reserved.
 *
 * FORMS is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * FORMS is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with FORMS.  If not, see <http://www.gnu.org/licenses/>.
 *
 */
 
/**
* FORMSRConnection.java -- returns R statistical software connection
*
* NOTE:  Currently this program is assuming no session management on the R/RServe side.  
* Rserve does support sessions, but I'm not sure if/how well it works on Windows servers.  
* Since FORMS is currently mainly supported in Windows environments, this program assumes
* that FORMS will need to maintain session information between R calls.  FORMS currently
* doesn't assume that any information from previous calls is maintained by R for a user.
*
*/

package com.owarchitects.forms.commons.comp;

import java.io.*;
import java.util.*;
import java.net.*;
import javax.servlet.*;
import javax.servlet.http.*;
import java.lang.Thread;
import org.apache.velocity.app.*;
import org.apache.velocity.*;
import org.rosuda.REngine.*;
import org.rosuda.REngine.Rserve.*;

public class FORMSRConnection extends FORMSResourceAccessor {

   public String outputText;
   public List images;

   // CONSTRUCTORS
   public FORMSRConnection(HttpServletRequest request,HttpServletResponse response) {

      super(request,response);
   
   }

   public void submitProgram(String progText) throws Exception {

      // initialize return objects
      images=new ArrayList();
      outputText="";

      // Get R-Configuration information
      FORMSConfig config=SessionObjectUtil.getConfigObj(getSession());
      String rServer=config.getRServer();
      Integer connectPort=new Integer(config.getRConnectPort());
      String rUserName=config.getRUserName();
      String rPassword=EncryptUtil.decryptString(config.getREncryptedPassword(),SessionObjectUtil.getInternalKeyObj(getSession()));

      Thread.sleep(10);

      if (rServer==null || rServer.length()<1 || connectPort==null) {
         outputText="ERROR:  R connection has not been configured.  Program cannot run";
      }

      RConnection rconn=new RConnection(rServer,connectPort);

      if (rUserName!=null && rUserName.length()>0 && rPassword!=null && rPassword.length()>0) {
         rconn.login(rUserName,rPassword);
      }

      FilenameFilter filter = new FilenameFilter() {
           public boolean accept(File dir, String name) {
               return name.toLowerCase().matches("r_outimage.*jpg$");
           }
      };

      // delete any pre-existing r image output
      String syswork=getAuth().getSyswork();
      File workdir=new File(syswork);
      File[] imgArray=workdir.listFiles(filter);
      Iterator i=Arrays.asList(imgArray).iterator();
      while (i.hasNext()) {
         File f=(File)i.next();
         f.delete();
      }

      // execute program statements

      try {

         // point image output to workdir
         if (syswork.indexOf("\\")>=0) {
            rconn.voidEval("jpeg(\"" + syswork.replace("\\","/") + "/" + "r_outimage_%03d.jpg\") \n");
         } else {
            rconn.voidEval("jpeg(\"" + syswork + "/" + "r_outimage_%03d.jpg\") \n");
         }

         // wrap program text in a function and call that function (Rserve likes to call one function at 
         // a time rather than run a series of calls)

         StringBuilder sb=new StringBuilder();
         sb.append("runReportProg <- function() { \n");
         sb.append(progText);
         sb.append("\n} \n");
         sb.append("paste(capture.output(print(runReportProg())),collapse=\"\\n\") \n");

         try {

            outputText=rconn.eval(sb.toString()).asString();

         } catch (Exception rex2) {

            outputText="ERROR:  Could not execute program request.  Either the R-server may not be available or there " +
                       "may be a problem with the login credentials FORMS is supplying.  Please contact your " +
                       "System Administrator.";

         }
         
         // I'm sure this is a problem on my end, but Rserve doesn't like to let go of the last image
         // so I'm just creating a final one.
         rconn.voidEval("dev.off() \n"); 
         rconn.voidEval("jpeg(\"c:/michael/junk/xxxxx.jpg\") \n");
         rconn.close();

         // Add images to image list
         images=new ArrayList();
         File[] imgNames=workdir.listFiles(filter);
         Arrays.sort(imgNames);
         i=Arrays.asList(imgNames).iterator();
         while (i.hasNext()) {
            File s=(File)i.next();
            images.add(s.getPath());
         }
         
         Thread.sleep(1000);

      } catch (Exception rex) {

         outputText="ERROR:  Could not execute program request.  Either the R-server may not be available or there " +
                    "may be a problem with the login credentials FORMS is supplying.  Please contact your " +
                    "System Administrator.";
      }
 
   }

   public String getTextOutput() {
      return outputText;
   }

   public List getImageOutput() {
      return images;
   }

}

