 
/**
 *
 * This program is part of:
 *
 *    FORMS [Forms-Oriented Research Management System]
 *
 * Copyright (C) 2009 OpenWare Architects.  All Rights Reserved.
 *
 * FORMS is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * FORMS is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with FORMS.  If not, see <http://www.gnu.org/licenses/>.
 *
 */
 
/**
* CkvarUtil.java -- Creates Comparison String Object to Verify Cookies
* Original Creation -- 08/2004 (MRH)
*/

package com.owarchitects.forms.commons.comp;

import java.util.*;
import java.math.*;
import java.io.*;
import org.apache.commons.io.FileUtils;

public class CkvarUtil {

   // Instances should not be created
   public CkvarUtil() {
      super();
   }

   // Compute value of ckvar
   protected static String getCkvarValue (String session,String user,String site,String study,String formname,String formtable) {
      // The following block prevents some problems
      if (session==null) session="";
      if (user==null) user="";
      if (site==null) site="";
      if (study==null) study="";
      if (formname==null) formname="";
      if (formtable==null) formtable="";
      // Eliminated sessionid from Ckvar as Service can cause problems as well as can expiring sessions. (MRH 04/19/2006)
      String all=session+user+site+study+formname+formtable+session+System.getProperty("java.io.tmpdir");
      byte[] b=all.getBytes();
      long total=0;
      for (int i = 0; i < (b.length)-1; i++) {
         total=total+(b[i]*i);
      }
      Random r=new Random(total);
      long f=Math.abs((r.nextLong()/3)+(r.nextLong()/3)+(r.nextLong()/3));
      return new Long(f).toString();

   }

}

