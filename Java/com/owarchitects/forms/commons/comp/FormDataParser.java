 
/**
 *
 * This program is part of:
 *
 *    FORMS [Forms-Oriented Research Management System]
 *
 * Copyright (C) 2009 OpenWare Architects.  All Rights Reserved.
 *
 * FORMS is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * FORMS is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with FORMS.  If not, see <http://www.gnu.org/licenses/>.
 *
 */
 
/**
* FormDataParser.java -- Parses form XML string, creating backup javascript string and creating
*                        an ArrayList containing variable names and values
* Original Creation -- 07/2008 (MRH)
*/

package com.owarchitects.forms.commons.comp;

import java.io.*;
import java.util.*;
import javax.servlet.http.*;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.io.FileUtils;
import org.apache.commons.lang.ArrayUtils.*;
import java.util.regex.*;
import org.apache.commons.httpclient.util.URIUtil;
import org.apache.commons.httpclient.URIException;

public class FormDataParser implements Serializable {

   private ArrayList fieldlist;
   private String backupjs;

   public FormDataParser(String instring,boolean createbackupjs) throws UnsupportedEncodingException,URIException {

      StringBuilder sb=new StringBuilder(instring);

      fieldlist=new ArrayList();
      StringBuilder sb_vname = new StringBuilder();
      StringBuilder sb_vvalue = new StringBuilder();
      String formPathVar=new String("XXX.YYY");
      char ch;
      int level=0;
      boolean captureName=false;
      boolean captureValue=true;

      for (int i=0;i<sb.length();i++) {
         ch = sb.charAt(i);
         if (ch=='<' && sb.charAt(i+1)!='/') {
            level++;
            sb_vname=new StringBuilder("");
            sb_vvalue=new StringBuilder("");
            captureName=true;
            captureValue=false;
         } else if (ch=='<') {
            // if sb_vname is missing then this is the end of a path field, so delete a block
            // from the path
            if (sb_vname.toString().trim().length()<1) {
               formPathVar=formPathVar.substring(0,formPathVar.lastIndexOf("."));
            }
            level--;
            captureValue=false;
            if (sb_vname.length()>=1) {
               HashMap fieldmap=new HashMap();
               fieldmap.put("fieldname",sb_vname.toString());
               fieldmap.put("fieldpath",formPathVar.substring(1));
               if (sb_vvalue.length()<1) {
                  // NOTE:  Need the space here for SAS use later
                  sb_vvalue=new StringBuilder(" ");
               }
		  // WORKAROUND FOR BUG IN ADOBE WHERE DROPDOWN LISTS, WHEN RETURNED TO BLANKS
		  // FILL THE EXPORT FILE WITH UNPRINTABLE CHARACTERS, UTF-8 WONT BE ABLE TO
		  // CONVERT ALL OF THEM, SO IT WONT BE EQUAL TO THE ISO-8859-1 ENCODING
		  if (!new String(sb_vvalue.toString().getBytes("UTF-8")).equals(
		       new String(sb_vvalue.toString().getBytes("ISO-8859-1")))
		      ) {
                  sb_vvalue=new StringBuilder(" ");
		  }
               fieldmap.put("fieldvalue",sb_vvalue.toString());
               fieldlist.add(fieldmap);
               sb_vname=new StringBuilder("");
               sb_vvalue=new StringBuilder("");
            } else {
               break;
            }
         } else if (ch=='>') {
            if (sb.charAt(i-1)!='/') {
               // LOCATE PATH TO VARIABLES 
               if (sb.charAt(i+1)=='\n' && captureName && !captureValue) {
                  formPathVar=formPathVar + "." + sb_vname; 
               }
               captureName=false;
               captureValue=true;
            } else {
               captureName=false;
               level--;
               if (sb_vname.length()>=1) {
                  HashMap fieldmap=new HashMap();
                  fieldmap.put("fieldname",sb_vname.toString());
                  fieldmap.put("fieldpath",formPathVar.substring(1));
                  if (sb_vvalue.length()<1) {
                     // NOTE:  Need the space here for SAS use later
                     sb_vvalue=new StringBuilder(" ");
                  }
		     // WORKAROUND FOR BUG IN ADOBE WHERE DROPDOWN LISTS, WHEN RETURNED TO BLANKS
		     // FILL THE EXPORT FILE WITH UNPRINTABLE CHARACTERS, UTF-8 WONT BE ABLE TO
		     // CONVERT ALL OF THEM, SO IT WONT BE EQUAL TO THE ISO-8859-1 ENCODING
		     if (!new String(sb_vvalue.toString().getBytes("UTF-8")).equals(
		          new String(sb_vvalue.toString().getBytes("ISO-8859-1")))
		         ) {
                     sb_vvalue=new StringBuilder(" ");
		     }
                  fieldmap.put("fieldvalue",sb_vvalue.toString());
                  fieldlist.add(fieldmap);
                  sb_vname=new StringBuilder("");
                  sb_vvalue=new StringBuilder("");
               } else {
                  break;
               }
            }
         } else if (captureName && ch!='/') {
            sb_vname.append(ch);
         } else if (captureValue) {
            sb_vvalue.append(ch);
         } 
      }

      //  CREATE STRINGBUFFER CONTAINING "BACK" JAVASCRIPT, SAVE AS AUTH VALUE

      // NOTE:  The resetData method call here is important.  When you go "BACK", it returns
      // you to the state when the form was first loaded.  If a value was entered and you
      // cleared it out, that would not be reflected when going back and the JavaScript only
      // contains code to set fields that have values (keeps it smaller).

      if (createbackupjs) {
         StringBuilder sb_sas=new StringBuilder("resetForm();\n");
         Iterator i=fieldlist.iterator();
         while (i.hasNext()) {
            HashMap map=(HashMap)i.next();
            if (((String)map.get("fieldname")).trim().length()>=1) {
               String encvalue=Base64.encode((String)map.get("fieldvalue"));
               if (encvalue.length()<100) {
                  sb_sas.append("try { setEncodedFormValue(getFormElementArrayById(\"" + (String)map.get("fieldname") + "\"),\"" + encvalue +
                                   "\"); } catch (e) {}\n" );
               } else { 
                  // Split long values onto multiple lines
                  ArrayList vlist2=new ArrayList();
                  while (1==1) {
                     String s;
                     try {
                        s=encvalue.substring(100);
                        encvalue=encvalue.substring(0,100);
                        vlist2.add("'" + encvalue + "' + \n");
                      } catch (Exception ee) {
                        vlist2.add("'" + encvalue + "' + \n");
                        break;
                      }
                      encvalue=s;
                  }
                  Iterator viter2=vlist2.iterator();
                  StringBuilder sb2=new StringBuilder();
                  while (viter2.hasNext()) {
                    String s=(String)viter2.next();
                    sb2.append(s);
                  }
                  encvalue=sb2.toString();
                  encvalue=encvalue.substring(0,encvalue.lastIndexOf("+"));
                  sb_sas.append("try { setEncodedFormValue(getFormElementArrayById(\"" + (String)map.get("fieldname") + "\")," + encvalue +
                                   "); } catch (e) {}\n" );
               }
            }
         }
         backupjs=sb_sas.toString();
      }

      // Process fieldlist to remove system values

      Iterator fiter=fieldlist.iterator();
      while (fiter.hasNext()) {

         HashMap fmap=(HashMap)fiter.next();
         String fname=(String)fmap.get("fieldname");
         if (
              fname.equalsIgnoreCase("dtdefid_") ||
              fname.equalsIgnoreCase("rmsPassValueField_") ||
              fname.equalsIgnoreCase("checkenrollform_") ||
              fname.toLowerCase().indexOf("global_")==0
            ) {
            fiter.remove();
         }

      }

   }
   
   // instring-only constructor.   createbackupjs defaults to true.
   public FormDataParser(String instring) throws UnsupportedEncodingException,URIException {
      this(instring,true);
   }

   public ArrayList getFieldList() {
      return fieldlist;
   }

   public String getBackupJs() {
      return backupjs;
   }


}

        
