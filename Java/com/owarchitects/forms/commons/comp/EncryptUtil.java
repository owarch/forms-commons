 
/**
 *
 * This program is part of:
 *
 *    FORMS [Forms-Oriented Research Management System]
 *
 * Copyright (C) 2009 OpenWare Architects.  All Rights Reserved.
 *
 * FORMS is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * FORMS is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with FORMS.  If not, see <http://www.gnu.org/licenses/>.
 *
 */
 
package com.owarchitects.forms.commons.comp;

import com.owarchitects.forms.commons.db.*;
import java.io.*;
import java.util.*;
import java.math.*;
import javax.crypto.*;
import javax.crypto.spec.*;
//import sun.security.x509.*;
//import sun.security.x509.*;
import org.bouncycastle.x509.*;
import org.bouncycastle.jce.provider.*;
import java.security.*;
import java.security.spec.RSAPublicKeySpec;
import javax.security.auth.x500.*;
import javax.security.cert.*;
import javax.servlet.http.HttpSession;
//import org.apache.axis2.rpc.client.RPCServiceClient;
//import org.apache.axis2.context.ServiceGroupContext;
import ro.tremend.upload.*;
import org.apache.commons.fileupload.FileItem;
import org.apache.commons.lang.StringUtils;


public class EncryptUtil {
   
   // Instances should not be created
   public EncryptUtil() {
      super();
   }

  /////////////////////////////////////
  // SYMMETRIC KEY RETRIEVAL METHODS //
  /////////////////////////////////////

  ////////////////////////
  // CONTROLLER METHODS //
  ////////////////////////

  // Retrieve secret key
  
  public static SecretKey getSecretKey(HttpSession session,String keyname,String passwd) throws FORMSException {

     File ksfile;
     KeyStore ks;
     SecretKey sk;

     try {
        ksfile=new File(session.getServletContext().getRealPath("/") + File.separator + SessionObjectUtil.getConfigObj(session).getKeyStore());
        if (!ksfile.exists()) {
           ksfile=new File(SessionObjectUtil.getConfigObj(session).getKeyStore());
        }
        if (!ksfile.exists()) {
           throw new FORMSException("Keystore file doesn't exist - check configuration file");
        }
        ks=java.security.KeyStore.getInstance("JCEKS");
        ks.load(new FileInputStream(ksfile),SessionObjectUtil.getConfigObj(session).getKeyStorePassword().toCharArray());
        if (!(ks.isKeyEntry(keyname))) {
           throw new FORMSNoEncryptKeyException();
        }
        try {
           // Verify can retrieve keys by password, if so, return public key
           sk=(SecretKey)ks.getKey(keyname,passwd.toCharArray());
           return sk;
        } catch (Exception np1) {
           throw new FORMSException(np1,"Could not retrieve secret key (password problem)?");
        }

     } catch (Exception e) {
        throw new FORMSException(e,"Could not retrieve secret key (keystore problem)?");
     } 

  }

  // Retrieve encryption key
  
  public static SecretKey getEncryptionKey(HttpSession session,String passwd) throws FORMSException {

     return getSecretKey(session,SessionObjectUtil.getConfigObj(session).getEncryptionKey(),passwd);
  
  }

  ///////////////////////////////////
  // Symmetric key creation method //
  ///////////////////////////////////
  
  public static SecretKey createSecretKey(HttpSession session,String kspassword,String keyname,String passwd) throws FORMSException {

     File ksfile;
     KeyStore ks;
     SecretKey sk;

     try {
        ksfile=new File(session.getServletContext().getRealPath("/") + File.separator + SessionObjectUtil.getConfigObj(session).getKeyStore());
        if (!ksfile.exists()) {
           ksfile=new File(SessionObjectUtil.getConfigObj(session).getKeyStore());
        }
        if (!ksfile.exists()) {
           throw new FORMSException("Keystore file doesn't exist - check configuration file");
        }
        ks=java.security.KeyStore.getInstance("JCEKS");
        ks.load(new FileInputStream(ksfile),SessionObjectUtil.getConfigObj(session).getKeyStorePassword().toCharArray());
        KeyGenerator kg = KeyGenerator.getInstance("DESede");
        sk=kg.generateKey();
        ks.load(null,kspassword.toCharArray());
        ks.setKeyEntry(keyname,sk,passwd.toCharArray(),null);
        FileOutputStream f = new FileOutputStream(ksfile);
        ks.store(f,kspassword.toCharArray());
        return sk;
     } catch (Exception e) {
        throw new FORMSException(e,"Could not create secret key " + keyname);
     } 

  }

  //////////////////////////////////////
  // ASYMMETRIC KEY RETRIEVAL METHODS //
  //////////////////////////////////////

  ////////////////////////
  // CONTROLLER METHODS //
  ////////////////////////

  // Retrieve (optionally create) public key

  public static PublicKey getPublicKey(HttpSession session,String user,String passwd) throws FORMSException {

     File ksfile;
     KeyStore ks;
     PublicKey pubkey;
     PrivateKey privkey;

     if (passwd==null) passwd="private";

     try {

        ksfile=new File(session.getServletContext().getRealPath("/") + File.separator + SessionObjectUtil.getConfigObj(session).getKeyStore());
        if (!ksfile.exists()) {
           ksfile=new File(SessionObjectUtil.getConfigObj(session).getKeyStore());
        }
        if (!ksfile.exists()) {
           throw new FORMSException("Keystore file doesn't exist - check configuration file");
        }
        ks=java.security.KeyStore.getInstance("JCEKS");
        ks.load(new FileInputStream(ksfile),SessionObjectUtil.getConfigObj(session).getKeyStorePassword().toCharArray());
        if (!(ks.isKeyEntry(user + "_public") && ks.isKeyEntry(user + "_private"))) {
           createKeys(ks,SessionObjectUtil.getConfigObj(session).getKeyStorePassword(),ksfile,user,passwd);
        }
        try {
           // Verify can retrieve keys by password, if so, return public key
           pubkey=(PublicKey)ks.getKey(user + "_public","public".toCharArray());
           try {
              privkey=(PrivateKey)ks.getKey(user + "_private",passwd.toCharArray());
           } catch (Exception ok1) {
              // Do nothing - Cannot retrieve if password not passed
           }
           return pubkey;
        } catch (Exception np1) {
           try {
              pubkey=(PublicKey)ks.getKey(user + "_public","public".toCharArray());
              privkey=(PrivateKey)ks.getKey(user + "_private","private".toCharArray());
              FileOutputStream f = new FileOutputStream(ksfile);
              ks.setKeyEntry(user + "_private",privkey,passwd.toCharArray(),getCertChain());
              ks.store(f,SessionObjectUtil.getConfigObj(session).getKeyStorePassword().toCharArray());
              privkey=(PrivateKey)ks.getKey(user + "_private",passwd.toCharArray());
              return pubkey;
           } catch (Exception np2) {
              throw new FORMSException(np2,"Could not retrieve private key (password problem)?");
           } 
        }

     } catch (Exception e) {
        throw new FORMSException(e,"Could not retrieve private key (keystore problem)?");
     } 

  }

  public static PublicKey getPublicKey(HttpSession session,String user) throws FORMSException {

     return getPublicKey(session,user,null);

  }

  public static PrivateKey getPrivateKey(HttpSession session,String user,String passwd) throws FORMSException {

     File ksfile;
     KeyStore ks;
     PublicKey pubkey;
     PrivateKey privkey;

     if (passwd==null) passwd="private";

     try {

        ksfile=new File(session.getServletContext().getRealPath("/") + File.separator + SessionObjectUtil.getConfigObj(session).getKeyStore());
        if (!ksfile.exists()) {
           ksfile=new File(SessionObjectUtil.getConfigObj(session).getKeyStore());
        }
        if (!ksfile.exists()) {
           throw new FORMSException("Keystore file doesn't exist - check configuration file");
        }
        ks=java.security.KeyStore.getInstance("JCEKS");
        ks.load(new FileInputStream(ksfile),SessionObjectUtil.getConfigObj(session).getKeyStorePassword().toCharArray());
        if (!(ks.isKeyEntry(user + "_public") && ks.isKeyEntry(user + "_private"))) {
           createKeys(ks,SessionObjectUtil.getConfigObj(session).getKeyStorePassword(),ksfile,user,passwd);
        }
        try {
           // Verify can retrieve keys by password, if so, return public key
           pubkey=(PublicKey)ks.getKey(user + "_public","public".toCharArray());
           privkey=(PrivateKey)ks.getKey(user + "_private",passwd.toCharArray());
           return privkey;
        } catch (Exception np1) {
           try {
              pubkey=(PublicKey)ks.getKey(user + "_public","public".toCharArray());
              privkey=(PrivateKey)ks.getKey(user + "_private","private".toCharArray());
              FileOutputStream f = new FileOutputStream(ksfile);
              ks.setKeyEntry(user + "_private",privkey,passwd.toCharArray(),getCertChain());
              ks.store(f,SessionObjectUtil.getConfigObj(session).getKeyStorePassword().toCharArray());
              privkey=(PrivateKey)ks.getKey(user + "_private",passwd.toCharArray());
              return privkey;
           } catch (Exception np2) {
              throw new FORMSException(np2,"Could not retrieve public key (password problem)?");
           } 
        }

     } catch (Exception e) {
        throw new FORMSException(e,"Could not retrieve public key (keystore problem)?" + SessionObjectUtil.getConfigObj(session).getKeyStorePassword());
     } 

  }

  public static PrivateKey getPrivateKey(HttpSession session,String user) throws FORMSException {

     return getPrivateKey(session,user,null);

  }


  public static String exportPublicKey(HttpSession session,String keyname) throws FORMSException {

     File ksfile;
     KeyStore ks;
     PublicKey pubkey;

     try {

        ksfile=new File(session.getServletContext().getRealPath("/") + File.separator + SessionObjectUtil.getConfigObj(session).getKeyStore());
        if (!ksfile.exists()) {
           throw new FORMSException("Keystore file doesn't exist - check configuration file");
        }
        ks=java.security.KeyStore.getInstance("JCEKS");
        ks.load(new FileInputStream(ksfile),SessionObjectUtil.getConfigObj(session).getKeyStorePassword().toCharArray());
        if (!(ks.isKeyEntry(keyname))) {
           throw new FORMSException("Key " + keyname + " doesn't exist");
        }
        try {
           // Verify can retrieve keys by password, if so, return public key
           pubkey=(PublicKey)ks.getKey(keyname,"public".toCharArray());
           return pubkey.toString();
        } catch (Exception np1) {
           throw new FORMSException(np1,"Could not retrieve private key");
        }

     } catch (Exception e) {
        throw new FORMSException(e,"Could not retrieve private key (keystore problem)?");
     } 

  }


  //////////
  //////////
  //////////
  //////////

  // Certificate chain required for storing private keys.  Not important for our
  // purposes, so we create simple self-signed chain for this purpose
  private static java.security.cert.Certificate[] getCertChain() throws FORMSException {
    
     try {

        Security.addProvider(new BouncyCastleProvider());

        KeyPairGenerator kpg=KeyPairGenerator.getInstance("RSA");
        kpg.initialize(1024);
        KeyPair kp=kpg.generateKeyPair();
                
        X509V1CertificateGenerator gc = new X509V1CertificateGenerator();
        X500Principal x500=new X500Principal("CN=FORMS Certificate");
        
        gc.setSerialNumber(BigInteger.valueOf(1));
        gc.setIssuerDN(x500);
        gc.setNotBefore(new Date(new Long(0).longValue()));
        gc.setNotAfter(new Date(new Long(1000*60*60*24*365*100).longValue()));
        gc.setSubjectDN(x500); 
        gc.setPublicKey(kp.getPublic());
        gc.setSignatureAlgorithm("MD5WithRSAEncryption");
        
        java.security.cert.X509Certificate cert = gc.generate(kp.getPrivate(), "BC");

        java.security.cert.Certificate[] chain = new java.security.cert.X509Certificate[1];
        chain[0] = cert;

        //
        // NOTE:  The following SUN-generated certificate creation code has been replaced
        //        by the above BouncyCastle creation code, to remove complation warnings
        //        about the possible removal of associated proprietary code from JRE.
        //
        /*
        // Generate certificate chain (required for storing PrivateKeys)
        CertAndKeyGen cakg = new CertAndKeyGen("RSA", "MD5WithRSA");
        cakg.generate(1024);
        PublicKey publicKey = cakg.getPublicKey();
        PrivateKey privateKey = cakg.getPrivateKey();
        X500Name name = new X500Name("FORMS", "FORMS", "FORMS", "FORMS", "FORMS", "FORMS");
        java.security.cert.Certificate[] chain = new java.security.cert.X509Certificate[1];
        chain[0] = cakg.getSelfCertificate(name, 999999999);
        */

        return chain;

     } catch (Exception e) {
        throw new FORMSException(e,"Error creating certificate chain");
     }

  }

  // NOTE: Replaces existing like-named keys
  public static void createKeyPair(HttpSession session,String keyname,String keypass) throws FORMSException {

     File ksfile;
     KeyStore ks;

     if (keypass==null) keypass="private";

     try {

        ksfile=new File(session.getServletContext().getRealPath("/") + File.separator + SessionObjectUtil.getConfigObj(session).getKeyStore());
        if (!ksfile.exists()) {
           throw new FORMSException("Keystore file doesn't exist - check configuration file");
        }
        ks=java.security.KeyStore.getInstance("JCEKS");
        ks.load(new FileInputStream(ksfile),SessionObjectUtil.getConfigObj(session).getKeyStorePassword().toCharArray());
        if (ks.isKeyEntry(keyname + "_public")) {
           ks.deleteEntry(keyname + "_public");
        }
        if (ks.isKeyEntry(keyname + "_private")) {
           ks.deleteEntry(keyname + "_private");
        }
        createKeys(ks,SessionObjectUtil.getConfigObj(session).getKeyStorePassword(),ksfile,keyname,keypass);

        PrivateKey testkey=(PrivateKey)ks.getKey(keyname + "_private",keypass.toCharArray());

     } catch (Exception nc) {
        throw new FORMSException("Could not create key pair");
     }

  }
  
  private static void createKeys(KeyStore ks,String kspassword,File ksfile,String keyname,String passwd) {

     try {

        KeyPairGenerator kpg=KeyPairGenerator.getInstance("RSA");
        kpg.initialize(1024);
        KeyPair kp=kpg.generateKeyPair();
        PublicKey pubkey=kp.getPublic();
        PrivateKey privkey=kp.getPrivate();
        ks.load(null,kspassword.toCharArray());
        ks.setKeyEntry(keyname + "_private",privkey,passwd.toCharArray(),getCertChain());
        ks.setKeyEntry(keyname + "_public",pubkey,"public".toCharArray(),null);
        FileOutputStream f = new FileOutputStream(ksfile);
        ks.store(f,kspassword.toCharArray());
     
     } catch (Exception e) { }

  }

  // NOTE: Import public key from FORMS exported string
  public static void importPublicKey(HttpSession session,String keystring,String keyname) throws FORMSException {

     try {

        File ksfile=new File(session.getServletContext().getRealPath("/") + File.separator + SessionObjectUtil.getConfigObj(session).getKeyStore());
        if (!ksfile.exists()) {
           throw new FORMSException("Keystore file doesn't exist - check configuration file");
        }

        FORMSRSAPublicKeySpec kspec=new FORMSRSAPublicKeySpec(keystring);
        KeyFactory rSAKeyFactory = KeyFactory.getInstance("RSA");
        PublicKey pubkey = rSAKeyFactory.generatePublic(new RSAPublicKeySpec(kspec.getModulus(),kspec.getPublicExponent()));

        KeyStore ks=java.security.KeyStore.getInstance("JCEKS");
        ks.load(new FileInputStream(ksfile),SessionObjectUtil.getConfigObj(session).getKeyStorePassword().toCharArray());
        ks.setKeyEntry(keyname + "_public",pubkey,"public".toCharArray(),null);
        FileOutputStream f = new FileOutputStream(ksfile);
        ks.store(f,SessionObjectUtil.getConfigObj(session).getKeyStorePassword().toCharArray());

     } catch (Exception e) {
        throw new FORMSException(e,"Could not import public key");
     } 

  }



  ///////////////////////////////////////////
  // Symmetric Key encrypt/decrypt methods //
  ///////////////////////////////////////////

  public static String encryptString(String ins, SecretKey key,
      String xform) throws Exception {

    if (ins==null || ins.length()<1) {
       return "";
    }

    byte[] b=ins.getBytes("ISO-8859-1");  
    Cipher cipher = Cipher.getInstance(xform);
    cipher.init(Cipher.ENCRYPT_MODE, key);
    return new String(Base64.encode(cipher.doFinal(b)),"ISO-8859-1");

  }

  public static String encryptString(String ins, SecretKey key) throws Exception {

     // Default is DESede
     return encryptString(ins,key,"DESede");

  }

  public static String decryptString(String ins, SecretKey key,
      String xform) throws Exception{

    if (ins==null || ins.length()<1) {
       return "";
    }

    byte[] b=Base64.decode(ins.getBytes("ISO-8859-1"));  
    Cipher cipher = Cipher.getInstance(xform);
    cipher.init(Cipher.DECRYPT_MODE, key);
    return new String(cipher.doFinal(b),"ISO-8859-1");

  }

  public static String decryptString(String ins, SecretKey key) throws Exception {

     // Default is DESede
     return decryptString(ins,key,"DESede");

  }


  ////////////////////////////////////////////
  // Asymmetric Key encrypt/decrypt methods //
  ////////////////////////////////////////////


  public static String encryptString(String ins, Key key,
      String xform) throws Exception {

    if (ins==null || ins.length()<1) {
       return "";
    }

    byte[] b=ins.getBytes("ISO-8859-1");  
    Cipher cipher = Cipher.getInstance(xform);
    cipher.init(Cipher.ENCRYPT_MODE, key);
    // NOTE:  returns an encoded String
    return new String(Base64.encode(cipher.doFinal(b)),"ISO-8859-1");

  }

  public static String encryptString(String ins, Key key) throws Exception {

     // Default is RSA
     return encryptString(ins,key,"RSA");


  }

  // Encrypt long string with asymmetric encryption
  public static String encryptLongString(String ins, Key key, 
     String xform) throws Exception {
     StringBuilder sb=new StringBuilder();
     for (int i=1;i<10000;i++) {
        String next=StringUtils.substring(ins,(100*i)-100,100*i);
        if (next.length()>0) {
           sb.append(encryptString(next,key,xform));
           if (i==10000) {
              throw new FORMSException("String too long to encrypt");
           }
        } else {
           break;
        }
     }
     return sb.toString();
  }   

  public static String encryptLongString(String ins, Key key) throws Exception {

     // Default is RSA
     return encryptLongString(ins,key,"RSA");

  }

  public static String decryptString(String ins, Key key,
      String xform) throws Exception{

    if (ins==null || ins.length()<1) {
       return "";
    }

    byte[] b=Base64.decode(ins.getBytes("ISO-8859-1"));  
    Cipher cipher = Cipher.getInstance(xform);
    cipher.init(Cipher.DECRYPT_MODE, key);
    return new String(cipher.doFinal(b),"ISO-8859-1");

  }

  public static String decryptString(String ins, Key key) throws Exception {

     // Default is RSA
     return decryptString(ins,key,"RSA");

  }

  // Decrypt long string with asymmetric encryption
  public static String decryptLongString(String ins, Key key,
      String xform) throws Exception{

    StringBuilder sb=new StringBuilder();
    String[] ina=ins.split("==*");
    for (int i=0;i<ina.length;i++) {
       if (ina[i].length()>0) {
          sb.append(decryptString(ina[i] + "=",key,xform));
       }
    }
    return sb.toString();

  }

  public static String decryptLongString(String ins, Key key) throws Exception {

     // Default is RSA
     return decryptLongString(ins,key,"RSA");

  }

  public static PBEInfo pbeEncrypt(String ins,Key key,String xform) throws Exception {

     // create PBE password
     SecureRandom random = new SecureRandom();
     byte bytes[] = new byte[15];
     random.nextBytes(bytes);
     String pbepw=new String(Base64.encode(bytes),"ISO-8859-1");

     // Encrypt message using PBE password
     PBEKeySpec pbeKeySpec = new PBEKeySpec(pbepw.toCharArray());
     SecretKeyFactory keyFactory = SecretKeyFactory.getInstance("PBEWithSHA1AndDESede");
     SecretKey pbeKey = keyFactory.generateSecret(pbeKeySpec);
     PBEParameterSpec pbeParamSpec = new PBEParameterSpec("formssalt".getBytes("ISO-8859-1"), 30);
     Cipher cipher = Cipher.getInstance("PBEWithSHA1AndDESede");
     cipher.init(Cipher.ENCRYPT_MODE, pbeKey, pbeParamSpec);
     byte[] encryptedBytes = cipher.doFinal(ins.getBytes("ISO-8859-1"));
     String outstr=new String(encryptedBytes,"ISO-8859-1");

     // Encrypt PBE password using local site site key
     String outpw=encryptString(pbepw,key,xform);

     PBEInfo pbeout=new PBEInfo();
     // NOTE:  returns an UNENCODED String
     pbeout.setPassword(outpw);
     pbeout.setContent(outstr);
     return pbeout;

  }   

  public static PBEInfo pbeEncrypt(String ins,Key key) throws Exception {

     // Default is RSA
     return pbeEncrypt(ins,key,"RSA");

  }   

  public static String pbeDecrypt(PBEInfo pbein,Key key,String xform) throws Exception {

     String strin=pbein.getContent();
     String pwin=pbein.getPassword();

     // Decrypt PBE password using local site site key
     String pbepw=decryptString(pwin,key,xform);

     // Decrypt message using PBE password
     PBEKeySpec pbeKeySpec = new PBEKeySpec(pbepw.toCharArray());
     SecretKeyFactory keyFactory = SecretKeyFactory.getInstance("PBEWithSHA1AndDESede");
     SecretKey pbeKey = keyFactory.generateSecret(pbeKeySpec);
     PBEParameterSpec pbeParamSpec = new PBEParameterSpec("formssalt".getBytes("ISO-8859-1"), 30);
     Cipher cipher = Cipher.getInstance("PBEWithSHA1AndDESede");
     cipher.init(Cipher.DECRYPT_MODE, pbeKey, pbeParamSpec);
     byte[] decryptedBytes = cipher.doFinal(strin.getBytes("ISO-8859-1"));
     String outstr=new String(decryptedBytes,"ISO-8859-1");

     return outstr;

  }   

  public static String pbeDecrypt(PBEInfo pbein,Key key) throws Exception {

     // Default is RSA
     return pbeDecrypt(pbein,key,"RSA");

  }   

  public static String serviceCheckEncrypt(HttpSession session,Linkedinst inst) throws FORMSException {

     try {

        File ksfile=new File(session.getServletContext().getRealPath("/") + File.separator + SessionObjectUtil.getConfigObj(session).getKeyStore());
        if (!ksfile.exists()) {
           throw new FORMSException("Keystore file doesn't exist - check configuration file");
        }
        KeyStore ks=java.security.KeyStore.getInstance("JCEKS");
        ks.load(new FileInputStream(ksfile),SessionObjectUtil.getConfigObj(session).getKeyStorePassword().toCharArray());
        // Verify can retrieve keys by password, if so, return public key
        PrivateKey privkey;
        if (inst!=null) {
           privkey=(PrivateKey)ks.getKey("inst_loc_" + inst.getAcronym() + "_private","private".toCharArray());
        } else {
           return null;
        }
        return encryptString(FORMSServiceConstants.CHECK_STRING,privkey);

     } catch (Exception e) {
        throw new FORMSException(e,"FORMSService Encryption Problem");
     } 

  }

  public static boolean serviceCheckDecrypt(HttpSession session,Linkedinst inst,String ins) throws FORMSException {

     try {

        File ksfile=new File(session.getServletContext().getRealPath("/") + File.separator + SessionObjectUtil.getConfigObj(session).getKeyStore());
        if (!ksfile.exists()) {
           return false;
        }
        KeyStore ks=java.security.KeyStore.getInstance("JCEKS");
        ks.load(new FileInputStream(ksfile),SessionObjectUtil.getConfigObj(session).getKeyStorePassword().toCharArray());
        // Verify can retrieve keys by password, if so, return public key
        PublicKey pubkey;
        if (inst!=null) {
           pubkey=(PublicKey)ks.getKey("inst_rem_" + inst.getAcronym() + "_public","public".toCharArray());
        } else {
           return true;
        }
        String ds=decryptString(ins,pubkey);
        if (ds.equals(FORMSServiceConstants.CHECK_STRING)) {
           return true;
        } else {
           return false;
        }

     } catch (Exception e) {
        return false;
     } 

  }

}

        
