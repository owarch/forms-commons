 
/**
 *
 * This program is part of:
 *
 *    FORMS [Forms-Oriented Research Management System]
 *
 * Copyright (C) 2009 OpenWare Architects.  All Rights Reserved.
 *
 * FORMS is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * FORMS is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with FORMS.  If not, see <http://www.gnu.org/licenses/>.
 *
 */
 
/*
 *
 * IframeReloaderController.java - Controller used to display progressive IFRAME output.  
 *                                                                  MRH 01/08/2007
 *
 */

package com.owarchitects.forms.commons.comp;

import com.owarchitects.forms.commons.db.*;
import java.util.*;
import java.io.*;
import java.beans.*;
import java.lang.reflect.*;
import org.springframework.web.servlet.*;
import org.springframework.web.servlet.mvc.*;
import org.springframework.web.context.support.*;
import javax.servlet.http.*;

public class IframeReloaderController extends FORMSMinimalController {

   // Don't need to perform auth checking
   {
      autoAuthCheckOff();
   }

   public ModelAndView submitRequest() throws FORMSException {

      try {
   
         ModelAndView mav=new ModelAndView();

         // Retrieve results object from session
         Object o=this.getSession().getAttribute("iframeresult");
         Object p=this.getSession().getAttribute("prevresult");

         if (o!=null) {

            // Loop until change in results object (Maximum 15 seconds)
            for (int i=1;i<=75;i++) {

               if (p!=null && o.equals(p)) {

                  // Wait for object to update.  Don't need to repull object as o is the actual object
                  // rather than a copy of it.
                  Thread.sleep(200);

               } else if (p!=null && ObjectXmlUtil.objectToXmlString(o).equals(ObjectXmlUtil.objectToXmlString(p))) {

                  // Cloned objects not guaranteed to match on equals and in testing they
                  // weren't, even though string representations did match, so we go
                  // through this extra comparison of xml string representations
                  Thread.sleep(200);

               } else {

                  break;

               }

            }   

            // Retrieve beanname and method to be invoked from input object, create method object
            Class ic=o.getClass();
            Method im;

            im=ic.getMethod("addAllToMAV",new Class[] { ModelAndView.class });
            // Merge parameter values into MAV & return MAV
            Object tmp=(String)im.invoke(o,new Object[] { mav });

            try {

               // return clone of object (results objects implement Cloneable)
               im=ic.getMethod("clone",new Class[] { });
               // Merge parameter values into MAV & return MAV
               Object prev=im.invoke(o,new Object[] { });
               this.getSession().setAttribute("prevresult",prev);

            } catch (Exception CloneNotSupportedException) {

               // otherwise return link to object (will cause o to always equal p, so refresh rate
               // will always be 5 seconds)
               this.getSession().setAttribute("prevresult",o);

            }

            return mav;

         } else {

            mav.addObject("status","NULL");
            return mav;

         }
   
      } catch (Exception e) { 
         throw new FORMSException("FORMS Controller exception:  " + Stack2string.getString(e));
      }

   }

}

