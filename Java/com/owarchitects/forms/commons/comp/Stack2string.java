 
/**
 *
 * This program is part of:
 *
 *    FORMS [Forms-Oriented Research Management System]
 *
 * Copyright (C) 2009 OpenWare Architects.  All Rights Reserved.
 *
 * FORMS is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * FORMS is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with FORMS.  If not, see <http://www.gnu.org/licenses/>.
 *
 */
 

/**
* stack2string - convert exception stack to string
* Original Creation:  02/2005 (MRH)
*/

package com.owarchitects.forms.commons.comp;

import java.io.*;
import java.util.*;
//import com.adobe.fdf.*;
//import com.adobe.fdf.exceptions.*;
//import javax.servlet.*;
//import javax.servlet.http.*;
//import org.apache.velocity.app.*;
//import org.apache.velocity.*;
//import java.sql.DriverManager;
//import java.sql.Connection;
//import java.sql.Statement;
//import java.sql.ResultSet;
//import java.sql.ResultSetMetaData;
//import java.sql.SQLException;

public class Stack2string {
   
   // Instances should not be created
   public Stack2string() {
      super();
   }

   public static String getString(Throwable e) {
      try {
         StringWriter sw = new StringWriter();
         PrintWriter pw = new PrintWriter(sw);
         e.printStackTrace(pw);
         return "------\r\n" + sw.toString() + "------\r\n";
      } catch(Exception e2) {
         return "bad Stack2string";
      }
   }

}


