 
/**
 *
 * This program is part of:
 *
 *    FORMS [Forms-Oriented Research Management System]
 *
 * Copyright (C) 2009 OpenWare Architects.  All Rights Reserved.
 *
 * FORMS is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * FORMS is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with FORMS.  If not, see <http://www.gnu.org/licenses/>.
 *
 */
 
/**
 * CsaFormRecordProcessorParms.java -- 
 *                           involving Service data transfers.
 * Original Creation -- 01/2008 (MRH)
 */

package com.owarchitects.forms.commons.comp;

import java.io.*;
import java.util.*;

public class CsaFormRecordProcessorParms extends FORMSCsaServiceParms implements Serializable {
 
   // NOTE:  Setting both mappingclassname and querystring is unneccesary
   //  mappingclassname is used for getTable
   //  querystring is used for execQuery

   // shared parameters

   public String datastring;
   public Long dtdefid;
   public Long auserid;

   // 

   public void setDatastring(String datastring) {
      this.datastring=datastring;
   }

   public String getDatastring() {
      return datastring;
   }

   // 

   public void setDtdefid(Long dtdefid) {
      this.dtdefid=dtdefid;
   }

   public Long getDtdefid() {
      return dtdefid;
   }

   // 

   public void setAuserid(Long auserid) {
      this.auserid=auserid;
   }

   public Long getAuserid() {
      return auserid;
   }

}

