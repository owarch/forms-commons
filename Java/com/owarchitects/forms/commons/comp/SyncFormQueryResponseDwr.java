 
/**
 *
 * This program is part of:
 *
 *    FORMS [Forms-Oriented Research Management System]
 *
 * Copyright (C) 2009 OpenWare Architects.  All Rights Reserved.
 *
 * FORMS is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * FORMS is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with FORMS.  If not, see <http://www.gnu.org/licenses/>.
 *
 */
 
package com.owarchitects.forms.commons.comp;

import com.owarchitects.forms.commons.db.*;
import java.io.*;
import java.net.*;
//import java.util.*;
//import java.text.*;
//import org.apache.regexp.RE;
//import org.apache.commons.lang.StringUtils;
//import org.apache.commons.io.FileUtils;
//import org.apache.commons.lang.ArrayUtils.*;
//import java.util.regex.*;
//import javax.crypto.*;
//import javax.crypto.spec.*;
//import java.security.*;
//import java.security.spec.*;
//import uk.ltd.getahead.dwr.WebContext;
//import uk.ltd.getahead.dwr.WebContextFactory;
//import javax.servlet.*;
//import javax.servlet.http.*;
//import org.apache.velocity.app.*;
//import org.apache.velocity.*;
//import org.apache.commons.httpclient.util.URIUtil;


public class SyncFormQueryResponseDwr extends FORMSDwr {

   // Main record submit function
   public void processQueryResponse(String inString) {
    
      try {

         Socket socket;
         try {
            socket=new Socket("localhost",54444);
         } catch (Exception e) {
            return;
         }
         PrintStream os=new PrintStream(socket.getOutputStream());
         BufferedReader is = new BufferedReader(new InputStreamReader(socket.getInputStream()));
         int c;
         while (is.readLine()!=null) {
            os.println(inString);
            os.flush();
         }
         return;

      } catch (java.io.IOException e) {
         return;
      }

   } 

}


