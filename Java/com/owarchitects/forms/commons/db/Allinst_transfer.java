package com.owarchitects.forms.commons.db;

import java.io.Serializable;
import java.util.Date;
import org.apache.commons.lang.builder.ToStringBuilder;


/** @author Hibernate CodeGenerator */
public class Allinst_transfer implements Serializable {

    /** identifier field */
    private long ainstid;

    /** nullable persistent field */
    private Boolean islocal;

    /** nullable persistent field */
    private Date recmodtime;

    /** nullable persistent field */
    private Long linstid;

    /** full constructor */
    public Allinst_transfer(long ainstid, Boolean islocal, Date recmodtime, Long linstid) {
        this.ainstid = ainstid;
        this.islocal = islocal;
        this.recmodtime = recmodtime;
        this.linstid = linstid;
    }

    /** default constructor */
    public Allinst_transfer() {
    }

    /** minimal constructor */
    public Allinst_transfer(long ainstid) {
        this.ainstid = ainstid;
    }

    public long getAinstid() {
        return this.ainstid;
    }

    public void setAinstid(long ainstid) {
        this.ainstid = ainstid;
    }

    public Boolean getIslocal() {
        return this.islocal;
    }

    public void setIslocal(Boolean islocal) {
        this.islocal = islocal;
    }

    public Date getRecmodtime() {
        return this.recmodtime;
    }

    public void setRecmodtime(Date recmodtime) {
        this.recmodtime = recmodtime;
    }

    public Long getLinstid() {
        return this.linstid;
    }

    public void setLinstid(Long linstid) {
        this.linstid = linstid;
    }

    public String toString() {
        return new ToStringBuilder(this)
            .append("ainstid", getAinstid())
            .toString();
    }

}
