package com.owarchitects.forms.commons.db;

import java.io.Serializable;
import java.util.Date;
import org.apache.commons.lang.builder.ToStringBuilder;


/** @author Hibernate CodeGenerator */
public class Userinfomodtime implements Serializable {

    /** identifier field */
    private long aumtid;

    /** nullable persistent field */
    private Date remotemod;

    /** nullable persistent field */
    private Date recmodtime;

    /** nullable persistent field */
    private com.owarchitects.forms.commons.db.Linkedinst linkedinst;

    /** full constructor */
    public Userinfomodtime(Date remotemod, Date recmodtime, com.owarchitects.forms.commons.db.Linkedinst linkedinst) {
        this.remotemod = remotemod;
        this.recmodtime = recmodtime;
        this.linkedinst = linkedinst;
    }

    /** default constructor */
    public Userinfomodtime() {
    }

    public long getAumtid() {
        return this.aumtid;
    }

    public void setAumtid(long aumtid) {
        this.aumtid = aumtid;
    }

    public Date getRemotemod() {
        return this.remotemod;
    }

    public void setRemotemod(Date remotemod) {
        this.remotemod = remotemod;
    }

    public Date getRecmodtime() {
        return this.recmodtime;
    }

    public void setRecmodtime(Date recmodtime) {
        this.recmodtime = recmodtime;
    }

    public com.owarchitects.forms.commons.db.Linkedinst getLinkedinst() {
        return this.linkedinst;
    }

    public void setLinkedinst(com.owarchitects.forms.commons.db.Linkedinst linkedinst) {
        this.linkedinst = linkedinst;
    }

    public String toString() {
        return new ToStringBuilder(this)
            .append("aumtid", getAumtid())
            .toString();
    }

}
