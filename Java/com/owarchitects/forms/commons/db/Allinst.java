package com.owarchitects.forms.commons.db;

import java.io.Serializable;
import java.util.Date;
import java.util.Set;
import org.apache.commons.lang.builder.ToStringBuilder;


/** @author Hibernate CodeGenerator */
public class Allinst implements Serializable {

    /** identifier field */
    private long ainstid;

    /** nullable persistent field */
    private Boolean islocal;

    /** nullable persistent field */
    private Date recmodtime;

    /** nullable persistent field */
    private com.owarchitects.forms.commons.db.Linkedinst linkedinst;

    /** persistent field */
    private Set allusers;

    /** persistent field */
    private Set datatabledef;

    /** persistent field */
    private Set formtypelist;

    /** full constructor */
    public Allinst(Boolean islocal, Date recmodtime, com.owarchitects.forms.commons.db.Linkedinst linkedinst, Set allusers, Set datatabledef, Set formtypelist) {
        this.islocal = islocal;
        this.recmodtime = recmodtime;
        this.linkedinst = linkedinst;
        this.allusers = allusers;
        this.datatabledef = datatabledef;
        this.formtypelist = formtypelist;
    }

    /** default constructor */
    public Allinst() {
    }

    /** minimal constructor */
    public Allinst(Set allusers, Set datatabledef, Set formtypelist) {
        this.allusers = allusers;
        this.datatabledef = datatabledef;
        this.formtypelist = formtypelist;
    }

    public long getAinstid() {
        return this.ainstid;
    }

    public void setAinstid(long ainstid) {
        this.ainstid = ainstid;
    }

    public Boolean getIslocal() {
        return this.islocal;
    }

    public void setIslocal(Boolean islocal) {
        this.islocal = islocal;
    }

    public Date getRecmodtime() {
        return this.recmodtime;
    }

    public void setRecmodtime(Date recmodtime) {
        this.recmodtime = recmodtime;
    }

    public com.owarchitects.forms.commons.db.Linkedinst getLinkedinst() {
        return this.linkedinst;
    }

    public void setLinkedinst(com.owarchitects.forms.commons.db.Linkedinst linkedinst) {
        this.linkedinst = linkedinst;
    }

    public Set getAllusers() {
        return this.allusers;
    }

    public void setAllusers(Set allusers) {
        this.allusers = allusers;
    }

    public Set getDatatabledef() {
        return this.datatabledef;
    }

    public void setDatatabledef(Set datatabledef) {
        this.datatabledef = datatabledef;
    }

    public Set getFormtypelist() {
        return this.formtypelist;
    }

    public void setFormtypelist(Set formtypelist) {
        this.formtypelist = formtypelist;
    }

    public String toString() {
        return new ToStringBuilder(this)
            .append("ainstid", getAinstid())
            .toString();
    }

}
