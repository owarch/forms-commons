package com.owarchitects.forms.commons.db;

import java.io.Serializable;
import java.util.Date;
import org.apache.commons.lang.builder.ToStringBuilder;


/** @author Hibernate CodeGenerator */
public class Disallowedfileextensions implements Serializable {

    /** identifier field */
    private long disfileextid;

    /** persistent field */
    private String extension;

    /** persistent field */
    private String description;

    /** nullable persistent field */
    private Date recmodtime;

    /** full constructor */
    public Disallowedfileextensions(String extension, String description, Date recmodtime) {
        this.extension = extension;
        this.description = description;
        this.recmodtime = recmodtime;
    }

    /** default constructor */
    public Disallowedfileextensions() {
    }

    /** minimal constructor */
    public Disallowedfileextensions(String extension, String description) {
        this.extension = extension;
        this.description = description;
    }

    public long getDisfileextid() {
        return this.disfileextid;
    }

    public void setDisfileextid(long disfileextid) {
        this.disfileextid = disfileextid;
    }

    public String getExtension() {
        return this.extension;
    }

    public void setExtension(String extension) {
        this.extension = extension;
    }

    public String getDescription() {
        return this.description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Date getRecmodtime() {
        return this.recmodtime;
    }

    public void setRecmodtime(Date recmodtime) {
        this.recmodtime = recmodtime;
    }

    public String toString() {
        return new ToStringBuilder(this)
            .append("disfileextid", getDisfileextid())
            .toString();
    }

}
