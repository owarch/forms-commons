package com.owarchitects.forms.commons.db;

import java.io.Serializable;
import java.util.Date;
import org.apache.commons.lang.builder.ToStringBuilder;


/** @author Hibernate CodeGenerator */
public class Linkedstudies_transfer implements Serializable {

    /** identifier field */
    private long lstdid;

    /** persistent field */
    private long linstid;

    /** persistent field */
    private long studyid;

    /** persistent field */
    private long remotestudyid;

    /** nullable persistent field */
    private Date recmodtime;

    /** full constructor */
    public Linkedstudies_transfer(long lstdid, long linstid, long studyid, long remotestudyid, Date recmodtime) {
        this.lstdid = lstdid;
        this.linstid = linstid;
        this.studyid = studyid;
        this.remotestudyid = remotestudyid;
        this.recmodtime = recmodtime;
    }

    /** default constructor */
    public Linkedstudies_transfer() {
    }

    /** minimal constructor */
    public Linkedstudies_transfer(long lstdid, long linstid, long studyid, long remotestudyid) {
        this.lstdid = lstdid;
        this.linstid = linstid;
        this.studyid = studyid;
        this.remotestudyid = remotestudyid;
    }

    public long getLstdid() {
        return this.lstdid;
    }

    public void setLstdid(long lstdid) {
        this.lstdid = lstdid;
    }

    public long getLinstid() {
        return this.linstid;
    }

    public void setLinstid(long linstid) {
        this.linstid = linstid;
    }

    public long getStudyid() {
        return this.studyid;
    }

    public void setStudyid(long studyid) {
        this.studyid = studyid;
    }

    public long getRemotestudyid() {
        return this.remotestudyid;
    }

    public void setRemotestudyid(long remotestudyid) {
        this.remotestudyid = remotestudyid;
    }

    public Date getRecmodtime() {
        return this.recmodtime;
    }

    public void setRecmodtime(Date recmodtime) {
        this.recmodtime = recmodtime;
    }

    public String toString() {
        return new ToStringBuilder(this)
            .append("lstdid", getLstdid())
            .toString();
    }

}
