package com.owarchitects.forms.commons.db;

import java.io.Serializable;
import java.util.Date;
import org.apache.commons.lang.builder.ToStringBuilder;


/** @author Hibernate CodeGenerator */
public class Remotemediacat_transfer implements Serializable {

    /** identifier field */
    private long rmediacatid;

    /** persistent field */
    private long lstdid;

    /** persistent field */
    private long rmediainfoid;

    /** persistent field */
    private long pmediahaid;

    /** nullable persistent field */
    private Date recmodtime;

    /** full constructor */
    public Remotemediacat_transfer(long rmediacatid, long lstdid, long rmediainfoid, long pmediahaid, Date recmodtime) {
        this.rmediacatid = rmediacatid;
        this.lstdid = lstdid;
        this.rmediainfoid = rmediainfoid;
        this.pmediahaid = pmediahaid;
        this.recmodtime = recmodtime;
    }

    /** default constructor */
    public Remotemediacat_transfer() {
    }

    /** minimal constructor */
    public Remotemediacat_transfer(long rmediacatid, long lstdid, long rmediainfoid, long pmediahaid) {
        this.rmediacatid = rmediacatid;
        this.lstdid = lstdid;
        this.rmediainfoid = rmediainfoid;
        this.pmediahaid = pmediahaid;
    }

    public long getRmediacatid() {
        return this.rmediacatid;
    }

    public void setRmediacatid(long rmediacatid) {
        this.rmediacatid = rmediacatid;
    }

    public long getLstdid() {
        return this.lstdid;
    }

    public void setLstdid(long lstdid) {
        this.lstdid = lstdid;
    }

    public long getRmediainfoid() {
        return this.rmediainfoid;
    }

    public void setRmediainfoid(long rmediainfoid) {
        this.rmediainfoid = rmediainfoid;
    }

    public long getPmediahaid() {
        return this.pmediahaid;
    }

    public void setPmediahaid(long pmediahaid) {
        this.pmediahaid = pmediahaid;
    }

    public Date getRecmodtime() {
        return this.recmodtime;
    }

    public void setRecmodtime(Date recmodtime) {
        this.recmodtime = recmodtime;
    }

    public String toString() {
        return new ToStringBuilder(this)
            .append("rmediacatid", getRmediacatid())
            .toString();
    }

}
