package com.owarchitects.forms.commons.db;

import java.io.Serializable;
import java.util.Date;
import org.apache.commons.lang.builder.ToStringBuilder;


/** @author Hibernate CodeGenerator */
public class Formtyperolepermassign_transfer implements Serializable {

    /** identifier field */
    private long dtrpid;

    /** persistent field */
    private long formtypelistid;

    /** persistent field */
    private long rroleid;

    /** persistent field */
    private Serializable permlevel;

    /** nullable persistent field */
    private Date recmodtime;

    /** full constructor */
    public Formtyperolepermassign_transfer(long dtrpid, long formtypelistid, long rroleid, Serializable permlevel, Date recmodtime) {
        this.dtrpid = dtrpid;
        this.formtypelistid = formtypelistid;
        this.rroleid = rroleid;
        this.permlevel = permlevel;
        this.recmodtime = recmodtime;
    }

    /** default constructor */
    public Formtyperolepermassign_transfer() {
    }

    /** minimal constructor */
    public Formtyperolepermassign_transfer(long dtrpid, long formtypelistid, long rroleid, Serializable permlevel) {
        this.dtrpid = dtrpid;
        this.formtypelistid = formtypelistid;
        this.rroleid = rroleid;
        this.permlevel = permlevel;
    }

    public long getDtrpid() {
        return this.dtrpid;
    }

    public void setDtrpid(long dtrpid) {
        this.dtrpid = dtrpid;
    }

    public long getFormtypelistid() {
        return this.formtypelistid;
    }

    public void setFormtypelistid(long formtypelistid) {
        this.formtypelistid = formtypelistid;
    }

    public long getRroleid() {
        return this.rroleid;
    }

    public void setRroleid(long rroleid) {
        this.rroleid = rroleid;
    }

    public Serializable getPermlevel() {
        return this.permlevel;
    }

    public void setPermlevel(Serializable permlevel) {
        this.permlevel = permlevel;
    }

    public Date getRecmodtime() {
        return this.recmodtime;
    }

    public void setRecmodtime(Date recmodtime) {
        this.recmodtime = recmodtime;
    }

    public String toString() {
        return new ToStringBuilder(this)
            .append("dtrpid", getDtrpid())
            .toString();
    }

}
