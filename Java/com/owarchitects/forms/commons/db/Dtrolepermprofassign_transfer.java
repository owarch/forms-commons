package com.owarchitects.forms.commons.db;

import java.io.Serializable;
import java.util.Date;
import org.apache.commons.lang.builder.ToStringBuilder;


/** @author Hibernate CodeGenerator */
public class Dtrolepermprofassign_transfer implements Serializable {

    /** identifier field */
    private long dtrppaid;

    /** persistent field */
    private long dtprofileid;

    /** persistent field */
    private long rroleid;

    /** persistent field */
    private Serializable permlevel;

    /** nullable persistent field */
    private Date recmodtime;

    /** full constructor */
    public Dtrolepermprofassign_transfer(long dtrppaid, long dtprofileid, long rroleid, Serializable permlevel, Date recmodtime) {
        this.dtrppaid = dtrppaid;
        this.dtprofileid = dtprofileid;
        this.rroleid = rroleid;
        this.permlevel = permlevel;
        this.recmodtime = recmodtime;
    }

    /** default constructor */
    public Dtrolepermprofassign_transfer() {
    }

    /** minimal constructor */
    public Dtrolepermprofassign_transfer(long dtrppaid, long dtprofileid, long rroleid, Serializable permlevel) {
        this.dtrppaid = dtrppaid;
        this.dtprofileid = dtprofileid;
        this.rroleid = rroleid;
        this.permlevel = permlevel;
    }

    public long getDtrppaid() {
        return this.dtrppaid;
    }

    public void setDtrppaid(long dtrppaid) {
        this.dtrppaid = dtrppaid;
    }

    public long getDtprofileid() {
        return this.dtprofileid;
    }

    public void setDtprofileid(long dtprofileid) {
        this.dtprofileid = dtprofileid;
    }

    public long getRroleid() {
        return this.rroleid;
    }

    public void setRroleid(long rroleid) {
        this.rroleid = rroleid;
    }

    public Serializable getPermlevel() {
        return this.permlevel;
    }

    public void setPermlevel(Serializable permlevel) {
        this.permlevel = permlevel;
    }

    public Date getRecmodtime() {
        return this.recmodtime;
    }

    public void setRecmodtime(Date recmodtime) {
        this.recmodtime = recmodtime;
    }

    public String toString() {
        return new ToStringBuilder(this)
            .append("dtrppaid", getDtrppaid())
            .toString();
    }

}
