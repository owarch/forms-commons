package com.owarchitects.forms.commons.db;

import java.io.Serializable;
import java.util.Date;
import org.apache.commons.lang.builder.ToStringBuilder;


/** @author Hibernate CodeGenerator */
public class Useroptions_transfer implements Serializable {

    /** identifier field */
    private long uoptionid;

    /** nullable persistent field */
    private Long luserid;

    /** nullable persistent field */
    private String optname;

    /** nullable persistent field */
    private String optval;

    /** nullable persistent field */
    private Date recmodtime;

    /** full constructor */
    public Useroptions_transfer(long uoptionid, Long luserid, String optname, String optval, Date recmodtime) {
        this.uoptionid = uoptionid;
        this.luserid = luserid;
        this.optname = optname;
        this.optval = optval;
        this.recmodtime = recmodtime;
    }

    /** default constructor */
    public Useroptions_transfer() {
    }

    /** minimal constructor */
    public Useroptions_transfer(long uoptionid) {
        this.uoptionid = uoptionid;
    }

    public long getUoptionid() {
        return this.uoptionid;
    }

    public void setUoptionid(long uoptionid) {
        this.uoptionid = uoptionid;
    }

    public Long getLuserid() {
        return this.luserid;
    }

    public void setLuserid(Long luserid) {
        this.luserid = luserid;
    }

    public String getOptname() {
        return this.optname;
    }

    public void setOptname(String optname) {
        this.optname = optname;
    }

    public String getOptval() {
        return this.optval;
    }

    public void setOptval(String optval) {
        this.optval = optval;
    }

    public Date getRecmodtime() {
        return this.recmodtime;
    }

    public void setRecmodtime(Date recmodtime) {
        this.recmodtime = recmodtime;
    }

    public String toString() {
        return new ToStringBuilder(this)
            .append("uoptionid", getUoptionid())
            .toString();
    }

}
