package com.owarchitects.forms.commons.db;

import java.io.Serializable;
import java.util.Date;
import org.apache.commons.lang.builder.ToStringBuilder;


/** @author Hibernate CodeGenerator */
public class Datatableha_transfer implements Serializable {

    /** identifier field */
    private long dthaid;

    /** persistent field */
    private long studyid;

    /** persistent field */
    private int visibility;

    /** nullable persistent field */
    private Long pdthaid;

    /** nullable persistent field */
    private String hadesc;

    /** nullable persistent field */
    private Integer haordr;

    /** nullable persistent field */
    private Date recmodtime;

    /** full constructor */
    public Datatableha_transfer(long dthaid, long studyid, int visibility, Long pdthaid, String hadesc, Integer haordr, Date recmodtime) {
        this.dthaid = dthaid;
        this.studyid = studyid;
        this.visibility = visibility;
        this.pdthaid = pdthaid;
        this.hadesc = hadesc;
        this.haordr = haordr;
        this.recmodtime = recmodtime;
    }

    /** default constructor */
    public Datatableha_transfer() {
    }

    /** minimal constructor */
    public Datatableha_transfer(long dthaid, long studyid, int visibility) {
        this.dthaid = dthaid;
        this.studyid = studyid;
        this.visibility = visibility;
    }

    public long getDthaid() {
        return this.dthaid;
    }

    public void setDthaid(long dthaid) {
        this.dthaid = dthaid;
    }

    public long getStudyid() {
        return this.studyid;
    }

    public void setStudyid(long studyid) {
        this.studyid = studyid;
    }

    public int getVisibility() {
        return this.visibility;
    }

    public void setVisibility(int visibility) {
        this.visibility = visibility;
    }

    public Long getPdthaid() {
        return this.pdthaid;
    }

    public void setPdthaid(Long pdthaid) {
        this.pdthaid = pdthaid;
    }

    public String getHadesc() {
        return this.hadesc;
    }

    public void setHadesc(String hadesc) {
        this.hadesc = hadesc;
    }

    public Integer getHaordr() {
        return this.haordr;
    }

    public void setHaordr(Integer haordr) {
        this.haordr = haordr;
    }

    public Date getRecmodtime() {
        return this.recmodtime;
    }

    public void setRecmodtime(Date recmodtime) {
        this.recmodtime = recmodtime;
    }

    public String toString() {
        return new ToStringBuilder(this)
            .append("dthaid", getDthaid())
            .toString();
    }

}
