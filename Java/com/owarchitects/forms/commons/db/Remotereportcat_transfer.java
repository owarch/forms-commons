package com.owarchitects.forms.commons.db;

import java.io.Serializable;
import java.util.Date;
import org.apache.commons.lang.builder.ToStringBuilder;


/** @author Hibernate CodeGenerator */
public class Remotereportcat_transfer implements Serializable {

    /** identifier field */
    private long rreportcatid;

    /** persistent field */
    private long lstdid;

    /** persistent field */
    private long rreportinfoid;

    /** persistent field */
    private long preporthaid;

    /** nullable persistent field */
    private Date recmodtime;

    /** full constructor */
    public Remotereportcat_transfer(long rreportcatid, long lstdid, long rreportinfoid, long preporthaid, Date recmodtime) {
        this.rreportcatid = rreportcatid;
        this.lstdid = lstdid;
        this.rreportinfoid = rreportinfoid;
        this.preporthaid = preporthaid;
        this.recmodtime = recmodtime;
    }

    /** default constructor */
    public Remotereportcat_transfer() {
    }

    /** minimal constructor */
    public Remotereportcat_transfer(long rreportcatid, long lstdid, long rreportinfoid, long preporthaid) {
        this.rreportcatid = rreportcatid;
        this.lstdid = lstdid;
        this.rreportinfoid = rreportinfoid;
        this.preporthaid = preporthaid;
    }

    public long getRreportcatid() {
        return this.rreportcatid;
    }

    public void setRreportcatid(long rreportcatid) {
        this.rreportcatid = rreportcatid;
    }

    public long getLstdid() {
        return this.lstdid;
    }

    public void setLstdid(long lstdid) {
        this.lstdid = lstdid;
    }

    public long getRreportinfoid() {
        return this.rreportinfoid;
    }

    public void setRreportinfoid(long rreportinfoid) {
        this.rreportinfoid = rreportinfoid;
    }

    public long getPreporthaid() {
        return this.preporthaid;
    }

    public void setPreporthaid(long preporthaid) {
        this.preporthaid = preporthaid;
    }

    public Date getRecmodtime() {
        return this.recmodtime;
    }

    public void setRecmodtime(Date recmodtime) {
        this.recmodtime = recmodtime;
    }

    public String toString() {
        return new ToStringBuilder(this)
            .append("rreportcatid", getRreportcatid())
            .toString();
    }

}
