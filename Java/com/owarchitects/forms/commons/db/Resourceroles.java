package com.owarchitects.forms.commons.db;

import java.io.Serializable;
import java.util.Date;
import java.util.Set;
import org.apache.commons.lang.builder.ToStringBuilder;


/** @author Hibernate CodeGenerator */
public class Resourceroles implements Serializable {

    /** identifier field */
    private long rroleid;

    /** persistent field */
    private int rolelevel;

    /** persistent field */
    private String roleacr;

    /** persistent field */
    private String roledesc;

    /** nullable persistent field */
    private Long siteid;

    /** nullable persistent field */
    private Long studyid;

    /** nullable persistent field */
    private Date recmodtime;

    /** persistent field */
    private Set parentroleassign;

    /** persistent field */
    private Set childroleassign;

    /** persistent field */
    private Set roleassign;

    /** persistent field */
    private Set sysrolepermassign;

    /** persistent field */
    private Set dtrolepermassign;

    /** persistent field */
    private Set dtrolepermprofassign;

    /** full constructor */
    public Resourceroles(int rolelevel, String roleacr, String roledesc, Long siteid, Long studyid, Date recmodtime, Set parentroleassign, Set childroleassign, Set roleassign, Set sysrolepermassign, Set dtrolepermassign, Set dtrolepermprofassign) {
        this.rolelevel = rolelevel;
        this.roleacr = roleacr;
        this.roledesc = roledesc;
        this.siteid = siteid;
        this.studyid = studyid;
        this.recmodtime = recmodtime;
        this.parentroleassign = parentroleassign;
        this.childroleassign = childroleassign;
        this.roleassign = roleassign;
        this.sysrolepermassign = sysrolepermassign;
        this.dtrolepermassign = dtrolepermassign;
        this.dtrolepermprofassign = dtrolepermprofassign;
    }

    /** default constructor */
    public Resourceroles() {
    }

    /** minimal constructor */
    public Resourceroles(int rolelevel, String roleacr, String roledesc, Set parentroleassign, Set childroleassign, Set roleassign, Set sysrolepermassign, Set dtrolepermassign, Set dtrolepermprofassign) {
        this.rolelevel = rolelevel;
        this.roleacr = roleacr;
        this.roledesc = roledesc;
        this.parentroleassign = parentroleassign;
        this.childroleassign = childroleassign;
        this.roleassign = roleassign;
        this.sysrolepermassign = sysrolepermassign;
        this.dtrolepermassign = dtrolepermassign;
        this.dtrolepermprofassign = dtrolepermprofassign;
    }

    public long getRroleid() {
        return this.rroleid;
    }

    public void setRroleid(long rroleid) {
        this.rroleid = rroleid;
    }

    public int getRolelevel() {
        return this.rolelevel;
    }

    public void setRolelevel(int rolelevel) {
        this.rolelevel = rolelevel;
    }

    public String getRoleacr() {
        return this.roleacr;
    }

    public void setRoleacr(String roleacr) {
        this.roleacr = roleacr;
    }

    public String getRoledesc() {
        return this.roledesc;
    }

    public void setRoledesc(String roledesc) {
        this.roledesc = roledesc;
    }

    public Long getSiteid() {
        return this.siteid;
    }

    public void setSiteid(Long siteid) {
        this.siteid = siteid;
    }

    public Long getStudyid() {
        return this.studyid;
    }

    public void setStudyid(Long studyid) {
        this.studyid = studyid;
    }

    public Date getRecmodtime() {
        return this.recmodtime;
    }

    public void setRecmodtime(Date recmodtime) {
        this.recmodtime = recmodtime;
    }

    public Set getParentroleassign() {
        return this.parentroleassign;
    }

    public void setParentroleassign(Set parentroleassign) {
        this.parentroleassign = parentroleassign;
    }

    public Set getChildroleassign() {
        return this.childroleassign;
    }

    public void setChildroleassign(Set childroleassign) {
        this.childroleassign = childroleassign;
    }

    public Set getRoleassign() {
        return this.roleassign;
    }

    public void setRoleassign(Set roleassign) {
        this.roleassign = roleassign;
    }

    public Set getSysrolepermassign() {
        return this.sysrolepermassign;
    }

    public void setSysrolepermassign(Set sysrolepermassign) {
        this.sysrolepermassign = sysrolepermassign;
    }

    public Set getDtrolepermassign() {
        return this.dtrolepermassign;
    }

    public void setDtrolepermassign(Set dtrolepermassign) {
        this.dtrolepermassign = dtrolepermassign;
    }

    public Set getDtrolepermprofassign() {
        return this.dtrolepermprofassign;
    }

    public void setDtrolepermprofassign(Set dtrolepermprofassign) {
        this.dtrolepermprofassign = dtrolepermprofassign;
    }

    public String toString() {
        return new ToStringBuilder(this)
            .append("rroleid", getRroleid())
            .toString();
    }

}
