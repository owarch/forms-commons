package com.owarchitects.forms.commons.db;

import java.io.Serializable;
import java.util.Date;
import java.util.Set;
import org.apache.commons.lang.builder.ToStringBuilder;


/** @author Hibernate CodeGenerator */
public class Studies implements Serializable {

    /** identifier field */
    private long studyid;

    /** persistent field */
    private String studyno;

    /** nullable persistent field */
    private String studydesc;

    /** nullable persistent field */
    private Boolean isarchived;

    /** nullable persistent field */
    private Date recmodtime;

    /** persistent field */
    private com.owarchitects.forms.commons.db.Sites sites;

    /** persistent field */
    private Set useridassign;

    /** persistent field */
    private Set linkedstudies;

    /** full constructor */
    public Studies(String studyno, String studydesc, Boolean isarchived, Date recmodtime, com.owarchitects.forms.commons.db.Sites sites, Set useridassign, Set linkedstudies) {
        this.studyno = studyno;
        this.studydesc = studydesc;
        this.isarchived = isarchived;
        this.recmodtime = recmodtime;
        this.sites = sites;
        this.useridassign = useridassign;
        this.linkedstudies = linkedstudies;
    }

    /** default constructor */
    public Studies() {
    }

    /** minimal constructor */
    public Studies(String studyno, com.owarchitects.forms.commons.db.Sites sites, Set useridassign, Set linkedstudies) {
        this.studyno = studyno;
        this.sites = sites;
        this.useridassign = useridassign;
        this.linkedstudies = linkedstudies;
    }

    public long getStudyid() {
        return this.studyid;
    }

    public void setStudyid(long studyid) {
        this.studyid = studyid;
    }

    public String getStudyno() {
        return this.studyno;
    }

    public void setStudyno(String studyno) {
        this.studyno = studyno;
    }

    public String getStudydesc() {
        return this.studydesc;
    }

    public void setStudydesc(String studydesc) {
        this.studydesc = studydesc;
    }

    public Boolean getIsarchived() {
        return this.isarchived;
    }

    public void setIsarchived(Boolean isarchived) {
        this.isarchived = isarchived;
    }

    public Date getRecmodtime() {
        return this.recmodtime;
    }

    public void setRecmodtime(Date recmodtime) {
        this.recmodtime = recmodtime;
    }

    public com.owarchitects.forms.commons.db.Sites getSites() {
        return this.sites;
    }

    public void setSites(com.owarchitects.forms.commons.db.Sites sites) {
        this.sites = sites;
    }

    public Set getUseridassign() {
        return this.useridassign;
    }

    public void setUseridassign(Set useridassign) {
        this.useridassign = useridassign;
    }

    public Set getLinkedstudies() {
        return this.linkedstudies;
    }

    public void setLinkedstudies(Set linkedstudies) {
        this.linkedstudies = linkedstudies;
    }

    public String toString() {
        return new ToStringBuilder(this)
            .append("studyid", getStudyid())
            .toString();
    }

}
