package com.owarchitects.forms.commons.db;

import java.io.Serializable;
import java.util.Date;
import org.apache.commons.lang.builder.ToStringBuilder;


/** @author Hibernate CodeGenerator */
public class Mediapermprofassign_transfer implements Serializable {

    /** identifier field */
    private long mrpid;

    /** persistent field */
    private long mediainfoid;

    /** nullable persistent field */
    private Date recmodtime;

    /** persistent field */
    private long mprofileid;

    /** full constructor */
    public Mediapermprofassign_transfer(long mrpid, long mediainfoid, Date recmodtime, long mprofileid) {
        this.mrpid = mrpid;
        this.mediainfoid = mediainfoid;
        this.recmodtime = recmodtime;
        this.mprofileid = mprofileid;
    }

    /** default constructor */
    public Mediapermprofassign_transfer() {
    }

    /** minimal constructor */
    public Mediapermprofassign_transfer(long mrpid, long mediainfoid, long mprofileid) {
        this.mrpid = mrpid;
        this.mediainfoid = mediainfoid;
        this.mprofileid = mprofileid;
    }

    public long getMrpid() {
        return this.mrpid;
    }

    public void setMrpid(long mrpid) {
        this.mrpid = mrpid;
    }

    public long getMediainfoid() {
        return this.mediainfoid;
    }

    public void setMediainfoid(long mediainfoid) {
        this.mediainfoid = mediainfoid;
    }

    public Date getRecmodtime() {
        return this.recmodtime;
    }

    public void setRecmodtime(Date recmodtime) {
        this.recmodtime = recmodtime;
    }

    public long getMprofileid() {
        return this.mprofileid;
    }

    public void setMprofileid(long mprofileid) {
        this.mprofileid = mprofileid;
    }

    public String toString() {
        return new ToStringBuilder(this)
            .append("mrpid", getMrpid())
            .toString();
    }

}
