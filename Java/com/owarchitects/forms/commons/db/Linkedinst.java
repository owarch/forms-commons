package com.owarchitects.forms.commons.db;

import java.io.Serializable;
import java.util.Date;
import java.util.Set;
import org.apache.commons.lang.builder.ToStringBuilder;


/** @author Hibernate CodeGenerator */
public class Linkedinst implements Serializable {

    /** identifier field */
    private long linstid;

    /** persistent field */
    private String acronym;

    /** nullable persistent field */
    private String description;

    /** nullable persistent field */
    private String url;

    /** nullable persistent field */
    private String connectstring;

    /** nullable persistent field */
    private Boolean isdisabled;

    /** nullable persistent field */
    private Date recmodtime;

    /** persistent field */
    private Set allinst;

    /** persistent field */
    private Set linkedstudies;

    /** persistent field */
    private Set userinfomodtime;

    /** full constructor */
    public Linkedinst(String acronym, String description, String url, String connectstring, Boolean isdisabled, Date recmodtime, Set allinst, Set linkedstudies, Set userinfomodtime) {
        this.acronym = acronym;
        this.description = description;
        this.url = url;
        this.connectstring = connectstring;
        this.isdisabled = isdisabled;
        this.recmodtime = recmodtime;
        this.allinst = allinst;
        this.linkedstudies = linkedstudies;
        this.userinfomodtime = userinfomodtime;
    }

    /** default constructor */
    public Linkedinst() {
    }

    /** minimal constructor */
    public Linkedinst(String acronym, Set allinst, Set linkedstudies, Set userinfomodtime) {
        this.acronym = acronym;
        this.allinst = allinst;
        this.linkedstudies = linkedstudies;
        this.userinfomodtime = userinfomodtime;
    }

    public long getLinstid() {
        return this.linstid;
    }

    public void setLinstid(long linstid) {
        this.linstid = linstid;
    }

    public String getAcronym() {
        return this.acronym;
    }

    public void setAcronym(String acronym) {
        this.acronym = acronym;
    }

    public String getDescription() {
        return this.description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getUrl() {
        return this.url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getConnectstring() {
        return this.connectstring;
    }

    public void setConnectstring(String connectstring) {
        this.connectstring = connectstring;
    }

    public Boolean getIsdisabled() {
        return this.isdisabled;
    }

    public void setIsdisabled(Boolean isdisabled) {
        this.isdisabled = isdisabled;
    }

    public Date getRecmodtime() {
        return this.recmodtime;
    }

    public void setRecmodtime(Date recmodtime) {
        this.recmodtime = recmodtime;
    }

    public Set getAllinst() {
        return this.allinst;
    }

    public void setAllinst(Set allinst) {
        this.allinst = allinst;
    }

    public Set getLinkedstudies() {
        return this.linkedstudies;
    }

    public void setLinkedstudies(Set linkedstudies) {
        this.linkedstudies = linkedstudies;
    }

    public Set getUserinfomodtime() {
        return this.userinfomodtime;
    }

    public void setUserinfomodtime(Set userinfomodtime) {
        this.userinfomodtime = userinfomodtime;
    }

    public String toString() {
        return new ToStringBuilder(this)
            .append("linstid", getLinstid())
            .toString();
    }

}
