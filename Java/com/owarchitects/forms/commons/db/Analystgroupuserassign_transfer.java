package com.owarchitects.forms.commons.db;

import java.io.Serializable;
import java.util.Date;
import org.apache.commons.lang.builder.ToStringBuilder;


/** @author Hibernate CodeGenerator */
public class Analystgroupuserassign_transfer implements Serializable {

    /** identifier field */
    private long aguaid;

    /** persistent field */
    private long agroupid;

    /** persistent field */
    private long auserid;

    /** nullable persistent field */
    private Date recmodtime;

    /** full constructor */
    public Analystgroupuserassign_transfer(long aguaid, long agroupid, long auserid, Date recmodtime) {
        this.aguaid = aguaid;
        this.agroupid = agroupid;
        this.auserid = auserid;
        this.recmodtime = recmodtime;
    }

    /** default constructor */
    public Analystgroupuserassign_transfer() {
    }

    /** minimal constructor */
    public Analystgroupuserassign_transfer(long aguaid, long agroupid, long auserid) {
        this.aguaid = aguaid;
        this.agroupid = agroupid;
        this.auserid = auserid;
    }

    public long getAguaid() {
        return this.aguaid;
    }

    public void setAguaid(long aguaid) {
        this.aguaid = aguaid;
    }

    public long getAgroupid() {
        return this.agroupid;
    }

    public void setAgroupid(long agroupid) {
        this.agroupid = agroupid;
    }

    public long getAuserid() {
        return this.auserid;
    }

    public void setAuserid(long auserid) {
        this.auserid = auserid;
    }

    public Date getRecmodtime() {
        return this.recmodtime;
    }

    public void setRecmodtime(Date recmodtime) {
        this.recmodtime = recmodtime;
    }

    public String toString() {
        return new ToStringBuilder(this)
            .append("aguaid", getAguaid())
            .toString();
    }

}
