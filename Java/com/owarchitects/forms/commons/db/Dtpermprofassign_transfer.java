package com.owarchitects.forms.commons.db;

import java.io.Serializable;
import java.util.Date;
import org.apache.commons.lang.builder.ToStringBuilder;


/** @author Hibernate CodeGenerator */
public class Dtpermprofassign_transfer implements Serializable {

    /** identifier field */
    private long dtrpid;

    /** persistent field */
    private long dtdefid;

    /** nullable persistent field */
    private Date recmodtime;

    /** persistent field */
    private long dtprofileid;

    /** full constructor */
    public Dtpermprofassign_transfer(long dtrpid, long dtdefid, Date recmodtime, long dtprofileid) {
        this.dtrpid = dtrpid;
        this.dtdefid = dtdefid;
        this.recmodtime = recmodtime;
        this.dtprofileid = dtprofileid;
    }

    /** default constructor */
    public Dtpermprofassign_transfer() {
    }

    /** minimal constructor */
    public Dtpermprofassign_transfer(long dtrpid, long dtdefid, long dtprofileid) {
        this.dtrpid = dtrpid;
        this.dtdefid = dtdefid;
        this.dtprofileid = dtprofileid;
    }

    public long getDtrpid() {
        return this.dtrpid;
    }

    public void setDtrpid(long dtrpid) {
        this.dtrpid = dtrpid;
    }

    public long getDtdefid() {
        return this.dtdefid;
    }

    public void setDtdefid(long dtdefid) {
        this.dtdefid = dtdefid;
    }

    public Date getRecmodtime() {
        return this.recmodtime;
    }

    public void setRecmodtime(Date recmodtime) {
        this.recmodtime = recmodtime;
    }

    public long getDtprofileid() {
        return this.dtprofileid;
    }

    public void setDtprofileid(long dtprofileid) {
        this.dtprofileid = dtprofileid;
    }

    public String toString() {
        return new ToStringBuilder(this)
            .append("dtrpid", getDtrpid())
            .toString();
    }

}
