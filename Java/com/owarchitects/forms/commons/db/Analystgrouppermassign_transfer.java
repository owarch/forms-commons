package com.owarchitects.forms.commons.db;

import java.io.Serializable;
import java.util.Date;
import org.apache.commons.lang.builder.ToStringBuilder;


/** @author Hibernate CodeGenerator */
public class Analystgrouppermassign_transfer implements Serializable {

    /** identifier field */
    private long agpid;

    /** persistent field */
    private long analystinfoid;

    /** nullable persistent field */
    private Date recmodtime;

    /** persistent field */
    private long agroupid;

    /** persistent field */
    private Serializable permlevel;

    /** full constructor */
    public Analystgrouppermassign_transfer(long agpid, long analystinfoid, Date recmodtime, long agroupid, Serializable permlevel) {
        this.agpid = agpid;
        this.analystinfoid = analystinfoid;
        this.recmodtime = recmodtime;
        this.agroupid = agroupid;
        this.permlevel = permlevel;
    }

    /** default constructor */
    public Analystgrouppermassign_transfer() {
    }

    /** minimal constructor */
    public Analystgrouppermassign_transfer(long agpid, long analystinfoid, long agroupid, Serializable permlevel) {
        this.agpid = agpid;
        this.analystinfoid = analystinfoid;
        this.agroupid = agroupid;
        this.permlevel = permlevel;
    }

    public long getAgpid() {
        return this.agpid;
    }

    public void setAgpid(long agpid) {
        this.agpid = agpid;
    }

    public long getAnalystinfoid() {
        return this.analystinfoid;
    }

    public void setAnalystinfoid(long analystinfoid) {
        this.analystinfoid = analystinfoid;
    }

    public Date getRecmodtime() {
        return this.recmodtime;
    }

    public void setRecmodtime(Date recmodtime) {
        this.recmodtime = recmodtime;
    }

    public long getAgroupid() {
        return this.agroupid;
    }

    public void setAgroupid(long agroupid) {
        this.agroupid = agroupid;
    }

    public Serializable getPermlevel() {
        return this.permlevel;
    }

    public void setPermlevel(Serializable permlevel) {
        this.permlevel = permlevel;
    }

    public String toString() {
        return new ToStringBuilder(this)
            .append("agpid", getAgpid())
            .toString();
    }

}
