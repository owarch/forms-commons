package com.owarchitects.forms.commons.db;

import java.io.Serializable;
import java.util.Date;
import org.apache.commons.lang.builder.ToStringBuilder;


/** @author Hibernate CodeGenerator */
public class Analystusersyspermassign_transfer implements Serializable {

    /** identifier field */
    private long auspid;

    /** persistent field */
    private long auserid;

    /** persistent field */
    private Serializable permlevel;

    /** nullable persistent field */
    private Date recmodtime;

    /** full constructor */
    public Analystusersyspermassign_transfer(long auspid, long auserid, Serializable permlevel, Date recmodtime) {
        this.auspid = auspid;
        this.auserid = auserid;
        this.permlevel = permlevel;
        this.recmodtime = recmodtime;
    }

    /** default constructor */
    public Analystusersyspermassign_transfer() {
    }

    /** minimal constructor */
    public Analystusersyspermassign_transfer(long auspid, long auserid, Serializable permlevel) {
        this.auspid = auspid;
        this.auserid = auserid;
        this.permlevel = permlevel;
    }

    public long getAuspid() {
        return this.auspid;
    }

    public void setAuspid(long auspid) {
        this.auspid = auspid;
    }

    public long getAuserid() {
        return this.auserid;
    }

    public void setAuserid(long auserid) {
        this.auserid = auserid;
    }

    public Serializable getPermlevel() {
        return this.permlevel;
    }

    public void setPermlevel(Serializable permlevel) {
        this.permlevel = permlevel;
    }

    public Date getRecmodtime() {
        return this.recmodtime;
    }

    public void setRecmodtime(Date recmodtime) {
        this.recmodtime = recmodtime;
    }

    public String toString() {
        return new ToStringBuilder(this)
            .append("auspid", getAuspid())
            .toString();
    }

}
