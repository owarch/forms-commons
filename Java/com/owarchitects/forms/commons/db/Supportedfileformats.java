package com.owarchitects.forms.commons.db;

import java.io.Serializable;
import java.util.Date;
import java.util.Set;
import org.apache.commons.lang.builder.ToStringBuilder;


/** @author Hibernate CodeGenerator */
public class Supportedfileformats implements Serializable {

    /** identifier field */
    private long fileformatid;

    /** persistent field */
    private String fileformat;

    /** persistent field */
    private String contenttype;

    /** nullable persistent field */
    private Date recmodtime;

    /** persistent field */
    private Set fileextensions;

    /** persistent field */
    private Set formformats;

    /** persistent field */
    private Set mediainfo;

    /** full constructor */
    public Supportedfileformats(String fileformat, String contenttype, Date recmodtime, Set fileextensions, Set formformats, Set mediainfo) {
        this.fileformat = fileformat;
        this.contenttype = contenttype;
        this.recmodtime = recmodtime;
        this.fileextensions = fileextensions;
        this.formformats = formformats;
        this.mediainfo = mediainfo;
    }

    /** default constructor */
    public Supportedfileformats() {
    }

    /** minimal constructor */
    public Supportedfileformats(String fileformat, String contenttype, Set fileextensions, Set formformats, Set mediainfo) {
        this.fileformat = fileformat;
        this.contenttype = contenttype;
        this.fileextensions = fileextensions;
        this.formformats = formformats;
        this.mediainfo = mediainfo;
    }

    public long getFileformatid() {
        return this.fileformatid;
    }

    public void setFileformatid(long fileformatid) {
        this.fileformatid = fileformatid;
    }

    public String getFileformat() {
        return this.fileformat;
    }

    public void setFileformat(String fileformat) {
        this.fileformat = fileformat;
    }

    public String getContenttype() {
        return this.contenttype;
    }

    public void setContenttype(String contenttype) {
        this.contenttype = contenttype;
    }

    public Date getRecmodtime() {
        return this.recmodtime;
    }

    public void setRecmodtime(Date recmodtime) {
        this.recmodtime = recmodtime;
    }

    public Set getFileextensions() {
        return this.fileextensions;
    }

    public void setFileextensions(Set fileextensions) {
        this.fileextensions = fileextensions;
    }

    public Set getFormformats() {
        return this.formformats;
    }

    public void setFormformats(Set formformats) {
        this.formformats = formformats;
    }

    public Set getMediainfo() {
        return this.mediainfo;
    }

    public void setMediainfo(Set mediainfo) {
        this.mediainfo = mediainfo;
    }

    public String toString() {
        return new ToStringBuilder(this)
            .append("fileformatid", getFileformatid())
            .toString();
    }

}
