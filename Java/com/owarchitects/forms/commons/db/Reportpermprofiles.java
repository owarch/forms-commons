package com.owarchitects.forms.commons.db;

import java.io.Serializable;
import java.util.Date;
import java.util.Set;
import org.apache.commons.lang.builder.ToStringBuilder;


/** @author Hibernate CodeGenerator */
public class Reportpermprofiles implements Serializable {

    /** identifier field */
    private long rprofileid;

    /** persistent field */
    private int profilelevel;

    /** nullable persistent field */
    private Long siteid;

    /** nullable persistent field */
    private Long studyid;

    /** persistent field */
    private int resourcetype;

    /** nullable persistent field */
    private String profileacr;

    /** nullable persistent field */
    private String profiledesc;

    /** nullable persistent field */
    private Date recmodtime;

    /** persistent field */
    private Set reportpermprofassign;

    /** persistent field */
    private Set reportrolepermprofassign;

    /** full constructor */
    public Reportpermprofiles(int profilelevel, Long siteid, Long studyid, int resourcetype, String profileacr, String profiledesc, Date recmodtime, Set reportpermprofassign, Set reportrolepermprofassign) {
        this.profilelevel = profilelevel;
        this.siteid = siteid;
        this.studyid = studyid;
        this.resourcetype = resourcetype;
        this.profileacr = profileacr;
        this.profiledesc = profiledesc;
        this.recmodtime = recmodtime;
        this.reportpermprofassign = reportpermprofassign;
        this.reportrolepermprofassign = reportrolepermprofassign;
    }

    /** default constructor */
    public Reportpermprofiles() {
    }

    /** minimal constructor */
    public Reportpermprofiles(int profilelevel, int resourcetype, Set reportpermprofassign, Set reportrolepermprofassign) {
        this.profilelevel = profilelevel;
        this.resourcetype = resourcetype;
        this.reportpermprofassign = reportpermprofassign;
        this.reportrolepermprofassign = reportrolepermprofassign;
    }

    public long getRprofileid() {
        return this.rprofileid;
    }

    public void setRprofileid(long rprofileid) {
        this.rprofileid = rprofileid;
    }

    public int getProfilelevel() {
        return this.profilelevel;
    }

    public void setProfilelevel(int profilelevel) {
        this.profilelevel = profilelevel;
    }

    public Long getSiteid() {
        return this.siteid;
    }

    public void setSiteid(Long siteid) {
        this.siteid = siteid;
    }

    public Long getStudyid() {
        return this.studyid;
    }

    public void setStudyid(Long studyid) {
        this.studyid = studyid;
    }

    public int getResourcetype() {
        return this.resourcetype;
    }

    public void setResourcetype(int resourcetype) {
        this.resourcetype = resourcetype;
    }

    public String getProfileacr() {
        return this.profileacr;
    }

    public void setProfileacr(String profileacr) {
        this.profileacr = profileacr;
    }

    public String getProfiledesc() {
        return this.profiledesc;
    }

    public void setProfiledesc(String profiledesc) {
        this.profiledesc = profiledesc;
    }

    public Date getRecmodtime() {
        return this.recmodtime;
    }

    public void setRecmodtime(Date recmodtime) {
        this.recmodtime = recmodtime;
    }

    public Set getReportpermprofassign() {
        return this.reportpermprofassign;
    }

    public void setReportpermprofassign(Set reportpermprofassign) {
        this.reportpermprofassign = reportpermprofassign;
    }

    public Set getReportrolepermprofassign() {
        return this.reportrolepermprofassign;
    }

    public void setReportrolepermprofassign(Set reportrolepermprofassign) {
        this.reportrolepermprofassign = reportrolepermprofassign;
    }

    public String toString() {
        return new ToStringBuilder(this)
            .append("rprofileid", getRprofileid())
            .toString();
    }

}
