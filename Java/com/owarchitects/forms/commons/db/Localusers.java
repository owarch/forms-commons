package com.owarchitects.forms.commons.db;

import java.io.Serializable;
import java.util.Date;
import java.util.Set;
import org.apache.commons.lang.builder.ToStringBuilder;


/** @author Hibernate CodeGenerator */
public class Localusers implements Serializable {

    /** identifier field */
    private long luserid;

    /** persistent field */
    private String username;

    /** nullable persistent field */
    private Date passworddt;

    /** nullable persistent field */
    private String usernumber;

    /** nullable persistent field */
    private String fullname;

    /** nullable persistent field */
    private String emailaddr;

    /** nullable persistent field */
    private Boolean isadmin;

    /** nullable persistent field */
    private Boolean istemppassword;

    /** nullable persistent field */
    private Boolean isdeleted;

    /** nullable persistent field */
    private Date accountexpiredate;

    /** nullable persistent field */
    private Date recmodtime;

    /** persistent field */
    private Set useridassign;

    /** persistent field */
    private Set pwhistory;

    /** full constructor */
    public Localusers(String username, Date passworddt, String usernumber, String fullname, String emailaddr, Boolean isadmin, Boolean istemppassword, Boolean isdeleted, Date accountexpiredate, Date recmodtime, Set useridassign, Set pwhistory) {
        this.username = username;
        this.passworddt = passworddt;
        this.usernumber = usernumber;
        this.fullname = fullname;
        this.emailaddr = emailaddr;
        this.isadmin = isadmin;
        this.istemppassword = istemppassword;
        this.isdeleted = isdeleted;
        this.accountexpiredate = accountexpiredate;
        this.recmodtime = recmodtime;
        this.useridassign = useridassign;
        this.pwhistory = pwhistory;
    }

    /** default constructor */
    public Localusers() {
    }

    /** minimal constructor */
    public Localusers(String username, Set useridassign, Set pwhistory) {
        this.username = username;
        this.useridassign = useridassign;
        this.pwhistory = pwhistory;
    }

    public long getLuserid() {
        return this.luserid;
    }

    public void setLuserid(long luserid) {
        this.luserid = luserid;
    }

    public String getUsername() {
        return this.username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public Date getPassworddt() {
        return this.passworddt;
    }

    public void setPassworddt(Date passworddt) {
        this.passworddt = passworddt;
    }

    public String getUsernumber() {
        return this.usernumber;
    }

    public void setUsernumber(String usernumber) {
        this.usernumber = usernumber;
    }

    public String getFullname() {
        return this.fullname;
    }

    public void setFullname(String fullname) {
        this.fullname = fullname;
    }

    public String getEmailaddr() {
        return this.emailaddr;
    }

    public void setEmailaddr(String emailaddr) {
        this.emailaddr = emailaddr;
    }

    public Boolean getIsadmin() {
        return this.isadmin;
    }

    public void setIsadmin(Boolean isadmin) {
        this.isadmin = isadmin;
    }

    public Boolean getIstemppassword() {
        return this.istemppassword;
    }

    public void setIstemppassword(Boolean istemppassword) {
        this.istemppassword = istemppassword;
    }

    public Boolean getIsdeleted() {
        return this.isdeleted;
    }

    public void setIsdeleted(Boolean isdeleted) {
        this.isdeleted = isdeleted;
    }

    public Date getAccountexpiredate() {
        return this.accountexpiredate;
    }

    public void setAccountexpiredate(Date accountexpiredate) {
        this.accountexpiredate = accountexpiredate;
    }

    public Date getRecmodtime() {
        return this.recmodtime;
    }

    public void setRecmodtime(Date recmodtime) {
        this.recmodtime = recmodtime;
    }

    public Set getUseridassign() {
        return this.useridassign;
    }

    public void setUseridassign(Set useridassign) {
        this.useridassign = useridassign;
    }

    public Set getPwhistory() {
        return this.pwhistory;
    }

    public void setPwhistory(Set pwhistory) {
        this.pwhistory = pwhistory;
    }

    public String toString() {
        return new ToStringBuilder(this)
            .append("luserid", getLuserid())
            .toString();
    }

}
