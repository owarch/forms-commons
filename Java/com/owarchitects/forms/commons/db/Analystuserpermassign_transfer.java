package com.owarchitects.forms.commons.db;

import java.io.Serializable;
import java.util.Date;
import org.apache.commons.lang.builder.ToStringBuilder;


/** @author Hibernate CodeGenerator */
public class Analystuserpermassign_transfer implements Serializable {

    /** identifier field */
    private long aupid;

    /** persistent field */
    private long analystinfoid;

    /** persistent field */
    private long auserid;

    /** persistent field */
    private Serializable permlevel;

    /** nullable persistent field */
    private Date recmodtime;

    /** full constructor */
    public Analystuserpermassign_transfer(long aupid, long analystinfoid, long auserid, Serializable permlevel, Date recmodtime) {
        this.aupid = aupid;
        this.analystinfoid = analystinfoid;
        this.auserid = auserid;
        this.permlevel = permlevel;
        this.recmodtime = recmodtime;
    }

    /** default constructor */
    public Analystuserpermassign_transfer() {
    }

    /** minimal constructor */
    public Analystuserpermassign_transfer(long aupid, long analystinfoid, long auserid, Serializable permlevel) {
        this.aupid = aupid;
        this.analystinfoid = analystinfoid;
        this.auserid = auserid;
        this.permlevel = permlevel;
    }

    public long getAupid() {
        return this.aupid;
    }

    public void setAupid(long aupid) {
        this.aupid = aupid;
    }

    public long getAnalystinfoid() {
        return this.analystinfoid;
    }

    public void setAnalystinfoid(long analystinfoid) {
        this.analystinfoid = analystinfoid;
    }

    public long getAuserid() {
        return this.auserid;
    }

    public void setAuserid(long auserid) {
        this.auserid = auserid;
    }

    public Serializable getPermlevel() {
        return this.permlevel;
    }

    public void setPermlevel(Serializable permlevel) {
        this.permlevel = permlevel;
    }

    public Date getRecmodtime() {
        return this.recmodtime;
    }

    public void setRecmodtime(Date recmodtime) {
        this.recmodtime = recmodtime;
    }

    public String toString() {
        return new ToStringBuilder(this)
            .append("aupid", getAupid())
            .toString();
    }

}
