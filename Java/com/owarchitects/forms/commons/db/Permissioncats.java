package com.owarchitects.forms.commons.db;

import java.io.Serializable;
import java.util.Date;
import java.util.Set;
import org.apache.commons.lang.builder.ToStringBuilder;


/** @author Hibernate CodeGenerator */
public class Permissioncats implements Serializable {

    /** identifier field */
    private long pcatid;

    /** persistent field */
    private int pcatvalue;

    /** persistent field */
    private int pcatscope;

    /** persistent field */
    private int pcatorder;

    /** persistent field */
    private String pcatdesc;

    /** nullable persistent field */
    private Date recmodtime;

    /** persistent field */
    private Set permissions;

    /** persistent field */
    private Set sysrolepermassign;

    /** persistent field */
    private Set sysuserpermassign;

    /** full constructor */
    public Permissioncats(int pcatvalue, int pcatscope, int pcatorder, String pcatdesc, Date recmodtime, Set permissions, Set sysrolepermassign, Set sysuserpermassign) {
        this.pcatvalue = pcatvalue;
        this.pcatscope = pcatscope;
        this.pcatorder = pcatorder;
        this.pcatdesc = pcatdesc;
        this.recmodtime = recmodtime;
        this.permissions = permissions;
        this.sysrolepermassign = sysrolepermassign;
        this.sysuserpermassign = sysuserpermassign;
    }

    /** default constructor */
    public Permissioncats() {
    }

    /** minimal constructor */
    public Permissioncats(int pcatvalue, int pcatscope, int pcatorder, String pcatdesc, Set permissions, Set sysrolepermassign, Set sysuserpermassign) {
        this.pcatvalue = pcatvalue;
        this.pcatscope = pcatscope;
        this.pcatorder = pcatorder;
        this.pcatdesc = pcatdesc;
        this.permissions = permissions;
        this.sysrolepermassign = sysrolepermassign;
        this.sysuserpermassign = sysuserpermassign;
    }

    public long getPcatid() {
        return this.pcatid;
    }

    public void setPcatid(long pcatid) {
        this.pcatid = pcatid;
    }

    public int getPcatvalue() {
        return this.pcatvalue;
    }

    public void setPcatvalue(int pcatvalue) {
        this.pcatvalue = pcatvalue;
    }

    public int getPcatscope() {
        return this.pcatscope;
    }

    public void setPcatscope(int pcatscope) {
        this.pcatscope = pcatscope;
    }

    public int getPcatorder() {
        return this.pcatorder;
    }

    public void setPcatorder(int pcatorder) {
        this.pcatorder = pcatorder;
    }

    public String getPcatdesc() {
        return this.pcatdesc;
    }

    public void setPcatdesc(String pcatdesc) {
        this.pcatdesc = pcatdesc;
    }

    public Date getRecmodtime() {
        return this.recmodtime;
    }

    public void setRecmodtime(Date recmodtime) {
        this.recmodtime = recmodtime;
    }

    public Set getPermissions() {
        return this.permissions;
    }

    public void setPermissions(Set permissions) {
        this.permissions = permissions;
    }

    public Set getSysrolepermassign() {
        return this.sysrolepermassign;
    }

    public void setSysrolepermassign(Set sysrolepermassign) {
        this.sysrolepermassign = sysrolepermassign;
    }

    public Set getSysuserpermassign() {
        return this.sysuserpermassign;
    }

    public void setSysuserpermassign(Set sysuserpermassign) {
        this.sysuserpermassign = sysuserpermassign;
    }

    public String toString() {
        return new ToStringBuilder(this)
            .append("pcatid", getPcatid())
            .toString();
    }

}
