package com.owarchitects.forms.commons.db;

import java.io.Serializable;
import java.util.Date;
import org.apache.commons.lang.builder.ToStringBuilder;


/** @author Hibernate CodeGenerator */
public class Formattrs_transfer implements Serializable {

    /** identifier field */
    private long formattrid;

    /** persistent field */
    private long dtdefid;

    /** nullable persistent field */
    private String attr;

    /** nullable persistent field */
    private String value;

    /** nullable persistent field */
    private Date recmodtime;

    /** full constructor */
    public Formattrs_transfer(long formattrid, long dtdefid, String attr, String value, Date recmodtime) {
        this.formattrid = formattrid;
        this.dtdefid = dtdefid;
        this.attr = attr;
        this.value = value;
        this.recmodtime = recmodtime;
    }

    /** default constructor */
    public Formattrs_transfer() {
    }

    /** minimal constructor */
    public Formattrs_transfer(long formattrid, long dtdefid) {
        this.formattrid = formattrid;
        this.dtdefid = dtdefid;
    }

    public long getFormattrid() {
        return this.formattrid;
    }

    public void setFormattrid(long formattrid) {
        this.formattrid = formattrid;
    }

    public long getDtdefid() {
        return this.dtdefid;
    }

    public void setDtdefid(long dtdefid) {
        this.dtdefid = dtdefid;
    }

    public String getAttr() {
        return this.attr;
    }

    public void setAttr(String attr) {
        this.attr = attr;
    }

    public String getValue() {
        return this.value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public Date getRecmodtime() {
        return this.recmodtime;
    }

    public void setRecmodtime(Date recmodtime) {
        this.recmodtime = recmodtime;
    }

    public String toString() {
        return new ToStringBuilder(this)
            .append("formattrid", getFormattrid())
            .toString();
    }

}
