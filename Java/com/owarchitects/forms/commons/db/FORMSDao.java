 
/**
 *
 * This program is part of:
 *
 *    FORMS [Forms-Oriented Research Management System]
 *
 * Copyright (C) 2009 OpenWare Architects.  All Rights Reserved.
 *
 * FORMS is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * FORMS is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with FORMS.  If not, see <http://www.gnu.org/licenses/>.
 *
 */
 
package com.owarchitects.forms.commons.db;

/**
 *  FORMSDao.java  -  FORMSMainDB Data Access Object with convenience methods
 */

import com.owarchitects.forms.commons.comp.*;
import java.io.Serializable;
import java.util.*;
import java.lang.reflect.*;
import org.hibernate.*;
import java.lang.annotation.*;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.orm.hibernate3.*;
//import org.springframework.transaction.support.TransactionSynchronizationManager;

@Repository
//@Transactional
public abstract class FORMSDao {

   private SessionFactory sf;

   public void setSessionFactory(SessionFactory sf) {
      this.sf=sf;
   }   

   public SessionFactory getSessionFactory() {
      return sf;
   }   

   public org.hibernate.classic.Session getCurrentSession() {
      return getSessionFactory().getCurrentSession();
   }

   public Query createQuery(String queryString) {
      return getCurrentSession().createQuery(queryString);
   }

   public Object get(Class c,Serializable id) {
      return getCurrentSession().get(c,id);
   }

   public Object get(Object o,Serializable id) {
      return getCurrentSession().get(o.getClass(),id);
   }

   public Serializable getIdentifier(Object o) {
      return getCurrentSession().getIdentifier(o);
   }

   public void save(Object o) {
      getCurrentSession().save(o);
   }

   public void update(Object o) {
      getCurrentSession().update(o);
   }

   public void saveOrUpdate(Object o) {
      getCurrentSession().saveOrUpdate(o);
   }

   public void merge(Object o) {
      getCurrentSession().merge(o);
   }

   public void persist(Object o) {
      getCurrentSession().save(o);
   }

   public void replicate(Object o) {
      getCurrentSession().replicate(o,ReplicationMode.EXCEPTION);
   }

   public void replicate(Object o,ReplicationMode r) {
      getCurrentSession().replicate(o,r);
   }

   public void delete(Object o) {
      getCurrentSession().delete(o);
   }

   public List getTable(String tableClassName) {
      Query q=createQuery("select t from " + tableClassName + " as t");
      List l=q.list();
      return l;
   }

   public List getTableIdentifiers(String tableClassName) {
      Session sess=getCurrentSession();
      Query q=sess.createQuery("select t from " + tableClassName + " as t");
      List r=(List) new ArrayList();
      List l=q.list();
      Iterator i=l.iterator();
      while (i.hasNext()) {
         Serializable s=sess.getIdentifier((Object)i.next());
         r.add(s);
      }
      return r;
   }

   public List getTable(String tableClassName,String byvar) {
      Query q=createQuery("select t from " + tableClassName + "as t order by " + byvar);
      List l=q.list();
      return l;
   }

   public List getTable(String tableClassName,String byvar,String dir) {
      Query q=createQuery("select t from " + tableClassName + "as t order by " + byvar + " " + dir);
      List l=q.list();
      return l;
   }

   public List execQuery(String queryString) {
      Query q=createQuery(queryString);
      List l=q.list();
      return l;
   }

   public List execQuery(String queryString,String name,Object value) {
      Query q=createQuery(queryString);
      q.setParameter(name,value);
      List l=q.list();
      return l;
   }

   public List execQuery(String queryString,String[] names,Object[] values) {
      Query q=createQuery(queryString);
      for (int i=0;i<names.length;i++) {
         q.setParameter(names[i],values[i]);
      }
      List l=q.list();
      return l;
   }

   public Object execUniqueQuery(String queryString) {
      Query q=createQuery(queryString);
      Object o=q.uniqueResult();
      return o;
   }

   public Object execUniqueQuery(String queryString,String name,Object value) {
      Query q=createQuery(queryString);
      q.setParameter(name,value);
      Object o=q.uniqueResult();
      return o;
   }

   public Object execUniqueQuery(String queryString,String[] names,Object[] values) {
      Query q=createQuery(queryString);
      for (int i=0;i<names.length;i++) {
         q.setParameter(names[i],values[i]);
      }
      Object o=q.uniqueResult();
      return o;
   }

   public List execPagedQuery(String queryString,int firstResult,int maxResults) {
      Query q=createQuery(queryString);
      q.setFirstResult(firstResult);
      q.setMaxResults(maxResults);
      List l=q.list();
      return l;
   }

   public List execPagedQuery(String queryString,String name,Object value,int firstResult,int maxResults) {
      Query q=createQuery(queryString);
      q.setParameter(name,value);
      q.setFirstResult(firstResult);
      q.setMaxResults(maxResults);
      List l=q.list();
      return l;
   }

   public List execPagedQuery(String queryString,String[] names,Object[] values,int firstResult,int maxResults) {
      Query q=createQuery(queryString);
      for (int i=0;i<names.length;i++) {
         q.setParameter(names[i],values[i]);
      }
      q.setFirstResult(firstResult);
      q.setMaxResults(maxResults);
      List l=q.list();
      return l;
   }

}







