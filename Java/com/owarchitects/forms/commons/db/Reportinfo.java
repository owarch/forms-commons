package com.owarchitects.forms.commons.db;

import java.io.Serializable;
import java.util.Date;
import java.util.Set;
import org.apache.commons.lang.builder.ToStringBuilder;


/** @author Hibernate CodeGenerator */
public class Reportinfo implements Serializable {

    /** identifier field */
    private long reportinfoid;

    /** nullable persistent field */
    private Long remotereportinfoid;

    /** persistent field */
    private long preporthaid;

    /** persistent field */
    private int resourcetype;

    /** nullable persistent field */
    private String description;

    /** nullable persistent field */
    private String creationdate;

    /** nullable persistent field */
    private String notes;

    /** nullable persistent field */
    private Date uploadtime;

    /** nullable persistent field */
    private Long uploaduser;

    /** persistent field */
    private com.owarchitects.forms.commons.db.Allinst allinst;

    /** persistent field */
    private com.owarchitects.forms.commons.db.Supportedfileformats supportedfileformats;

    /** persistent field */
    private Set reportstore;

    /** full constructor */
    public Reportinfo(Long remotereportinfoid, long preporthaid, int resourcetype, String description, String creationdate, String notes, Date uploadtime, Long uploaduser, com.owarchitects.forms.commons.db.Allinst allinst, com.owarchitects.forms.commons.db.Supportedfileformats supportedfileformats, Set reportstore) {
        this.remotereportinfoid = remotereportinfoid;
        this.preporthaid = preporthaid;
        this.resourcetype = resourcetype;
        this.description = description;
        this.creationdate = creationdate;
        this.notes = notes;
        this.uploadtime = uploadtime;
        this.uploaduser = uploaduser;
        this.allinst = allinst;
        this.supportedfileformats = supportedfileformats;
        this.reportstore = reportstore;
    }

    /** default constructor */
    public Reportinfo() {
    }

    /** minimal constructor */
    public Reportinfo(long preporthaid, int resourcetype, com.owarchitects.forms.commons.db.Allinst allinst, com.owarchitects.forms.commons.db.Supportedfileformats supportedfileformats, Set reportstore) {
        this.preporthaid = preporthaid;
        this.resourcetype = resourcetype;
        this.allinst = allinst;
        this.supportedfileformats = supportedfileformats;
        this.reportstore = reportstore;
    }

    public long getReportinfoid() {
        return this.reportinfoid;
    }

    public void setReportinfoid(long reportinfoid) {
        this.reportinfoid = reportinfoid;
    }

    public Long getRemotereportinfoid() {
        return this.remotereportinfoid;
    }

    public void setRemotereportinfoid(Long remotereportinfoid) {
        this.remotereportinfoid = remotereportinfoid;
    }

    public long getPreporthaid() {
        return this.preporthaid;
    }

    public void setPreporthaid(long preporthaid) {
        this.preporthaid = preporthaid;
    }

    public int getResourcetype() {
        return this.resourcetype;
    }

    public void setResourcetype(int resourcetype) {
        this.resourcetype = resourcetype;
    }

    public String getDescription() {
        return this.description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getCreationdate() {
        return this.creationdate;
    }

    public void setCreationdate(String creationdate) {
        this.creationdate = creationdate;
    }

    public String getNotes() {
        return this.notes;
    }

    public void setNotes(String notes) {
        this.notes = notes;
    }

    public Date getUploadtime() {
        return this.uploadtime;
    }

    public void setUploadtime(Date uploadtime) {
        this.uploadtime = uploadtime;
    }

    public Long getUploaduser() {
        return this.uploaduser;
    }

    public void setUploaduser(Long uploaduser) {
        this.uploaduser = uploaduser;
    }

    public com.owarchitects.forms.commons.db.Allinst getAllinst() {
        return this.allinst;
    }

    public void setAllinst(com.owarchitects.forms.commons.db.Allinst allinst) {
        this.allinst = allinst;
    }

    public com.owarchitects.forms.commons.db.Supportedfileformats getSupportedfileformats() {
        return this.supportedfileformats;
    }

    public void setSupportedfileformats(com.owarchitects.forms.commons.db.Supportedfileformats supportedfileformats) {
        this.supportedfileformats = supportedfileformats;
    }

    public Set getReportstore() {
        return this.reportstore;
    }

    public void setReportstore(Set reportstore) {
        this.reportstore = reportstore;
    }

    public String toString() {
        return new ToStringBuilder(this)
            .append("reportinfoid", getReportinfoid())
            .toString();
    }

}
