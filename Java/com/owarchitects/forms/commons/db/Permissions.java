package com.owarchitects.forms.commons.db;

import java.io.Serializable;
import java.util.Date;
import org.apache.commons.lang.builder.ToStringBuilder;


/** @author Hibernate CodeGenerator */
public class Permissions implements Serializable {

    /** identifier field */
    private long permid;

    /** persistent field */
    private int permpos;

    /** persistent field */
    private String permacr;

    /** persistent field */
    private String permdesc;

    /** persistent field */
    private int permorder;

    /** persistent field */
    private boolean isdeleted;

    /** nullable persistent field */
    private Date recmodtime;

    /** persistent field */
    private com.owarchitects.forms.commons.db.Permissioncats permissioncats;

    /** full constructor */
    public Permissions(int permpos, String permacr, String permdesc, int permorder, boolean isdeleted, Date recmodtime, com.owarchitects.forms.commons.db.Permissioncats permissioncats) {
        this.permpos = permpos;
        this.permacr = permacr;
        this.permdesc = permdesc;
        this.permorder = permorder;
        this.isdeleted = isdeleted;
        this.recmodtime = recmodtime;
        this.permissioncats = permissioncats;
    }

    /** default constructor */
    public Permissions() {
    }

    /** minimal constructor */
    public Permissions(int permpos, String permacr, String permdesc, int permorder, boolean isdeleted, com.owarchitects.forms.commons.db.Permissioncats permissioncats) {
        this.permpos = permpos;
        this.permacr = permacr;
        this.permdesc = permdesc;
        this.permorder = permorder;
        this.isdeleted = isdeleted;
        this.permissioncats = permissioncats;
    }

    public long getPermid() {
        return this.permid;
    }

    public void setPermid(long permid) {
        this.permid = permid;
    }

    public int getPermpos() {
        return this.permpos;
    }

    public void setPermpos(int permpos) {
        this.permpos = permpos;
    }

    public String getPermacr() {
        return this.permacr;
    }

    public void setPermacr(String permacr) {
        this.permacr = permacr;
    }

    public String getPermdesc() {
        return this.permdesc;
    }

    public void setPermdesc(String permdesc) {
        this.permdesc = permdesc;
    }

    public int getPermorder() {
        return this.permorder;
    }

    public void setPermorder(int permorder) {
        this.permorder = permorder;
    }

    public boolean isIsdeleted() {
        return this.isdeleted;
    }

    public void setIsdeleted(boolean isdeleted) {
        this.isdeleted = isdeleted;
    }

    public Date getRecmodtime() {
        return this.recmodtime;
    }

    public void setRecmodtime(Date recmodtime) {
        this.recmodtime = recmodtime;
    }

    public com.owarchitects.forms.commons.db.Permissioncats getPermissioncats() {
        return this.permissioncats;
    }

    public void setPermissioncats(com.owarchitects.forms.commons.db.Permissioncats permissioncats) {
        this.permissioncats = permissioncats;
    }

    public String toString() {
        return new ToStringBuilder(this)
            .append("permid", getPermid())
            .toString();
    }

}
