package com.owarchitects.forms.commons.db;

import java.io.Serializable;
import java.util.Date;
import org.apache.commons.lang.builder.ToStringBuilder;


/** @author Hibernate CodeGenerator */
public class Systemlog implements Serializable {

    /** identifier field */
    private long slid;

    /** nullable persistent field */
    private String userdesc;

    /** nullable persistent field */
    private String ipaddr;

    /** nullable persistent field */
    private String servletpath;

    /** nullable persistent field */
    private String detail;

    /** nullable persistent field */
    private Date timestamp;

    /** nullable persistent field */
    private Date recmodtime;

    /** full constructor */
    public Systemlog(String userdesc, String ipaddr, String servletpath, String detail, Date timestamp, Date recmodtime) {
        this.userdesc = userdesc;
        this.ipaddr = ipaddr;
        this.servletpath = servletpath;
        this.detail = detail;
        this.timestamp = timestamp;
        this.recmodtime = recmodtime;
    }

    /** default constructor */
    public Systemlog() {
    }

    public long getSlid() {
        return this.slid;
    }

    public void setSlid(long slid) {
        this.slid = slid;
    }

    public String getUserdesc() {
        return this.userdesc;
    }

    public void setUserdesc(String userdesc) {
        this.userdesc = userdesc;
    }

    public String getIpaddr() {
        return this.ipaddr;
    }

    public void setIpaddr(String ipaddr) {
        this.ipaddr = ipaddr;
    }

    public String getServletpath() {
        return this.servletpath;
    }

    public void setServletpath(String servletpath) {
        this.servletpath = servletpath;
    }

    public String getDetail() {
        return this.detail;
    }

    public void setDetail(String detail) {
        this.detail = detail;
    }

    public Date getTimestamp() {
        return this.timestamp;
    }

    public void setTimestamp(Date timestamp) {
        this.timestamp = timestamp;
    }

    public Date getRecmodtime() {
        return this.recmodtime;
    }

    public void setRecmodtime(Date recmodtime) {
        this.recmodtime = recmodtime;
    }

    public String toString() {
        return new ToStringBuilder(this)
            .append("slid", getSlid())
            .toString();
    }

}
