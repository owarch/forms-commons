package com.owarchitects.forms.commons.db;

import java.io.Serializable;
import java.util.Date;
import org.apache.commons.lang.builder.ToStringBuilder;


/** @author Hibernate CodeGenerator */
public class Analystroles_transfer implements Serializable {

    /** identifier field */
    private long aroleid;

    /** persistent field */
    private String roleacr;

    /** persistent field */
    private String roledesc;

    /** nullable persistent field */
    private Date recmodtime;

    /** full constructor */
    public Analystroles_transfer(long aroleid, String roleacr, String roledesc, Date recmodtime) {
        this.aroleid = aroleid;
        this.roleacr = roleacr;
        this.roledesc = roledesc;
        this.recmodtime = recmodtime;
    }

    /** default constructor */
    public Analystroles_transfer() {
    }

    /** minimal constructor */
    public Analystroles_transfer(long aroleid, String roleacr, String roledesc) {
        this.aroleid = aroleid;
        this.roleacr = roleacr;
        this.roledesc = roledesc;
    }

    public long getAroleid() {
        return this.aroleid;
    }

    public void setAroleid(long aroleid) {
        this.aroleid = aroleid;
    }

    public String getRoleacr() {
        return this.roleacr;
    }

    public void setRoleacr(String roleacr) {
        this.roleacr = roleacr;
    }

    public String getRoledesc() {
        return this.roledesc;
    }

    public void setRoledesc(String roledesc) {
        this.roledesc = roledesc;
    }

    public Date getRecmodtime() {
        return this.recmodtime;
    }

    public void setRecmodtime(Date recmodtime) {
        this.recmodtime = recmodtime;
    }

    public String toString() {
        return new ToStringBuilder(this)
            .append("aroleid", getAroleid())
            .toString();
    }

}
