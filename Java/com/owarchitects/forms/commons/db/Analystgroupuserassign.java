package com.owarchitects.forms.commons.db;

import java.io.Serializable;
import java.util.Date;
import org.apache.commons.lang.builder.ToStringBuilder;


/** @author Hibernate CodeGenerator */
public class Analystgroupuserassign implements Serializable {

    /** identifier field */
    private long aguaid;

    /** nullable persistent field */
    private Date recmodtime;

    /** persistent field */
    private com.owarchitects.forms.commons.db.Analystgroups group;

    /** persistent field */
    private com.owarchitects.forms.commons.db.Allusers user;

    /** full constructor */
    public Analystgroupuserassign(Date recmodtime, com.owarchitects.forms.commons.db.Analystgroups group, com.owarchitects.forms.commons.db.Allusers user) {
        this.recmodtime = recmodtime;
        this.group = group;
        this.user = user;
    }

    /** default constructor */
    public Analystgroupuserassign() {
    }

    /** minimal constructor */
    public Analystgroupuserassign(com.owarchitects.forms.commons.db.Analystgroups group, com.owarchitects.forms.commons.db.Allusers user) {
        this.group = group;
        this.user = user;
    }

    public long getAguaid() {
        return this.aguaid;
    }

    public void setAguaid(long aguaid) {
        this.aguaid = aguaid;
    }

    public Date getRecmodtime() {
        return this.recmodtime;
    }

    public void setRecmodtime(Date recmodtime) {
        this.recmodtime = recmodtime;
    }

    public com.owarchitects.forms.commons.db.Analystgroups getGroup() {
        return this.group;
    }

    public void setGroup(com.owarchitects.forms.commons.db.Analystgroups group) {
        this.group = group;
    }

    public com.owarchitects.forms.commons.db.Allusers getUser() {
        return this.user;
    }

    public void setUser(com.owarchitects.forms.commons.db.Allusers user) {
        this.user = user;
    }

    public String toString() {
        return new ToStringBuilder(this)
            .append("aguaid", getAguaid())
            .toString();
    }

}
