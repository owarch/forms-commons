package com.owarchitects.forms.commons.db;

import java.io.Serializable;
import java.util.Date;
import java.util.Set;
import org.apache.commons.lang.builder.ToStringBuilder;


/** @author Hibernate CodeGenerator */
public class Nested implements Serializable {

    /** identifier field */
    private long id;

    /** nullable persistent field */
    private Long pid;

    /** persistent field */
    private String showstr;

    /** nullable persistent field */
    private Date recmodtime;

    /** nullable persistent field */
    private com.owarchitects.forms.commons.db.Nested parent;

    /** persistent field */
    private Set children;

    /** full constructor */
    public Nested(Long pid, String showstr, Date recmodtime, com.owarchitects.forms.commons.db.Nested parent, Set children) {
        this.pid = pid;
        this.showstr = showstr;
        this.recmodtime = recmodtime;
        this.parent = parent;
        this.children = children;
    }

    /** default constructor */
    public Nested() {
    }

    /** minimal constructor */
    public Nested(String showstr, Set children) {
        this.showstr = showstr;
        this.children = children;
    }

    public long getId() {
        return this.id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public Long getPid() {
        return this.pid;
    }

    public void setPid(Long pid) {
        this.pid = pid;
    }

    public String getShowstr() {
        return this.showstr;
    }

    public void setShowstr(String showstr) {
        this.showstr = showstr;
    }

    public Date getRecmodtime() {
        return this.recmodtime;
    }

    public void setRecmodtime(Date recmodtime) {
        this.recmodtime = recmodtime;
    }

    public com.owarchitects.forms.commons.db.Nested getParent() {
        return this.parent;
    }

    public void setParent(com.owarchitects.forms.commons.db.Nested parent) {
        this.parent = parent;
    }

    public Set getChildren() {
        return this.children;
    }

    public void setChildren(Set children) {
        this.children = children;
    }

    public String toString() {
        return new ToStringBuilder(this)
            .append("id", getId())
            .toString();
    }

}
