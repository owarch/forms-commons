package com.owarchitects.forms.commons.db;

import java.io.Serializable;
import java.util.Date;
import org.apache.commons.lang.builder.ToStringBuilder;


/** @author Hibernate CodeGenerator */
public class Userinfomodtime_transfer implements Serializable {

    /** identifier field */
    private long aumtid;

    /** nullable persistent field */
    private Date remotemod;

    /** nullable persistent field */
    private Date recmodtime;

    /** persistent field */
    private long linstid;

    /** full constructor */
    public Userinfomodtime_transfer(long aumtid, Date remotemod, Date recmodtime, long linstid) {
        this.aumtid = aumtid;
        this.remotemod = remotemod;
        this.recmodtime = recmodtime;
        this.linstid = linstid;
    }

    /** default constructor */
    public Userinfomodtime_transfer() {
    }

    /** minimal constructor */
    public Userinfomodtime_transfer(long aumtid, long linstid) {
        this.aumtid = aumtid;
        this.linstid = linstid;
    }

    public long getAumtid() {
        return this.aumtid;
    }

    public void setAumtid(long aumtid) {
        this.aumtid = aumtid;
    }

    public Date getRemotemod() {
        return this.remotemod;
    }

    public void setRemotemod(Date remotemod) {
        this.remotemod = remotemod;
    }

    public Date getRecmodtime() {
        return this.recmodtime;
    }

    public void setRecmodtime(Date recmodtime) {
        this.recmodtime = recmodtime;
    }

    public long getLinstid() {
        return this.linstid;
    }

    public void setLinstid(long linstid) {
        this.linstid = linstid;
    }

    public String toString() {
        return new ToStringBuilder(this)
            .append("aumtid", getAumtid())
            .toString();
    }

}
