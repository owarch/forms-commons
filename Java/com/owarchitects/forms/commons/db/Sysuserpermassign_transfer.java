package com.owarchitects.forms.commons.db;

import java.io.Serializable;
import java.util.Date;
import org.apache.commons.lang.builder.ToStringBuilder;


/** @author Hibernate CodeGenerator */
public class Sysuserpermassign_transfer implements Serializable {

    /** identifier field */
    private long supid;

    /** persistent field */
    private long auserid;

    /** persistent field */
    private long pcatid;

    /** nullable persistent field */
    private Long siteid;

    /** nullable persistent field */
    private Long studyid;

    /** persistent field */
    private Serializable permlevel;

    /** nullable persistent field */
    private Date recmodtime;

    /** full constructor */
    public Sysuserpermassign_transfer(long supid, long auserid, long pcatid, Long siteid, Long studyid, Serializable permlevel, Date recmodtime) {
        this.supid = supid;
        this.auserid = auserid;
        this.pcatid = pcatid;
        this.siteid = siteid;
        this.studyid = studyid;
        this.permlevel = permlevel;
        this.recmodtime = recmodtime;
    }

    /** default constructor */
    public Sysuserpermassign_transfer() {
    }

    /** minimal constructor */
    public Sysuserpermassign_transfer(long supid, long auserid, long pcatid, Serializable permlevel) {
        this.supid = supid;
        this.auserid = auserid;
        this.pcatid = pcatid;
        this.permlevel = permlevel;
    }

    public long getSupid() {
        return this.supid;
    }

    public void setSupid(long supid) {
        this.supid = supid;
    }

    public long getAuserid() {
        return this.auserid;
    }

    public void setAuserid(long auserid) {
        this.auserid = auserid;
    }

    public long getPcatid() {
        return this.pcatid;
    }

    public void setPcatid(long pcatid) {
        this.pcatid = pcatid;
    }

    public Long getSiteid() {
        return this.siteid;
    }

    public void setSiteid(Long siteid) {
        this.siteid = siteid;
    }

    public Long getStudyid() {
        return this.studyid;
    }

    public void setStudyid(Long studyid) {
        this.studyid = studyid;
    }

    public Serializable getPermlevel() {
        return this.permlevel;
    }

    public void setPermlevel(Serializable permlevel) {
        this.permlevel = permlevel;
    }

    public Date getRecmodtime() {
        return this.recmodtime;
    }

    public void setRecmodtime(Date recmodtime) {
        this.recmodtime = recmodtime;
    }

    public String toString() {
        return new ToStringBuilder(this)
            .append("supid", getSupid())
            .toString();
    }

}
