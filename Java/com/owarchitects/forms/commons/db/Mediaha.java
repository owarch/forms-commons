package com.owarchitects.forms.commons.db;

import java.io.Serializable;
import org.apache.commons.lang.builder.ToStringBuilder;


/** @author Hibernate CodeGenerator */
public class Mediaha implements Serializable {

    /** identifier field */
    private long mediahaid;

    /** persistent field */
    private int visibility;

    /** nullable persistent field */
    private Long pmediahaid;

    /** nullable persistent field */
    private String hadesc;

    /** nullable persistent field */
    private Integer haordr;

    /** persistent field */
    private com.owarchitects.forms.commons.db.Studies studies;

    /** full constructor */
    public Mediaha(int visibility, Long pmediahaid, String hadesc, Integer haordr, com.owarchitects.forms.commons.db.Studies studies) {
        this.visibility = visibility;
        this.pmediahaid = pmediahaid;
        this.hadesc = hadesc;
        this.haordr = haordr;
        this.studies = studies;
    }

    /** default constructor */
    public Mediaha() {
    }

    /** minimal constructor */
    public Mediaha(int visibility, com.owarchitects.forms.commons.db.Studies studies) {
        this.visibility = visibility;
        this.studies = studies;
    }

    public long getMediahaid() {
        return this.mediahaid;
    }

    public void setMediahaid(long mediahaid) {
        this.mediahaid = mediahaid;
    }

    public int getVisibility() {
        return this.visibility;
    }

    public void setVisibility(int visibility) {
        this.visibility = visibility;
    }

    public Long getPmediahaid() {
        return this.pmediahaid;
    }

    public void setPmediahaid(Long pmediahaid) {
        this.pmediahaid = pmediahaid;
    }

    public String getHadesc() {
        return this.hadesc;
    }

    public void setHadesc(String hadesc) {
        this.hadesc = hadesc;
    }

    public Integer getHaordr() {
        return this.haordr;
    }

    public void setHaordr(Integer haordr) {
        this.haordr = haordr;
    }

    public com.owarchitects.forms.commons.db.Studies getStudies() {
        return this.studies;
    }

    public void setStudies(com.owarchitects.forms.commons.db.Studies studies) {
        this.studies = studies;
    }

    public String toString() {
        return new ToStringBuilder(this)
            .append("mediahaid", getMediahaid())
            .toString();
    }

}
