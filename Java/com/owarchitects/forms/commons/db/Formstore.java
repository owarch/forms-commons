package com.owarchitects.forms.commons.db;

import java.io.Serializable;
import java.util.Date;
import org.apache.commons.lang.builder.ToStringBuilder;


/** @author Hibernate CodeGenerator */
public class Formstore implements Serializable {

    /** identifier field */
    private long formstoreid;

    /** persistent field */
    private String formcontent;

    /** nullable persistent field */
    private Date recmodtime;

    /** persistent field */
    private com.owarchitects.forms.commons.db.Formtypes formtypes;

    /** full constructor */
    public Formstore(String formcontent, Date recmodtime, com.owarchitects.forms.commons.db.Formtypes formtypes) {
        this.formcontent = formcontent;
        this.recmodtime = recmodtime;
        this.formtypes = formtypes;
    }

    /** default constructor */
    public Formstore() {
    }

    /** minimal constructor */
    public Formstore(String formcontent, com.owarchitects.forms.commons.db.Formtypes formtypes) {
        this.formcontent = formcontent;
        this.formtypes = formtypes;
    }

    public long getFormstoreid() {
        return this.formstoreid;
    }

    public void setFormstoreid(long formstoreid) {
        this.formstoreid = formstoreid;
    }

    public String getFormcontent() {
        return this.formcontent;
    }

    public void setFormcontent(String formcontent) {
        this.formcontent = formcontent;
    }

    public Date getRecmodtime() {
        return this.recmodtime;
    }

    public void setRecmodtime(Date recmodtime) {
        this.recmodtime = recmodtime;
    }

    public com.owarchitects.forms.commons.db.Formtypes getFormtypes() {
        return this.formtypes;
    }

    public void setFormtypes(com.owarchitects.forms.commons.db.Formtypes formtypes) {
        this.formtypes = formtypes;
    }

    public String toString() {
        return new ToStringBuilder(this)
            .append("formstoreid", getFormstoreid())
            .toString();
    }

}
