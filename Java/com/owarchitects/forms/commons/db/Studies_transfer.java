package com.owarchitects.forms.commons.db;

import java.io.Serializable;
import java.util.Date;
import org.apache.commons.lang.builder.ToStringBuilder;


/** @author Hibernate CodeGenerator */
public class Studies_transfer implements Serializable {

    /** identifier field */
    private long studyid;

    /** persistent field */
    private long siteid;

    /** persistent field */
    private String studyno;

    /** nullable persistent field */
    private String studydesc;

    /** nullable persistent field */
    private Boolean isarchived;

    /** nullable persistent field */
    private Date recmodtime;

    /** full constructor */
    public Studies_transfer(long studyid, long siteid, String studyno, String studydesc, Boolean isarchived, Date recmodtime) {
        this.studyid = studyid;
        this.siteid = siteid;
        this.studyno = studyno;
        this.studydesc = studydesc;
        this.isarchived = isarchived;
        this.recmodtime = recmodtime;
    }

    /** default constructor */
    public Studies_transfer() {
    }

    /** minimal constructor */
    public Studies_transfer(long studyid, long siteid, String studyno) {
        this.studyid = studyid;
        this.siteid = siteid;
        this.studyno = studyno;
    }

    public long getStudyid() {
        return this.studyid;
    }

    public void setStudyid(long studyid) {
        this.studyid = studyid;
    }

    public long getSiteid() {
        return this.siteid;
    }

    public void setSiteid(long siteid) {
        this.siteid = siteid;
    }

    public String getStudyno() {
        return this.studyno;
    }

    public void setStudyno(String studyno) {
        this.studyno = studyno;
    }

    public String getStudydesc() {
        return this.studydesc;
    }

    public void setStudydesc(String studydesc) {
        this.studydesc = studydesc;
    }

    public Boolean getIsarchived() {
        return this.isarchived;
    }

    public void setIsarchived(Boolean isarchived) {
        this.isarchived = isarchived;
    }

    public Date getRecmodtime() {
        return this.recmodtime;
    }

    public void setRecmodtime(Date recmodtime) {
        this.recmodtime = recmodtime;
    }

    public String toString() {
        return new ToStringBuilder(this)
            .append("studyid", getStudyid())
            .toString();
    }

}
