package com.owarchitects.forms.commons.db;

import java.io.Serializable;
import java.util.Date;
import org.apache.commons.lang.builder.ToStringBuilder;


/** @author Hibernate CodeGenerator */
public class Mediarolepermprofassign_transfer implements Serializable {

    /** identifier field */
    private long mrppaid;

    /** persistent field */
    private long mprofileid;

    /** persistent field */
    private long rroleid;

    /** persistent field */
    private Serializable permlevel;

    /** nullable persistent field */
    private Date recmodtime;

    /** full constructor */
    public Mediarolepermprofassign_transfer(long mrppaid, long mprofileid, long rroleid, Serializable permlevel, Date recmodtime) {
        this.mrppaid = mrppaid;
        this.mprofileid = mprofileid;
        this.rroleid = rroleid;
        this.permlevel = permlevel;
        this.recmodtime = recmodtime;
    }

    /** default constructor */
    public Mediarolepermprofassign_transfer() {
    }

    /** minimal constructor */
    public Mediarolepermprofassign_transfer(long mrppaid, long mprofileid, long rroleid, Serializable permlevel) {
        this.mrppaid = mrppaid;
        this.mprofileid = mprofileid;
        this.rroleid = rroleid;
        this.permlevel = permlevel;
    }

    public long getMrppaid() {
        return this.mrppaid;
    }

    public void setMrppaid(long mrppaid) {
        this.mrppaid = mrppaid;
    }

    public long getMprofileid() {
        return this.mprofileid;
    }

    public void setMprofileid(long mprofileid) {
        this.mprofileid = mprofileid;
    }

    public long getRroleid() {
        return this.rroleid;
    }

    public void setRroleid(long rroleid) {
        this.rroleid = rroleid;
    }

    public Serializable getPermlevel() {
        return this.permlevel;
    }

    public void setPermlevel(Serializable permlevel) {
        this.permlevel = permlevel;
    }

    public Date getRecmodtime() {
        return this.recmodtime;
    }

    public void setRecmodtime(Date recmodtime) {
        this.recmodtime = recmodtime;
    }

    public String toString() {
        return new ToStringBuilder(this)
            .append("mrppaid", getMrppaid())
            .toString();
    }

}
