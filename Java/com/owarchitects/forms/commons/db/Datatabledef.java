package com.owarchitects.forms.commons.db;

import java.io.Serializable;
import java.util.Date;
import java.util.Set;
import org.apache.commons.lang.builder.ToStringBuilder;


/** @author Hibernate CodeGenerator */
public class Datatabledef implements Serializable {

    /** identifier field */
    private long dtdefid;

    /** nullable persistent field */
    private Long remotedtdefid;

    /** persistent field */
    private long pformhaid;

    /** persistent field */
    private long pdthaid;

    /** persistent field */
    private String dtacr;

    /** nullable persistent field */
    private String dtdesc;

    /** nullable persistent field */
    private String formno;

    /** nullable persistent field */
    private String formver;

    /** nullable persistent field */
    private String formacr;

    /** nullable persistent field */
    private String formdesc;

    /** nullable persistent field */
    private Integer displayas;

    /** nullable persistent field */
    private Boolean ssaonly;

    /** nullable persistent field */
    private Boolean isarchived;

    /** nullable persistent field */
    private Boolean isenrollform;

    /** nullable persistent field */
    private String notes;

    /** nullable persistent field */
    private Long uploaduser;

    /** nullable persistent field */
    private Date uploadtime;

    /** nullable persistent field */
    private Date recmodtime;

    /** persistent field */
    private com.owarchitects.forms.commons.db.Allinst allinst;

    /** persistent field */
    private Set formtypes;

    /** persistent field */
    private Set datatablecoldef;

    /** persistent field */
    private Set formattrs;

    /** persistent field */
    private Set dtfieldattrs;

    /** full constructor */
    public Datatabledef(Long remotedtdefid, long pformhaid, long pdthaid, String dtacr, String dtdesc, String formno, String formver, String formacr, String formdesc, Integer displayas, Boolean ssaonly, Boolean isarchived, Boolean isenrollform, String notes, Long uploaduser, Date uploadtime, Date recmodtime, com.owarchitects.forms.commons.db.Allinst allinst, Set formtypes, Set datatablecoldef, Set formattrs, Set dtfieldattrs) {
        this.remotedtdefid = remotedtdefid;
        this.pformhaid = pformhaid;
        this.pdthaid = pdthaid;
        this.dtacr = dtacr;
        this.dtdesc = dtdesc;
        this.formno = formno;
        this.formver = formver;
        this.formacr = formacr;
        this.formdesc = formdesc;
        this.displayas = displayas;
        this.ssaonly = ssaonly;
        this.isarchived = isarchived;
        this.isenrollform = isenrollform;
        this.notes = notes;
        this.uploaduser = uploaduser;
        this.uploadtime = uploadtime;
        this.recmodtime = recmodtime;
        this.allinst = allinst;
        this.formtypes = formtypes;
        this.datatablecoldef = datatablecoldef;
        this.formattrs = formattrs;
        this.dtfieldattrs = dtfieldattrs;
    }

    /** default constructor */
    public Datatabledef() {
    }

    /** minimal constructor */
    public Datatabledef(long pformhaid, long pdthaid, String dtacr, com.owarchitects.forms.commons.db.Allinst allinst, Set formtypes, Set datatablecoldef, Set formattrs, Set dtfieldattrs) {
        this.pformhaid = pformhaid;
        this.pdthaid = pdthaid;
        this.dtacr = dtacr;
        this.allinst = allinst;
        this.formtypes = formtypes;
        this.datatablecoldef = datatablecoldef;
        this.formattrs = formattrs;
        this.dtfieldattrs = dtfieldattrs;
    }

    public long getDtdefid() {
        return this.dtdefid;
    }

    public void setDtdefid(long dtdefid) {
        this.dtdefid = dtdefid;
    }

    public Long getRemotedtdefid() {
        return this.remotedtdefid;
    }

    public void setRemotedtdefid(Long remotedtdefid) {
        this.remotedtdefid = remotedtdefid;
    }

    public long getPformhaid() {
        return this.pformhaid;
    }

    public void setPformhaid(long pformhaid) {
        this.pformhaid = pformhaid;
    }

    public long getPdthaid() {
        return this.pdthaid;
    }

    public void setPdthaid(long pdthaid) {
        this.pdthaid = pdthaid;
    }

    public String getDtacr() {
        return this.dtacr;
    }

    public void setDtacr(String dtacr) {
        this.dtacr = dtacr;
    }

    public String getDtdesc() {
        return this.dtdesc;
    }

    public void setDtdesc(String dtdesc) {
        this.dtdesc = dtdesc;
    }

    public String getFormno() {
        return this.formno;
    }

    public void setFormno(String formno) {
        this.formno = formno;
    }

    public String getFormver() {
        return this.formver;
    }

    public void setFormver(String formver) {
        this.formver = formver;
    }

    public String getFormacr() {
        return this.formacr;
    }

    public void setFormacr(String formacr) {
        this.formacr = formacr;
    }

    public String getFormdesc() {
        return this.formdesc;
    }

    public void setFormdesc(String formdesc) {
        this.formdesc = formdesc;
    }

    public Integer getDisplayas() {
        return this.displayas;
    }

    public void setDisplayas(Integer displayas) {
        this.displayas = displayas;
    }

    public Boolean getSsaonly() {
        return this.ssaonly;
    }

    public void setSsaonly(Boolean ssaonly) {
        this.ssaonly = ssaonly;
    }

    public Boolean getIsarchived() {
        return this.isarchived;
    }

    public void setIsarchived(Boolean isarchived) {
        this.isarchived = isarchived;
    }

    public Boolean getIsenrollform() {
        return this.isenrollform;
    }

    public void setIsenrollform(Boolean isenrollform) {
        this.isenrollform = isenrollform;
    }

    public String getNotes() {
        return this.notes;
    }

    public void setNotes(String notes) {
        this.notes = notes;
    }

    public Long getUploaduser() {
        return this.uploaduser;
    }

    public void setUploaduser(Long uploaduser) {
        this.uploaduser = uploaduser;
    }

    public Date getUploadtime() {
        return this.uploadtime;
    }

    public void setUploadtime(Date uploadtime) {
        this.uploadtime = uploadtime;
    }

    public Date getRecmodtime() {
        return this.recmodtime;
    }

    public void setRecmodtime(Date recmodtime) {
        this.recmodtime = recmodtime;
    }

    public com.owarchitects.forms.commons.db.Allinst getAllinst() {
        return this.allinst;
    }

    public void setAllinst(com.owarchitects.forms.commons.db.Allinst allinst) {
        this.allinst = allinst;
    }

    public Set getFormtypes() {
        return this.formtypes;
    }

    public void setFormtypes(Set formtypes) {
        this.formtypes = formtypes;
    }

    public Set getDatatablecoldef() {
        return this.datatablecoldef;
    }

    public void setDatatablecoldef(Set datatablecoldef) {
        this.datatablecoldef = datatablecoldef;
    }

    public Set getFormattrs() {
        return this.formattrs;
    }

    public void setFormattrs(Set formattrs) {
        this.formattrs = formattrs;
    }

    public Set getDtfieldattrs() {
        return this.dtfieldattrs;
    }

    public void setDtfieldattrs(Set dtfieldattrs) {
        this.dtfieldattrs = dtfieldattrs;
    }

    public String toString() {
        return new ToStringBuilder(this)
            .append("dtdefid", getDtdefid())
            .toString();
    }

}
