package com.owarchitects.forms.commons.db;

import java.io.Serializable;
import java.util.Date;
import org.apache.commons.lang.builder.ToStringBuilder;


/** @author Hibernate CodeGenerator */
public class Mediarolepermprofassign implements Serializable {

    /** identifier field */
    private long mrppaid;

    /** persistent field */
    private Serializable permlevel;

    /** nullable persistent field */
    private Date recmodtime;

    /** persistent field */
    private com.owarchitects.forms.commons.db.Mediapermprofiles mediapermprofiles;

    /** persistent field */
    private com.owarchitects.forms.commons.db.Resourceroles resourceroles;

    /** full constructor */
    public Mediarolepermprofassign(Serializable permlevel, Date recmodtime, com.owarchitects.forms.commons.db.Mediapermprofiles mediapermprofiles, com.owarchitects.forms.commons.db.Resourceroles resourceroles) {
        this.permlevel = permlevel;
        this.recmodtime = recmodtime;
        this.mediapermprofiles = mediapermprofiles;
        this.resourceroles = resourceroles;
    }

    /** default constructor */
    public Mediarolepermprofassign() {
    }

    /** minimal constructor */
    public Mediarolepermprofassign(Serializable permlevel, com.owarchitects.forms.commons.db.Mediapermprofiles mediapermprofiles, com.owarchitects.forms.commons.db.Resourceroles resourceroles) {
        this.permlevel = permlevel;
        this.mediapermprofiles = mediapermprofiles;
        this.resourceroles = resourceroles;
    }

    public long getMrppaid() {
        return this.mrppaid;
    }

    public void setMrppaid(long mrppaid) {
        this.mrppaid = mrppaid;
    }

    public Serializable getPermlevel() {
        return this.permlevel;
    }

    public void setPermlevel(Serializable permlevel) {
        this.permlevel = permlevel;
    }

    public Date getRecmodtime() {
        return this.recmodtime;
    }

    public void setRecmodtime(Date recmodtime) {
        this.recmodtime = recmodtime;
    }

    public com.owarchitects.forms.commons.db.Mediapermprofiles getMediapermprofiles() {
        return this.mediapermprofiles;
    }

    public void setMediapermprofiles(com.owarchitects.forms.commons.db.Mediapermprofiles mediapermprofiles) {
        this.mediapermprofiles = mediapermprofiles;
    }

    public com.owarchitects.forms.commons.db.Resourceroles getResourceroles() {
        return this.resourceroles;
    }

    public void setResourceroles(com.owarchitects.forms.commons.db.Resourceroles resourceroles) {
        this.resourceroles = resourceroles;
    }

    public String toString() {
        return new ToStringBuilder(this)
            .append("mrppaid", getMrppaid())
            .toString();
    }

}
