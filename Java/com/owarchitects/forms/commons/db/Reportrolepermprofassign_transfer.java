package com.owarchitects.forms.commons.db;

import java.io.Serializable;
import java.util.Date;
import org.apache.commons.lang.builder.ToStringBuilder;


/** @author Hibernate CodeGenerator */
public class Reportrolepermprofassign_transfer implements Serializable {

    /** identifier field */
    private long rrppaid;

    /** persistent field */
    private long rprofileid;

    /** persistent field */
    private long rroleid;

    /** persistent field */
    private Serializable permlevel;

    /** nullable persistent field */
    private Date recmodtime;

    /** full constructor */
    public Reportrolepermprofassign_transfer(long rrppaid, long rprofileid, long rroleid, Serializable permlevel, Date recmodtime) {
        this.rrppaid = rrppaid;
        this.rprofileid = rprofileid;
        this.rroleid = rroleid;
        this.permlevel = permlevel;
        this.recmodtime = recmodtime;
    }

    /** default constructor */
    public Reportrolepermprofassign_transfer() {
    }

    /** minimal constructor */
    public Reportrolepermprofassign_transfer(long rrppaid, long rprofileid, long rroleid, Serializable permlevel) {
        this.rrppaid = rrppaid;
        this.rprofileid = rprofileid;
        this.rroleid = rroleid;
        this.permlevel = permlevel;
    }

    public long getRrppaid() {
        return this.rrppaid;
    }

    public void setRrppaid(long rrppaid) {
        this.rrppaid = rrppaid;
    }

    public long getRprofileid() {
        return this.rprofileid;
    }

    public void setRprofileid(long rprofileid) {
        this.rprofileid = rprofileid;
    }

    public long getRroleid() {
        return this.rroleid;
    }

    public void setRroleid(long rroleid) {
        this.rroleid = rroleid;
    }

    public Serializable getPermlevel() {
        return this.permlevel;
    }

    public void setPermlevel(Serializable permlevel) {
        this.permlevel = permlevel;
    }

    public Date getRecmodtime() {
        return this.recmodtime;
    }

    public void setRecmodtime(Date recmodtime) {
        this.recmodtime = recmodtime;
    }

    public String toString() {
        return new ToStringBuilder(this)
            .append("rrppaid", getRrppaid())
            .toString();
    }

}
