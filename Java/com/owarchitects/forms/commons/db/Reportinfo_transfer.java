package com.owarchitects.forms.commons.db;

import java.io.Serializable;
import java.util.Date;
import org.apache.commons.lang.builder.ToStringBuilder;


/** @author Hibernate CodeGenerator */
public class Reportinfo_transfer implements Serializable {

    /** identifier field */
    private long reportinfoid;

    /** persistent field */
    private long ainstid;

    /** nullable persistent field */
    private Long remotereportinfoid;

    /** persistent field */
    private long preporthaid;

    /** nullable persistent field */
    private Boolean isletter;

    /** nullable persistent field */
    private String description;

    /** nullable persistent field */
    private String creationdate;

    /** nullable persistent field */
    private String notes;

    /** nullable persistent field */
    private Date uploadtime;

    /** nullable persistent field */
    private Long uploaduser;

    /** persistent field */
    private long fileformatid;

    /** full constructor */
    public Reportinfo_transfer(long reportinfoid, long ainstid, Long remotereportinfoid, long preporthaid, Boolean isletter, String description, String creationdate, String notes, Date uploadtime, Long uploaduser, long fileformatid) {
        this.reportinfoid = reportinfoid;
        this.ainstid = ainstid;
        this.remotereportinfoid = remotereportinfoid;
        this.preporthaid = preporthaid;
        this.isletter = isletter;
        this.description = description;
        this.creationdate = creationdate;
        this.notes = notes;
        this.uploadtime = uploadtime;
        this.uploaduser = uploaduser;
        this.fileformatid = fileformatid;
    }

    /** default constructor */
    public Reportinfo_transfer() {
    }

    /** minimal constructor */
    public Reportinfo_transfer(long reportinfoid, long ainstid, long preporthaid, long fileformatid) {
        this.reportinfoid = reportinfoid;
        this.ainstid = ainstid;
        this.preporthaid = preporthaid;
        this.fileformatid = fileformatid;
    }

    public long getReportinfoid() {
        return this.reportinfoid;
    }

    public void setReportinfoid(long reportinfoid) {
        this.reportinfoid = reportinfoid;
    }

    public long getAinstid() {
        return this.ainstid;
    }

    public void setAinstid(long ainstid) {
        this.ainstid = ainstid;
    }

    public Long getRemotereportinfoid() {
        return this.remotereportinfoid;
    }

    public void setRemotereportinfoid(Long remotereportinfoid) {
        this.remotereportinfoid = remotereportinfoid;
    }

    public long getPreporthaid() {
        return this.preporthaid;
    }

    public void setPreporthaid(long preporthaid) {
        this.preporthaid = preporthaid;
    }

    public Boolean getIsletter() {
        return this.isletter;
    }

    public void setIsletter(Boolean isletter) {
        this.isletter = isletter;
    }

    public String getDescription() {
        return this.description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getCreationdate() {
        return this.creationdate;
    }

    public void setCreationdate(String creationdate) {
        this.creationdate = creationdate;
    }

    public String getNotes() {
        return this.notes;
    }

    public void setNotes(String notes) {
        this.notes = notes;
    }

    public Date getUploadtime() {
        return this.uploadtime;
    }

    public void setUploadtime(Date uploadtime) {
        this.uploadtime = uploadtime;
    }

    public Long getUploaduser() {
        return this.uploaduser;
    }

    public void setUploaduser(Long uploaduser) {
        this.uploaduser = uploaduser;
    }

    public long getFileformatid() {
        return this.fileformatid;
    }

    public void setFileformatid(long fileformatid) {
        this.fileformatid = fileformatid;
    }

    public String toString() {
        return new ToStringBuilder(this)
            .append("reportinfoid", getReportinfoid())
            .toString();
    }

}
