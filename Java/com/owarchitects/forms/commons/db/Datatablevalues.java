package com.owarchitects.forms.commons.db;

import java.io.Serializable;
import org.apache.commons.lang.builder.ToStringBuilder;


/** @author Hibernate CodeGenerator */
public class Datatablevalues implements Serializable {

    /** identifier field */
    private long dtvalueid;

    /** persistent field */
    private String varname;

    /** nullable persistent field */
    private String varvalue;

    /** persistent field */
    private com.owarchitects.forms.commons.db.Datatablerecords datatablerecords;

    /** full constructor */
    public Datatablevalues(String varname, String varvalue, com.owarchitects.forms.commons.db.Datatablerecords datatablerecords) {
        this.varname = varname;
        this.varvalue = varvalue;
        this.datatablerecords = datatablerecords;
    }

    /** default constructor */
    public Datatablevalues() {
    }

    /** minimal constructor */
    public Datatablevalues(String varname, com.owarchitects.forms.commons.db.Datatablerecords datatablerecords) {
        this.varname = varname;
        this.datatablerecords = datatablerecords;
    }

    public long getDtvalueid() {
        return this.dtvalueid;
    }

    public void setDtvalueid(long dtvalueid) {
        this.dtvalueid = dtvalueid;
    }

    public String getVarname() {
        return this.varname;
    }

    public void setVarname(String varname) {
        this.varname = varname;
    }

    public String getVarvalue() {
        return this.varvalue;
    }

    public void setVarvalue(String varvalue) {
        this.varvalue = varvalue;
    }

    public com.owarchitects.forms.commons.db.Datatablerecords getDatatablerecords() {
        return this.datatablerecords;
    }

    public void setDatatablerecords(com.owarchitects.forms.commons.db.Datatablerecords datatablerecords) {
        this.datatablerecords = datatablerecords;
    }

    public String toString() {
        return new ToStringBuilder(this)
            .append("dtvalueid", getDtvalueid())
            .toString();
    }

}
