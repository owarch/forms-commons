package com.owarchitects.forms.commons.db;

import java.io.Serializable;
import java.util.Date;
import java.util.Set;
import org.apache.commons.lang.builder.ToStringBuilder;


/** @author Hibernate CodeGenerator */
public class Allusers implements Serializable {

    /** identifier field */
    private long auserid;

    /** persistent field */
    private long luserid;

    /** nullable persistent field */
    private String userdesc;

    /** nullable persistent field */
    private Boolean isdeleted;

    /** nullable persistent field */
    private Date recmodtime;

    /** persistent field */
    private com.owarchitects.forms.commons.db.Allinst allinst;

    /** persistent field */
    private Set rroleuserassign;

    /** persistent field */
    private Set analystgroupuserassign;

    /** persistent field */
    private Set sysuserpermassign;

    /** persistent field */
    private Set dtuserpermassign;

    /** full constructor */
    public Allusers(long luserid, String userdesc, Boolean isdeleted, Date recmodtime, com.owarchitects.forms.commons.db.Allinst allinst, Set rroleuserassign, Set analystgroupuserassign, Set sysuserpermassign, Set dtuserpermassign) {
        this.luserid = luserid;
        this.userdesc = userdesc;
        this.isdeleted = isdeleted;
        this.recmodtime = recmodtime;
        this.allinst = allinst;
        this.rroleuserassign = rroleuserassign;
        this.analystgroupuserassign = analystgroupuserassign;
        this.sysuserpermassign = sysuserpermassign;
        this.dtuserpermassign = dtuserpermassign;
    }

    /** default constructor */
    public Allusers() {
    }

    /** minimal constructor */
    public Allusers(long luserid, com.owarchitects.forms.commons.db.Allinst allinst, Set rroleuserassign, Set analystgroupuserassign, Set sysuserpermassign, Set dtuserpermassign) {
        this.luserid = luserid;
        this.allinst = allinst;
        this.rroleuserassign = rroleuserassign;
        this.analystgroupuserassign = analystgroupuserassign;
        this.sysuserpermassign = sysuserpermassign;
        this.dtuserpermassign = dtuserpermassign;
    }

    public long getAuserid() {
        return this.auserid;
    }

    public void setAuserid(long auserid) {
        this.auserid = auserid;
    }

    public long getLuserid() {
        return this.luserid;
    }

    public void setLuserid(long luserid) {
        this.luserid = luserid;
    }

    public String getUserdesc() {
        return this.userdesc;
    }

    public void setUserdesc(String userdesc) {
        this.userdesc = userdesc;
    }

    public Boolean getIsdeleted() {
        return this.isdeleted;
    }

    public void setIsdeleted(Boolean isdeleted) {
        this.isdeleted = isdeleted;
    }

    public Date getRecmodtime() {
        return this.recmodtime;
    }

    public void setRecmodtime(Date recmodtime) {
        this.recmodtime = recmodtime;
    }

    public com.owarchitects.forms.commons.db.Allinst getAllinst() {
        return this.allinst;
    }

    public void setAllinst(com.owarchitects.forms.commons.db.Allinst allinst) {
        this.allinst = allinst;
    }

    public Set getRroleuserassign() {
        return this.rroleuserassign;
    }

    public void setRroleuserassign(Set rroleuserassign) {
        this.rroleuserassign = rroleuserassign;
    }

    public Set getAnalystgroupuserassign() {
        return this.analystgroupuserassign;
    }

    public void setAnalystgroupuserassign(Set analystgroupuserassign) {
        this.analystgroupuserassign = analystgroupuserassign;
    }

    public Set getSysuserpermassign() {
        return this.sysuserpermassign;
    }

    public void setSysuserpermassign(Set sysuserpermassign) {
        this.sysuserpermassign = sysuserpermassign;
    }

    public Set getDtuserpermassign() {
        return this.dtuserpermassign;
    }

    public void setDtuserpermassign(Set dtuserpermassign) {
        this.dtuserpermassign = dtuserpermassign;
    }

    public String toString() {
        return new ToStringBuilder(this)
            .append("auserid", getAuserid())
            .toString();
    }

}
