package com.owarchitects.forms.commons.db;

import java.io.Serializable;
import java.util.Date;
import java.util.Set;
import org.apache.commons.lang.builder.ToStringBuilder;


/** @author Hibernate CodeGenerator */
public class Analystroles implements Serializable {

    /** identifier field */
    private long aroleid;

    /** persistent field */
    private String roleacr;

    /** persistent field */
    private String roledesc;

    /** nullable persistent field */
    private Date recmodtime;

    /** persistent field */
    private Set analystroleuserassign;

    /** persistent field */
    private Set analystrolepermassign;

    /** full constructor */
    public Analystroles(String roleacr, String roledesc, Date recmodtime, Set analystroleuserassign, Set analystrolepermassign) {
        this.roleacr = roleacr;
        this.roledesc = roledesc;
        this.recmodtime = recmodtime;
        this.analystroleuserassign = analystroleuserassign;
        this.analystrolepermassign = analystrolepermassign;
    }

    /** default constructor */
    public Analystroles() {
    }

    /** minimal constructor */
    public Analystroles(String roleacr, String roledesc, Set analystroleuserassign, Set analystrolepermassign) {
        this.roleacr = roleacr;
        this.roledesc = roledesc;
        this.analystroleuserassign = analystroleuserassign;
        this.analystrolepermassign = analystrolepermassign;
    }

    public long getAroleid() {
        return this.aroleid;
    }

    public void setAroleid(long aroleid) {
        this.aroleid = aroleid;
    }

    public String getRoleacr() {
        return this.roleacr;
    }

    public void setRoleacr(String roleacr) {
        this.roleacr = roleacr;
    }

    public String getRoledesc() {
        return this.roledesc;
    }

    public void setRoledesc(String roledesc) {
        this.roledesc = roledesc;
    }

    public Date getRecmodtime() {
        return this.recmodtime;
    }

    public void setRecmodtime(Date recmodtime) {
        this.recmodtime = recmodtime;
    }

    public Set getAnalystroleuserassign() {
        return this.analystroleuserassign;
    }

    public void setAnalystroleuserassign(Set analystroleuserassign) {
        this.analystroleuserassign = analystroleuserassign;
    }

    public Set getAnalystrolepermassign() {
        return this.analystrolepermassign;
    }

    public void setAnalystrolepermassign(Set analystrolepermassign) {
        this.analystrolepermassign = analystrolepermassign;
    }

    public String toString() {
        return new ToStringBuilder(this)
            .append("aroleid", getAroleid())
            .toString();
    }

}
