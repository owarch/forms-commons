package com.owarchitects.forms.commons.db;

import java.io.Serializable;
import java.util.Date;
import org.apache.commons.lang.builder.ToStringBuilder;


/** @author Hibernate CodeGenerator */
public class Mediapermprofassign implements Serializable {

    /** identifier field */
    private long mrpid;

    /** persistent field */
    private long mediainfoid;

    /** nullable persistent field */
    private Date recmodtime;

    /** persistent field */
    private com.owarchitects.forms.commons.db.Mediapermprofiles mediapermprofiles;

    /** full constructor */
    public Mediapermprofassign(long mediainfoid, Date recmodtime, com.owarchitects.forms.commons.db.Mediapermprofiles mediapermprofiles) {
        this.mediainfoid = mediainfoid;
        this.recmodtime = recmodtime;
        this.mediapermprofiles = mediapermprofiles;
    }

    /** default constructor */
    public Mediapermprofassign() {
    }

    /** minimal constructor */
    public Mediapermprofassign(long mediainfoid, com.owarchitects.forms.commons.db.Mediapermprofiles mediapermprofiles) {
        this.mediainfoid = mediainfoid;
        this.mediapermprofiles = mediapermprofiles;
    }

    public long getMrpid() {
        return this.mrpid;
    }

    public void setMrpid(long mrpid) {
        this.mrpid = mrpid;
    }

    public long getMediainfoid() {
        return this.mediainfoid;
    }

    public void setMediainfoid(long mediainfoid) {
        this.mediainfoid = mediainfoid;
    }

    public Date getRecmodtime() {
        return this.recmodtime;
    }

    public void setRecmodtime(Date recmodtime) {
        this.recmodtime = recmodtime;
    }

    public com.owarchitects.forms.commons.db.Mediapermprofiles getMediapermprofiles() {
        return this.mediapermprofiles;
    }

    public void setMediapermprofiles(com.owarchitects.forms.commons.db.Mediapermprofiles mediapermprofiles) {
        this.mediapermprofiles = mediapermprofiles;
    }

    public String toString() {
        return new ToStringBuilder(this)
            .append("mrpid", getMrpid())
            .toString();
    }

}
