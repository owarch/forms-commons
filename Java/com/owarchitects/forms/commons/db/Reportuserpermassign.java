package com.owarchitects.forms.commons.db;

import java.io.Serializable;
import java.util.Date;
import org.apache.commons.lang.builder.ToStringBuilder;


/** @author Hibernate CodeGenerator */
public class Reportuserpermassign implements Serializable {

    /** identifier field */
    private long rupid;

    /** persistent field */
    private long reportinfoid;

    /** persistent field */
    private Serializable permlevel;

    /** nullable persistent field */
    private Date recmodtime;

    /** persistent field */
    private com.owarchitects.forms.commons.db.Allusers allusers;

    /** full constructor */
    public Reportuserpermassign(long reportinfoid, Serializable permlevel, Date recmodtime, com.owarchitects.forms.commons.db.Allusers allusers) {
        this.reportinfoid = reportinfoid;
        this.permlevel = permlevel;
        this.recmodtime = recmodtime;
        this.allusers = allusers;
    }

    /** default constructor */
    public Reportuserpermassign() {
    }

    /** minimal constructor */
    public Reportuserpermassign(long reportinfoid, Serializable permlevel, com.owarchitects.forms.commons.db.Allusers allusers) {
        this.reportinfoid = reportinfoid;
        this.permlevel = permlevel;
        this.allusers = allusers;
    }

    public long getRupid() {
        return this.rupid;
    }

    public void setRupid(long rupid) {
        this.rupid = rupid;
    }

    public long getReportinfoid() {
        return this.reportinfoid;
    }

    public void setReportinfoid(long reportinfoid) {
        this.reportinfoid = reportinfoid;
    }

    public Serializable getPermlevel() {
        return this.permlevel;
    }

    public void setPermlevel(Serializable permlevel) {
        this.permlevel = permlevel;
    }

    public Date getRecmodtime() {
        return this.recmodtime;
    }

    public void setRecmodtime(Date recmodtime) {
        this.recmodtime = recmodtime;
    }

    public com.owarchitects.forms.commons.db.Allusers getAllusers() {
        return this.allusers;
    }

    public void setAllusers(com.owarchitects.forms.commons.db.Allusers allusers) {
        this.allusers = allusers;
    }

    public String toString() {
        return new ToStringBuilder(this)
            .append("rupid", getRupid())
            .toString();
    }

}
