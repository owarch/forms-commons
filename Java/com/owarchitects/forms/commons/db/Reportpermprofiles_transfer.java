package com.owarchitects.forms.commons.db;

import java.io.Serializable;
import java.util.Date;
import org.apache.commons.lang.builder.ToStringBuilder;


/** @author Hibernate CodeGenerator */
public class Reportpermprofiles_transfer implements Serializable {

    /** identifier field */
    private long rprofileid;

    /** persistent field */
    private int profilelevel;

    /** nullable persistent field */
    private Long siteid;

    /** nullable persistent field */
    private Long studyid;

    /** nullable persistent field */
    private String profileacr;

    /** nullable persistent field */
    private String profiledesc;

    /** nullable persistent field */
    private Date recmodtime;

    /** full constructor */
    public Reportpermprofiles_transfer(long rprofileid, int profilelevel, Long siteid, Long studyid, String profileacr, String profiledesc, Date recmodtime) {
        this.rprofileid = rprofileid;
        this.profilelevel = profilelevel;
        this.siteid = siteid;
        this.studyid = studyid;
        this.profileacr = profileacr;
        this.profiledesc = profiledesc;
        this.recmodtime = recmodtime;
    }

    /** default constructor */
    public Reportpermprofiles_transfer() {
    }

    /** minimal constructor */
    public Reportpermprofiles_transfer(long rprofileid, int profilelevel) {
        this.rprofileid = rprofileid;
        this.profilelevel = profilelevel;
    }

    public long getRprofileid() {
        return this.rprofileid;
    }

    public void setRprofileid(long rprofileid) {
        this.rprofileid = rprofileid;
    }

    public int getProfilelevel() {
        return this.profilelevel;
    }

    public void setProfilelevel(int profilelevel) {
        this.profilelevel = profilelevel;
    }

    public Long getSiteid() {
        return this.siteid;
    }

    public void setSiteid(Long siteid) {
        this.siteid = siteid;
    }

    public Long getStudyid() {
        return this.studyid;
    }

    public void setStudyid(Long studyid) {
        this.studyid = studyid;
    }

    public String getProfileacr() {
        return this.profileacr;
    }

    public void setProfileacr(String profileacr) {
        this.profileacr = profileacr;
    }

    public String getProfiledesc() {
        return this.profiledesc;
    }

    public void setProfiledesc(String profiledesc) {
        this.profiledesc = profiledesc;
    }

    public Date getRecmodtime() {
        return this.recmodtime;
    }

    public void setRecmodtime(Date recmodtime) {
        this.recmodtime = recmodtime;
    }

    public String toString() {
        return new ToStringBuilder(this)
            .append("rprofileid", getRprofileid())
            .toString();
    }

}
