 
/**
 *
 * This program is part of:
 *
 *    FORMS [Forms-Oriented Research Management System]
 *
 * Copyright (C) 2009 OpenWare Architects.  All Rights Reserved.
 *
 * FORMS is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * FORMS is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with FORMS.  If not, see <http://www.gnu.org/licenses/>.
 *
 */
 
package com.owarchitects.forms.commons.db;

/**
 *  LocalusersDAO.java  -  Localusers Data Access Object with convenience methods
 */

import java.util.List;
import org.springframework.stereotype.Repository;
//import org.springframework.transaction.annotation.Transactional;

@Repository
//@Transactional
public class LocalusersDAO extends MainDAO {

   public List getCurrentUsers() {

      return execQuery(
               "select u from Localusers u where isdeleted=false"
             );
             
   }

   public List getDeletedUsers() {

      return execQuery(
               "select u from Localusers u where isdeleted=true"
             );
             
   }

   public Localusers getRecord(long luserid) {

      return (Localusers)execUniqueQuery(
               "select u from Localusers u where u.luserid=:luserid","luserid",luserid
             );
             
   }

   public Localusers getCurrentRecordByUsername(String username) {

      return (Localusers)execUniqueQuery(
               "select u from Localusers u where upper(u.username)=upper(:username) and isdeleted=false","username",username
             );
             
   }

   public Localusers getCurrentRecordByUsernumber(String usernumber) {

      return (Localusers)execUniqueQuery(
               "select u from Localusers u where u.usernumber=:usernumber and isdeleted=false","usernumber",usernumber
             );
             
   }

   public List getDeletedRecordsByUsername(String username) {

      return execQuery(
               "select u from Localusers u where upper(u.username)=upper(:username) and isdeleted=true","username",username
             );
             
   }

   public List getDeletedRecordsByUsernumber(String usernumber) {

      return execQuery(
               "select u from Localusers u where u.usernumber=:usernumber and isdeleted=true","usernumber",usernumber
             );
             
   }

   public List getAllRecordsByUsername(String username) {

      return execQuery(
               "select u from Localusers u where upper(u.username)=upper(:username) ","username",username
             );
             
   }

   public List getAllRecordsByUsernumber(String usernumber) {

      return execQuery(
               "select u from Localusers u where u.usernumber=:usernumber","usernumber",usernumber
             );
             
   }

}

