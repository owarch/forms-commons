package com.owarchitects.forms.commons.db;

import java.io.Serializable;
import java.util.Date;
import java.util.Set;
import org.apache.commons.lang.builder.ToStringBuilder;


/** @author Hibernate CodeGenerator */
public class Linkedstudies implements Serializable {

    /** identifier field */
    private long lstdid;

    /** persistent field */
    private long remotestudyid;

    /** nullable persistent field */
    private Date recmodtime;

    /** persistent field */
    private com.owarchitects.forms.commons.db.Linkedinst linkedinst;

    /** persistent field */
    private com.owarchitects.forms.commons.db.Studies studies;

    /** persistent field */
    private Set dtmodtime;

    /** full constructor */
    public Linkedstudies(long remotestudyid, Date recmodtime, com.owarchitects.forms.commons.db.Linkedinst linkedinst, com.owarchitects.forms.commons.db.Studies studies, Set dtmodtime) {
        this.remotestudyid = remotestudyid;
        this.recmodtime = recmodtime;
        this.linkedinst = linkedinst;
        this.studies = studies;
        this.dtmodtime = dtmodtime;
    }

    /** default constructor */
    public Linkedstudies() {
    }

    /** minimal constructor */
    public Linkedstudies(long remotestudyid, com.owarchitects.forms.commons.db.Linkedinst linkedinst, com.owarchitects.forms.commons.db.Studies studies, Set dtmodtime) {
        this.remotestudyid = remotestudyid;
        this.linkedinst = linkedinst;
        this.studies = studies;
        this.dtmodtime = dtmodtime;
    }

    public long getLstdid() {
        return this.lstdid;
    }

    public void setLstdid(long lstdid) {
        this.lstdid = lstdid;
    }

    public long getRemotestudyid() {
        return this.remotestudyid;
    }

    public void setRemotestudyid(long remotestudyid) {
        this.remotestudyid = remotestudyid;
    }

    public Date getRecmodtime() {
        return this.recmodtime;
    }

    public void setRecmodtime(Date recmodtime) {
        this.recmodtime = recmodtime;
    }

    public com.owarchitects.forms.commons.db.Linkedinst getLinkedinst() {
        return this.linkedinst;
    }

    public void setLinkedinst(com.owarchitects.forms.commons.db.Linkedinst linkedinst) {
        this.linkedinst = linkedinst;
    }

    public com.owarchitects.forms.commons.db.Studies getStudies() {
        return this.studies;
    }

    public void setStudies(com.owarchitects.forms.commons.db.Studies studies) {
        this.studies = studies;
    }

    public Set getDtmodtime() {
        return this.dtmodtime;
    }

    public void setDtmodtime(Set dtmodtime) {
        this.dtmodtime = dtmodtime;
    }

    public String toString() {
        return new ToStringBuilder(this)
            .append("lstdid", getLstdid())
            .toString();
    }

}
