package com.owarchitects.forms.commons.db;

import java.io.Serializable;
import org.apache.commons.lang.builder.ToStringBuilder;


/** @author Hibernate CodeGenerator */
public class Ssainfo implements Serializable {

    /** identifier field */
    private long ssainfoid;

    /** persistent field */
    private long csaid;

    /** persistent field */
    private String ssaurl;

    /** full constructor */
    public Ssainfo(long csaid, String ssaurl) {
        this.csaid = csaid;
        this.ssaurl = ssaurl;
    }

    /** default constructor */
    public Ssainfo() {
    }

    public long getSsainfoid() {
        return this.ssainfoid;
    }

    protected void setSsainfoid(long ssainfoid) {
        this.ssainfoid = ssainfoid;
    }

    public long getCsaid() {
        return this.csaid;
    }

    public void setCsaid(long csaid) {
        this.csaid = csaid;
    }

    public String getSsaurl() {
        return this.ssaurl;
    }

    public void setSsaurl(String ssaurl) {
        this.ssaurl = ssaurl;
    }

    public String toString() {
        return new ToStringBuilder(this)
            .append("ssainfoid", getSsainfoid())
            .toString();
    }

}
