package com.owarchitects.forms.commons.db;

import java.io.Serializable;
import java.util.Date;
import org.apache.commons.lang.builder.ToStringBuilder;


/** @author Hibernate CodeGenerator */
public class Formstore_transfer implements Serializable {

    /** identifier field */
    private long formstoreid;

    /** persistent field */
    private long formtypeid;

    /** persistent field */
    private String formcontent;

    /** nullable persistent field */
    private Date recmodtime;

    /** full constructor */
    public Formstore_transfer(long formstoreid, long formtypeid, String formcontent, Date recmodtime) {
        this.formstoreid = formstoreid;
        this.formtypeid = formtypeid;
        this.formcontent = formcontent;
        this.recmodtime = recmodtime;
    }

    /** default constructor */
    public Formstore_transfer() {
    }

    /** minimal constructor */
    public Formstore_transfer(long formstoreid, long formtypeid, String formcontent) {
        this.formstoreid = formstoreid;
        this.formtypeid = formtypeid;
        this.formcontent = formcontent;
    }

    public long getFormstoreid() {
        return this.formstoreid;
    }

    public void setFormstoreid(long formstoreid) {
        this.formstoreid = formstoreid;
    }

    public long getFormtypeid() {
        return this.formtypeid;
    }

    public void setFormtypeid(long formtypeid) {
        this.formtypeid = formtypeid;
    }

    public String getFormcontent() {
        return this.formcontent;
    }

    public void setFormcontent(String formcontent) {
        this.formcontent = formcontent;
    }

    public Date getRecmodtime() {
        return this.recmodtime;
    }

    public void setRecmodtime(Date recmodtime) {
        this.recmodtime = recmodtime;
    }

    public String toString() {
        return new ToStringBuilder(this)
            .append("formstoreid", getFormstoreid())
            .toString();
    }

}
