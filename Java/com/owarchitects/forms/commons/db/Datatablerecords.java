package com.owarchitects.forms.commons.db;

import java.io.Serializable;
import java.util.Date;
import java.util.Set;
import org.apache.commons.lang.builder.ToStringBuilder;


/** @author Hibernate CodeGenerator */
public class Datatablerecords implements Serializable {

    /** identifier field */
    private long datarecordid;

    /** nullable persistent field */
    private Boolean ishold;

    /** nullable persistent field */
    private Long saveuser;

    /** nullable persistent field */
    private Date savetime;

    /** nullable persistent field */
    private Boolean isaudit;

    /** nullable persistent field */
    private Character audittype;

    /** nullable persistent field */
    private Long audituser;

    /** nullable persistent field */
    private Date audittime;

    /** nullable persistent field */
    private Boolean islocked;

    /** nullable persistent field */
    private Long lockuser;

    /** nullable persistent field */
    private Date locktime;

    /** nullable persistent field */
    private String lockinfo;

    /** persistent field */
    private com.owarchitects.forms.commons.db.Datatabledef datatabledef;

    /** persistent field */
    private Set datatablevalues;

    /** full constructor */
    public Datatablerecords(Boolean ishold, Long saveuser, Date savetime, Boolean isaudit, Character audittype, Long audituser, Date audittime, Boolean islocked, Long lockuser, Date locktime, String lockinfo, com.owarchitects.forms.commons.db.Datatabledef datatabledef, Set datatablevalues) {
        this.ishold = ishold;
        this.saveuser = saveuser;
        this.savetime = savetime;
        this.isaudit = isaudit;
        this.audittype = audittype;
        this.audituser = audituser;
        this.audittime = audittime;
        this.islocked = islocked;
        this.lockuser = lockuser;
        this.locktime = locktime;
        this.lockinfo = lockinfo;
        this.datatabledef = datatabledef;
        this.datatablevalues = datatablevalues;
    }

    /** default constructor */
    public Datatablerecords() {
    }

    /** minimal constructor */
    public Datatablerecords(com.owarchitects.forms.commons.db.Datatabledef datatabledef, Set datatablevalues) {
        this.datatabledef = datatabledef;
        this.datatablevalues = datatablevalues;
    }

    public long getDatarecordid() {
        return this.datarecordid;
    }

    public void setDatarecordid(long datarecordid) {
        this.datarecordid = datarecordid;
    }

    public Boolean getIshold() {
        return this.ishold;
    }

    public void setIshold(Boolean ishold) {
        this.ishold = ishold;
    }

    public Long getSaveuser() {
        return this.saveuser;
    }

    public void setSaveuser(Long saveuser) {
        this.saveuser = saveuser;
    }

    public Date getSavetime() {
        return this.savetime;
    }

    public void setSavetime(Date savetime) {
        this.savetime = savetime;
    }

    public Boolean getIsaudit() {
        return this.isaudit;
    }

    public void setIsaudit(Boolean isaudit) {
        this.isaudit = isaudit;
    }

    public Character getAudittype() {
        return this.audittype;
    }

    public void setAudittype(Character audittype) {
        this.audittype = audittype;
    }

    public Long getAudituser() {
        return this.audituser;
    }

    public void setAudituser(Long audituser) {
        this.audituser = audituser;
    }

    public Date getAudittime() {
        return this.audittime;
    }

    public void setAudittime(Date audittime) {
        this.audittime = audittime;
    }

    public Boolean getIslocked() {
        return this.islocked;
    }

    public void setIslocked(Boolean islocked) {
        this.islocked = islocked;
    }

    public Long getLockuser() {
        return this.lockuser;
    }

    public void setLockuser(Long lockuser) {
        this.lockuser = lockuser;
    }

    public Date getLocktime() {
        return this.locktime;
    }

    public void setLocktime(Date locktime) {
        this.locktime = locktime;
    }

    public String getLockinfo() {
        return this.lockinfo;
    }

    public void setLockinfo(String lockinfo) {
        this.lockinfo = lockinfo;
    }

    public com.owarchitects.forms.commons.db.Datatabledef getDatatabledef() {
        return this.datatabledef;
    }

    public void setDatatabledef(com.owarchitects.forms.commons.db.Datatabledef datatabledef) {
        this.datatabledef = datatabledef;
    }

    public Set getDatatablevalues() {
        return this.datatablevalues;
    }

    public void setDatatablevalues(Set datatablevalues) {
        this.datatablevalues = datatablevalues;
    }

    public String toString() {
        return new ToStringBuilder(this)
            .append("datarecordid", getDatarecordid())
            .toString();
    }

}
