package com.owarchitects.forms.commons.db;

import java.io.Serializable;
import org.apache.commons.lang.builder.ToStringBuilder;


/** @author Hibernate CodeGenerator */
public class Analystha implements Serializable {

    /** identifier field */
    private long analysthaid;

    /** nullable persistent field */
    private Long panalysthaid;

    /** nullable persistent field */
    private String hadesc;

    /** nullable persistent field */
    private Integer haordr;

    /** persistent field */
    private long createuser;

    /** full constructor */
    public Analystha(Long panalysthaid, String hadesc, Integer haordr, long createuser) {
        this.panalysthaid = panalysthaid;
        this.hadesc = hadesc;
        this.haordr = haordr;
        this.createuser = createuser;
    }

    /** default constructor */
    public Analystha() {
    }

    /** minimal constructor */
    public Analystha(long createuser) {
        this.createuser = createuser;
    }

    public long getAnalysthaid() {
        return this.analysthaid;
    }

    public void setAnalysthaid(long analysthaid) {
        this.analysthaid = analysthaid;
    }

    public Long getPanalysthaid() {
        return this.panalysthaid;
    }

    public void setPanalysthaid(Long panalysthaid) {
        this.panalysthaid = panalysthaid;
    }

    public String getHadesc() {
        return this.hadesc;
    }

    public void setHadesc(String hadesc) {
        this.hadesc = hadesc;
    }

    public Integer getHaordr() {
        return this.haordr;
    }

    public void setHaordr(Integer haordr) {
        this.haordr = haordr;
    }

    public long getCreateuser() {
        return this.createuser;
    }

    public void setCreateuser(long createuser) {
        this.createuser = createuser;
    }

    public String toString() {
        return new ToStringBuilder(this)
            .append("analysthaid", getAnalysthaid())
            .toString();
    }

}
