package com.owarchitects.forms.commons.db;

import java.io.Serializable;
import java.util.Date;
import java.util.Set;
import org.apache.commons.lang.builder.ToStringBuilder;


/** @author Hibernate CodeGenerator */
public class Sites implements Serializable {

    /** identifier field */
    private long siteid;

    /** persistent field */
    private String siteno;

    /** nullable persistent field */
    private String sitedesc;

    /** nullable persistent field */
    private Date recmodtime;

    /** persistent field */
    private Set studies;

    /** full constructor */
    public Sites(String siteno, String sitedesc, Date recmodtime, Set studies) {
        this.siteno = siteno;
        this.sitedesc = sitedesc;
        this.recmodtime = recmodtime;
        this.studies = studies;
    }

    /** default constructor */
    public Sites() {
    }

    /** minimal constructor */
    public Sites(String siteno, Set studies) {
        this.siteno = siteno;
        this.studies = studies;
    }

    public long getSiteid() {
        return this.siteid;
    }

    public void setSiteid(long siteid) {
        this.siteid = siteid;
    }

    public String getSiteno() {
        return this.siteno;
    }

    public void setSiteno(String siteno) {
        this.siteno = siteno;
    }

    public String getSitedesc() {
        return this.sitedesc;
    }

    public void setSitedesc(String sitedesc) {
        this.sitedesc = sitedesc;
    }

    public Date getRecmodtime() {
        return this.recmodtime;
    }

    public void setRecmodtime(Date recmodtime) {
        this.recmodtime = recmodtime;
    }

    public Set getStudies() {
        return this.studies;
    }

    public void setStudies(Set studies) {
        this.studies = studies;
    }

    public String toString() {
        return new ToStringBuilder(this)
            .append("siteid", getSiteid())
            .toString();
    }

}
