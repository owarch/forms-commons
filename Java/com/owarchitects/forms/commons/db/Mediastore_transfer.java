package com.owarchitects.forms.commons.db;

import java.io.Serializable;
import org.apache.commons.lang.builder.ToStringBuilder;


/** @author Hibernate CodeGenerator */
public class Mediastore_transfer implements Serializable {

    /** identifier field */
    private long mediastoreid;

    /** persistent field */
    private long mediainfoid;

    /** persistent field */
    private String mediacontent;

    /** full constructor */
    public Mediastore_transfer(long mediastoreid, long mediainfoid, String mediacontent) {
        this.mediastoreid = mediastoreid;
        this.mediainfoid = mediainfoid;
        this.mediacontent = mediacontent;
    }

    /** default constructor */
    public Mediastore_transfer() {
    }

    public long getMediastoreid() {
        return this.mediastoreid;
    }

    public void setMediastoreid(long mediastoreid) {
        this.mediastoreid = mediastoreid;
    }

    public long getMediainfoid() {
        return this.mediainfoid;
    }

    public void setMediainfoid(long mediainfoid) {
        this.mediainfoid = mediainfoid;
    }

    public String getMediacontent() {
        return this.mediacontent;
    }

    public void setMediacontent(String mediacontent) {
        this.mediacontent = mediacontent;
    }

    public String toString() {
        return new ToStringBuilder(this)
            .append("mediastoreid", getMediastoreid())
            .toString();
    }

}
