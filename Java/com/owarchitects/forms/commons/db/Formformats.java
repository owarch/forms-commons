package com.owarchitects.forms.commons.db;

import java.io.Serializable;
import java.util.Date;
import java.util.Set;
import org.apache.commons.lang.builder.ToStringBuilder;


/** @author Hibernate CodeGenerator */
public class Formformats implements Serializable {

    /** identifier field */
    private long formformatid;

    /** nullable persistent field */
    private Date recmodtime;

    /** persistent field */
    private com.owarchitects.forms.commons.db.Supportedfileformats supportedfileformats;

    /** persistent field */
    private Set formtypes;

    /** full constructor */
    public Formformats(Date recmodtime, com.owarchitects.forms.commons.db.Supportedfileformats supportedfileformats, Set formtypes) {
        this.recmodtime = recmodtime;
        this.supportedfileformats = supportedfileformats;
        this.formtypes = formtypes;
    }

    /** default constructor */
    public Formformats() {
    }

    /** minimal constructor */
    public Formformats(com.owarchitects.forms.commons.db.Supportedfileformats supportedfileformats, Set formtypes) {
        this.supportedfileformats = supportedfileformats;
        this.formtypes = formtypes;
    }

    public long getFormformatid() {
        return this.formformatid;
    }

    public void setFormformatid(long formformatid) {
        this.formformatid = formformatid;
    }

    public Date getRecmodtime() {
        return this.recmodtime;
    }

    public void setRecmodtime(Date recmodtime) {
        this.recmodtime = recmodtime;
    }

    public com.owarchitects.forms.commons.db.Supportedfileformats getSupportedfileformats() {
        return this.supportedfileformats;
    }

    public void setSupportedfileformats(com.owarchitects.forms.commons.db.Supportedfileformats supportedfileformats) {
        this.supportedfileformats = supportedfileformats;
    }

    public Set getFormtypes() {
        return this.formtypes;
    }

    public void setFormtypes(Set formtypes) {
        this.formtypes = formtypes;
    }

    public String toString() {
        return new ToStringBuilder(this)
            .append("formformatid", getFormformatid())
            .toString();
    }

}
