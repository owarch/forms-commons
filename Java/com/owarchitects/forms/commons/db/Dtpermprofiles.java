package com.owarchitects.forms.commons.db;

import java.io.Serializable;
import java.util.Date;
import java.util.Set;
import org.apache.commons.lang.builder.ToStringBuilder;


/** @author Hibernate CodeGenerator */
public class Dtpermprofiles implements Serializable {

    /** identifier field */
    private long dtprofileid;

    /** persistent field */
    private int profilelevel;

    /** nullable persistent field */
    private Long siteid;

    /** nullable persistent field */
    private Long studyid;

    /** nullable persistent field */
    private String profileacr;

    /** nullable persistent field */
    private String profiledesc;

    /** nullable persistent field */
    private Date recmodtime;

    /** persistent field */
    private Set dtpermprofassign;

    /** persistent field */
    private Set dtrolepermprofassign;

    /** full constructor */
    public Dtpermprofiles(int profilelevel, Long siteid, Long studyid, String profileacr, String profiledesc, Date recmodtime, Set dtpermprofassign, Set dtrolepermprofassign) {
        this.profilelevel = profilelevel;
        this.siteid = siteid;
        this.studyid = studyid;
        this.profileacr = profileacr;
        this.profiledesc = profiledesc;
        this.recmodtime = recmodtime;
        this.dtpermprofassign = dtpermprofassign;
        this.dtrolepermprofassign = dtrolepermprofassign;
    }

    /** default constructor */
    public Dtpermprofiles() {
    }

    /** minimal constructor */
    public Dtpermprofiles(int profilelevel, Set dtpermprofassign, Set dtrolepermprofassign) {
        this.profilelevel = profilelevel;
        this.dtpermprofassign = dtpermprofassign;
        this.dtrolepermprofassign = dtrolepermprofassign;
    }

    public long getDtprofileid() {
        return this.dtprofileid;
    }

    public void setDtprofileid(long dtprofileid) {
        this.dtprofileid = dtprofileid;
    }

    public int getProfilelevel() {
        return this.profilelevel;
    }

    public void setProfilelevel(int profilelevel) {
        this.profilelevel = profilelevel;
    }

    public Long getSiteid() {
        return this.siteid;
    }

    public void setSiteid(Long siteid) {
        this.siteid = siteid;
    }

    public Long getStudyid() {
        return this.studyid;
    }

    public void setStudyid(Long studyid) {
        this.studyid = studyid;
    }

    public String getProfileacr() {
        return this.profileacr;
    }

    public void setProfileacr(String profileacr) {
        this.profileacr = profileacr;
    }

    public String getProfiledesc() {
        return this.profiledesc;
    }

    public void setProfiledesc(String profiledesc) {
        this.profiledesc = profiledesc;
    }

    public Date getRecmodtime() {
        return this.recmodtime;
    }

    public void setRecmodtime(Date recmodtime) {
        this.recmodtime = recmodtime;
    }

    public Set getDtpermprofassign() {
        return this.dtpermprofassign;
    }

    public void setDtpermprofassign(Set dtpermprofassign) {
        this.dtpermprofassign = dtpermprofassign;
    }

    public Set getDtrolepermprofassign() {
        return this.dtrolepermprofassign;
    }

    public void setDtrolepermprofassign(Set dtrolepermprofassign) {
        this.dtrolepermprofassign = dtrolepermprofassign;
    }

    public String toString() {
        return new ToStringBuilder(this)
            .append("dtprofileid", getDtprofileid())
            .toString();
    }

}
