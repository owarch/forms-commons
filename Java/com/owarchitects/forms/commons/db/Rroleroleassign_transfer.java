package com.owarchitects.forms.commons.db;

import java.io.Serializable;
import java.util.Date;
import org.apache.commons.lang.builder.ToStringBuilder;


/** @author Hibernate CodeGenerator */
public class Rroleroleassign_transfer implements Serializable {

    /** identifier field */
    private long rrraid;

    /** persistent field */
    private long prroleid;

    /** persistent field */
    private long crroleid;

    /** nullable persistent field */
    private Date recmodtime;

    /** full constructor */
    public Rroleroleassign_transfer(long rrraid, long prroleid, long crroleid, Date recmodtime) {
        this.rrraid = rrraid;
        this.prroleid = prroleid;
        this.crroleid = crroleid;
        this.recmodtime = recmodtime;
    }

    /** default constructor */
    public Rroleroleassign_transfer() {
    }

    /** minimal constructor */
    public Rroleroleassign_transfer(long rrraid, long prroleid, long crroleid) {
        this.rrraid = rrraid;
        this.prroleid = prroleid;
        this.crroleid = crroleid;
    }

    public long getRrraid() {
        return this.rrraid;
    }

    public void setRrraid(long rrraid) {
        this.rrraid = rrraid;
    }

    public long getPrroleid() {
        return this.prroleid;
    }

    public void setPrroleid(long prroleid) {
        this.prroleid = prroleid;
    }

    public long getCrroleid() {
        return this.crroleid;
    }

    public void setCrroleid(long crroleid) {
        this.crroleid = crroleid;
    }

    public Date getRecmodtime() {
        return this.recmodtime;
    }

    public void setRecmodtime(Date recmodtime) {
        this.recmodtime = recmodtime;
    }

    public String toString() {
        return new ToStringBuilder(this)
            .append("rrraid", getRrraid())
            .toString();
    }

}
