package com.owarchitects.forms.commons.db;

import java.io.Serializable;
import java.util.Date;
import org.apache.commons.lang.builder.ToStringBuilder;


/** @author Hibernate CodeGenerator */
public class Datatableha implements Serializable {

    /** identifier field */
    private long dthaid;

    /** persistent field */
    private int visibility;

    /** nullable persistent field */
    private Long pdthaid;

    /** nullable persistent field */
    private String hadesc;

    /** nullable persistent field */
    private Integer haordr;

    /** nullable persistent field */
    private Date recmodtime;

    /** persistent field */
    private com.owarchitects.forms.commons.db.Studies studies;

    /** full constructor */
    public Datatableha(int visibility, Long pdthaid, String hadesc, Integer haordr, Date recmodtime, com.owarchitects.forms.commons.db.Studies studies) {
        this.visibility = visibility;
        this.pdthaid = pdthaid;
        this.hadesc = hadesc;
        this.haordr = haordr;
        this.recmodtime = recmodtime;
        this.studies = studies;
    }

    /** default constructor */
    public Datatableha() {
    }

    /** minimal constructor */
    public Datatableha(int visibility, com.owarchitects.forms.commons.db.Studies studies) {
        this.visibility = visibility;
        this.studies = studies;
    }

    public long getDthaid() {
        return this.dthaid;
    }

    public void setDthaid(long dthaid) {
        this.dthaid = dthaid;
    }

    public int getVisibility() {
        return this.visibility;
    }

    public void setVisibility(int visibility) {
        this.visibility = visibility;
    }

    public Long getPdthaid() {
        return this.pdthaid;
    }

    public void setPdthaid(Long pdthaid) {
        this.pdthaid = pdthaid;
    }

    public String getHadesc() {
        return this.hadesc;
    }

    public void setHadesc(String hadesc) {
        this.hadesc = hadesc;
    }

    public Integer getHaordr() {
        return this.haordr;
    }

    public void setHaordr(Integer haordr) {
        this.haordr = haordr;
    }

    public Date getRecmodtime() {
        return this.recmodtime;
    }

    public void setRecmodtime(Date recmodtime) {
        this.recmodtime = recmodtime;
    }

    public com.owarchitects.forms.commons.db.Studies getStudies() {
        return this.studies;
    }

    public void setStudies(com.owarchitects.forms.commons.db.Studies studies) {
        this.studies = studies;
    }

    public String toString() {
        return new ToStringBuilder(this)
            .append("dthaid", getDthaid())
            .toString();
    }

}
