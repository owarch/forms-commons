package com.owarchitects.forms.commons.db;

import java.io.Serializable;
import java.util.Date;
import org.apache.commons.lang.builder.ToStringBuilder;


/** @author Hibernate CodeGenerator */
public class Dtrolepermassign_transfer implements Serializable {

    /** identifier field */
    private long dtrpid;

    /** persistent field */
    private long dtdefid;

    /** nullable persistent field */
    private Date recmodtime;

    /** persistent field */
    private long rroleid;

    /** persistent field */
    private Serializable permlevel;

    /** full constructor */
    public Dtrolepermassign_transfer(long dtrpid, long dtdefid, Date recmodtime, long rroleid, Serializable permlevel) {
        this.dtrpid = dtrpid;
        this.dtdefid = dtdefid;
        this.recmodtime = recmodtime;
        this.rroleid = rroleid;
        this.permlevel = permlevel;
    }

    /** default constructor */
    public Dtrolepermassign_transfer() {
    }

    /** minimal constructor */
    public Dtrolepermassign_transfer(long dtrpid, long dtdefid, long rroleid, Serializable permlevel) {
        this.dtrpid = dtrpid;
        this.dtdefid = dtdefid;
        this.rroleid = rroleid;
        this.permlevel = permlevel;
    }

    public long getDtrpid() {
        return this.dtrpid;
    }

    public void setDtrpid(long dtrpid) {
        this.dtrpid = dtrpid;
    }

    public long getDtdefid() {
        return this.dtdefid;
    }

    public void setDtdefid(long dtdefid) {
        this.dtdefid = dtdefid;
    }

    public Date getRecmodtime() {
        return this.recmodtime;
    }

    public void setRecmodtime(Date recmodtime) {
        this.recmodtime = recmodtime;
    }

    public long getRroleid() {
        return this.rroleid;
    }

    public void setRroleid(long rroleid) {
        this.rroleid = rroleid;
    }

    public Serializable getPermlevel() {
        return this.permlevel;
    }

    public void setPermlevel(Serializable permlevel) {
        this.permlevel = permlevel;
    }

    public String toString() {
        return new ToStringBuilder(this)
            .append("dtrpid", getDtrpid())
            .toString();
    }

}
