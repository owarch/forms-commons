package com.owarchitects.forms.commons.db;

import java.io.Serializable;
import java.util.Date;
import org.apache.commons.lang.builder.ToStringBuilder;


/** @author Hibernate CodeGenerator */
public class Analysthauserpermassign implements Serializable {

    /** identifier field */
    private long ahaupid;

    /** persistent field */
    private long analysthaid;

    /** persistent field */
    private Serializable permlevel;

    /** nullable persistent field */
    private Date recmodtime;

    /** persistent field */
    private com.owarchitects.forms.commons.db.Allusers allusers;

    /** full constructor */
    public Analysthauserpermassign(long analysthaid, Serializable permlevel, Date recmodtime, com.owarchitects.forms.commons.db.Allusers allusers) {
        this.analysthaid = analysthaid;
        this.permlevel = permlevel;
        this.recmodtime = recmodtime;
        this.allusers = allusers;
    }

    /** default constructor */
    public Analysthauserpermassign() {
    }

    /** minimal constructor */
    public Analysthauserpermassign(long analysthaid, Serializable permlevel, com.owarchitects.forms.commons.db.Allusers allusers) {
        this.analysthaid = analysthaid;
        this.permlevel = permlevel;
        this.allusers = allusers;
    }

    public long getAhaupid() {
        return this.ahaupid;
    }

    public void setAhaupid(long ahaupid) {
        this.ahaupid = ahaupid;
    }

    public long getAnalysthaid() {
        return this.analysthaid;
    }

    public void setAnalysthaid(long analysthaid) {
        this.analysthaid = analysthaid;
    }

    public Serializable getPermlevel() {
        return this.permlevel;
    }

    public void setPermlevel(Serializable permlevel) {
        this.permlevel = permlevel;
    }

    public Date getRecmodtime() {
        return this.recmodtime;
    }

    public void setRecmodtime(Date recmodtime) {
        this.recmodtime = recmodtime;
    }

    public com.owarchitects.forms.commons.db.Allusers getAllusers() {
        return this.allusers;
    }

    public void setAllusers(com.owarchitects.forms.commons.db.Allusers allusers) {
        this.allusers = allusers;
    }

    public String toString() {
        return new ToStringBuilder(this)
            .append("ahaupid", getAhaupid())
            .toString();
    }

}
