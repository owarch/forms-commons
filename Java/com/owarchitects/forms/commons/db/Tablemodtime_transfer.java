package com.owarchitects.forms.commons.db;

import java.io.Serializable;
import java.util.Date;
import org.apache.commons.lang.builder.ToStringBuilder;


/** @author Hibernate CodeGenerator */
public class Tablemodtime_transfer implements Serializable {

    /** identifier field */
    private long tmtid;

    /** nullable persistent field */
    private String classname;

    /** nullable persistent field */
    private Date tablemodtime;

    /** full constructor */
    public Tablemodtime_transfer(long tmtid, String classname, Date tablemodtime) {
        this.tmtid = tmtid;
        this.classname = classname;
        this.tablemodtime = tablemodtime;
    }

    /** default constructor */
    public Tablemodtime_transfer() {
    }

    /** minimal constructor */
    public Tablemodtime_transfer(long tmtid) {
        this.tmtid = tmtid;
    }

    public long getTmtid() {
        return this.tmtid;
    }

    public void setTmtid(long tmtid) {
        this.tmtid = tmtid;
    }

    public String getClassname() {
        return this.classname;
    }

    public void setClassname(String classname) {
        this.classname = classname;
    }

    public Date getTablemodtime() {
        return this.tablemodtime;
    }

    public void setTablemodtime(Date tablemodtime) {
        this.tablemodtime = tablemodtime;
    }

    public String toString() {
        return new ToStringBuilder(this)
            .append("tmtid", getTmtid())
            .toString();
    }

}
