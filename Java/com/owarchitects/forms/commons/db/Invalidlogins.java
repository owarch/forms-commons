package com.owarchitects.forms.commons.db;

import java.io.Serializable;
import java.util.Date;
import org.apache.commons.lang.builder.ToStringBuilder;


/** @author Hibernate CodeGenerator */
public class Invalidlogins implements Serializable {

    /** identifier field */
    private long invalidloginid;

    /** persistent field */
    private String ipaddr;

    /** nullable persistent field */
    private String hostname;

    /** nullable persistent field */
    private Long currcount;

    /** nullable persistent field */
    private Long totcount;

    /** nullable persistent field */
    private String prevuname;

    /** nullable persistent field */
    private Date prevtime;

    /** full constructor */
    public Invalidlogins(String ipaddr, String hostname, Long currcount, Long totcount, String prevuname, Date prevtime) {
        this.ipaddr = ipaddr;
        this.hostname = hostname;
        this.currcount = currcount;
        this.totcount = totcount;
        this.prevuname = prevuname;
        this.prevtime = prevtime;
    }

    /** default constructor */
    public Invalidlogins() {
    }

    /** minimal constructor */
    public Invalidlogins(String ipaddr) {
        this.ipaddr = ipaddr;
    }

    public long getInvalidloginid() {
        return this.invalidloginid;
    }

    public void setInvalidloginid(long invalidloginid) {
        this.invalidloginid = invalidloginid;
    }

    public String getIpaddr() {
        return this.ipaddr;
    }

    public void setIpaddr(String ipaddr) {
        this.ipaddr = ipaddr;
    }

    public String getHostname() {
        return this.hostname;
    }

    public void setHostname(String hostname) {
        this.hostname = hostname;
    }

    public Long getCurrcount() {
        return this.currcount;
    }

    public void setCurrcount(Long currcount) {
        this.currcount = currcount;
    }

    public Long getTotcount() {
        return this.totcount;
    }

    public void setTotcount(Long totcount) {
        this.totcount = totcount;
    }

    public String getPrevuname() {
        return this.prevuname;
    }

    public void setPrevuname(String prevuname) {
        this.prevuname = prevuname;
    }

    public Date getPrevtime() {
        return this.prevtime;
    }

    public void setPrevtime(Date prevtime) {
        this.prevtime = prevtime;
    }

    public String toString() {
        return new ToStringBuilder(this)
            .append("invalidloginid", getInvalidloginid())
            .toString();
    }

}
