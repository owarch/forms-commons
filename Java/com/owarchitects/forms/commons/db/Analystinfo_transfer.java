package com.owarchitects.forms.commons.db;

import java.io.Serializable;
import java.util.Date;
import org.apache.commons.lang.builder.ToStringBuilder;


/** @author Hibernate CodeGenerator */
public class Analystinfo_transfer implements Serializable {

    /** identifier field */
    private long analystinfoid;

    /** persistent field */
    private long panalysthaid;

    /** nullable persistent field */
    private String filename;

    /** nullable persistent field */
    private Long filesize;

    /** nullable persistent field */
    private String description;

    /** nullable persistent field */
    private String creationdate;

    /** nullable persistent field */
    private String notes;

    /** nullable persistent field */
    private Date uploadtime;

    /** nullable persistent field */
    private Long uploaduser;

    /** persistent field */
    private long fileformatid;

    /** full constructor */
    public Analystinfo_transfer(long analystinfoid, long panalysthaid, String filename, Long filesize, String description, String creationdate, String notes, Date uploadtime, Long uploaduser, long fileformatid) {
        this.analystinfoid = analystinfoid;
        this.panalysthaid = panalysthaid;
        this.filename = filename;
        this.filesize = filesize;
        this.description = description;
        this.creationdate = creationdate;
        this.notes = notes;
        this.uploadtime = uploadtime;
        this.uploaduser = uploaduser;
        this.fileformatid = fileformatid;
    }

    /** default constructor */
    public Analystinfo_transfer() {
    }

    /** minimal constructor */
    public Analystinfo_transfer(long analystinfoid, long panalysthaid, long fileformatid) {
        this.analystinfoid = analystinfoid;
        this.panalysthaid = panalysthaid;
        this.fileformatid = fileformatid;
    }

    public long getAnalystinfoid() {
        return this.analystinfoid;
    }

    public void setAnalystinfoid(long analystinfoid) {
        this.analystinfoid = analystinfoid;
    }

    public long getPanalysthaid() {
        return this.panalysthaid;
    }

    public void setPanalysthaid(long panalysthaid) {
        this.panalysthaid = panalysthaid;
    }

    public String getFilename() {
        return this.filename;
    }

    public void setFilename(String filename) {
        this.filename = filename;
    }

    public Long getFilesize() {
        return this.filesize;
    }

    public void setFilesize(Long filesize) {
        this.filesize = filesize;
    }

    public String getDescription() {
        return this.description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getCreationdate() {
        return this.creationdate;
    }

    public void setCreationdate(String creationdate) {
        this.creationdate = creationdate;
    }

    public String getNotes() {
        return this.notes;
    }

    public void setNotes(String notes) {
        this.notes = notes;
    }

    public Date getUploadtime() {
        return this.uploadtime;
    }

    public void setUploadtime(Date uploadtime) {
        this.uploadtime = uploadtime;
    }

    public Long getUploaduser() {
        return this.uploaduser;
    }

    public void setUploaduser(Long uploaduser) {
        this.uploaduser = uploaduser;
    }

    public long getFileformatid() {
        return this.fileformatid;
    }

    public void setFileformatid(long fileformatid) {
        this.fileformatid = fileformatid;
    }

    public String toString() {
        return new ToStringBuilder(this)
            .append("analystinfoid", getAnalystinfoid())
            .toString();
    }

}
