package com.owarchitects.forms.commons.db;

import java.io.Serializable;
import java.util.Date;
import java.util.Set;
import org.apache.commons.lang.builder.ToStringBuilder;


/** @author Hibernate CodeGenerator */
public class Mediapermprofiles implements Serializable {

    /** identifier field */
    private long mprofileid;

    /** persistent field */
    private int profilelevel;

    /** nullable persistent field */
    private Long siteid;

    /** nullable persistent field */
    private Long studyid;

    /** nullable persistent field */
    private String profileacr;

    /** nullable persistent field */
    private String profiledesc;

    /** nullable persistent field */
    private Date recmodtime;

    /** persistent field */
    private Set mediapermprofassign;

    /** persistent field */
    private Set mediarolepermprofassign;

    /** full constructor */
    public Mediapermprofiles(int profilelevel, Long siteid, Long studyid, String profileacr, String profiledesc, Date recmodtime, Set mediapermprofassign, Set mediarolepermprofassign) {
        this.profilelevel = profilelevel;
        this.siteid = siteid;
        this.studyid = studyid;
        this.profileacr = profileacr;
        this.profiledesc = profiledesc;
        this.recmodtime = recmodtime;
        this.mediapermprofassign = mediapermprofassign;
        this.mediarolepermprofassign = mediarolepermprofassign;
    }

    /** default constructor */
    public Mediapermprofiles() {
    }

    /** minimal constructor */
    public Mediapermprofiles(int profilelevel, Set mediapermprofassign, Set mediarolepermprofassign) {
        this.profilelevel = profilelevel;
        this.mediapermprofassign = mediapermprofassign;
        this.mediarolepermprofassign = mediarolepermprofassign;
    }

    public long getMprofileid() {
        return this.mprofileid;
    }

    public void setMprofileid(long mprofileid) {
        this.mprofileid = mprofileid;
    }

    public int getProfilelevel() {
        return this.profilelevel;
    }

    public void setProfilelevel(int profilelevel) {
        this.profilelevel = profilelevel;
    }

    public Long getSiteid() {
        return this.siteid;
    }

    public void setSiteid(Long siteid) {
        this.siteid = siteid;
    }

    public Long getStudyid() {
        return this.studyid;
    }

    public void setStudyid(Long studyid) {
        this.studyid = studyid;
    }

    public String getProfileacr() {
        return this.profileacr;
    }

    public void setProfileacr(String profileacr) {
        this.profileacr = profileacr;
    }

    public String getProfiledesc() {
        return this.profiledesc;
    }

    public void setProfiledesc(String profiledesc) {
        this.profiledesc = profiledesc;
    }

    public Date getRecmodtime() {
        return this.recmodtime;
    }

    public void setRecmodtime(Date recmodtime) {
        this.recmodtime = recmodtime;
    }

    public Set getMediapermprofassign() {
        return this.mediapermprofassign;
    }

    public void setMediapermprofassign(Set mediapermprofassign) {
        this.mediapermprofassign = mediapermprofassign;
    }

    public Set getMediarolepermprofassign() {
        return this.mediarolepermprofassign;
    }

    public void setMediarolepermprofassign(Set mediarolepermprofassign) {
        this.mediarolepermprofassign = mediarolepermprofassign;
    }

    public String toString() {
        return new ToStringBuilder(this)
            .append("mprofileid", getMprofileid())
            .toString();
    }

}
