package com.owarchitects.forms.commons.db;

import java.io.Serializable;
import org.apache.commons.lang.builder.ToStringBuilder;


/** @author Hibernate CodeGenerator */
public class Embeddedinst implements Serializable {

    /** identifier field */
    private long einstid;

    /** persistent field */
    private String acronym;

    /** nullable persistent field */
    private String codeword;

    /** full constructor */
    public Embeddedinst(String acronym, String codeword) {
        this.acronym = acronym;
        this.codeword = codeword;
    }

    /** default constructor */
    public Embeddedinst() {
    }

    /** minimal constructor */
    public Embeddedinst(String acronym) {
        this.acronym = acronym;
    }

    public long getEinstid() {
        return this.einstid;
    }

    public void setEinstid(long einstid) {
        this.einstid = einstid;
    }

    public String getAcronym() {
        return this.acronym;
    }

    public void setAcronym(String acronym) {
        this.acronym = acronym;
    }

    public String getCodeword() {
        return this.codeword;
    }

    public void setCodeword(String codeword) {
        this.codeword = codeword;
    }

    public String toString() {
        return new ToStringBuilder(this)
            .append("einstid", getEinstid())
            .toString();
    }

}
