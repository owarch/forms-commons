package com.owarchitects.forms.commons.db;

import java.io.Serializable;
import java.util.Date;
import org.apache.commons.lang.builder.ToStringBuilder;


/** @author Hibernate CodeGenerator */
public class Rroleuserassign implements Serializable {

    /** identifier field */
    private long rruaid;

    /** nullable persistent field */
    private Date recmodtime;

    /** persistent field */
    private com.owarchitects.forms.commons.db.Resourceroles role;

    /** persistent field */
    private com.owarchitects.forms.commons.db.Allusers user;

    /** full constructor */
    public Rroleuserassign(Date recmodtime, com.owarchitects.forms.commons.db.Resourceroles role, com.owarchitects.forms.commons.db.Allusers user) {
        this.recmodtime = recmodtime;
        this.role = role;
        this.user = user;
    }

    /** default constructor */
    public Rroleuserassign() {
    }

    /** minimal constructor */
    public Rroleuserassign(com.owarchitects.forms.commons.db.Resourceroles role, com.owarchitects.forms.commons.db.Allusers user) {
        this.role = role;
        this.user = user;
    }

    public long getRruaid() {
        return this.rruaid;
    }

    public void setRruaid(long rruaid) {
        this.rruaid = rruaid;
    }

    public Date getRecmodtime() {
        return this.recmodtime;
    }

    public void setRecmodtime(Date recmodtime) {
        this.recmodtime = recmodtime;
    }

    public com.owarchitects.forms.commons.db.Resourceroles getRole() {
        return this.role;
    }

    public void setRole(com.owarchitects.forms.commons.db.Resourceroles role) {
        this.role = role;
    }

    public com.owarchitects.forms.commons.db.Allusers getUser() {
        return this.user;
    }

    public void setUser(com.owarchitects.forms.commons.db.Allusers user) {
        this.user = user;
    }

    public String toString() {
        return new ToStringBuilder(this)
            .append("rruaid", getRruaid())
            .toString();
    }

}
