package com.owarchitects.forms.commons.db;

import java.io.Serializable;
import java.util.Date;
import org.apache.commons.lang.builder.ToStringBuilder;


/** @author Hibernate CodeGenerator */
public class Allusers_transfer implements Serializable {

    /** identifier field */
    private long auserid;

    /** persistent field */
    private long ainstid;

    /** persistent field */
    private long luserid;

    /** nullable persistent field */
    private String userdesc;

    /** nullable persistent field */
    private Boolean isdeleted;

    /** nullable persistent field */
    private Date recmodtime;

    /** full constructor */
    public Allusers_transfer(long auserid, long ainstid, long luserid, String userdesc, Boolean isdeleted, Date recmodtime) {
        this.auserid = auserid;
        this.ainstid = ainstid;
        this.luserid = luserid;
        this.userdesc = userdesc;
        this.isdeleted = isdeleted;
        this.recmodtime = recmodtime;
    }

    /** default constructor */
    public Allusers_transfer() {
    }

    /** minimal constructor */
    public Allusers_transfer(long auserid, long ainstid, long luserid) {
        this.auserid = auserid;
        this.ainstid = ainstid;
        this.luserid = luserid;
    }

    public long getAuserid() {
        return this.auserid;
    }

    public void setAuserid(long auserid) {
        this.auserid = auserid;
    }

    public long getAinstid() {
        return this.ainstid;
    }

    public void setAinstid(long ainstid) {
        this.ainstid = ainstid;
    }

    public long getLuserid() {
        return this.luserid;
    }

    public void setLuserid(long luserid) {
        this.luserid = luserid;
    }

    public String getUserdesc() {
        return this.userdesc;
    }

    public void setUserdesc(String userdesc) {
        this.userdesc = userdesc;
    }

    public Boolean getIsdeleted() {
        return this.isdeleted;
    }

    public void setIsdeleted(Boolean isdeleted) {
        this.isdeleted = isdeleted;
    }

    public Date getRecmodtime() {
        return this.recmodtime;
    }

    public void setRecmodtime(Date recmodtime) {
        this.recmodtime = recmodtime;
    }

    public String toString() {
        return new ToStringBuilder(this)
            .append("auserid", getAuserid())
            .toString();
    }

}
