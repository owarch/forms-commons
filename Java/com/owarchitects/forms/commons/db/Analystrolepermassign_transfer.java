package com.owarchitects.forms.commons.db;

import java.io.Serializable;
import java.util.Date;
import org.apache.commons.lang.builder.ToStringBuilder;


/** @author Hibernate CodeGenerator */
public class Analystrolepermassign_transfer implements Serializable {

    /** identifier field */
    private long arpid;

    /** nullable persistent field */
    private Date recmodtime;

    /** persistent field */
    private long aroleid;

    /** persistent field */
    private Serializable permlevel;

    /** full constructor */
    public Analystrolepermassign_transfer(long arpid, Date recmodtime, long aroleid, Serializable permlevel) {
        this.arpid = arpid;
        this.recmodtime = recmodtime;
        this.aroleid = aroleid;
        this.permlevel = permlevel;
    }

    /** default constructor */
    public Analystrolepermassign_transfer() {
    }

    /** minimal constructor */
    public Analystrolepermassign_transfer(long arpid, long aroleid, Serializable permlevel) {
        this.arpid = arpid;
        this.aroleid = aroleid;
        this.permlevel = permlevel;
    }

    public long getArpid() {
        return this.arpid;
    }

    public void setArpid(long arpid) {
        this.arpid = arpid;
    }

    public Date getRecmodtime() {
        return this.recmodtime;
    }

    public void setRecmodtime(Date recmodtime) {
        this.recmodtime = recmodtime;
    }

    public long getAroleid() {
        return this.aroleid;
    }

    public void setAroleid(long aroleid) {
        this.aroleid = aroleid;
    }

    public Serializable getPermlevel() {
        return this.permlevel;
    }

    public void setPermlevel(Serializable permlevel) {
        this.permlevel = permlevel;
    }

    public String toString() {
        return new ToStringBuilder(this)
            .append("arpid", getArpid())
            .toString();
    }

}
