package com.owarchitects.forms.commons.db;

import java.io.Serializable;
import java.util.Date;
import org.apache.commons.lang.builder.ToStringBuilder;


/** @author Hibernate CodeGenerator */
public class Permissions_transfer implements Serializable {

    /** identifier field */
    private long permid;

    /** persistent field */
    private long pcatid;

    /** persistent field */
    private int permpos;

    /** persistent field */
    private String permacr;

    /** persistent field */
    private String permdesc;

    /** persistent field */
    private int permorder;

    /** persistent field */
    private boolean isdeleted;

    /** nullable persistent field */
    private Date recmodtime;

    /** full constructor */
    public Permissions_transfer(long permid, long pcatid, int permpos, String permacr, String permdesc, int permorder, boolean isdeleted, Date recmodtime) {
        this.permid = permid;
        this.pcatid = pcatid;
        this.permpos = permpos;
        this.permacr = permacr;
        this.permdesc = permdesc;
        this.permorder = permorder;
        this.isdeleted = isdeleted;
        this.recmodtime = recmodtime;
    }

    /** default constructor */
    public Permissions_transfer() {
    }

    /** minimal constructor */
    public Permissions_transfer(long permid, long pcatid, int permpos, String permacr, String permdesc, int permorder, boolean isdeleted) {
        this.permid = permid;
        this.pcatid = pcatid;
        this.permpos = permpos;
        this.permacr = permacr;
        this.permdesc = permdesc;
        this.permorder = permorder;
        this.isdeleted = isdeleted;
    }

    public long getPermid() {
        return this.permid;
    }

    public void setPermid(long permid) {
        this.permid = permid;
    }

    public long getPcatid() {
        return this.pcatid;
    }

    public void setPcatid(long pcatid) {
        this.pcatid = pcatid;
    }

    public int getPermpos() {
        return this.permpos;
    }

    public void setPermpos(int permpos) {
        this.permpos = permpos;
    }

    public String getPermacr() {
        return this.permacr;
    }

    public void setPermacr(String permacr) {
        this.permacr = permacr;
    }

    public String getPermdesc() {
        return this.permdesc;
    }

    public void setPermdesc(String permdesc) {
        this.permdesc = permdesc;
    }

    public int getPermorder() {
        return this.permorder;
    }

    public void setPermorder(int permorder) {
        this.permorder = permorder;
    }

    public boolean isIsdeleted() {
        return this.isdeleted;
    }

    public void setIsdeleted(boolean isdeleted) {
        this.isdeleted = isdeleted;
    }

    public Date getRecmodtime() {
        return this.recmodtime;
    }

    public void setRecmodtime(Date recmodtime) {
        this.recmodtime = recmodtime;
    }

    public String toString() {
        return new ToStringBuilder(this)
            .append("permid", getPermid())
            .toString();
    }

}
