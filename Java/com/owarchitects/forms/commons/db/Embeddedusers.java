package com.owarchitects.forms.commons.db;

import java.io.Serializable;
import org.apache.commons.lang.builder.ToStringBuilder;


/** @author Hibernate CodeGenerator */
public class Embeddedusers implements Serializable {

    /** identifier field */
    private long euserid;

    /** persistent field */
    private String username;

    /** persistent field */
    private String password;

    /** nullable persistent field */
    private Boolean isdisabled;

    /** nullable persistent field */
    private Integer badlogincount;

    /** nullable persistent field */
    private String fkpassword;

    /** nullable persistent field */
    private String dbpassword;

    /** full constructor */
    public Embeddedusers(String username, String password, Boolean isdisabled, Integer badlogincount, String fkpassword, String dbpassword) {
        this.username = username;
        this.password = password;
        this.isdisabled = isdisabled;
        this.badlogincount = badlogincount;
        this.fkpassword = fkpassword;
        this.dbpassword = dbpassword;
    }

    /** default constructor */
    public Embeddedusers() {
    }

    /** minimal constructor */
    public Embeddedusers(String username, String password) {
        this.username = username;
        this.password = password;
    }

    public long getEuserid() {
        return this.euserid;
    }

    public void setEuserid(long euserid) {
        this.euserid = euserid;
    }

    public String getUsername() {
        return this.username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return this.password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public Boolean getIsdisabled() {
        return this.isdisabled;
    }

    public void setIsdisabled(Boolean isdisabled) {
        this.isdisabled = isdisabled;
    }

    public Integer getBadlogincount() {
        return this.badlogincount;
    }

    public void setBadlogincount(Integer badlogincount) {
        this.badlogincount = badlogincount;
    }

    public String getFkpassword() {
        return this.fkpassword;
    }

    public void setFkpassword(String fkpassword) {
        this.fkpassword = fkpassword;
    }

    public String getDbpassword() {
        return this.dbpassword;
    }

    public void setDbpassword(String dbpassword) {
        this.dbpassword = dbpassword;
    }

    public String toString() {
        return new ToStringBuilder(this)
            .append("euserid", getEuserid())
            .toString();
    }

}
