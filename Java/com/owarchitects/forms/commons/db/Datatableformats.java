package com.owarchitects.forms.commons.db;

import java.io.Serializable;
import java.util.Date;
import org.apache.commons.lang.builder.ToStringBuilder;


/** @author Hibernate CodeGenerator */
public class Datatableformats implements Serializable {

    /** identifier field */
    private long dtformatid;

    /** nullable persistent field */
    private Date recmodtime;

    /** persistent field */
    private com.owarchitects.forms.commons.db.Supportedfileformats supportedfileformats;

    /** full constructor */
    public Datatableformats(Date recmodtime, com.owarchitects.forms.commons.db.Supportedfileformats supportedfileformats) {
        this.recmodtime = recmodtime;
        this.supportedfileformats = supportedfileformats;
    }

    /** default constructor */
    public Datatableformats() {
    }

    /** minimal constructor */
    public Datatableformats(com.owarchitects.forms.commons.db.Supportedfileformats supportedfileformats) {
        this.supportedfileformats = supportedfileformats;
    }

    public long getDtformatid() {
        return this.dtformatid;
    }

    public void setDtformatid(long dtformatid) {
        this.dtformatid = dtformatid;
    }

    public Date getRecmodtime() {
        return this.recmodtime;
    }

    public void setRecmodtime(Date recmodtime) {
        this.recmodtime = recmodtime;
    }

    public com.owarchitects.forms.commons.db.Supportedfileformats getSupportedfileformats() {
        return this.supportedfileformats;
    }

    public void setSupportedfileformats(com.owarchitects.forms.commons.db.Supportedfileformats supportedfileformats) {
        this.supportedfileformats = supportedfileformats;
    }

    public String toString() {
        return new ToStringBuilder(this)
            .append("dtformatid", getDtformatid())
            .toString();
    }

}
