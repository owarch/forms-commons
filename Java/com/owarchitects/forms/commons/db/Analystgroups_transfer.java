package com.owarchitects.forms.commons.db;

import java.io.Serializable;
import java.util.Date;
import org.apache.commons.lang.builder.ToStringBuilder;


/** @author Hibernate CodeGenerator */
public class Analystgroups_transfer implements Serializable {

    /** identifier field */
    private long agroupid;

    /** persistent field */
    private String groupacr;

    /** persistent field */
    private String groupdesc;

    /** nullable persistent field */
    private Long createuser;

    /** nullable persistent field */
    private Date recmodtime;

    /** full constructor */
    public Analystgroups_transfer(long agroupid, String groupacr, String groupdesc, Long createuser, Date recmodtime) {
        this.agroupid = agroupid;
        this.groupacr = groupacr;
        this.groupdesc = groupdesc;
        this.createuser = createuser;
        this.recmodtime = recmodtime;
    }

    /** default constructor */
    public Analystgroups_transfer() {
    }

    /** minimal constructor */
    public Analystgroups_transfer(long agroupid, String groupacr, String groupdesc) {
        this.agroupid = agroupid;
        this.groupacr = groupacr;
        this.groupdesc = groupdesc;
    }

    public long getAgroupid() {
        return this.agroupid;
    }

    public void setAgroupid(long agroupid) {
        this.agroupid = agroupid;
    }

    public String getGroupacr() {
        return this.groupacr;
    }

    public void setGroupacr(String groupacr) {
        this.groupacr = groupacr;
    }

    public String getGroupdesc() {
        return this.groupdesc;
    }

    public void setGroupdesc(String groupdesc) {
        this.groupdesc = groupdesc;
    }

    public Long getCreateuser() {
        return this.createuser;
    }

    public void setCreateuser(Long createuser) {
        this.createuser = createuser;
    }

    public Date getRecmodtime() {
        return this.recmodtime;
    }

    public void setRecmodtime(Date recmodtime) {
        this.recmodtime = recmodtime;
    }

    public String toString() {
        return new ToStringBuilder(this)
            .append("agroupid", getAgroupid())
            .toString();
    }

}
