package com.owarchitects.forms.commons.db;

import java.io.Serializable;
import java.util.Date;
import org.apache.commons.lang.builder.ToStringBuilder;


/** @author Hibernate CodeGenerator */
public class Dtmodtime_transfer implements Serializable {

    /** identifier field */
    private long dtmtid;

    /** nullable persistent field */
    private Date localmod;

    /** nullable persistent field */
    private Date remotemod;

    /** nullable persistent field */
    private Date recmodtime;

    /** persistent field */
    private long lstdid;

    /** full constructor */
    public Dtmodtime_transfer(long dtmtid, Date localmod, Date remotemod, Date recmodtime, long lstdid) {
        this.dtmtid = dtmtid;
        this.localmod = localmod;
        this.remotemod = remotemod;
        this.recmodtime = recmodtime;
        this.lstdid = lstdid;
    }

    /** default constructor */
    public Dtmodtime_transfer() {
    }

    /** minimal constructor */
    public Dtmodtime_transfer(long dtmtid, long lstdid) {
        this.dtmtid = dtmtid;
        this.lstdid = lstdid;
    }

    public long getDtmtid() {
        return this.dtmtid;
    }

    public void setDtmtid(long dtmtid) {
        this.dtmtid = dtmtid;
    }

    public Date getLocalmod() {
        return this.localmod;
    }

    public void setLocalmod(Date localmod) {
        this.localmod = localmod;
    }

    public Date getRemotemod() {
        return this.remotemod;
    }

    public void setRemotemod(Date remotemod) {
        this.remotemod = remotemod;
    }

    public Date getRecmodtime() {
        return this.recmodtime;
    }

    public void setRecmodtime(Date recmodtime) {
        this.recmodtime = recmodtime;
    }

    public long getLstdid() {
        return this.lstdid;
    }

    public void setLstdid(long lstdid) {
        this.lstdid = lstdid;
    }

    public String toString() {
        return new ToStringBuilder(this)
            .append("dtmtid", getDtmtid())
            .toString();
    }

}
