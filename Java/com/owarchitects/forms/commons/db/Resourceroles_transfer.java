package com.owarchitects.forms.commons.db;

import java.io.Serializable;
import java.util.Date;
import org.apache.commons.lang.builder.ToStringBuilder;


/** @author Hibernate CodeGenerator */
public class Resourceroles_transfer implements Serializable {

    /** identifier field */
    private long rroleid;

    /** persistent field */
    private int rolelevel;

    /** persistent field */
    private String roleacr;

    /** persistent field */
    private String roledesc;

    /** nullable persistent field */
    private Long siteid;

    /** nullable persistent field */
    private Long studyid;

    /** nullable persistent field */
    private Date recmodtime;

    /** full constructor */
    public Resourceroles_transfer(long rroleid, int rolelevel, String roleacr, String roledesc, Long siteid, Long studyid, Date recmodtime) {
        this.rroleid = rroleid;
        this.rolelevel = rolelevel;
        this.roleacr = roleacr;
        this.roledesc = roledesc;
        this.siteid = siteid;
        this.studyid = studyid;
        this.recmodtime = recmodtime;
    }

    /** default constructor */
    public Resourceroles_transfer() {
    }

    /** minimal constructor */
    public Resourceroles_transfer(long rroleid, int rolelevel, String roleacr, String roledesc) {
        this.rroleid = rroleid;
        this.rolelevel = rolelevel;
        this.roleacr = roleacr;
        this.roledesc = roledesc;
    }

    public long getRroleid() {
        return this.rroleid;
    }

    public void setRroleid(long rroleid) {
        this.rroleid = rroleid;
    }

    public int getRolelevel() {
        return this.rolelevel;
    }

    public void setRolelevel(int rolelevel) {
        this.rolelevel = rolelevel;
    }

    public String getRoleacr() {
        return this.roleacr;
    }

    public void setRoleacr(String roleacr) {
        this.roleacr = roleacr;
    }

    public String getRoledesc() {
        return this.roledesc;
    }

    public void setRoledesc(String roledesc) {
        this.roledesc = roledesc;
    }

    public Long getSiteid() {
        return this.siteid;
    }

    public void setSiteid(Long siteid) {
        this.siteid = siteid;
    }

    public Long getStudyid() {
        return this.studyid;
    }

    public void setStudyid(Long studyid) {
        this.studyid = studyid;
    }

    public Date getRecmodtime() {
        return this.recmodtime;
    }

    public void setRecmodtime(Date recmodtime) {
        this.recmodtime = recmodtime;
    }

    public String toString() {
        return new ToStringBuilder(this)
            .append("rroleid", getRroleid())
            .toString();
    }

}
