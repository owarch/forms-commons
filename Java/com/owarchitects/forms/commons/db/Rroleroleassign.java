package com.owarchitects.forms.commons.db;

import java.io.Serializable;
import java.util.Date;
import org.apache.commons.lang.builder.ToStringBuilder;


/** @author Hibernate CodeGenerator */
public class Rroleroleassign implements Serializable {

    /** identifier field */
    private long rrraid;

    /** nullable persistent field */
    private Date recmodtime;

    /** persistent field */
    private com.owarchitects.forms.commons.db.Resourceroles parentrole;

    /** persistent field */
    private com.owarchitects.forms.commons.db.Resourceroles childrole;

    /** full constructor */
    public Rroleroleassign(Date recmodtime, com.owarchitects.forms.commons.db.Resourceroles parentrole, com.owarchitects.forms.commons.db.Resourceroles childrole) {
        this.recmodtime = recmodtime;
        this.parentrole = parentrole;
        this.childrole = childrole;
    }

    /** default constructor */
    public Rroleroleassign() {
    }

    /** minimal constructor */
    public Rroleroleassign(com.owarchitects.forms.commons.db.Resourceroles parentrole, com.owarchitects.forms.commons.db.Resourceroles childrole) {
        this.parentrole = parentrole;
        this.childrole = childrole;
    }

    public long getRrraid() {
        return this.rrraid;
    }

    public void setRrraid(long rrraid) {
        this.rrraid = rrraid;
    }

    public Date getRecmodtime() {
        return this.recmodtime;
    }

    public void setRecmodtime(Date recmodtime) {
        this.recmodtime = recmodtime;
    }

    public com.owarchitects.forms.commons.db.Resourceroles getParentrole() {
        return this.parentrole;
    }

    public void setParentrole(com.owarchitects.forms.commons.db.Resourceroles parentrole) {
        this.parentrole = parentrole;
    }

    public com.owarchitects.forms.commons.db.Resourceroles getChildrole() {
        return this.childrole;
    }

    public void setChildrole(com.owarchitects.forms.commons.db.Resourceroles childrole) {
        this.childrole = childrole;
    }

    public String toString() {
        return new ToStringBuilder(this)
            .append("rrraid", getRrraid())
            .toString();
    }

}
