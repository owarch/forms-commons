package com.owarchitects.forms.commons.db;

import java.io.Serializable;
import java.util.Date;
import org.apache.commons.lang.builder.ToStringBuilder;


/** @author Hibernate CodeGenerator */
public class Analystgrouppermassign implements Serializable {

    /** identifier field */
    private long agpid;

    /** persistent field */
    private long analystinfoid;

    /** nullable persistent field */
    private Date recmodtime;

    /** persistent field */
    private Serializable permlevel;

    /** persistent field */
    private com.owarchitects.forms.commons.db.Analystgroups analystgroups;

    /** full constructor */
    public Analystgrouppermassign(long analystinfoid, Date recmodtime, Serializable permlevel, com.owarchitects.forms.commons.db.Analystgroups analystgroups) {
        this.analystinfoid = analystinfoid;
        this.recmodtime = recmodtime;
        this.permlevel = permlevel;
        this.analystgroups = analystgroups;
    }

    /** default constructor */
    public Analystgrouppermassign() {
    }

    /** minimal constructor */
    public Analystgrouppermassign(long analystinfoid, Serializable permlevel, com.owarchitects.forms.commons.db.Analystgroups analystgroups) {
        this.analystinfoid = analystinfoid;
        this.permlevel = permlevel;
        this.analystgroups = analystgroups;
    }

    public long getAgpid() {
        return this.agpid;
    }

    public void setAgpid(long agpid) {
        this.agpid = agpid;
    }

    public long getAnalystinfoid() {
        return this.analystinfoid;
    }

    public void setAnalystinfoid(long analystinfoid) {
        this.analystinfoid = analystinfoid;
    }

    public Date getRecmodtime() {
        return this.recmodtime;
    }

    public void setRecmodtime(Date recmodtime) {
        this.recmodtime = recmodtime;
    }

    public Serializable getPermlevel() {
        return this.permlevel;
    }

    public void setPermlevel(Serializable permlevel) {
        this.permlevel = permlevel;
    }

    public com.owarchitects.forms.commons.db.Analystgroups getAnalystgroups() {
        return this.analystgroups;
    }

    public void setAnalystgroups(com.owarchitects.forms.commons.db.Analystgroups analystgroups) {
        this.analystgroups = analystgroups;
    }

    public String toString() {
        return new ToStringBuilder(this)
            .append("agpid", getAgpid())
            .toString();
    }

}
