package com.owarchitects.forms.commons.db;

import java.io.Serializable;
import java.util.Date;
import org.apache.commons.lang.builder.ToStringBuilder;


/** @author Hibernate CodeGenerator */
public class Formattrs implements Serializable {

    /** identifier field */
    private long formattrid;

    /** nullable persistent field */
    private String attr;

    /** nullable persistent field */
    private String value;

    /** nullable persistent field */
    private Date recmodtime;

    /** persistent field */
    private com.owarchitects.forms.commons.db.Datatabledef datatabledef;

    /** full constructor */
    public Formattrs(String attr, String value, Date recmodtime, com.owarchitects.forms.commons.db.Datatabledef datatabledef) {
        this.attr = attr;
        this.value = value;
        this.recmodtime = recmodtime;
        this.datatabledef = datatabledef;
    }

    /** default constructor */
    public Formattrs() {
    }

    /** minimal constructor */
    public Formattrs(com.owarchitects.forms.commons.db.Datatabledef datatabledef) {
        this.datatabledef = datatabledef;
    }

    public long getFormattrid() {
        return this.formattrid;
    }

    public void setFormattrid(long formattrid) {
        this.formattrid = formattrid;
    }

    public String getAttr() {
        return this.attr;
    }

    public void setAttr(String attr) {
        this.attr = attr;
    }

    public String getValue() {
        return this.value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public Date getRecmodtime() {
        return this.recmodtime;
    }

    public void setRecmodtime(Date recmodtime) {
        this.recmodtime = recmodtime;
    }

    public com.owarchitects.forms.commons.db.Datatabledef getDatatabledef() {
        return this.datatabledef;
    }

    public void setDatatabledef(com.owarchitects.forms.commons.db.Datatabledef datatabledef) {
        this.datatabledef = datatabledef;
    }

    public String toString() {
        return new ToStringBuilder(this)
            .append("formattrid", getFormattrid())
            .toString();
    }

}
