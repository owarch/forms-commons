package com.owarchitects.forms.commons.db;

import java.io.Serializable;
import java.util.Date;
import org.apache.commons.lang.builder.ToStringBuilder;


/** @author Hibernate CodeGenerator */
public class Analysthagrouppermassign_transfer implements Serializable {

    /** identifier field */
    private long ahagpid;

    /** persistent field */
    private long analysthaid;

    /** nullable persistent field */
    private Date recmodtime;

    /** persistent field */
    private long agroupid;

    /** persistent field */
    private Serializable permlevel;

    /** full constructor */
    public Analysthagrouppermassign_transfer(long ahagpid, long analysthaid, Date recmodtime, long agroupid, Serializable permlevel) {
        this.ahagpid = ahagpid;
        this.analysthaid = analysthaid;
        this.recmodtime = recmodtime;
        this.agroupid = agroupid;
        this.permlevel = permlevel;
    }

    /** default constructor */
    public Analysthagrouppermassign_transfer() {
    }

    /** minimal constructor */
    public Analysthagrouppermassign_transfer(long ahagpid, long analysthaid, long agroupid, Serializable permlevel) {
        this.ahagpid = ahagpid;
        this.analysthaid = analysthaid;
        this.agroupid = agroupid;
        this.permlevel = permlevel;
    }

    public long getAhagpid() {
        return this.ahagpid;
    }

    public void setAhagpid(long ahagpid) {
        this.ahagpid = ahagpid;
    }

    public long getAnalysthaid() {
        return this.analysthaid;
    }

    public void setAnalysthaid(long analysthaid) {
        this.analysthaid = analysthaid;
    }

    public Date getRecmodtime() {
        return this.recmodtime;
    }

    public void setRecmodtime(Date recmodtime) {
        this.recmodtime = recmodtime;
    }

    public long getAgroupid() {
        return this.agroupid;
    }

    public void setAgroupid(long agroupid) {
        this.agroupid = agroupid;
    }

    public Serializable getPermlevel() {
        return this.permlevel;
    }

    public void setPermlevel(Serializable permlevel) {
        this.permlevel = permlevel;
    }

    public String toString() {
        return new ToStringBuilder(this)
            .append("ahagpid", getAhagpid())
            .toString();
    }

}
