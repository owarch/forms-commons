package com.owarchitects.forms.commons.db;

import java.io.Serializable;
import java.util.Date;
import org.apache.commons.lang.builder.ToStringBuilder;


/** @author Hibernate CodeGenerator */
public class Mediauserpermassign implements Serializable {

    /** identifier field */
    private long mupid;

    /** persistent field */
    private long mediainfoid;

    /** persistent field */
    private Serializable permlevel;

    /** nullable persistent field */
    private Date recmodtime;

    /** persistent field */
    private com.owarchitects.forms.commons.db.Allusers allusers;

    /** full constructor */
    public Mediauserpermassign(long mediainfoid, Serializable permlevel, Date recmodtime, com.owarchitects.forms.commons.db.Allusers allusers) {
        this.mediainfoid = mediainfoid;
        this.permlevel = permlevel;
        this.recmodtime = recmodtime;
        this.allusers = allusers;
    }

    /** default constructor */
    public Mediauserpermassign() {
    }

    /** minimal constructor */
    public Mediauserpermassign(long mediainfoid, Serializable permlevel, com.owarchitects.forms.commons.db.Allusers allusers) {
        this.mediainfoid = mediainfoid;
        this.permlevel = permlevel;
        this.allusers = allusers;
    }

    public long getMupid() {
        return this.mupid;
    }

    public void setMupid(long mupid) {
        this.mupid = mupid;
    }

    public long getMediainfoid() {
        return this.mediainfoid;
    }

    public void setMediainfoid(long mediainfoid) {
        this.mediainfoid = mediainfoid;
    }

    public Serializable getPermlevel() {
        return this.permlevel;
    }

    public void setPermlevel(Serializable permlevel) {
        this.permlevel = permlevel;
    }

    public Date getRecmodtime() {
        return this.recmodtime;
    }

    public void setRecmodtime(Date recmodtime) {
        this.recmodtime = recmodtime;
    }

    public com.owarchitects.forms.commons.db.Allusers getAllusers() {
        return this.allusers;
    }

    public void setAllusers(com.owarchitects.forms.commons.db.Allusers allusers) {
        this.allusers = allusers;
    }

    public String toString() {
        return new ToStringBuilder(this)
            .append("mupid", getMupid())
            .toString();
    }

}
