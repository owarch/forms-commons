package com.owarchitects.forms.commons.db;

import java.io.Serializable;
import org.apache.commons.lang.builder.ToStringBuilder;


/** @author Hibernate CodeGenerator */
public class Analyststore_transfer implements Serializable {

    /** identifier field */
    private long analyststoreid;

    /** persistent field */
    private long analystinfoid;

    /** persistent field */
    private String analystcontent;

    /** full constructor */
    public Analyststore_transfer(long analyststoreid, long analystinfoid, String analystcontent) {
        this.analyststoreid = analyststoreid;
        this.analystinfoid = analystinfoid;
        this.analystcontent = analystcontent;
    }

    /** default constructor */
    public Analyststore_transfer() {
    }

    public long getAnalyststoreid() {
        return this.analyststoreid;
    }

    public void setAnalyststoreid(long analyststoreid) {
        this.analyststoreid = analyststoreid;
    }

    public long getAnalystinfoid() {
        return this.analystinfoid;
    }

    public void setAnalystinfoid(long analystinfoid) {
        this.analystinfoid = analystinfoid;
    }

    public String getAnalystcontent() {
        return this.analystcontent;
    }

    public void setAnalystcontent(String analystcontent) {
        this.analystcontent = analystcontent;
    }

    public String toString() {
        return new ToStringBuilder(this)
            .append("analyststoreid", getAnalyststoreid())
            .toString();
    }

}
