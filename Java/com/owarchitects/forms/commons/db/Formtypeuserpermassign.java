package com.owarchitects.forms.commons.db;

import java.io.Serializable;
import java.util.Date;
import org.apache.commons.lang.builder.ToStringBuilder;


/** @author Hibernate CodeGenerator */
public class Formtypeuserpermassign implements Serializable {

    /** identifier field */
    private long dtupid;

    /** persistent field */
    private long formtypelistid;

    /** persistent field */
    private Serializable permlevel;

    /** nullable persistent field */
    private Date recmodtime;

    /** persistent field */
    private com.owarchitects.forms.commons.db.Allusers allusers;

    /** full constructor */
    public Formtypeuserpermassign(long formtypelistid, Serializable permlevel, Date recmodtime, com.owarchitects.forms.commons.db.Allusers allusers) {
        this.formtypelistid = formtypelistid;
        this.permlevel = permlevel;
        this.recmodtime = recmodtime;
        this.allusers = allusers;
    }

    /** default constructor */
    public Formtypeuserpermassign() {
    }

    /** minimal constructor */
    public Formtypeuserpermassign(long formtypelistid, Serializable permlevel, com.owarchitects.forms.commons.db.Allusers allusers) {
        this.formtypelistid = formtypelistid;
        this.permlevel = permlevel;
        this.allusers = allusers;
    }

    public long getDtupid() {
        return this.dtupid;
    }

    public void setDtupid(long dtupid) {
        this.dtupid = dtupid;
    }

    public long getFormtypelistid() {
        return this.formtypelistid;
    }

    public void setFormtypelistid(long formtypelistid) {
        this.formtypelistid = formtypelistid;
    }

    public Serializable getPermlevel() {
        return this.permlevel;
    }

    public void setPermlevel(Serializable permlevel) {
        this.permlevel = permlevel;
    }

    public Date getRecmodtime() {
        return this.recmodtime;
    }

    public void setRecmodtime(Date recmodtime) {
        this.recmodtime = recmodtime;
    }

    public com.owarchitects.forms.commons.db.Allusers getAllusers() {
        return this.allusers;
    }

    public void setAllusers(com.owarchitects.forms.commons.db.Allusers allusers) {
        this.allusers = allusers;
    }

    public String toString() {
        return new ToStringBuilder(this)
            .append("dtupid", getDtupid())
            .toString();
    }

}
