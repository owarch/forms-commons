package com.owarchitects.forms.commons.db;

import java.io.Serializable;
import java.util.Date;
import java.util.Set;
import org.apache.commons.lang.builder.ToStringBuilder;


/** @author Hibernate CodeGenerator */
public class Formtypes implements Serializable {

    /** identifier field */
    private long formtypeid;

    /** nullable persistent field */
    private Long ainstid;

    /** nullable persistent field */
    private Long remoteformtypeid;

    /** nullable persistent field */
    private String filename;

    /** nullable persistent field */
    private Long filesize;

    /** nullable persistent field */
    private Long uploaduser;

    /** nullable persistent field */
    private Date uploadtime;

    /** nullable persistent field */
    private String notes;

    /** nullable persistent field */
    private Date recmodtime;

    /** persistent field */
    private com.owarchitects.forms.commons.db.Datatabledef datatabledef;

    /** persistent field */
    private com.owarchitects.forms.commons.db.Formtypelist formtypelist;

    /** persistent field */
    private com.owarchitects.forms.commons.db.Formformats formformats;

    /** persistent field */
    private Set formstore;

    /** full constructor */
    public Formtypes(Long ainstid, Long remoteformtypeid, String filename, Long filesize, Long uploaduser, Date uploadtime, String notes, Date recmodtime, com.owarchitects.forms.commons.db.Datatabledef datatabledef, com.owarchitects.forms.commons.db.Formtypelist formtypelist, com.owarchitects.forms.commons.db.Formformats formformats, Set formstore) {
        this.ainstid = ainstid;
        this.remoteformtypeid = remoteformtypeid;
        this.filename = filename;
        this.filesize = filesize;
        this.uploaduser = uploaduser;
        this.uploadtime = uploadtime;
        this.notes = notes;
        this.recmodtime = recmodtime;
        this.datatabledef = datatabledef;
        this.formtypelist = formtypelist;
        this.formformats = formformats;
        this.formstore = formstore;
    }

    /** default constructor */
    public Formtypes() {
    }

    /** minimal constructor */
    public Formtypes(com.owarchitects.forms.commons.db.Datatabledef datatabledef, com.owarchitects.forms.commons.db.Formtypelist formtypelist, com.owarchitects.forms.commons.db.Formformats formformats, Set formstore) {
        this.datatabledef = datatabledef;
        this.formtypelist = formtypelist;
        this.formformats = formformats;
        this.formstore = formstore;
    }

    public long getFormtypeid() {
        return this.formtypeid;
    }

    public void setFormtypeid(long formtypeid) {
        this.formtypeid = formtypeid;
    }

    public Long getAinstid() {
        return this.ainstid;
    }

    public void setAinstid(Long ainstid) {
        this.ainstid = ainstid;
    }

    public Long getRemoteformtypeid() {
        return this.remoteformtypeid;
    }

    public void setRemoteformtypeid(Long remoteformtypeid) {
        this.remoteformtypeid = remoteformtypeid;
    }

    public String getFilename() {
        return this.filename;
    }

    public void setFilename(String filename) {
        this.filename = filename;
    }

    public Long getFilesize() {
        return this.filesize;
    }

    public void setFilesize(Long filesize) {
        this.filesize = filesize;
    }

    public Long getUploaduser() {
        return this.uploaduser;
    }

    public void setUploaduser(Long uploaduser) {
        this.uploaduser = uploaduser;
    }

    public Date getUploadtime() {
        return this.uploadtime;
    }

    public void setUploadtime(Date uploadtime) {
        this.uploadtime = uploadtime;
    }

    public String getNotes() {
        return this.notes;
    }

    public void setNotes(String notes) {
        this.notes = notes;
    }

    public Date getRecmodtime() {
        return this.recmodtime;
    }

    public void setRecmodtime(Date recmodtime) {
        this.recmodtime = recmodtime;
    }

    public com.owarchitects.forms.commons.db.Datatabledef getDatatabledef() {
        return this.datatabledef;
    }

    public void setDatatabledef(com.owarchitects.forms.commons.db.Datatabledef datatabledef) {
        this.datatabledef = datatabledef;
    }

    public com.owarchitects.forms.commons.db.Formtypelist getFormtypelist() {
        return this.formtypelist;
    }

    public void setFormtypelist(com.owarchitects.forms.commons.db.Formtypelist formtypelist) {
        this.formtypelist = formtypelist;
    }

    public com.owarchitects.forms.commons.db.Formformats getFormformats() {
        return this.formformats;
    }

    public void setFormformats(com.owarchitects.forms.commons.db.Formformats formformats) {
        this.formformats = formformats;
    }

    public Set getFormstore() {
        return this.formstore;
    }

    public void setFormstore(Set formstore) {
        this.formstore = formstore;
    }

    public String toString() {
        return new ToStringBuilder(this)
            .append("formtypeid", getFormtypeid())
            .toString();
    }

}
