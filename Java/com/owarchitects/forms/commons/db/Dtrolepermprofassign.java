package com.owarchitects.forms.commons.db;

import java.io.Serializable;
import java.util.Date;
import org.apache.commons.lang.builder.ToStringBuilder;


/** @author Hibernate CodeGenerator */
public class Dtrolepermprofassign implements Serializable {

    /** identifier field */
    private long dtrppaid;

    /** persistent field */
    private Serializable permlevel;

    /** nullable persistent field */
    private Date recmodtime;

    /** persistent field */
    private com.owarchitects.forms.commons.db.Dtpermprofiles dtpermprofiles;

    /** persistent field */
    private com.owarchitects.forms.commons.db.Resourceroles resourceroles;

    /** full constructor */
    public Dtrolepermprofassign(Serializable permlevel, Date recmodtime, com.owarchitects.forms.commons.db.Dtpermprofiles dtpermprofiles, com.owarchitects.forms.commons.db.Resourceroles resourceroles) {
        this.permlevel = permlevel;
        this.recmodtime = recmodtime;
        this.dtpermprofiles = dtpermprofiles;
        this.resourceroles = resourceroles;
    }

    /** default constructor */
    public Dtrolepermprofassign() {
    }

    /** minimal constructor */
    public Dtrolepermprofassign(Serializable permlevel, com.owarchitects.forms.commons.db.Dtpermprofiles dtpermprofiles, com.owarchitects.forms.commons.db.Resourceroles resourceroles) {
        this.permlevel = permlevel;
        this.dtpermprofiles = dtpermprofiles;
        this.resourceroles = resourceroles;
    }

    public long getDtrppaid() {
        return this.dtrppaid;
    }

    public void setDtrppaid(long dtrppaid) {
        this.dtrppaid = dtrppaid;
    }

    public Serializable getPermlevel() {
        return this.permlevel;
    }

    public void setPermlevel(Serializable permlevel) {
        this.permlevel = permlevel;
    }

    public Date getRecmodtime() {
        return this.recmodtime;
    }

    public void setRecmodtime(Date recmodtime) {
        this.recmodtime = recmodtime;
    }

    public com.owarchitects.forms.commons.db.Dtpermprofiles getDtpermprofiles() {
        return this.dtpermprofiles;
    }

    public void setDtpermprofiles(com.owarchitects.forms.commons.db.Dtpermprofiles dtpermprofiles) {
        this.dtpermprofiles = dtpermprofiles;
    }

    public com.owarchitects.forms.commons.db.Resourceroles getResourceroles() {
        return this.resourceroles;
    }

    public void setResourceroles(com.owarchitects.forms.commons.db.Resourceroles resourceroles) {
        this.resourceroles = resourceroles;
    }

    public String toString() {
        return new ToStringBuilder(this)
            .append("dtrppaid", getDtrppaid())
            .toString();
    }

}
