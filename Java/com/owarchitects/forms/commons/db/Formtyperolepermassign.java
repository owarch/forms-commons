package com.owarchitects.forms.commons.db;

import java.io.Serializable;
import java.util.Date;
import org.apache.commons.lang.builder.ToStringBuilder;


/** @author Hibernate CodeGenerator */
public class Formtyperolepermassign implements Serializable {

    /** identifier field */
    private long dtrpid;

    /** persistent field */
    private long formtypelistid;

    /** persistent field */
    private Serializable permlevel;

    /** nullable persistent field */
    private Date recmodtime;

    /** persistent field */
    private com.owarchitects.forms.commons.db.Resourceroles resourceroles;

    /** full constructor */
    public Formtyperolepermassign(long formtypelistid, Serializable permlevel, Date recmodtime, com.owarchitects.forms.commons.db.Resourceroles resourceroles) {
        this.formtypelistid = formtypelistid;
        this.permlevel = permlevel;
        this.recmodtime = recmodtime;
        this.resourceroles = resourceroles;
    }

    /** default constructor */
    public Formtyperolepermassign() {
    }

    /** minimal constructor */
    public Formtyperolepermassign(long formtypelistid, Serializable permlevel, com.owarchitects.forms.commons.db.Resourceroles resourceroles) {
        this.formtypelistid = formtypelistid;
        this.permlevel = permlevel;
        this.resourceroles = resourceroles;
    }

    public long getDtrpid() {
        return this.dtrpid;
    }

    public void setDtrpid(long dtrpid) {
        this.dtrpid = dtrpid;
    }

    public long getFormtypelistid() {
        return this.formtypelistid;
    }

    public void setFormtypelistid(long formtypelistid) {
        this.formtypelistid = formtypelistid;
    }

    public Serializable getPermlevel() {
        return this.permlevel;
    }

    public void setPermlevel(Serializable permlevel) {
        this.permlevel = permlevel;
    }

    public Date getRecmodtime() {
        return this.recmodtime;
    }

    public void setRecmodtime(Date recmodtime) {
        this.recmodtime = recmodtime;
    }

    public com.owarchitects.forms.commons.db.Resourceroles getResourceroles() {
        return this.resourceroles;
    }

    public void setResourceroles(com.owarchitects.forms.commons.db.Resourceroles resourceroles) {
        this.resourceroles = resourceroles;
    }

    public String toString() {
        return new ToStringBuilder(this)
            .append("dtrpid", getDtrpid())
            .toString();
    }

}
