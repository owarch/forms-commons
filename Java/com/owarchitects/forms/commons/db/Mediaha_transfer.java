package com.owarchitects.forms.commons.db;

import java.io.Serializable;
import org.apache.commons.lang.builder.ToStringBuilder;


/** @author Hibernate CodeGenerator */
public class Mediaha_transfer implements Serializable {

    /** identifier field */
    private long mediahaid;

    /** persistent field */
    private long studyid;

    /** persistent field */
    private int visibility;

    /** nullable persistent field */
    private Long pmediahaid;

    /** nullable persistent field */
    private String hadesc;

    /** nullable persistent field */
    private Integer haordr;

    /** full constructor */
    public Mediaha_transfer(long mediahaid, long studyid, int visibility, Long pmediahaid, String hadesc, Integer haordr) {
        this.mediahaid = mediahaid;
        this.studyid = studyid;
        this.visibility = visibility;
        this.pmediahaid = pmediahaid;
        this.hadesc = hadesc;
        this.haordr = haordr;
    }

    /** default constructor */
    public Mediaha_transfer() {
    }

    /** minimal constructor */
    public Mediaha_transfer(long mediahaid, long studyid, int visibility) {
        this.mediahaid = mediahaid;
        this.studyid = studyid;
        this.visibility = visibility;
    }

    public long getMediahaid() {
        return this.mediahaid;
    }

    public void setMediahaid(long mediahaid) {
        this.mediahaid = mediahaid;
    }

    public long getStudyid() {
        return this.studyid;
    }

    public void setStudyid(long studyid) {
        this.studyid = studyid;
    }

    public int getVisibility() {
        return this.visibility;
    }

    public void setVisibility(int visibility) {
        this.visibility = visibility;
    }

    public Long getPmediahaid() {
        return this.pmediahaid;
    }

    public void setPmediahaid(Long pmediahaid) {
        this.pmediahaid = pmediahaid;
    }

    public String getHadesc() {
        return this.hadesc;
    }

    public void setHadesc(String hadesc) {
        this.hadesc = hadesc;
    }

    public Integer getHaordr() {
        return this.haordr;
    }

    public void setHaordr(Integer haordr) {
        this.haordr = haordr;
    }

    public String toString() {
        return new ToStringBuilder(this)
            .append("mediahaid", getMediahaid())
            .toString();
    }

}
