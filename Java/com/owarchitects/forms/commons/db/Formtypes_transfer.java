package com.owarchitects.forms.commons.db;

import java.io.Serializable;
import java.util.Date;
import org.apache.commons.lang.builder.ToStringBuilder;


/** @author Hibernate CodeGenerator */
public class Formtypes_transfer implements Serializable {

    /** identifier field */
    private long formtypeid;

    /** persistent field */
    private long dtdefid;

    /** persistent field */
    private long formtypelistid;

    /** persistent field */
    private long formformatid;

    /** nullable persistent field */
    private Long ainstid;

    /** nullable persistent field */
    private Long remoteformtypeid;

    /** nullable persistent field */
    private String filename;

    /** nullable persistent field */
    private Long filesize;

    /** nullable persistent field */
    private Long uploaduser;

    /** nullable persistent field */
    private Date uploadtime;

    /** nullable persistent field */
    private String notes;

    /** nullable persistent field */
    private Date recmodtime;

    /** full constructor */
    public Formtypes_transfer(long formtypeid, long dtdefid, long formtypelistid, long formformatid, Long ainstid, Long remoteformtypeid, String filename, Long filesize, Long uploaduser, Date uploadtime, String notes, Date recmodtime) {
        this.formtypeid = formtypeid;
        this.dtdefid = dtdefid;
        this.formtypelistid = formtypelistid;
        this.formformatid = formformatid;
        this.ainstid = ainstid;
        this.remoteformtypeid = remoteformtypeid;
        this.filename = filename;
        this.filesize = filesize;
        this.uploaduser = uploaduser;
        this.uploadtime = uploadtime;
        this.notes = notes;
        this.recmodtime = recmodtime;
    }

    /** default constructor */
    public Formtypes_transfer() {
    }

    /** minimal constructor */
    public Formtypes_transfer(long formtypeid, long dtdefid, long formtypelistid, long formformatid) {
        this.formtypeid = formtypeid;
        this.dtdefid = dtdefid;
        this.formtypelistid = formtypelistid;
        this.formformatid = formformatid;
    }

    public long getFormtypeid() {
        return this.formtypeid;
    }

    public void setFormtypeid(long formtypeid) {
        this.formtypeid = formtypeid;
    }

    public long getDtdefid() {
        return this.dtdefid;
    }

    public void setDtdefid(long dtdefid) {
        this.dtdefid = dtdefid;
    }

    public long getFormtypelistid() {
        return this.formtypelistid;
    }

    public void setFormtypelistid(long formtypelistid) {
        this.formtypelistid = formtypelistid;
    }

    public long getFormformatid() {
        return this.formformatid;
    }

    public void setFormformatid(long formformatid) {
        this.formformatid = formformatid;
    }

    public Long getAinstid() {
        return this.ainstid;
    }

    public void setAinstid(Long ainstid) {
        this.ainstid = ainstid;
    }

    public Long getRemoteformtypeid() {
        return this.remoteformtypeid;
    }

    public void setRemoteformtypeid(Long remoteformtypeid) {
        this.remoteformtypeid = remoteformtypeid;
    }

    public String getFilename() {
        return this.filename;
    }

    public void setFilename(String filename) {
        this.filename = filename;
    }

    public Long getFilesize() {
        return this.filesize;
    }

    public void setFilesize(Long filesize) {
        this.filesize = filesize;
    }

    public Long getUploaduser() {
        return this.uploaduser;
    }

    public void setUploaduser(Long uploaduser) {
        this.uploaduser = uploaduser;
    }

    public Date getUploadtime() {
        return this.uploadtime;
    }

    public void setUploadtime(Date uploadtime) {
        this.uploadtime = uploadtime;
    }

    public String getNotes() {
        return this.notes;
    }

    public void setNotes(String notes) {
        this.notes = notes;
    }

    public Date getRecmodtime() {
        return this.recmodtime;
    }

    public void setRecmodtime(Date recmodtime) {
        this.recmodtime = recmodtime;
    }

    public String toString() {
        return new ToStringBuilder(this)
            .append("formtypeid", getFormtypeid())
            .toString();
    }

}
