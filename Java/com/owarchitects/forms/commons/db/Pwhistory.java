package com.owarchitects.forms.commons.db;

import java.io.Serializable;
import java.util.Date;
import org.apache.commons.lang.builder.ToStringBuilder;


/** @author Hibernate CodeGenerator */
public class Pwhistory implements Serializable {

    /** identifier field */
    private long pwhid;

    /** persistent field */
    private String username;

    /** persistent field */
    private String password;

    /** nullable persistent field */
    private Date passworddt;

    /** nullable persistent field */
    private Date recmodtime;

    /** persistent field */
    private com.owarchitects.forms.commons.db.Localusers localusers;

    /** full constructor */
    public Pwhistory(String username, String password, Date passworddt, Date recmodtime, com.owarchitects.forms.commons.db.Localusers localusers) {
        this.username = username;
        this.password = password;
        this.passworddt = passworddt;
        this.recmodtime = recmodtime;
        this.localusers = localusers;
    }

    /** default constructor */
    public Pwhistory() {
    }

    /** minimal constructor */
    public Pwhistory(String username, String password, com.owarchitects.forms.commons.db.Localusers localusers) {
        this.username = username;
        this.password = password;
        this.localusers = localusers;
    }

    public long getPwhid() {
        return this.pwhid;
    }

    public void setPwhid(long pwhid) {
        this.pwhid = pwhid;
    }

    public String getUsername() {
        return this.username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return this.password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public Date getPassworddt() {
        return this.passworddt;
    }

    public void setPassworddt(Date passworddt) {
        this.passworddt = passworddt;
    }

    public Date getRecmodtime() {
        return this.recmodtime;
    }

    public void setRecmodtime(Date recmodtime) {
        this.recmodtime = recmodtime;
    }

    public com.owarchitects.forms.commons.db.Localusers getLocalusers() {
        return this.localusers;
    }

    public void setLocalusers(com.owarchitects.forms.commons.db.Localusers localusers) {
        this.localusers = localusers;
    }

    public String toString() {
        return new ToStringBuilder(this)
            .append("pwhid", getPwhid())
            .toString();
    }

}
