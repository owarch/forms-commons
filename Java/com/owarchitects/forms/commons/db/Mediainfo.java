package com.owarchitects.forms.commons.db;

import java.io.Serializable;
import java.util.Date;
import java.util.Set;
import org.apache.commons.lang.builder.ToStringBuilder;


/** @author Hibernate CodeGenerator */
public class Mediainfo implements Serializable {

    /** identifier field */
    private long mediainfoid;

    /** nullable persistent field */
    private Long remotemediainfoid;

    /** persistent field */
    private long pmediahaid;

    /** nullable persistent field */
    private String filename;

    /** nullable persistent field */
    private Long filesize;

    /** nullable persistent field */
    private String description;

    /** nullable persistent field */
    private String creationdate;

    /** nullable persistent field */
    private String notes;

    /** nullable persistent field */
    private Date uploadtime;

    /** nullable persistent field */
    private Long uploaduser;

    /** persistent field */
    private com.owarchitects.forms.commons.db.Allinst allinst;

    /** persistent field */
    private com.owarchitects.forms.commons.db.Supportedfileformats supportedfileformats;

    /** persistent field */
    private Set mediastore;

    /** full constructor */
    public Mediainfo(Long remotemediainfoid, long pmediahaid, String filename, Long filesize, String description, String creationdate, String notes, Date uploadtime, Long uploaduser, com.owarchitects.forms.commons.db.Allinst allinst, com.owarchitects.forms.commons.db.Supportedfileformats supportedfileformats, Set mediastore) {
        this.remotemediainfoid = remotemediainfoid;
        this.pmediahaid = pmediahaid;
        this.filename = filename;
        this.filesize = filesize;
        this.description = description;
        this.creationdate = creationdate;
        this.notes = notes;
        this.uploadtime = uploadtime;
        this.uploaduser = uploaduser;
        this.allinst = allinst;
        this.supportedfileformats = supportedfileformats;
        this.mediastore = mediastore;
    }

    /** default constructor */
    public Mediainfo() {
    }

    /** minimal constructor */
    public Mediainfo(long pmediahaid, com.owarchitects.forms.commons.db.Allinst allinst, com.owarchitects.forms.commons.db.Supportedfileformats supportedfileformats, Set mediastore) {
        this.pmediahaid = pmediahaid;
        this.allinst = allinst;
        this.supportedfileformats = supportedfileformats;
        this.mediastore = mediastore;
    }

    public long getMediainfoid() {
        return this.mediainfoid;
    }

    public void setMediainfoid(long mediainfoid) {
        this.mediainfoid = mediainfoid;
    }

    public Long getRemotemediainfoid() {
        return this.remotemediainfoid;
    }

    public void setRemotemediainfoid(Long remotemediainfoid) {
        this.remotemediainfoid = remotemediainfoid;
    }

    public long getPmediahaid() {
        return this.pmediahaid;
    }

    public void setPmediahaid(long pmediahaid) {
        this.pmediahaid = pmediahaid;
    }

    public String getFilename() {
        return this.filename;
    }

    public void setFilename(String filename) {
        this.filename = filename;
    }

    public Long getFilesize() {
        return this.filesize;
    }

    public void setFilesize(Long filesize) {
        this.filesize = filesize;
    }

    public String getDescription() {
        return this.description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getCreationdate() {
        return this.creationdate;
    }

    public void setCreationdate(String creationdate) {
        this.creationdate = creationdate;
    }

    public String getNotes() {
        return this.notes;
    }

    public void setNotes(String notes) {
        this.notes = notes;
    }

    public Date getUploadtime() {
        return this.uploadtime;
    }

    public void setUploadtime(Date uploadtime) {
        this.uploadtime = uploadtime;
    }

    public Long getUploaduser() {
        return this.uploaduser;
    }

    public void setUploaduser(Long uploaduser) {
        this.uploaduser = uploaduser;
    }

    public com.owarchitects.forms.commons.db.Allinst getAllinst() {
        return this.allinst;
    }

    public void setAllinst(com.owarchitects.forms.commons.db.Allinst allinst) {
        this.allinst = allinst;
    }

    public com.owarchitects.forms.commons.db.Supportedfileformats getSupportedfileformats() {
        return this.supportedfileformats;
    }

    public void setSupportedfileformats(com.owarchitects.forms.commons.db.Supportedfileformats supportedfileformats) {
        this.supportedfileformats = supportedfileformats;
    }

    public Set getMediastore() {
        return this.mediastore;
    }

    public void setMediastore(Set mediastore) {
        this.mediastore = mediastore;
    }

    public String toString() {
        return new ToStringBuilder(this)
            .append("mediainfoid", getMediainfoid())
            .toString();
    }

}
