package com.owarchitects.forms.commons.db;

import java.io.Serializable;
import org.apache.commons.lang.builder.ToStringBuilder;


/** @author Hibernate CodeGenerator */
public class Reportstore implements Serializable {

    /** identifier field */
    private long reportstoreid;

    /** persistent field */
    private int contenttype;

    /** persistent field */
    private String reportcontent;

    /** persistent field */
    private com.owarchitects.forms.commons.db.Reportinfo reportinfo;

    /** full constructor */
    public Reportstore(int contenttype, String reportcontent, com.owarchitects.forms.commons.db.Reportinfo reportinfo) {
        this.contenttype = contenttype;
        this.reportcontent = reportcontent;
        this.reportinfo = reportinfo;
    }

    /** default constructor */
    public Reportstore() {
    }

    public long getReportstoreid() {
        return this.reportstoreid;
    }

    public void setReportstoreid(long reportstoreid) {
        this.reportstoreid = reportstoreid;
    }

    public int getContenttype() {
        return this.contenttype;
    }

    public void setContenttype(int contenttype) {
        this.contenttype = contenttype;
    }

    public String getReportcontent() {
        return this.reportcontent;
    }

    public void setReportcontent(String reportcontent) {
        this.reportcontent = reportcontent;
    }

    public com.owarchitects.forms.commons.db.Reportinfo getReportinfo() {
        return this.reportinfo;
    }

    public void setReportinfo(com.owarchitects.forms.commons.db.Reportinfo reportinfo) {
        this.reportinfo = reportinfo;
    }

    public String toString() {
        return new ToStringBuilder(this)
            .append("reportstoreid", getReportstoreid())
            .toString();
    }

}
