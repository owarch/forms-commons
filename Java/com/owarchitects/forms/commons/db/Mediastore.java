package com.owarchitects.forms.commons.db;

import java.io.Serializable;
import java.sql.Blob;
import org.apache.commons.lang.builder.ToStringBuilder;


/** @author Hibernate CodeGenerator */
public class Mediastore implements Serializable {

    /** identifier field */
    private long mediastoreid;

    /** persistent field */
    private Blob mediacontent;

    /** persistent field */
    private com.owarchitects.forms.commons.db.Mediainfo mediainfo;

    /** full constructor */
    public Mediastore(Blob mediacontent, com.owarchitects.forms.commons.db.Mediainfo mediainfo) {
        this.mediacontent = mediacontent;
        this.mediainfo = mediainfo;
    }

    /** default constructor */
    public Mediastore() {
    }

    public long getMediastoreid() {
        return this.mediastoreid;
    }

    public void setMediastoreid(long mediastoreid) {
        this.mediastoreid = mediastoreid;
    }

    public Blob getMediacontent() {
        return this.mediacontent;
    }

    public void setMediacontent(Blob mediacontent) {
        this.mediacontent = mediacontent;
    }

    public com.owarchitects.forms.commons.db.Mediainfo getMediainfo() {
        return this.mediainfo;
    }

    public void setMediainfo(com.owarchitects.forms.commons.db.Mediainfo mediainfo) {
        this.mediainfo = mediainfo;
    }

    public String toString() {
        return new ToStringBuilder(this)
            .append("mediastoreid", getMediastoreid())
            .toString();
    }

}
