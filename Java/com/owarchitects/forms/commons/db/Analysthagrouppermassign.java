package com.owarchitects.forms.commons.db;

import java.io.Serializable;
import java.util.Date;
import org.apache.commons.lang.builder.ToStringBuilder;


/** @author Hibernate CodeGenerator */
public class Analysthagrouppermassign implements Serializable {

    /** identifier field */
    private long ahagpid;

    /** persistent field */
    private long analysthaid;

    /** nullable persistent field */
    private Date recmodtime;

    /** persistent field */
    private Serializable permlevel;

    /** persistent field */
    private com.owarchitects.forms.commons.db.Analystgroups analystgroups;

    /** full constructor */
    public Analysthagrouppermassign(long analysthaid, Date recmodtime, Serializable permlevel, com.owarchitects.forms.commons.db.Analystgroups analystgroups) {
        this.analysthaid = analysthaid;
        this.recmodtime = recmodtime;
        this.permlevel = permlevel;
        this.analystgroups = analystgroups;
    }

    /** default constructor */
    public Analysthagrouppermassign() {
    }

    /** minimal constructor */
    public Analysthagrouppermassign(long analysthaid, Serializable permlevel, com.owarchitects.forms.commons.db.Analystgroups analystgroups) {
        this.analysthaid = analysthaid;
        this.permlevel = permlevel;
        this.analystgroups = analystgroups;
    }

    public long getAhagpid() {
        return this.ahagpid;
    }

    public void setAhagpid(long ahagpid) {
        this.ahagpid = ahagpid;
    }

    public long getAnalysthaid() {
        return this.analysthaid;
    }

    public void setAnalysthaid(long analysthaid) {
        this.analysthaid = analysthaid;
    }

    public Date getRecmodtime() {
        return this.recmodtime;
    }

    public void setRecmodtime(Date recmodtime) {
        this.recmodtime = recmodtime;
    }

    public Serializable getPermlevel() {
        return this.permlevel;
    }

    public void setPermlevel(Serializable permlevel) {
        this.permlevel = permlevel;
    }

    public com.owarchitects.forms.commons.db.Analystgroups getAnalystgroups() {
        return this.analystgroups;
    }

    public void setAnalystgroups(com.owarchitects.forms.commons.db.Analystgroups analystgroups) {
        this.analystgroups = analystgroups;
    }

    public String toString() {
        return new ToStringBuilder(this)
            .append("ahagpid", getAhagpid())
            .toString();
    }

}
