package com.owarchitects.forms.commons.db;

import java.io.Serializable;
import org.apache.commons.lang.builder.ToStringBuilder;


/** @author Hibernate CodeGenerator */
public class Reportstore_transfer implements Serializable {

    /** identifier field */
    private long reportstoreid;

    /** persistent field */
    private long reportinfoid;

    /** persistent field */
    private int contenttype;

    /** persistent field */
    private String reportcontent;

    /** full constructor */
    public Reportstore_transfer(long reportstoreid, long reportinfoid, int contenttype, String reportcontent) {
        this.reportstoreid = reportstoreid;
        this.reportinfoid = reportinfoid;
        this.contenttype = contenttype;
        this.reportcontent = reportcontent;
    }

    /** default constructor */
    public Reportstore_transfer() {
    }

    public long getReportstoreid() {
        return this.reportstoreid;
    }

    public void setReportstoreid(long reportstoreid) {
        this.reportstoreid = reportstoreid;
    }

    public long getReportinfoid() {
        return this.reportinfoid;
    }

    public void setReportinfoid(long reportinfoid) {
        this.reportinfoid = reportinfoid;
    }

    public int getContenttype() {
        return this.contenttype;
    }

    public void setContenttype(int contenttype) {
        this.contenttype = contenttype;
    }

    public String getReportcontent() {
        return this.reportcontent;
    }

    public void setReportcontent(String reportcontent) {
        this.reportcontent = reportcontent;
    }

    public String toString() {
        return new ToStringBuilder(this)
            .append("reportstoreid", getReportstoreid())
            .toString();
    }

}
