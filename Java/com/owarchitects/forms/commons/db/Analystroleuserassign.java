package com.owarchitects.forms.commons.db;

import java.io.Serializable;
import java.util.Date;
import org.apache.commons.lang.builder.ToStringBuilder;


/** @author Hibernate CodeGenerator */
public class Analystroleuserassign implements Serializable {

    /** identifier field */
    private long aruaid;

    /** nullable persistent field */
    private Date recmodtime;

    /** persistent field */
    private com.owarchitects.forms.commons.db.Analystroles role;

    /** persistent field */
    private com.owarchitects.forms.commons.db.Allusers user;

    /** full constructor */
    public Analystroleuserassign(Date recmodtime, com.owarchitects.forms.commons.db.Analystroles role, com.owarchitects.forms.commons.db.Allusers user) {
        this.recmodtime = recmodtime;
        this.role = role;
        this.user = user;
    }

    /** default constructor */
    public Analystroleuserassign() {
    }

    /** minimal constructor */
    public Analystroleuserassign(com.owarchitects.forms.commons.db.Analystroles role, com.owarchitects.forms.commons.db.Allusers user) {
        this.role = role;
        this.user = user;
    }

    public long getAruaid() {
        return this.aruaid;
    }

    public void setAruaid(long aruaid) {
        this.aruaid = aruaid;
    }

    public Date getRecmodtime() {
        return this.recmodtime;
    }

    public void setRecmodtime(Date recmodtime) {
        this.recmodtime = recmodtime;
    }

    public com.owarchitects.forms.commons.db.Analystroles getRole() {
        return this.role;
    }

    public void setRole(com.owarchitects.forms.commons.db.Analystroles role) {
        this.role = role;
    }

    public com.owarchitects.forms.commons.db.Allusers getUser() {
        return this.user;
    }

    public void setUser(com.owarchitects.forms.commons.db.Allusers user) {
        this.user = user;
    }

    public String toString() {
        return new ToStringBuilder(this)
            .append("aruaid", getAruaid())
            .toString();
    }

}
