package com.owarchitects.forms.commons.db;

import java.io.Serializable;
import java.util.Date;
import org.apache.commons.lang.builder.ToStringBuilder;


/** @author Hibernate CodeGenerator */
public class Formtypeuserpermassign_transfer implements Serializable {

    /** identifier field */
    private long dtupid;

    /** persistent field */
    private long formtypelistid;

    /** persistent field */
    private long auserid;

    /** persistent field */
    private Serializable permlevel;

    /** nullable persistent field */
    private Date recmodtime;

    /** full constructor */
    public Formtypeuserpermassign_transfer(long dtupid, long formtypelistid, long auserid, Serializable permlevel, Date recmodtime) {
        this.dtupid = dtupid;
        this.formtypelistid = formtypelistid;
        this.auserid = auserid;
        this.permlevel = permlevel;
        this.recmodtime = recmodtime;
    }

    /** default constructor */
    public Formtypeuserpermassign_transfer() {
    }

    /** minimal constructor */
    public Formtypeuserpermassign_transfer(long dtupid, long formtypelistid, long auserid, Serializable permlevel) {
        this.dtupid = dtupid;
        this.formtypelistid = formtypelistid;
        this.auserid = auserid;
        this.permlevel = permlevel;
    }

    public long getDtupid() {
        return this.dtupid;
    }

    public void setDtupid(long dtupid) {
        this.dtupid = dtupid;
    }

    public long getFormtypelistid() {
        return this.formtypelistid;
    }

    public void setFormtypelistid(long formtypelistid) {
        this.formtypelistid = formtypelistid;
    }

    public long getAuserid() {
        return this.auserid;
    }

    public void setAuserid(long auserid) {
        this.auserid = auserid;
    }

    public Serializable getPermlevel() {
        return this.permlevel;
    }

    public void setPermlevel(Serializable permlevel) {
        this.permlevel = permlevel;
    }

    public Date getRecmodtime() {
        return this.recmodtime;
    }

    public void setRecmodtime(Date recmodtime) {
        this.recmodtime = recmodtime;
    }

    public String toString() {
        return new ToStringBuilder(this)
            .append("dtupid", getDtupid())
            .toString();
    }

}
