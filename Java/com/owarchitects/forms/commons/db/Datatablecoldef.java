package com.owarchitects.forms.commons.db;

import java.io.Serializable;
import java.util.Date;
import org.apache.commons.lang.builder.ToStringBuilder;


/** @author Hibernate CodeGenerator */
public class Datatablecoldef implements Serializable {

    /** identifier field */
    private long dtcoldefid;

    /** persistent field */
    private String columnname;

    /** nullable persistent field */
    private Long columnorder;

    /** nullable persistent field */
    private Boolean iskeyfield;

    /** nullable persistent field */
    private Boolean isidfield;

    /** nullable persistent field */
    private Boolean isecode;

    /** nullable persistent field */
    private Date recmodtime;

    /** persistent field */
    private com.owarchitects.forms.commons.db.Datatabledef datatabledef;

    /** full constructor */
    public Datatablecoldef(String columnname, Long columnorder, Boolean iskeyfield, Boolean isidfield, Boolean isecode, Date recmodtime, com.owarchitects.forms.commons.db.Datatabledef datatabledef) {
        this.columnname = columnname;
        this.columnorder = columnorder;
        this.iskeyfield = iskeyfield;
        this.isidfield = isidfield;
        this.isecode = isecode;
        this.recmodtime = recmodtime;
        this.datatabledef = datatabledef;
    }

    /** default constructor */
    public Datatablecoldef() {
    }

    /** minimal constructor */
    public Datatablecoldef(String columnname, com.owarchitects.forms.commons.db.Datatabledef datatabledef) {
        this.columnname = columnname;
        this.datatabledef = datatabledef;
    }

    public long getDtcoldefid() {
        return this.dtcoldefid;
    }

    public void setDtcoldefid(long dtcoldefid) {
        this.dtcoldefid = dtcoldefid;
    }

    public String getColumnname() {
        return this.columnname;
    }

    public void setColumnname(String columnname) {
        this.columnname = columnname;
    }

    public Long getColumnorder() {
        return this.columnorder;
    }

    public void setColumnorder(Long columnorder) {
        this.columnorder = columnorder;
    }

    public Boolean getIskeyfield() {
        return this.iskeyfield;
    }

    public void setIskeyfield(Boolean iskeyfield) {
        this.iskeyfield = iskeyfield;
    }

    public Boolean getIsidfield() {
        return this.isidfield;
    }

    public void setIsidfield(Boolean isidfield) {
        this.isidfield = isidfield;
    }

    public Boolean getIsecode() {
        return this.isecode;
    }

    public void setIsecode(Boolean isecode) {
        this.isecode = isecode;
    }

    public Date getRecmodtime() {
        return this.recmodtime;
    }

    public void setRecmodtime(Date recmodtime) {
        this.recmodtime = recmodtime;
    }

    public com.owarchitects.forms.commons.db.Datatabledef getDatatabledef() {
        return this.datatabledef;
    }

    public void setDatatabledef(com.owarchitects.forms.commons.db.Datatabledef datatabledef) {
        this.datatabledef = datatabledef;
    }

    public String toString() {
        return new ToStringBuilder(this)
            .append("dtcoldefid", getDtcoldefid())
            .toString();
    }

}
