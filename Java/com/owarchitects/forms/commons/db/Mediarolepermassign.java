package com.owarchitects.forms.commons.db;

import java.io.Serializable;
import java.util.Date;
import org.apache.commons.lang.builder.ToStringBuilder;


/** @author Hibernate CodeGenerator */
public class Mediarolepermassign implements Serializable {

    /** identifier field */
    private long mrpid;

    /** persistent field */
    private long mediainfoid;

    /** nullable persistent field */
    private Date recmodtime;

    /** persistent field */
    private Serializable permlevel;

    /** persistent field */
    private com.owarchitects.forms.commons.db.Resourceroles resourceroles;

    /** full constructor */
    public Mediarolepermassign(long mediainfoid, Date recmodtime, Serializable permlevel, com.owarchitects.forms.commons.db.Resourceroles resourceroles) {
        this.mediainfoid = mediainfoid;
        this.recmodtime = recmodtime;
        this.permlevel = permlevel;
        this.resourceroles = resourceroles;
    }

    /** default constructor */
    public Mediarolepermassign() {
    }

    /** minimal constructor */
    public Mediarolepermassign(long mediainfoid, Serializable permlevel, com.owarchitects.forms.commons.db.Resourceroles resourceroles) {
        this.mediainfoid = mediainfoid;
        this.permlevel = permlevel;
        this.resourceroles = resourceroles;
    }

    public long getMrpid() {
        return this.mrpid;
    }

    public void setMrpid(long mrpid) {
        this.mrpid = mrpid;
    }

    public long getMediainfoid() {
        return this.mediainfoid;
    }

    public void setMediainfoid(long mediainfoid) {
        this.mediainfoid = mediainfoid;
    }

    public Date getRecmodtime() {
        return this.recmodtime;
    }

    public void setRecmodtime(Date recmodtime) {
        this.recmodtime = recmodtime;
    }

    public Serializable getPermlevel() {
        return this.permlevel;
    }

    public void setPermlevel(Serializable permlevel) {
        this.permlevel = permlevel;
    }

    public com.owarchitects.forms.commons.db.Resourceroles getResourceroles() {
        return this.resourceroles;
    }

    public void setResourceroles(com.owarchitects.forms.commons.db.Resourceroles resourceroles) {
        this.resourceroles = resourceroles;
    }

    public String toString() {
        return new ToStringBuilder(this)
            .append("mrpid", getMrpid())
            .toString();
    }

}
