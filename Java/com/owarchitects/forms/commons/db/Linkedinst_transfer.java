package com.owarchitects.forms.commons.db;

import java.io.Serializable;
import java.util.Date;
import org.apache.commons.lang.builder.ToStringBuilder;


/** @author Hibernate CodeGenerator */
public class Linkedinst_transfer implements Serializable {

    /** identifier field */
    private long linstid;

    /** persistent field */
    private String acronym;

    /** nullable persistent field */
    private String description;

    /** nullable persistent field */
    private String url;

    /** nullable persistent field */
    private String connectstring;

    /** nullable persistent field */
    private Boolean isdisabled;

    /** nullable persistent field */
    private Date recmodtime;

    /** full constructor */
    public Linkedinst_transfer(long linstid, String acronym, String description, String url, String connectstring, Boolean isdisabled, Date recmodtime) {
        this.linstid = linstid;
        this.acronym = acronym;
        this.description = description;
        this.url = url;
        this.connectstring = connectstring;
        this.isdisabled = isdisabled;
        this.recmodtime = recmodtime;
    }

    /** default constructor */
    public Linkedinst_transfer() {
    }

    /** minimal constructor */
    public Linkedinst_transfer(long linstid, String acronym) {
        this.linstid = linstid;
        this.acronym = acronym;
    }

    public long getLinstid() {
        return this.linstid;
    }

    public void setLinstid(long linstid) {
        this.linstid = linstid;
    }

    public String getAcronym() {
        return this.acronym;
    }

    public void setAcronym(String acronym) {
        this.acronym = acronym;
    }

    public String getDescription() {
        return this.description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getUrl() {
        return this.url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getConnectstring() {
        return this.connectstring;
    }

    public void setConnectstring(String connectstring) {
        this.connectstring = connectstring;
    }

    public Boolean getIsdisabled() {
        return this.isdisabled;
    }

    public void setIsdisabled(Boolean isdisabled) {
        this.isdisabled = isdisabled;
    }

    public Date getRecmodtime() {
        return this.recmodtime;
    }

    public void setRecmodtime(Date recmodtime) {
        this.recmodtime = recmodtime;
    }

    public String toString() {
        return new ToStringBuilder(this)
            .append("linstid", getLinstid())
            .toString();
    }

}
