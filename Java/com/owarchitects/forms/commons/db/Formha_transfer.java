package com.owarchitects.forms.commons.db;

import java.io.Serializable;
import java.util.Date;
import org.apache.commons.lang.builder.ToStringBuilder;


/** @author Hibernate CodeGenerator */
public class Formha_transfer implements Serializable {

    /** identifier field */
    private long formhaid;

    /** persistent field */
    private long studyid;

    /** persistent field */
    private int visibility;

    /** nullable persistent field */
    private Long pformhaid;

    /** nullable persistent field */
    private String hadesc;

    /** nullable persistent field */
    private Integer haordr;

    /** nullable persistent field */
    private Date recmodtime;

    /** full constructor */
    public Formha_transfer(long formhaid, long studyid, int visibility, Long pformhaid, String hadesc, Integer haordr, Date recmodtime) {
        this.formhaid = formhaid;
        this.studyid = studyid;
        this.visibility = visibility;
        this.pformhaid = pformhaid;
        this.hadesc = hadesc;
        this.haordr = haordr;
        this.recmodtime = recmodtime;
    }

    /** default constructor */
    public Formha_transfer() {
    }

    /** minimal constructor */
    public Formha_transfer(long formhaid, long studyid, int visibility) {
        this.formhaid = formhaid;
        this.studyid = studyid;
        this.visibility = visibility;
    }

    public long getFormhaid() {
        return this.formhaid;
    }

    public void setFormhaid(long formhaid) {
        this.formhaid = formhaid;
    }

    public long getStudyid() {
        return this.studyid;
    }

    public void setStudyid(long studyid) {
        this.studyid = studyid;
    }

    public int getVisibility() {
        return this.visibility;
    }

    public void setVisibility(int visibility) {
        this.visibility = visibility;
    }

    public Long getPformhaid() {
        return this.pformhaid;
    }

    public void setPformhaid(Long pformhaid) {
        this.pformhaid = pformhaid;
    }

    public String getHadesc() {
        return this.hadesc;
    }

    public void setHadesc(String hadesc) {
        this.hadesc = hadesc;
    }

    public Integer getHaordr() {
        return this.haordr;
    }

    public void setHaordr(Integer haordr) {
        this.haordr = haordr;
    }

    public Date getRecmodtime() {
        return this.recmodtime;
    }

    public void setRecmodtime(Date recmodtime) {
        this.recmodtime = recmodtime;
    }

    public String toString() {
        return new ToStringBuilder(this)
            .append("formhaid", getFormhaid())
            .toString();
    }

}
