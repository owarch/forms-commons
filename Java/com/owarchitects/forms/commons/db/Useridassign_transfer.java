package com.owarchitects.forms.commons.db;

import java.io.Serializable;
import org.apache.commons.lang.builder.ToStringBuilder;


/** @author Hibernate CodeGenerator */
public class Useridassign_transfer implements Serializable {

    /** identifier field */
    private long uiaid;

    /** persistent field */
    private long luserid;

    /** persistent field */
    private long studyid;

    /** persistent field */
    private String varname;

    /** nullable persistent field */
    private String varvalue;

    /** full constructor */
    public Useridassign_transfer(long uiaid, long luserid, long studyid, String varname, String varvalue) {
        this.uiaid = uiaid;
        this.luserid = luserid;
        this.studyid = studyid;
        this.varname = varname;
        this.varvalue = varvalue;
    }

    /** default constructor */
    public Useridassign_transfer() {
    }

    /** minimal constructor */
    public Useridassign_transfer(long uiaid, long luserid, long studyid, String varname) {
        this.uiaid = uiaid;
        this.luserid = luserid;
        this.studyid = studyid;
        this.varname = varname;
    }

    public long getUiaid() {
        return this.uiaid;
    }

    public void setUiaid(long uiaid) {
        this.uiaid = uiaid;
    }

    public long getLuserid() {
        return this.luserid;
    }

    public void setLuserid(long luserid) {
        this.luserid = luserid;
    }

    public long getStudyid() {
        return this.studyid;
    }

    public void setStudyid(long studyid) {
        this.studyid = studyid;
    }

    public String getVarname() {
        return this.varname;
    }

    public void setVarname(String varname) {
        this.varname = varname;
    }

    public String getVarvalue() {
        return this.varvalue;
    }

    public void setVarvalue(String varvalue) {
        this.varvalue = varvalue;
    }

    public String toString() {
        return new ToStringBuilder(this)
            .append("uiaid", getUiaid())
            .toString();
    }

}
