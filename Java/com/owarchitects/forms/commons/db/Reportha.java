package com.owarchitects.forms.commons.db;

import java.io.Serializable;
import org.apache.commons.lang.builder.ToStringBuilder;


/** @author Hibernate CodeGenerator */
public class Reportha implements Serializable {

    /** identifier field */
    private long reporthaid;

    /** persistent field */
    private int visibility;

    /** nullable persistent field */
    private Long preporthaid;

    /** persistent field */
    private int resourcetype;

    /** nullable persistent field */
    private String hadesc;

    /** nullable persistent field */
    private Integer haordr;

    /** persistent field */
    private com.owarchitects.forms.commons.db.Studies studies;

    /** full constructor */
    public Reportha(int visibility, Long preporthaid, int resourcetype, String hadesc, Integer haordr, com.owarchitects.forms.commons.db.Studies studies) {
        this.visibility = visibility;
        this.preporthaid = preporthaid;
        this.resourcetype = resourcetype;
        this.hadesc = hadesc;
        this.haordr = haordr;
        this.studies = studies;
    }

    /** default constructor */
    public Reportha() {
    }

    /** minimal constructor */
    public Reportha(int visibility, int resourcetype, com.owarchitects.forms.commons.db.Studies studies) {
        this.visibility = visibility;
        this.resourcetype = resourcetype;
        this.studies = studies;
    }

    public long getReporthaid() {
        return this.reporthaid;
    }

    public void setReporthaid(long reporthaid) {
        this.reporthaid = reporthaid;
    }

    public int getVisibility() {
        return this.visibility;
    }

    public void setVisibility(int visibility) {
        this.visibility = visibility;
    }

    public Long getPreporthaid() {
        return this.preporthaid;
    }

    public void setPreporthaid(Long preporthaid) {
        this.preporthaid = preporthaid;
    }

    public int getResourcetype() {
        return this.resourcetype;
    }

    public void setResourcetype(int resourcetype) {
        this.resourcetype = resourcetype;
    }

    public String getHadesc() {
        return this.hadesc;
    }

    public void setHadesc(String hadesc) {
        this.hadesc = hadesc;
    }

    public Integer getHaordr() {
        return this.haordr;
    }

    public void setHaordr(Integer haordr) {
        this.haordr = haordr;
    }

    public com.owarchitects.forms.commons.db.Studies getStudies() {
        return this.studies;
    }

    public void setStudies(com.owarchitects.forms.commons.db.Studies studies) {
        this.studies = studies;
    }

    public String toString() {
        return new ToStringBuilder(this)
            .append("reporthaid", getReporthaid())
            .toString();
    }

}
