package com.owarchitects.forms.commons.db;

import java.io.Serializable;
import java.util.Date;
import java.util.Set;
import org.apache.commons.lang.builder.ToStringBuilder;


/** @author Hibernate CodeGenerator */
public class Formtypelist implements Serializable {

    /** identifier field */
    private long formtypelistid;

    /** nullable persistent field */
    private Long remoteformtypelistid;

    /** persistent field */
    private int visibility;

    /** persistent field */
    private String formtype;

    /** nullable persistent field */
    private Long createuser;

    /** nullable persistent field */
    private Date recmodtime;

    /** persistent field */
    private com.owarchitects.forms.commons.db.Studies studies;

    /** persistent field */
    private com.owarchitects.forms.commons.db.Allinst allinst;

    /** persistent field */
    private Set formtypes;

    /** full constructor */
    public Formtypelist(Long remoteformtypelistid, int visibility, String formtype, Long createuser, Date recmodtime, com.owarchitects.forms.commons.db.Studies studies, com.owarchitects.forms.commons.db.Allinst allinst, Set formtypes) {
        this.remoteformtypelistid = remoteformtypelistid;
        this.visibility = visibility;
        this.formtype = formtype;
        this.createuser = createuser;
        this.recmodtime = recmodtime;
        this.studies = studies;
        this.allinst = allinst;
        this.formtypes = formtypes;
    }

    /** default constructor */
    public Formtypelist() {
    }

    /** minimal constructor */
    public Formtypelist(int visibility, String formtype, com.owarchitects.forms.commons.db.Studies studies, com.owarchitects.forms.commons.db.Allinst allinst, Set formtypes) {
        this.visibility = visibility;
        this.formtype = formtype;
        this.studies = studies;
        this.allinst = allinst;
        this.formtypes = formtypes;
    }

    public long getFormtypelistid() {
        return this.formtypelistid;
    }

    public void setFormtypelistid(long formtypelistid) {
        this.formtypelistid = formtypelistid;
    }

    public Long getRemoteformtypelistid() {
        return this.remoteformtypelistid;
    }

    public void setRemoteformtypelistid(Long remoteformtypelistid) {
        this.remoteformtypelistid = remoteformtypelistid;
    }

    public int getVisibility() {
        return this.visibility;
    }

    public void setVisibility(int visibility) {
        this.visibility = visibility;
    }

    public String getFormtype() {
        return this.formtype;
    }

    public void setFormtype(String formtype) {
        this.formtype = formtype;
    }

    public Long getCreateuser() {
        return this.createuser;
    }

    public void setCreateuser(Long createuser) {
        this.createuser = createuser;
    }

    public Date getRecmodtime() {
        return this.recmodtime;
    }

    public void setRecmodtime(Date recmodtime) {
        this.recmodtime = recmodtime;
    }

    public com.owarchitects.forms.commons.db.Studies getStudies() {
        return this.studies;
    }

    public void setStudies(com.owarchitects.forms.commons.db.Studies studies) {
        this.studies = studies;
    }

    public com.owarchitects.forms.commons.db.Allinst getAllinst() {
        return this.allinst;
    }

    public void setAllinst(com.owarchitects.forms.commons.db.Allinst allinst) {
        this.allinst = allinst;
    }

    public Set getFormtypes() {
        return this.formtypes;
    }

    public void setFormtypes(Set formtypes) {
        this.formtypes = formtypes;
    }

    public String toString() {
        return new ToStringBuilder(this)
            .append("formtypelistid", getFormtypelistid())
            .toString();
    }

}
