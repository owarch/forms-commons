package com.owarchitects.forms.commons.db;

import java.io.Serializable;
import java.util.Date;
import org.apache.commons.lang.builder.ToStringBuilder;


/** @author Hibernate CodeGenerator */
public class Sites_transfer implements Serializable {

    /** identifier field */
    private long siteid;

    /** persistent field */
    private String siteno;

    /** nullable persistent field */
    private String sitedesc;

    /** nullable persistent field */
    private Date recmodtime;

    /** full constructor */
    public Sites_transfer(long siteid, String siteno, String sitedesc, Date recmodtime) {
        this.siteid = siteid;
        this.siteno = siteno;
        this.sitedesc = sitedesc;
        this.recmodtime = recmodtime;
    }

    /** default constructor */
    public Sites_transfer() {
    }

    /** minimal constructor */
    public Sites_transfer(long siteid, String siteno) {
        this.siteid = siteid;
        this.siteno = siteno;
    }

    public long getSiteid() {
        return this.siteid;
    }

    public void setSiteid(long siteid) {
        this.siteid = siteid;
    }

    public String getSiteno() {
        return this.siteno;
    }

    public void setSiteno(String siteno) {
        this.siteno = siteno;
    }

    public String getSitedesc() {
        return this.sitedesc;
    }

    public void setSitedesc(String sitedesc) {
        this.sitedesc = sitedesc;
    }

    public Date getRecmodtime() {
        return this.recmodtime;
    }

    public void setRecmodtime(Date recmodtime) {
        this.recmodtime = recmodtime;
    }

    public String toString() {
        return new ToStringBuilder(this)
            .append("siteid", getSiteid())
            .toString();
    }

}
