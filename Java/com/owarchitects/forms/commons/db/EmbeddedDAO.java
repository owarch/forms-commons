 
/**
 *
 * This program is part of:
 *
 *    FORMS [Forms-Oriented Research Management System]
 *
 * Copyright (C) 2009 OpenWare Architects.  All Rights Reserved.
 *
 * FORMS is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * FORMS is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with FORMS.  If not, see <http://www.gnu.org/licenses/>.
 *
 */
 
package com.owarchitects.forms.commons.db;

/**
 *  EmbeddedDAO.java  -  FORMSEmbeddedDB Data Access Object with convenience methods
 */

//import java.util.*;
//import org.hibernate.*;
//import java.lang.annotation.*;

//import java.sql.SQLException;
//import java.util.ArrayList;
//import java.util.Iterator;
//import java.util.List;

//import org.hibernate.Hibernate;
//import org.hibernate.HibernateException;
//import org.hibernate.Query;
//import org.hibernate.Session;

//import org.springframework.orm.hibernate3.HibernateCallback;
//import org.springframework.orm.hibernate3.support.HibernateDaoSupport;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
//import org.springframework.context.*;
//import org.springframework.context.support.*;
//import org.springframework.core.io.*;
import org.springframework.orm.hibernate3.*;
//import org.springframework.orm.hibernate3.support.HibernateDaoSupport;
//import org.springframework.transaction.support.TransactionSynchronizationManager;

@Repository
//@Transactional
public class EmbeddedDAO extends FORMSDao {

}

