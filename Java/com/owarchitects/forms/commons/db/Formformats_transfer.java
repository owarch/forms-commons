package com.owarchitects.forms.commons.db;

import java.io.Serializable;
import java.util.Date;
import org.apache.commons.lang.builder.ToStringBuilder;


/** @author Hibernate CodeGenerator */
public class Formformats_transfer implements Serializable {

    /** identifier field */
    private long formformatid;

    /** nullable persistent field */
    private Date recmodtime;

    /** persistent field */
    private long fileformatid;

    /** full constructor */
    public Formformats_transfer(long formformatid, Date recmodtime, long fileformatid) {
        this.formformatid = formformatid;
        this.recmodtime = recmodtime;
        this.fileformatid = fileformatid;
    }

    /** default constructor */
    public Formformats_transfer() {
    }

    /** minimal constructor */
    public Formformats_transfer(long formformatid, long fileformatid) {
        this.formformatid = formformatid;
        this.fileformatid = fileformatid;
    }

    public long getFormformatid() {
        return this.formformatid;
    }

    public void setFormformatid(long formformatid) {
        this.formformatid = formformatid;
    }

    public Date getRecmodtime() {
        return this.recmodtime;
    }

    public void setRecmodtime(Date recmodtime) {
        this.recmodtime = recmodtime;
    }

    public long getFileformatid() {
        return this.fileformatid;
    }

    public void setFileformatid(long fileformatid) {
        this.fileformatid = fileformatid;
    }

    public String toString() {
        return new ToStringBuilder(this)
            .append("formformatid", getFormformatid())
            .toString();
    }

}
