package com.owarchitects.forms.commons.db;

import java.io.Serializable;
import java.util.Date;
import org.apache.commons.lang.builder.ToStringBuilder;


/** @author Hibernate CodeGenerator */
public class Reportrolepermassign_transfer implements Serializable {

    /** identifier field */
    private long rrpid;

    /** persistent field */
    private long reportinfoid;

    /** nullable persistent field */
    private Date recmodtime;

    /** persistent field */
    private long rroleid;

    /** persistent field */
    private Serializable permlevel;

    /** full constructor */
    public Reportrolepermassign_transfer(long rrpid, long reportinfoid, Date recmodtime, long rroleid, Serializable permlevel) {
        this.rrpid = rrpid;
        this.reportinfoid = reportinfoid;
        this.recmodtime = recmodtime;
        this.rroleid = rroleid;
        this.permlevel = permlevel;
    }

    /** default constructor */
    public Reportrolepermassign_transfer() {
    }

    /** minimal constructor */
    public Reportrolepermassign_transfer(long rrpid, long reportinfoid, long rroleid, Serializable permlevel) {
        this.rrpid = rrpid;
        this.reportinfoid = reportinfoid;
        this.rroleid = rroleid;
        this.permlevel = permlevel;
    }

    public long getRrpid() {
        return this.rrpid;
    }

    public void setRrpid(long rrpid) {
        this.rrpid = rrpid;
    }

    public long getReportinfoid() {
        return this.reportinfoid;
    }

    public void setReportinfoid(long reportinfoid) {
        this.reportinfoid = reportinfoid;
    }

    public Date getRecmodtime() {
        return this.recmodtime;
    }

    public void setRecmodtime(Date recmodtime) {
        this.recmodtime = recmodtime;
    }

    public long getRroleid() {
        return this.rroleid;
    }

    public void setRroleid(long rroleid) {
        this.rroleid = rroleid;
    }

    public Serializable getPermlevel() {
        return this.permlevel;
    }

    public void setPermlevel(Serializable permlevel) {
        this.permlevel = permlevel;
    }

    public String toString() {
        return new ToStringBuilder(this)
            .append("rrpid", getRrpid())
            .toString();
    }

}
