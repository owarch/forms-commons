package com.owarchitects.forms.commons.db;

import java.io.Serializable;
import java.util.Date;
import org.apache.commons.lang.builder.ToStringBuilder;


/** @author Hibernate CodeGenerator */
public class Analystuserpermassign implements Serializable {

    /** identifier field */
    private long aupid;

    /** persistent field */
    private long analystinfoid;

    /** persistent field */
    private Serializable permlevel;

    /** nullable persistent field */
    private Date recmodtime;

    /** persistent field */
    private com.owarchitects.forms.commons.db.Allusers allusers;

    /** full constructor */
    public Analystuserpermassign(long analystinfoid, Serializable permlevel, Date recmodtime, com.owarchitects.forms.commons.db.Allusers allusers) {
        this.analystinfoid = analystinfoid;
        this.permlevel = permlevel;
        this.recmodtime = recmodtime;
        this.allusers = allusers;
    }

    /** default constructor */
    public Analystuserpermassign() {
    }

    /** minimal constructor */
    public Analystuserpermassign(long analystinfoid, Serializable permlevel, com.owarchitects.forms.commons.db.Allusers allusers) {
        this.analystinfoid = analystinfoid;
        this.permlevel = permlevel;
        this.allusers = allusers;
    }

    public long getAupid() {
        return this.aupid;
    }

    public void setAupid(long aupid) {
        this.aupid = aupid;
    }

    public long getAnalystinfoid() {
        return this.analystinfoid;
    }

    public void setAnalystinfoid(long analystinfoid) {
        this.analystinfoid = analystinfoid;
    }

    public Serializable getPermlevel() {
        return this.permlevel;
    }

    public void setPermlevel(Serializable permlevel) {
        this.permlevel = permlevel;
    }

    public Date getRecmodtime() {
        return this.recmodtime;
    }

    public void setRecmodtime(Date recmodtime) {
        this.recmodtime = recmodtime;
    }

    public com.owarchitects.forms.commons.db.Allusers getAllusers() {
        return this.allusers;
    }

    public void setAllusers(com.owarchitects.forms.commons.db.Allusers allusers) {
        this.allusers = allusers;
    }

    public String toString() {
        return new ToStringBuilder(this)
            .append("aupid", getAupid())
            .toString();
    }

}
