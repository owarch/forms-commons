package com.owarchitects.forms.commons.db;

import java.io.Serializable;
import java.util.Date;
import org.apache.commons.lang.builder.ToStringBuilder;


/** @author Hibernate CodeGenerator */
public class Reportrolepermprofassign implements Serializable {

    /** identifier field */
    private long rrppaid;

    /** persistent field */
    private Serializable permlevel;

    /** nullable persistent field */
    private Date recmodtime;

    /** persistent field */
    private com.owarchitects.forms.commons.db.Reportpermprofiles reportpermprofiles;

    /** persistent field */
    private com.owarchitects.forms.commons.db.Resourceroles resourceroles;

    /** full constructor */
    public Reportrolepermprofassign(Serializable permlevel, Date recmodtime, com.owarchitects.forms.commons.db.Reportpermprofiles reportpermprofiles, com.owarchitects.forms.commons.db.Resourceroles resourceroles) {
        this.permlevel = permlevel;
        this.recmodtime = recmodtime;
        this.reportpermprofiles = reportpermprofiles;
        this.resourceroles = resourceroles;
    }

    /** default constructor */
    public Reportrolepermprofassign() {
    }

    /** minimal constructor */
    public Reportrolepermprofassign(Serializable permlevel, com.owarchitects.forms.commons.db.Reportpermprofiles reportpermprofiles, com.owarchitects.forms.commons.db.Resourceroles resourceroles) {
        this.permlevel = permlevel;
        this.reportpermprofiles = reportpermprofiles;
        this.resourceroles = resourceroles;
    }

    public long getRrppaid() {
        return this.rrppaid;
    }

    public void setRrppaid(long rrppaid) {
        this.rrppaid = rrppaid;
    }

    public Serializable getPermlevel() {
        return this.permlevel;
    }

    public void setPermlevel(Serializable permlevel) {
        this.permlevel = permlevel;
    }

    public Date getRecmodtime() {
        return this.recmodtime;
    }

    public void setRecmodtime(Date recmodtime) {
        this.recmodtime = recmodtime;
    }

    public com.owarchitects.forms.commons.db.Reportpermprofiles getReportpermprofiles() {
        return this.reportpermprofiles;
    }

    public void setReportpermprofiles(com.owarchitects.forms.commons.db.Reportpermprofiles reportpermprofiles) {
        this.reportpermprofiles = reportpermprofiles;
    }

    public com.owarchitects.forms.commons.db.Resourceroles getResourceroles() {
        return this.resourceroles;
    }

    public void setResourceroles(com.owarchitects.forms.commons.db.Resourceroles resourceroles) {
        this.resourceroles = resourceroles;
    }

    public String toString() {
        return new ToStringBuilder(this)
            .append("rrppaid", getRrppaid())
            .toString();
    }

}
