package com.owarchitects.forms.commons.db;

import java.io.Serializable;
import java.util.Date;
import org.apache.commons.lang.builder.ToStringBuilder;


/** @author Hibernate CodeGenerator */
public class Mediainfo_transfer implements Serializable {

    /** identifier field */
    private long mediainfoid;

    /** persistent field */
    private long ainstid;

    /** nullable persistent field */
    private Long remotemediainfoid;

    /** persistent field */
    private long pmediahaid;

    /** nullable persistent field */
    private String filename;

    /** nullable persistent field */
    private Long filesize;

    /** nullable persistent field */
    private String description;

    /** nullable persistent field */
    private String creationdate;

    /** nullable persistent field */
    private String notes;

    /** nullable persistent field */
    private Date uploadtime;

    /** nullable persistent field */
    private Long uploaduser;

    /** persistent field */
    private long fileformatid;

    /** full constructor */
    public Mediainfo_transfer(long mediainfoid, long ainstid, Long remotemediainfoid, long pmediahaid, String filename, Long filesize, String description, String creationdate, String notes, Date uploadtime, Long uploaduser, long fileformatid) {
        this.mediainfoid = mediainfoid;
        this.ainstid = ainstid;
        this.remotemediainfoid = remotemediainfoid;
        this.pmediahaid = pmediahaid;
        this.filename = filename;
        this.filesize = filesize;
        this.description = description;
        this.creationdate = creationdate;
        this.notes = notes;
        this.uploadtime = uploadtime;
        this.uploaduser = uploaduser;
        this.fileformatid = fileformatid;
    }

    /** default constructor */
    public Mediainfo_transfer() {
    }

    /** minimal constructor */
    public Mediainfo_transfer(long mediainfoid, long ainstid, long pmediahaid, long fileformatid) {
        this.mediainfoid = mediainfoid;
        this.ainstid = ainstid;
        this.pmediahaid = pmediahaid;
        this.fileformatid = fileformatid;
    }

    public long getMediainfoid() {
        return this.mediainfoid;
    }

    public void setMediainfoid(long mediainfoid) {
        this.mediainfoid = mediainfoid;
    }

    public long getAinstid() {
        return this.ainstid;
    }

    public void setAinstid(long ainstid) {
        this.ainstid = ainstid;
    }

    public Long getRemotemediainfoid() {
        return this.remotemediainfoid;
    }

    public void setRemotemediainfoid(Long remotemediainfoid) {
        this.remotemediainfoid = remotemediainfoid;
    }

    public long getPmediahaid() {
        return this.pmediahaid;
    }

    public void setPmediahaid(long pmediahaid) {
        this.pmediahaid = pmediahaid;
    }

    public String getFilename() {
        return this.filename;
    }

    public void setFilename(String filename) {
        this.filename = filename;
    }

    public Long getFilesize() {
        return this.filesize;
    }

    public void setFilesize(Long filesize) {
        this.filesize = filesize;
    }

    public String getDescription() {
        return this.description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getCreationdate() {
        return this.creationdate;
    }

    public void setCreationdate(String creationdate) {
        this.creationdate = creationdate;
    }

    public String getNotes() {
        return this.notes;
    }

    public void setNotes(String notes) {
        this.notes = notes;
    }

    public Date getUploadtime() {
        return this.uploadtime;
    }

    public void setUploadtime(Date uploadtime) {
        this.uploadtime = uploadtime;
    }

    public Long getUploaduser() {
        return this.uploaduser;
    }

    public void setUploaduser(Long uploaduser) {
        this.uploaduser = uploaduser;
    }

    public long getFileformatid() {
        return this.fileformatid;
    }

    public void setFileformatid(long fileformatid) {
        this.fileformatid = fileformatid;
    }

    public String toString() {
        return new ToStringBuilder(this)
            .append("mediainfoid", getMediainfoid())
            .toString();
    }

}
