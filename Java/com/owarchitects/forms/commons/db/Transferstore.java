 
/**
 *
 * This program is part of:
 *
 *    FORMS [Forms-Oriented Research Management System]
 *
 * Copyright (C) 2009 OpenWare Architects.  All Rights Reserved.
 *
 * FORMS is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * FORMS is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with FORMS.  If not, see <http://www.gnu.org/licenses/>.
 *
 */
package com.owarchitects.forms.commons.db;

import java.io.Serializable;
import org.apache.commons.lang.builder.ToStringBuilder;


/** @author Hibernate CodeGenerator */
public class Transferstore implements Serializable {

    /** identifier field */
    private long mediastoreid;

    /** persistent field */
    private String mediacontent;

    /** persistent field */
    private com.owarchitects.forms.commons.db.Mediainfo mediainfo;

    /** full constructor */
    public Transferstore(String mediacontent, com.owarchitects.forms.commons.db.Mediainfo mediainfo) {
        this.mediacontent = mediacontent;
        this.mediainfo = mediainfo;
    }

    /** default constructor */
    public Transferstore() {
    }

    public long getMediastoreid() {
        return this.mediastoreid;
    }

    public void setMediastoreid(long mediastoreid) {
        this.mediastoreid = mediastoreid;
    }

    public String getMediacontent() {
        return this.mediacontent;
    }

    public void setMediacontent(String mediacontent) {
        this.mediacontent = mediacontent;
    }

    public com.owarchitects.forms.commons.db.Mediainfo getMediainfo() {
        return this.mediainfo;
    }

    public void setMediainfo(com.owarchitects.forms.commons.db.Mediainfo mediainfo) {
        this.mediainfo = mediainfo;
    }

    public String toString() {
        return new ToStringBuilder(this)
            .append("mediastoreid", getMediastoreid())
            .toString();
    }

}
