package com.owarchitects.forms.commons.db;

import java.io.Serializable;
import java.util.Date;
import org.apache.commons.lang.builder.ToStringBuilder;


/** @author Hibernate CodeGenerator */
public class Fileextensions implements Serializable {

    /** identifier field */
    private long fileextid;

    /** persistent field */
    private String extension;

    /** nullable persistent field */
    private Date recmodtime;

    /** persistent field */
    private com.owarchitects.forms.commons.db.Supportedfileformats supportedfileformats;

    /** full constructor */
    public Fileextensions(String extension, Date recmodtime, com.owarchitects.forms.commons.db.Supportedfileformats supportedfileformats) {
        this.extension = extension;
        this.recmodtime = recmodtime;
        this.supportedfileformats = supportedfileformats;
    }

    /** default constructor */
    public Fileextensions() {
    }

    /** minimal constructor */
    public Fileextensions(String extension, com.owarchitects.forms.commons.db.Supportedfileformats supportedfileformats) {
        this.extension = extension;
        this.supportedfileformats = supportedfileformats;
    }

    public long getFileextid() {
        return this.fileextid;
    }

    public void setFileextid(long fileextid) {
        this.fileextid = fileextid;
    }

    public String getExtension() {
        return this.extension;
    }

    public void setExtension(String extension) {
        this.extension = extension;
    }

    public Date getRecmodtime() {
        return this.recmodtime;
    }

    public void setRecmodtime(Date recmodtime) {
        this.recmodtime = recmodtime;
    }

    public com.owarchitects.forms.commons.db.Supportedfileformats getSupportedfileformats() {
        return this.supportedfileformats;
    }

    public void setSupportedfileformats(com.owarchitects.forms.commons.db.Supportedfileformats supportedfileformats) {
        this.supportedfileformats = supportedfileformats;
    }

    public String toString() {
        return new ToStringBuilder(this)
            .append("fileextid", getFileextid())
            .toString();
    }

}
