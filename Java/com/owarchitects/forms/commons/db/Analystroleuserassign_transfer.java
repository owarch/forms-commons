package com.owarchitects.forms.commons.db;

import java.io.Serializable;
import java.util.Date;
import org.apache.commons.lang.builder.ToStringBuilder;


/** @author Hibernate CodeGenerator */
public class Analystroleuserassign_transfer implements Serializable {

    /** identifier field */
    private long aruaid;

    /** persistent field */
    private long aroleid;

    /** persistent field */
    private long auserid;

    /** nullable persistent field */
    private Date recmodtime;

    /** full constructor */
    public Analystroleuserassign_transfer(long aruaid, long aroleid, long auserid, Date recmodtime) {
        this.aruaid = aruaid;
        this.aroleid = aroleid;
        this.auserid = auserid;
        this.recmodtime = recmodtime;
    }

    /** default constructor */
    public Analystroleuserassign_transfer() {
    }

    /** minimal constructor */
    public Analystroleuserassign_transfer(long aruaid, long aroleid, long auserid) {
        this.aruaid = aruaid;
        this.aroleid = aroleid;
        this.auserid = auserid;
    }

    public long getAruaid() {
        return this.aruaid;
    }

    public void setAruaid(long aruaid) {
        this.aruaid = aruaid;
    }

    public long getAroleid() {
        return this.aroleid;
    }

    public void setAroleid(long aroleid) {
        this.aroleid = aroleid;
    }

    public long getAuserid() {
        return this.auserid;
    }

    public void setAuserid(long auserid) {
        this.auserid = auserid;
    }

    public Date getRecmodtime() {
        return this.recmodtime;
    }

    public void setRecmodtime(Date recmodtime) {
        this.recmodtime = recmodtime;
    }

    public String toString() {
        return new ToStringBuilder(this)
            .append("aruaid", getAruaid())
            .toString();
    }

}
