package com.owarchitects.forms.commons.db;

import java.io.Serializable;
import java.util.Date;
import org.apache.commons.lang.builder.ToStringBuilder;


/** @author Hibernate CodeGenerator */
public class Formtypelist_transfer implements Serializable {

    /** identifier field */
    private long formtypelistid;

    /** nullable persistent field */
    private Long studyid;

    /** nullable persistent field */
    private Long ainstid;

    /** nullable persistent field */
    private Long remoteformtypelistid;

    /** persistent field */
    private int visibility;

    /** persistent field */
    private String formtype;

    /** nullable persistent field */
    private Long createuser;

    /** nullable persistent field */
    private Date recmodtime;

    /** full constructor */
    public Formtypelist_transfer(long formtypelistid, Long studyid, Long ainstid, Long remoteformtypelistid, int visibility, String formtype, Long createuser, Date recmodtime) {
        this.formtypelistid = formtypelistid;
        this.studyid = studyid;
        this.ainstid = ainstid;
        this.remoteformtypelistid = remoteformtypelistid;
        this.visibility = visibility;
        this.formtype = formtype;
        this.createuser = createuser;
        this.recmodtime = recmodtime;
    }

    /** default constructor */
    public Formtypelist_transfer() {
    }

    /** minimal constructor */
    public Formtypelist_transfer(long formtypelistid, int visibility, String formtype) {
        this.formtypelistid = formtypelistid;
        this.visibility = visibility;
        this.formtype = formtype;
    }

    public long getFormtypelistid() {
        return this.formtypelistid;
    }

    public void setFormtypelistid(long formtypelistid) {
        this.formtypelistid = formtypelistid;
    }

    public Long getStudyid() {
        return this.studyid;
    }

    public void setStudyid(Long studyid) {
        this.studyid = studyid;
    }

    public Long getAinstid() {
        return this.ainstid;
    }

    public void setAinstid(Long ainstid) {
        this.ainstid = ainstid;
    }

    public Long getRemoteformtypelistid() {
        return this.remoteformtypelistid;
    }

    public void setRemoteformtypelistid(Long remoteformtypelistid) {
        this.remoteformtypelistid = remoteformtypelistid;
    }

    public int getVisibility() {
        return this.visibility;
    }

    public void setVisibility(int visibility) {
        this.visibility = visibility;
    }

    public String getFormtype() {
        return this.formtype;
    }

    public void setFormtype(String formtype) {
        this.formtype = formtype;
    }

    public Long getCreateuser() {
        return this.createuser;
    }

    public void setCreateuser(Long createuser) {
        this.createuser = createuser;
    }

    public Date getRecmodtime() {
        return this.recmodtime;
    }

    public void setRecmodtime(Date recmodtime) {
        this.recmodtime = recmodtime;
    }

    public String toString() {
        return new ToStringBuilder(this)
            .append("formtypelistid", getFormtypelistid())
            .toString();
    }

}
