package com.owarchitects.forms.commons.db;

import java.io.Serializable;
import java.util.Date;
import org.apache.commons.lang.builder.ToStringBuilder;


/** @author Hibernate CodeGenerator */
public class Fileextensions_transfer implements Serializable {

    /** identifier field */
    private long fileextid;

    /** persistent field */
    private String extension;

    /** nullable persistent field */
    private Date recmodtime;

    /** nullable persistent field */
    private Long fileformatid;

    /** full constructor */
    public Fileextensions_transfer(long fileextid, String extension, Date recmodtime, Long fileformatid) {
        this.fileextid = fileextid;
        this.extension = extension;
        this.recmodtime = recmodtime;
        this.fileformatid = fileformatid;
    }

    /** default constructor */
    public Fileextensions_transfer() {
    }

    /** minimal constructor */
    public Fileextensions_transfer(long fileextid, String extension) {
        this.fileextid = fileextid;
        this.extension = extension;
    }

    public long getFileextid() {
        return this.fileextid;
    }

    public void setFileextid(long fileextid) {
        this.fileextid = fileextid;
    }

    public String getExtension() {
        return this.extension;
    }

    public void setExtension(String extension) {
        this.extension = extension;
    }

    public Date getRecmodtime() {
        return this.recmodtime;
    }

    public void setRecmodtime(Date recmodtime) {
        this.recmodtime = recmodtime;
    }

    public Long getFileformatid() {
        return this.fileformatid;
    }

    public void setFileformatid(Long fileformatid) {
        this.fileformatid = fileformatid;
    }

    public String toString() {
        return new ToStringBuilder(this)
            .append("fileextid", getFileextid())
            .toString();
    }

}
