package com.owarchitects.forms.commons.db;

import java.io.Serializable;
import java.util.Date;
import org.apache.commons.lang.builder.ToStringBuilder;


/** @author Hibernate CodeGenerator */
public class Sysuserpermassign implements Serializable {

    /** identifier field */
    private long supid;

    /** nullable persistent field */
    private Long siteid;

    /** nullable persistent field */
    private Long studyid;

    /** persistent field */
    private Serializable permlevel;

    /** nullable persistent field */
    private Date recmodtime;

    /** persistent field */
    private com.owarchitects.forms.commons.db.Allusers user;

    /** persistent field */
    private com.owarchitects.forms.commons.db.Permissioncats permissioncats;

    /** full constructor */
    public Sysuserpermassign(Long siteid, Long studyid, Serializable permlevel, Date recmodtime, com.owarchitects.forms.commons.db.Allusers user, com.owarchitects.forms.commons.db.Permissioncats permissioncats) {
        this.siteid = siteid;
        this.studyid = studyid;
        this.permlevel = permlevel;
        this.recmodtime = recmodtime;
        this.user = user;
        this.permissioncats = permissioncats;
    }

    /** default constructor */
    public Sysuserpermassign() {
    }

    /** minimal constructor */
    public Sysuserpermassign(Serializable permlevel, com.owarchitects.forms.commons.db.Allusers user, com.owarchitects.forms.commons.db.Permissioncats permissioncats) {
        this.permlevel = permlevel;
        this.user = user;
        this.permissioncats = permissioncats;
    }

    public long getSupid() {
        return this.supid;
    }

    public void setSupid(long supid) {
        this.supid = supid;
    }

    public Long getSiteid() {
        return this.siteid;
    }

    public void setSiteid(Long siteid) {
        this.siteid = siteid;
    }

    public Long getStudyid() {
        return this.studyid;
    }

    public void setStudyid(Long studyid) {
        this.studyid = studyid;
    }

    public Serializable getPermlevel() {
        return this.permlevel;
    }

    public void setPermlevel(Serializable permlevel) {
        this.permlevel = permlevel;
    }

    public Date getRecmodtime() {
        return this.recmodtime;
    }

    public void setRecmodtime(Date recmodtime) {
        this.recmodtime = recmodtime;
    }

    public com.owarchitects.forms.commons.db.Allusers getUser() {
        return this.user;
    }

    public void setUser(com.owarchitects.forms.commons.db.Allusers user) {
        this.user = user;
    }

    public com.owarchitects.forms.commons.db.Permissioncats getPermissioncats() {
        return this.permissioncats;
    }

    public void setPermissioncats(com.owarchitects.forms.commons.db.Permissioncats permissioncats) {
        this.permissioncats = permissioncats;
    }

    public String toString() {
        return new ToStringBuilder(this)
            .append("supid", getSupid())
            .toString();
    }

}
