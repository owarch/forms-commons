package com.owarchitects.forms.commons.db;

import java.io.Serializable;
import java.util.Date;
import org.apache.commons.lang.builder.ToStringBuilder;


/** @author Hibernate CodeGenerator */
public class Dtfieldattrdefaults implements Serializable {

    /** identifier field */
    private long dtfattrdid;

    /** persistent field */
    private int visibility;

    /** nullable persistent field */
    private String fieldname;

    /** nullable persistent field */
    private String attr;

    /** nullable persistent field */
    private String value;

    /** nullable persistent field */
    private Date recmodtime;

    /** persistent field */
    private com.owarchitects.forms.commons.db.Studies studies;

    /** full constructor */
    public Dtfieldattrdefaults(int visibility, String fieldname, String attr, String value, Date recmodtime, com.owarchitects.forms.commons.db.Studies studies) {
        this.visibility = visibility;
        this.fieldname = fieldname;
        this.attr = attr;
        this.value = value;
        this.recmodtime = recmodtime;
        this.studies = studies;
    }

    /** default constructor */
    public Dtfieldattrdefaults() {
    }

    /** minimal constructor */
    public Dtfieldattrdefaults(int visibility, com.owarchitects.forms.commons.db.Studies studies) {
        this.visibility = visibility;
        this.studies = studies;
    }

    public long getDtfattrdid() {
        return this.dtfattrdid;
    }

    public void setDtfattrdid(long dtfattrdid) {
        this.dtfattrdid = dtfattrdid;
    }

    public int getVisibility() {
        return this.visibility;
    }

    public void setVisibility(int visibility) {
        this.visibility = visibility;
    }

    public String getFieldname() {
        return this.fieldname;
    }

    public void setFieldname(String fieldname) {
        this.fieldname = fieldname;
    }

    public String getAttr() {
        return this.attr;
    }

    public void setAttr(String attr) {
        this.attr = attr;
    }

    public String getValue() {
        return this.value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public Date getRecmodtime() {
        return this.recmodtime;
    }

    public void setRecmodtime(Date recmodtime) {
        this.recmodtime = recmodtime;
    }

    public com.owarchitects.forms.commons.db.Studies getStudies() {
        return this.studies;
    }

    public void setStudies(com.owarchitects.forms.commons.db.Studies studies) {
        this.studies = studies;
    }

    public String toString() {
        return new ToStringBuilder(this)
            .append("dtfattrdid", getDtfattrdid())
            .toString();
    }

}
