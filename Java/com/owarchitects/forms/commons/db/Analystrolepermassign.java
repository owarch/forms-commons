package com.owarchitects.forms.commons.db;

import java.io.Serializable;
import java.util.Date;
import org.apache.commons.lang.builder.ToStringBuilder;


/** @author Hibernate CodeGenerator */
public class Analystrolepermassign implements Serializable {

    /** identifier field */
    private long arpid;

    /** nullable persistent field */
    private Date recmodtime;

    /** persistent field */
    private Serializable permlevel;

    /** persistent field */
    private com.owarchitects.forms.commons.db.Analystroles analystroles;

    /** full constructor */
    public Analystrolepermassign(Date recmodtime, Serializable permlevel, com.owarchitects.forms.commons.db.Analystroles analystroles) {
        this.recmodtime = recmodtime;
        this.permlevel = permlevel;
        this.analystroles = analystroles;
    }

    /** default constructor */
    public Analystrolepermassign() {
    }

    /** minimal constructor */
    public Analystrolepermassign(Serializable permlevel, com.owarchitects.forms.commons.db.Analystroles analystroles) {
        this.permlevel = permlevel;
        this.analystroles = analystroles;
    }

    public long getArpid() {
        return this.arpid;
    }

    public void setArpid(long arpid) {
        this.arpid = arpid;
    }

    public Date getRecmodtime() {
        return this.recmodtime;
    }

    public void setRecmodtime(Date recmodtime) {
        this.recmodtime = recmodtime;
    }

    public Serializable getPermlevel() {
        return this.permlevel;
    }

    public void setPermlevel(Serializable permlevel) {
        this.permlevel = permlevel;
    }

    public com.owarchitects.forms.commons.db.Analystroles getAnalystroles() {
        return this.analystroles;
    }

    public void setAnalystroles(com.owarchitects.forms.commons.db.Analystroles analystroles) {
        this.analystroles = analystroles;
    }

    public String toString() {
        return new ToStringBuilder(this)
            .append("arpid", getArpid())
            .toString();
    }

}
