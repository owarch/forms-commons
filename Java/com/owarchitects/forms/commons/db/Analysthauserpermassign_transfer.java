package com.owarchitects.forms.commons.db;

import java.io.Serializable;
import java.util.Date;
import org.apache.commons.lang.builder.ToStringBuilder;


/** @author Hibernate CodeGenerator */
public class Analysthauserpermassign_transfer implements Serializable {

    /** identifier field */
    private long ahaupid;

    /** persistent field */
    private long analysthaid;

    /** persistent field */
    private long auserid;

    /** persistent field */
    private Serializable permlevel;

    /** nullable persistent field */
    private Date recmodtime;

    /** full constructor */
    public Analysthauserpermassign_transfer(long ahaupid, long analysthaid, long auserid, Serializable permlevel, Date recmodtime) {
        this.ahaupid = ahaupid;
        this.analysthaid = analysthaid;
        this.auserid = auserid;
        this.permlevel = permlevel;
        this.recmodtime = recmodtime;
    }

    /** default constructor */
    public Analysthauserpermassign_transfer() {
    }

    /** minimal constructor */
    public Analysthauserpermassign_transfer(long ahaupid, long analysthaid, long auserid, Serializable permlevel) {
        this.ahaupid = ahaupid;
        this.analysthaid = analysthaid;
        this.auserid = auserid;
        this.permlevel = permlevel;
    }

    public long getAhaupid() {
        return this.ahaupid;
    }

    public void setAhaupid(long ahaupid) {
        this.ahaupid = ahaupid;
    }

    public long getAnalysthaid() {
        return this.analysthaid;
    }

    public void setAnalysthaid(long analysthaid) {
        this.analysthaid = analysthaid;
    }

    public long getAuserid() {
        return this.auserid;
    }

    public void setAuserid(long auserid) {
        this.auserid = auserid;
    }

    public Serializable getPermlevel() {
        return this.permlevel;
    }

    public void setPermlevel(Serializable permlevel) {
        this.permlevel = permlevel;
    }

    public Date getRecmodtime() {
        return this.recmodtime;
    }

    public void setRecmodtime(Date recmodtime) {
        this.recmodtime = recmodtime;
    }

    public String toString() {
        return new ToStringBuilder(this)
            .append("ahaupid", getAhaupid())
            .toString();
    }

}
