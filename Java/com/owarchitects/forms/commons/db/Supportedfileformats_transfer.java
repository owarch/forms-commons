package com.owarchitects.forms.commons.db;

import java.io.Serializable;
import java.util.Date;
import org.apache.commons.lang.builder.ToStringBuilder;


/** @author Hibernate CodeGenerator */
public class Supportedfileformats_transfer implements Serializable {

    /** identifier field */
    private long fileformatid;

    /** persistent field */
    private String fileformat;

    /** persistent field */
    private String contenttype;

    /** nullable persistent field */
    private Date recmodtime;

    /** nullable persistent field */
    private Long fileextid;

    /** full constructor */
    public Supportedfileformats_transfer(long fileformatid, String fileformat, String contenttype, Date recmodtime, Long fileextid) {
        this.fileformatid = fileformatid;
        this.fileformat = fileformat;
        this.contenttype = contenttype;
        this.recmodtime = recmodtime;
        this.fileextid = fileextid;
    }

    /** default constructor */
    public Supportedfileformats_transfer() {
    }

    /** minimal constructor */
    public Supportedfileformats_transfer(long fileformatid, String fileformat, String contenttype) {
        this.fileformatid = fileformatid;
        this.fileformat = fileformat;
        this.contenttype = contenttype;
    }

    public long getFileformatid() {
        return this.fileformatid;
    }

    public void setFileformatid(long fileformatid) {
        this.fileformatid = fileformatid;
    }

    public String getFileformat() {
        return this.fileformat;
    }

    public void setFileformat(String fileformat) {
        this.fileformat = fileformat;
    }

    public String getContenttype() {
        return this.contenttype;
    }

    public void setContenttype(String contenttype) {
        this.contenttype = contenttype;
    }

    public Date getRecmodtime() {
        return this.recmodtime;
    }

    public void setRecmodtime(Date recmodtime) {
        this.recmodtime = recmodtime;
    }

    public Long getFileextid() {
        return this.fileextid;
    }

    public void setFileextid(Long fileextid) {
        this.fileextid = fileextid;
    }

    public String toString() {
        return new ToStringBuilder(this)
            .append("fileformatid", getFileformatid())
            .toString();
    }

}
