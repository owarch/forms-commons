package com.owarchitects.forms.commons.db;

import java.io.Serializable;
import java.util.Date;
import org.apache.commons.lang.builder.ToStringBuilder;


/** @author Hibernate CodeGenerator */
public class Nested_transfer implements Serializable {

    /** identifier field */
    private long id;

    /** nullable persistent field */
    private Long pid;

    /** persistent field */
    private String showstr;

    /** nullable persistent field */
    private Date recmodtime;

    /** full constructor */
    public Nested_transfer(long id, Long pid, String showstr, Date recmodtime) {
        this.id = id;
        this.pid = pid;
        this.showstr = showstr;
        this.recmodtime = recmodtime;
    }

    /** default constructor */
    public Nested_transfer() {
    }

    /** minimal constructor */
    public Nested_transfer(long id, String showstr) {
        this.id = id;
        this.showstr = showstr;
    }

    public long getId() {
        return this.id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public Long getPid() {
        return this.pid;
    }

    public void setPid(Long pid) {
        this.pid = pid;
    }

    public String getShowstr() {
        return this.showstr;
    }

    public void setShowstr(String showstr) {
        this.showstr = showstr;
    }

    public Date getRecmodtime() {
        return this.recmodtime;
    }

    public void setRecmodtime(Date recmodtime) {
        this.recmodtime = recmodtime;
    }

    public String toString() {
        return new ToStringBuilder(this)
            .append("id", getId())
            .toString();
    }

}
