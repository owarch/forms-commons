package com.owarchitects.forms.commons.db;

import java.io.Serializable;
import org.apache.commons.lang.builder.ToStringBuilder;


/** @author Hibernate CodeGenerator */
public class Useridassign implements Serializable {

    /** identifier field */
    private long uiaid;

    /** persistent field */
    private String varname;

    /** nullable persistent field */
    private String varvalue;

    /** persistent field */
    private com.owarchitects.forms.commons.db.Localusers localusers;

    /** persistent field */
    private com.owarchitects.forms.commons.db.Studies studies;

    /** full constructor */
    public Useridassign(String varname, String varvalue, com.owarchitects.forms.commons.db.Localusers localusers, com.owarchitects.forms.commons.db.Studies studies) {
        this.varname = varname;
        this.varvalue = varvalue;
        this.localusers = localusers;
        this.studies = studies;
    }

    /** default constructor */
    public Useridassign() {
    }

    /** minimal constructor */
    public Useridassign(String varname, com.owarchitects.forms.commons.db.Localusers localusers, com.owarchitects.forms.commons.db.Studies studies) {
        this.varname = varname;
        this.localusers = localusers;
        this.studies = studies;
    }

    public long getUiaid() {
        return this.uiaid;
    }

    public void setUiaid(long uiaid) {
        this.uiaid = uiaid;
    }

    public String getVarname() {
        return this.varname;
    }

    public void setVarname(String varname) {
        this.varname = varname;
    }

    public String getVarvalue() {
        return this.varvalue;
    }

    public void setVarvalue(String varvalue) {
        this.varvalue = varvalue;
    }

    public com.owarchitects.forms.commons.db.Localusers getLocalusers() {
        return this.localusers;
    }

    public void setLocalusers(com.owarchitects.forms.commons.db.Localusers localusers) {
        this.localusers = localusers;
    }

    public com.owarchitects.forms.commons.db.Studies getStudies() {
        return this.studies;
    }

    public void setStudies(com.owarchitects.forms.commons.db.Studies studies) {
        this.studies = studies;
    }

    public String toString() {
        return new ToStringBuilder(this)
            .append("uiaid", getUiaid())
            .toString();
    }

}
