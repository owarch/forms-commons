package com.owarchitects.forms.commons.db;

import java.io.Serializable;
import java.util.Date;
import org.apache.commons.lang.builder.ToStringBuilder;


/** @author Hibernate CodeGenerator */
public class Reportpermprofassign_transfer implements Serializable {

    /** identifier field */
    private long rrpid;

    /** persistent field */
    private long reportinfoid;

    /** nullable persistent field */
    private Date recmodtime;

    /** persistent field */
    private long rprofileid;

    /** full constructor */
    public Reportpermprofassign_transfer(long rrpid, long reportinfoid, Date recmodtime, long rprofileid) {
        this.rrpid = rrpid;
        this.reportinfoid = reportinfoid;
        this.recmodtime = recmodtime;
        this.rprofileid = rprofileid;
    }

    /** default constructor */
    public Reportpermprofassign_transfer() {
    }

    /** minimal constructor */
    public Reportpermprofassign_transfer(long rrpid, long reportinfoid, long rprofileid) {
        this.rrpid = rrpid;
        this.reportinfoid = reportinfoid;
        this.rprofileid = rprofileid;
    }

    public long getRrpid() {
        return this.rrpid;
    }

    public void setRrpid(long rrpid) {
        this.rrpid = rrpid;
    }

    public long getReportinfoid() {
        return this.reportinfoid;
    }

    public void setReportinfoid(long reportinfoid) {
        this.reportinfoid = reportinfoid;
    }

    public Date getRecmodtime() {
        return this.recmodtime;
    }

    public void setRecmodtime(Date recmodtime) {
        this.recmodtime = recmodtime;
    }

    public long getRprofileid() {
        return this.rprofileid;
    }

    public void setRprofileid(long rprofileid) {
        this.rprofileid = rprofileid;
    }

    public String toString() {
        return new ToStringBuilder(this)
            .append("rrpid", getRrpid())
            .toString();
    }

}
