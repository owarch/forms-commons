package com.owarchitects.forms.commons.db;

import java.io.Serializable;
import java.sql.Blob;
import org.apache.commons.lang.builder.ToStringBuilder;


/** @author Hibernate CodeGenerator */
public class Analyststore implements Serializable {

    /** identifier field */
    private long analyststoreid;

    /** persistent field */
    private Blob analystcontent;

    /** persistent field */
    private com.owarchitects.forms.commons.db.Analystinfo analystinfo;

    /** full constructor */
    public Analyststore(Blob analystcontent, com.owarchitects.forms.commons.db.Analystinfo analystinfo) {
        this.analystcontent = analystcontent;
        this.analystinfo = analystinfo;
    }

    /** default constructor */
    public Analyststore() {
    }

    public long getAnalyststoreid() {
        return this.analyststoreid;
    }

    public void setAnalyststoreid(long analyststoreid) {
        this.analyststoreid = analyststoreid;
    }

    public Blob getAnalystcontent() {
        return this.analystcontent;
    }

    public void setAnalystcontent(Blob analystcontent) {
        this.analystcontent = analystcontent;
    }

    public com.owarchitects.forms.commons.db.Analystinfo getAnalystinfo() {
        return this.analystinfo;
    }

    public void setAnalystinfo(com.owarchitects.forms.commons.db.Analystinfo analystinfo) {
        this.analystinfo = analystinfo;
    }

    public String toString() {
        return new ToStringBuilder(this)
            .append("analyststoreid", getAnalyststoreid())
            .toString();
    }

}
