package com.owarchitects.forms.commons.db;

import java.io.Serializable;
import java.util.Date;
import org.apache.commons.lang.builder.ToStringBuilder;


/** @author Hibernate CodeGenerator */
public class Mediarolepermassign_transfer implements Serializable {

    /** identifier field */
    private long mrpid;

    /** persistent field */
    private long mediainfoid;

    /** nullable persistent field */
    private Date recmodtime;

    /** persistent field */
    private long rroleid;

    /** persistent field */
    private Serializable permlevel;

    /** full constructor */
    public Mediarolepermassign_transfer(long mrpid, long mediainfoid, Date recmodtime, long rroleid, Serializable permlevel) {
        this.mrpid = mrpid;
        this.mediainfoid = mediainfoid;
        this.recmodtime = recmodtime;
        this.rroleid = rroleid;
        this.permlevel = permlevel;
    }

    /** default constructor */
    public Mediarolepermassign_transfer() {
    }

    /** minimal constructor */
    public Mediarolepermassign_transfer(long mrpid, long mediainfoid, long rroleid, Serializable permlevel) {
        this.mrpid = mrpid;
        this.mediainfoid = mediainfoid;
        this.rroleid = rroleid;
        this.permlevel = permlevel;
    }

    public long getMrpid() {
        return this.mrpid;
    }

    public void setMrpid(long mrpid) {
        this.mrpid = mrpid;
    }

    public long getMediainfoid() {
        return this.mediainfoid;
    }

    public void setMediainfoid(long mediainfoid) {
        this.mediainfoid = mediainfoid;
    }

    public Date getRecmodtime() {
        return this.recmodtime;
    }

    public void setRecmodtime(Date recmodtime) {
        this.recmodtime = recmodtime;
    }

    public long getRroleid() {
        return this.rroleid;
    }

    public void setRroleid(long rroleid) {
        this.rroleid = rroleid;
    }

    public Serializable getPermlevel() {
        return this.permlevel;
    }

    public void setPermlevel(Serializable permlevel) {
        this.permlevel = permlevel;
    }

    public String toString() {
        return new ToStringBuilder(this)
            .append("mrpid", getMrpid())
            .toString();
    }

}
