package com.owarchitects.forms.commons.db;

import java.io.Serializable;
import java.util.Date;
import org.apache.commons.lang.builder.ToStringBuilder;


/** @author Hibernate CodeGenerator */
public class Dtmodtime implements Serializable {

    /** identifier field */
    private long dtmtid;

    /** nullable persistent field */
    private Date localmod;

    /** nullable persistent field */
    private Date remotemod;

    /** nullable persistent field */
    private Date recmodtime;

    /** nullable persistent field */
    private com.owarchitects.forms.commons.db.Linkedstudies linkedstudies;

    /** full constructor */
    public Dtmodtime(Date localmod, Date remotemod, Date recmodtime, com.owarchitects.forms.commons.db.Linkedstudies linkedstudies) {
        this.localmod = localmod;
        this.remotemod = remotemod;
        this.recmodtime = recmodtime;
        this.linkedstudies = linkedstudies;
    }

    /** default constructor */
    public Dtmodtime() {
    }

    public long getDtmtid() {
        return this.dtmtid;
    }

    public void setDtmtid(long dtmtid) {
        this.dtmtid = dtmtid;
    }

    public Date getLocalmod() {
        return this.localmod;
    }

    public void setLocalmod(Date localmod) {
        this.localmod = localmod;
    }

    public Date getRemotemod() {
        return this.remotemod;
    }

    public void setRemotemod(Date remotemod) {
        this.remotemod = remotemod;
    }

    public Date getRecmodtime() {
        return this.recmodtime;
    }

    public void setRecmodtime(Date recmodtime) {
        this.recmodtime = recmodtime;
    }

    public com.owarchitects.forms.commons.db.Linkedstudies getLinkedstudies() {
        return this.linkedstudies;
    }

    public void setLinkedstudies(com.owarchitects.forms.commons.db.Linkedstudies linkedstudies) {
        this.linkedstudies = linkedstudies;
    }

    public String toString() {
        return new ToStringBuilder(this)
            .append("dtmtid", getDtmtid())
            .toString();
    }

}
