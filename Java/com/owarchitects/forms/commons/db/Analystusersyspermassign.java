package com.owarchitects.forms.commons.db;

import java.io.Serializable;
import java.util.Date;
import org.apache.commons.lang.builder.ToStringBuilder;


/** @author Hibernate CodeGenerator */
public class Analystusersyspermassign implements Serializable {

    /** identifier field */
    private long auspid;

    /** persistent field */
    private Serializable permlevel;

    /** nullable persistent field */
    private Date recmodtime;

    /** persistent field */
    private com.owarchitects.forms.commons.db.Allusers allusers;

    /** full constructor */
    public Analystusersyspermassign(Serializable permlevel, Date recmodtime, com.owarchitects.forms.commons.db.Allusers allusers) {
        this.permlevel = permlevel;
        this.recmodtime = recmodtime;
        this.allusers = allusers;
    }

    /** default constructor */
    public Analystusersyspermassign() {
    }

    /** minimal constructor */
    public Analystusersyspermassign(Serializable permlevel, com.owarchitects.forms.commons.db.Allusers allusers) {
        this.permlevel = permlevel;
        this.allusers = allusers;
    }

    public long getAuspid() {
        return this.auspid;
    }

    public void setAuspid(long auspid) {
        this.auspid = auspid;
    }

    public Serializable getPermlevel() {
        return this.permlevel;
    }

    public void setPermlevel(Serializable permlevel) {
        this.permlevel = permlevel;
    }

    public Date getRecmodtime() {
        return this.recmodtime;
    }

    public void setRecmodtime(Date recmodtime) {
        this.recmodtime = recmodtime;
    }

    public com.owarchitects.forms.commons.db.Allusers getAllusers() {
        return this.allusers;
    }

    public void setAllusers(com.owarchitects.forms.commons.db.Allusers allusers) {
        this.allusers = allusers;
    }

    public String toString() {
        return new ToStringBuilder(this)
            .append("auspid", getAuspid())
            .toString();
    }

}
