package com.owarchitects.forms.commons.db;

import java.io.Serializable;
import java.util.Date;
import org.apache.commons.lang.builder.ToStringBuilder;


/** @author Hibernate CodeGenerator */
public class Mediauserpermassign_transfer implements Serializable {

    /** identifier field */
    private long mupid;

    /** persistent field */
    private long mediainfoid;

    /** persistent field */
    private long auserid;

    /** persistent field */
    private Serializable permlevel;

    /** nullable persistent field */
    private Date recmodtime;

    /** full constructor */
    public Mediauserpermassign_transfer(long mupid, long mediainfoid, long auserid, Serializable permlevel, Date recmodtime) {
        this.mupid = mupid;
        this.mediainfoid = mediainfoid;
        this.auserid = auserid;
        this.permlevel = permlevel;
        this.recmodtime = recmodtime;
    }

    /** default constructor */
    public Mediauserpermassign_transfer() {
    }

    /** minimal constructor */
    public Mediauserpermassign_transfer(long mupid, long mediainfoid, long auserid, Serializable permlevel) {
        this.mupid = mupid;
        this.mediainfoid = mediainfoid;
        this.auserid = auserid;
        this.permlevel = permlevel;
    }

    public long getMupid() {
        return this.mupid;
    }

    public void setMupid(long mupid) {
        this.mupid = mupid;
    }

    public long getMediainfoid() {
        return this.mediainfoid;
    }

    public void setMediainfoid(long mediainfoid) {
        this.mediainfoid = mediainfoid;
    }

    public long getAuserid() {
        return this.auserid;
    }

    public void setAuserid(long auserid) {
        this.auserid = auserid;
    }

    public Serializable getPermlevel() {
        return this.permlevel;
    }

    public void setPermlevel(Serializable permlevel) {
        this.permlevel = permlevel;
    }

    public Date getRecmodtime() {
        return this.recmodtime;
    }

    public void setRecmodtime(Date recmodtime) {
        this.recmodtime = recmodtime;
    }

    public String toString() {
        return new ToStringBuilder(this)
            .append("mupid", getMupid())
            .toString();
    }

}
