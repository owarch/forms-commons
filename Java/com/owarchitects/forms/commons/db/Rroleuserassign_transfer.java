package com.owarchitects.forms.commons.db;

import java.io.Serializable;
import java.util.Date;
import org.apache.commons.lang.builder.ToStringBuilder;


/** @author Hibernate CodeGenerator */
public class Rroleuserassign_transfer implements Serializable {

    /** identifier field */
    private long rruaid;

    /** persistent field */
    private long rroleid;

    /** persistent field */
    private long auserid;

    /** nullable persistent field */
    private Date recmodtime;

    /** full constructor */
    public Rroleuserassign_transfer(long rruaid, long rroleid, long auserid, Date recmodtime) {
        this.rruaid = rruaid;
        this.rroleid = rroleid;
        this.auserid = auserid;
        this.recmodtime = recmodtime;
    }

    /** default constructor */
    public Rroleuserassign_transfer() {
    }

    /** minimal constructor */
    public Rroleuserassign_transfer(long rruaid, long rroleid, long auserid) {
        this.rruaid = rruaid;
        this.rroleid = rroleid;
        this.auserid = auserid;
    }

    public long getRruaid() {
        return this.rruaid;
    }

    public void setRruaid(long rruaid) {
        this.rruaid = rruaid;
    }

    public long getRroleid() {
        return this.rroleid;
    }

    public void setRroleid(long rroleid) {
        this.rroleid = rroleid;
    }

    public long getAuserid() {
        return this.auserid;
    }

    public void setAuserid(long auserid) {
        this.auserid = auserid;
    }

    public Date getRecmodtime() {
        return this.recmodtime;
    }

    public void setRecmodtime(Date recmodtime) {
        this.recmodtime = recmodtime;
    }

    public String toString() {
        return new ToStringBuilder(this)
            .append("rruaid", getRruaid())
            .toString();
    }

}
