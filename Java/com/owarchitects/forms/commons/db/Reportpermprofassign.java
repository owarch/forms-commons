package com.owarchitects.forms.commons.db;

import java.io.Serializable;
import java.util.Date;
import org.apache.commons.lang.builder.ToStringBuilder;


/** @author Hibernate CodeGenerator */
public class Reportpermprofassign implements Serializable {

    /** identifier field */
    private long rrpid;

    /** persistent field */
    private long reportinfoid;

    /** nullable persistent field */
    private Date recmodtime;

    /** persistent field */
    private com.owarchitects.forms.commons.db.Reportpermprofiles reportpermprofiles;

    /** full constructor */
    public Reportpermprofassign(long reportinfoid, Date recmodtime, com.owarchitects.forms.commons.db.Reportpermprofiles reportpermprofiles) {
        this.reportinfoid = reportinfoid;
        this.recmodtime = recmodtime;
        this.reportpermprofiles = reportpermprofiles;
    }

    /** default constructor */
    public Reportpermprofassign() {
    }

    /** minimal constructor */
    public Reportpermprofassign(long reportinfoid, com.owarchitects.forms.commons.db.Reportpermprofiles reportpermprofiles) {
        this.reportinfoid = reportinfoid;
        this.reportpermprofiles = reportpermprofiles;
    }

    public long getRrpid() {
        return this.rrpid;
    }

    public void setRrpid(long rrpid) {
        this.rrpid = rrpid;
    }

    public long getReportinfoid() {
        return this.reportinfoid;
    }

    public void setReportinfoid(long reportinfoid) {
        this.reportinfoid = reportinfoid;
    }

    public Date getRecmodtime() {
        return this.recmodtime;
    }

    public void setRecmodtime(Date recmodtime) {
        this.recmodtime = recmodtime;
    }

    public com.owarchitects.forms.commons.db.Reportpermprofiles getReportpermprofiles() {
        return this.reportpermprofiles;
    }

    public void setReportpermprofiles(com.owarchitects.forms.commons.db.Reportpermprofiles reportpermprofiles) {
        this.reportpermprofiles = reportpermprofiles;
    }

    public String toString() {
        return new ToStringBuilder(this)
            .append("rrpid", getRrpid())
            .toString();
    }

}
