package com.owarchitects.forms.commons.db;

import java.io.Serializable;
import java.util.Date;
import java.util.Set;
import org.apache.commons.lang.builder.ToStringBuilder;


/** @author Hibernate CodeGenerator */
public class Analystgroups implements Serializable {

    /** identifier field */
    private long agroupid;

    /** persistent field */
    private String groupacr;

    /** persistent field */
    private String groupdesc;

    /** nullable persistent field */
    private Long createuser;

    /** nullable persistent field */
    private Date recmodtime;

    /** persistent field */
    private Set analystgroupuserassign;

    /** persistent field */
    private Set analystgrouppermassign;

    /** persistent field */
    private Set analysthagrouppermassign;

    /** full constructor */
    public Analystgroups(String groupacr, String groupdesc, Long createuser, Date recmodtime, Set analystgroupuserassign, Set analystgrouppermassign, Set analysthagrouppermassign) {
        this.groupacr = groupacr;
        this.groupdesc = groupdesc;
        this.createuser = createuser;
        this.recmodtime = recmodtime;
        this.analystgroupuserassign = analystgroupuserassign;
        this.analystgrouppermassign = analystgrouppermassign;
        this.analysthagrouppermassign = analysthagrouppermassign;
    }

    /** default constructor */
    public Analystgroups() {
    }

    /** minimal constructor */
    public Analystgroups(String groupacr, String groupdesc, Set analystgroupuserassign, Set analystgrouppermassign, Set analysthagrouppermassign) {
        this.groupacr = groupacr;
        this.groupdesc = groupdesc;
        this.analystgroupuserassign = analystgroupuserassign;
        this.analystgrouppermassign = analystgrouppermassign;
        this.analysthagrouppermassign = analysthagrouppermassign;
    }

    public long getAgroupid() {
        return this.agroupid;
    }

    public void setAgroupid(long agroupid) {
        this.agroupid = agroupid;
    }

    public String getGroupacr() {
        return this.groupacr;
    }

    public void setGroupacr(String groupacr) {
        this.groupacr = groupacr;
    }

    public String getGroupdesc() {
        return this.groupdesc;
    }

    public void setGroupdesc(String groupdesc) {
        this.groupdesc = groupdesc;
    }

    public Long getCreateuser() {
        return this.createuser;
    }

    public void setCreateuser(Long createuser) {
        this.createuser = createuser;
    }

    public Date getRecmodtime() {
        return this.recmodtime;
    }

    public void setRecmodtime(Date recmodtime) {
        this.recmodtime = recmodtime;
    }

    public Set getAnalystgroupuserassign() {
        return this.analystgroupuserassign;
    }

    public void setAnalystgroupuserassign(Set analystgroupuserassign) {
        this.analystgroupuserassign = analystgroupuserassign;
    }

    public Set getAnalystgrouppermassign() {
        return this.analystgrouppermassign;
    }

    public void setAnalystgrouppermassign(Set analystgrouppermassign) {
        this.analystgrouppermassign = analystgrouppermassign;
    }

    public Set getAnalysthagrouppermassign() {
        return this.analysthagrouppermassign;
    }

    public void setAnalysthagrouppermassign(Set analysthagrouppermassign) {
        this.analysthagrouppermassign = analysthagrouppermassign;
    }

    public String toString() {
        return new ToStringBuilder(this)
            .append("agroupid", getAgroupid())
            .toString();
    }

}
