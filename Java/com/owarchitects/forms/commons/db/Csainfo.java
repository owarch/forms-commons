package com.owarchitects.forms.commons.db;

import java.io.Serializable;
import java.util.Date;
import org.apache.commons.lang.builder.ToStringBuilder;


/** @author Hibernate CodeGenerator */
public class Csainfo implements Serializable {

    /** identifier field */
    private long csaid;

    /** nullable persistent field */
    private String fkpassword;

    /** nullable persistent field */
    private String dbpassword;

    /** nullable persistent field */
    private Date recmodtime;

    /** full constructor */
    public Csainfo(String fkpassword, String dbpassword, Date recmodtime) {
        this.fkpassword = fkpassword;
        this.dbpassword = dbpassword;
        this.recmodtime = recmodtime;
    }

    /** default constructor */
    public Csainfo() {
    }

    public long getCsaid() {
        return this.csaid;
    }

    public void setCsaid(long csaid) {
        this.csaid = csaid;
    }

    public String getFkpassword() {
        return this.fkpassword;
    }

    public void setFkpassword(String fkpassword) {
        this.fkpassword = fkpassword;
    }

    public String getDbpassword() {
        return this.dbpassword;
    }

    public void setDbpassword(String dbpassword) {
        this.dbpassword = dbpassword;
    }

    public Date getRecmodtime() {
        return this.recmodtime;
    }

    public void setRecmodtime(Date recmodtime) {
        this.recmodtime = recmodtime;
    }

    public String toString() {
        return new ToStringBuilder(this)
            .append("csaid", getCsaid())
            .toString();
    }

}
