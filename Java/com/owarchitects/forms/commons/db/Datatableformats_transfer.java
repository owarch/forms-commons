package com.owarchitects.forms.commons.db;

import java.io.Serializable;
import java.util.Date;
import org.apache.commons.lang.builder.ToStringBuilder;


/** @author Hibernate CodeGenerator */
public class Datatableformats_transfer implements Serializable {

    /** identifier field */
    private long dtformatid;

    /** nullable persistent field */
    private Date recmodtime;

    /** persistent field */
    private long fileformatid;

    /** full constructor */
    public Datatableformats_transfer(long dtformatid, Date recmodtime, long fileformatid) {
        this.dtformatid = dtformatid;
        this.recmodtime = recmodtime;
        this.fileformatid = fileformatid;
    }

    /** default constructor */
    public Datatableformats_transfer() {
    }

    /** minimal constructor */
    public Datatableformats_transfer(long dtformatid, long fileformatid) {
        this.dtformatid = dtformatid;
        this.fileformatid = fileformatid;
    }

    public long getDtformatid() {
        return this.dtformatid;
    }

    public void setDtformatid(long dtformatid) {
        this.dtformatid = dtformatid;
    }

    public Date getRecmodtime() {
        return this.recmodtime;
    }

    public void setRecmodtime(Date recmodtime) {
        this.recmodtime = recmodtime;
    }

    public long getFileformatid() {
        return this.fileformatid;
    }

    public void setFileformatid(long fileformatid) {
        this.fileformatid = fileformatid;
    }

    public String toString() {
        return new ToStringBuilder(this)
            .append("dtformatid", getDtformatid())
            .toString();
    }

}
