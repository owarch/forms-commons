package com.owarchitects.forms.commons.db;

import java.io.Serializable;
import java.util.Date;
import org.apache.commons.lang.builder.ToStringBuilder;


/** @author Hibernate CodeGenerator */
public class Sysrolepermassign implements Serializable {

    /** identifier field */
    private long srpid;

    /** persistent field */
    private Serializable permlevel;

    /** nullable persistent field */
    private Date recmodtime;

    /** persistent field */
    private com.owarchitects.forms.commons.db.Resourceroles role;

    /** persistent field */
    private com.owarchitects.forms.commons.db.Permissioncats permissioncats;

    /** full constructor */
    public Sysrolepermassign(Serializable permlevel, Date recmodtime, com.owarchitects.forms.commons.db.Resourceroles role, com.owarchitects.forms.commons.db.Permissioncats permissioncats) {
        this.permlevel = permlevel;
        this.recmodtime = recmodtime;
        this.role = role;
        this.permissioncats = permissioncats;
    }

    /** default constructor */
    public Sysrolepermassign() {
    }

    /** minimal constructor */
    public Sysrolepermassign(Serializable permlevel, com.owarchitects.forms.commons.db.Resourceroles role, com.owarchitects.forms.commons.db.Permissioncats permissioncats) {
        this.permlevel = permlevel;
        this.role = role;
        this.permissioncats = permissioncats;
    }

    public long getSrpid() {
        return this.srpid;
    }

    public void setSrpid(long srpid) {
        this.srpid = srpid;
    }

    public Serializable getPermlevel() {
        return this.permlevel;
    }

    public void setPermlevel(Serializable permlevel) {
        this.permlevel = permlevel;
    }

    public Date getRecmodtime() {
        return this.recmodtime;
    }

    public void setRecmodtime(Date recmodtime) {
        this.recmodtime = recmodtime;
    }

    public com.owarchitects.forms.commons.db.Resourceroles getRole() {
        return this.role;
    }

    public void setRole(com.owarchitects.forms.commons.db.Resourceroles role) {
        this.role = role;
    }

    public com.owarchitects.forms.commons.db.Permissioncats getPermissioncats() {
        return this.permissioncats;
    }

    public void setPermissioncats(com.owarchitects.forms.commons.db.Permissioncats permissioncats) {
        this.permissioncats = permissioncats;
    }

    public String toString() {
        return new ToStringBuilder(this)
            .append("srpid", getSrpid())
            .toString();
    }

}
