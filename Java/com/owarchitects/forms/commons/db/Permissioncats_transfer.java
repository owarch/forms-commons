package com.owarchitects.forms.commons.db;

import java.io.Serializable;
import java.util.Date;
import org.apache.commons.lang.builder.ToStringBuilder;


/** @author Hibernate CodeGenerator */
public class Permissioncats_transfer implements Serializable {

    /** identifier field */
    private long pcatid;

    /** persistent field */
    private int pcatvalue;

    /** persistent field */
    private int pcatscope;

    /** persistent field */
    private int pcatorder;

    /** persistent field */
    private String pcatdesc;

    /** nullable persistent field */
    private Date recmodtime;

    /** full constructor */
    public Permissioncats_transfer(long pcatid, int pcatvalue, int pcatscope, int pcatorder, String pcatdesc, Date recmodtime) {
        this.pcatid = pcatid;
        this.pcatvalue = pcatvalue;
        this.pcatscope = pcatscope;
        this.pcatorder = pcatorder;
        this.pcatdesc = pcatdesc;
        this.recmodtime = recmodtime;
    }

    /** default constructor */
    public Permissioncats_transfer() {
    }

    /** minimal constructor */
    public Permissioncats_transfer(long pcatid, int pcatvalue, int pcatscope, int pcatorder, String pcatdesc) {
        this.pcatid = pcatid;
        this.pcatvalue = pcatvalue;
        this.pcatscope = pcatscope;
        this.pcatorder = pcatorder;
        this.pcatdesc = pcatdesc;
    }

    public long getPcatid() {
        return this.pcatid;
    }

    public void setPcatid(long pcatid) {
        this.pcatid = pcatid;
    }

    public int getPcatvalue() {
        return this.pcatvalue;
    }

    public void setPcatvalue(int pcatvalue) {
        this.pcatvalue = pcatvalue;
    }

    public int getPcatscope() {
        return this.pcatscope;
    }

    public void setPcatscope(int pcatscope) {
        this.pcatscope = pcatscope;
    }

    public int getPcatorder() {
        return this.pcatorder;
    }

    public void setPcatorder(int pcatorder) {
        this.pcatorder = pcatorder;
    }

    public String getPcatdesc() {
        return this.pcatdesc;
    }

    public void setPcatdesc(String pcatdesc) {
        this.pcatdesc = pcatdesc;
    }

    public Date getRecmodtime() {
        return this.recmodtime;
    }

    public void setRecmodtime(Date recmodtime) {
        this.recmodtime = recmodtime;
    }

    public String toString() {
        return new ToStringBuilder(this)
            .append("pcatid", getPcatid())
            .toString();
    }

}
