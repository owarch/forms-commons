package com.owarchitects.forms.commons.db;

import java.io.Serializable;
import java.util.Date;
import org.apache.commons.lang.builder.ToStringBuilder;


/** @author Hibernate CodeGenerator */
public class Reportuserpermassign_transfer implements Serializable {

    /** identifier field */
    private long rupid;

    /** persistent field */
    private long reportinfoid;

    /** persistent field */
    private long auserid;

    /** persistent field */
    private Serializable permlevel;

    /** nullable persistent field */
    private Date recmodtime;

    /** full constructor */
    public Reportuserpermassign_transfer(long rupid, long reportinfoid, long auserid, Serializable permlevel, Date recmodtime) {
        this.rupid = rupid;
        this.reportinfoid = reportinfoid;
        this.auserid = auserid;
        this.permlevel = permlevel;
        this.recmodtime = recmodtime;
    }

    /** default constructor */
    public Reportuserpermassign_transfer() {
    }

    /** minimal constructor */
    public Reportuserpermassign_transfer(long rupid, long reportinfoid, long auserid, Serializable permlevel) {
        this.rupid = rupid;
        this.reportinfoid = reportinfoid;
        this.auserid = auserid;
        this.permlevel = permlevel;
    }

    public long getRupid() {
        return this.rupid;
    }

    public void setRupid(long rupid) {
        this.rupid = rupid;
    }

    public long getReportinfoid() {
        return this.reportinfoid;
    }

    public void setReportinfoid(long reportinfoid) {
        this.reportinfoid = reportinfoid;
    }

    public long getAuserid() {
        return this.auserid;
    }

    public void setAuserid(long auserid) {
        this.auserid = auserid;
    }

    public Serializable getPermlevel() {
        return this.permlevel;
    }

    public void setPermlevel(Serializable permlevel) {
        this.permlevel = permlevel;
    }

    public Date getRecmodtime() {
        return this.recmodtime;
    }

    public void setRecmodtime(Date recmodtime) {
        this.recmodtime = recmodtime;
    }

    public String toString() {
        return new ToStringBuilder(this)
            .append("rupid", getRupid())
            .toString();
    }

}
