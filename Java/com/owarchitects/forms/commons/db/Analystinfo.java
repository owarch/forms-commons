package com.owarchitects.forms.commons.db;

import java.io.Serializable;
import java.util.Date;
import java.util.Set;
import org.apache.commons.lang.builder.ToStringBuilder;


/** @author Hibernate CodeGenerator */
public class Analystinfo implements Serializable {

    /** identifier field */
    private long analystinfoid;

    /** persistent field */
    private long panalysthaid;

    /** nullable persistent field */
    private String filename;

    /** nullable persistent field */
    private Long filesize;

    /** nullable persistent field */
    private String description;

    /** nullable persistent field */
    private String creationdate;

    /** nullable persistent field */
    private String notes;

    /** nullable persistent field */
    private Date uploadtime;

    /** nullable persistent field */
    private Long uploaduser;

    /** persistent field */
    private com.owarchitects.forms.commons.db.Supportedfileformats supportedfileformats;

    /** persistent field */
    private Set analyststore;

    /** full constructor */
    public Analystinfo(long panalysthaid, String filename, Long filesize, String description, String creationdate, String notes, Date uploadtime, Long uploaduser, com.owarchitects.forms.commons.db.Supportedfileformats supportedfileformats, Set analyststore) {
        this.panalysthaid = panalysthaid;
        this.filename = filename;
        this.filesize = filesize;
        this.description = description;
        this.creationdate = creationdate;
        this.notes = notes;
        this.uploadtime = uploadtime;
        this.uploaduser = uploaduser;
        this.supportedfileformats = supportedfileformats;
        this.analyststore = analyststore;
    }

    /** default constructor */
    public Analystinfo() {
    }

    /** minimal constructor */
    public Analystinfo(long panalysthaid, com.owarchitects.forms.commons.db.Supportedfileformats supportedfileformats, Set analyststore) {
        this.panalysthaid = panalysthaid;
        this.supportedfileformats = supportedfileformats;
        this.analyststore = analyststore;
    }

    public long getAnalystinfoid() {
        return this.analystinfoid;
    }

    public void setAnalystinfoid(long analystinfoid) {
        this.analystinfoid = analystinfoid;
    }

    public long getPanalysthaid() {
        return this.panalysthaid;
    }

    public void setPanalysthaid(long panalysthaid) {
        this.panalysthaid = panalysthaid;
    }

    public String getFilename() {
        return this.filename;
    }

    public void setFilename(String filename) {
        this.filename = filename;
    }

    public Long getFilesize() {
        return this.filesize;
    }

    public void setFilesize(Long filesize) {
        this.filesize = filesize;
    }

    public String getDescription() {
        return this.description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getCreationdate() {
        return this.creationdate;
    }

    public void setCreationdate(String creationdate) {
        this.creationdate = creationdate;
    }

    public String getNotes() {
        return this.notes;
    }

    public void setNotes(String notes) {
        this.notes = notes;
    }

    public Date getUploadtime() {
        return this.uploadtime;
    }

    public void setUploadtime(Date uploadtime) {
        this.uploadtime = uploadtime;
    }

    public Long getUploaduser() {
        return this.uploaduser;
    }

    public void setUploaduser(Long uploaduser) {
        this.uploaduser = uploaduser;
    }

    public com.owarchitects.forms.commons.db.Supportedfileformats getSupportedfileformats() {
        return this.supportedfileformats;
    }

    public void setSupportedfileformats(com.owarchitects.forms.commons.db.Supportedfileformats supportedfileformats) {
        this.supportedfileformats = supportedfileformats;
    }

    public Set getAnalyststore() {
        return this.analyststore;
    }

    public void setAnalyststore(Set analyststore) {
        this.analyststore = analyststore;
    }

    public String toString() {
        return new ToStringBuilder(this)
            .append("analystinfoid", getAnalystinfoid())
            .toString();
    }

}
