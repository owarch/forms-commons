package com.owarchitects.forms.commons.db;

import java.io.Serializable;
import java.util.Date;
import org.apache.commons.lang.builder.ToStringBuilder;


/** @author Hibernate CodeGenerator */
public class Pwhistory_transfer implements Serializable {

    /** identifier field */
    private long pwhid;

    /** persistent field */
    private long luserid;

    /** persistent field */
    private String username;

    /** persistent field */
    private String password;

    /** nullable persistent field */
    private Date passworddt;

    /** nullable persistent field */
    private Date recmodtime;

    /** full constructor */
    public Pwhistory_transfer(long pwhid, long luserid, String username, String password, Date passworddt, Date recmodtime) {
        this.pwhid = pwhid;
        this.luserid = luserid;
        this.username = username;
        this.password = password;
        this.passworddt = passworddt;
        this.recmodtime = recmodtime;
    }

    /** default constructor */
    public Pwhistory_transfer() {
    }

    /** minimal constructor */
    public Pwhistory_transfer(long pwhid, long luserid, String username, String password) {
        this.pwhid = pwhid;
        this.luserid = luserid;
        this.username = username;
        this.password = password;
    }

    public long getPwhid() {
        return this.pwhid;
    }

    public void setPwhid(long pwhid) {
        this.pwhid = pwhid;
    }

    public long getLuserid() {
        return this.luserid;
    }

    public void setLuserid(long luserid) {
        this.luserid = luserid;
    }

    public String getUsername() {
        return this.username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return this.password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public Date getPassworddt() {
        return this.passworddt;
    }

    public void setPassworddt(Date passworddt) {
        this.passworddt = passworddt;
    }

    public Date getRecmodtime() {
        return this.recmodtime;
    }

    public void setRecmodtime(Date recmodtime) {
        this.recmodtime = recmodtime;
    }

    public String toString() {
        return new ToStringBuilder(this)
            .append("pwhid", getPwhid())
            .toString();
    }

}
