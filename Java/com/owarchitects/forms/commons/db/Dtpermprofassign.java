package com.owarchitects.forms.commons.db;

import java.io.Serializable;
import java.util.Date;
import org.apache.commons.lang.builder.ToStringBuilder;


/** @author Hibernate CodeGenerator */
public class Dtpermprofassign implements Serializable {

    /** identifier field */
    private long dtrpid;

    /** persistent field */
    private long dtdefid;

    /** nullable persistent field */
    private Date recmodtime;

    /** persistent field */
    private com.owarchitects.forms.commons.db.Dtpermprofiles dtpermprofiles;

    /** full constructor */
    public Dtpermprofassign(long dtdefid, Date recmodtime, com.owarchitects.forms.commons.db.Dtpermprofiles dtpermprofiles) {
        this.dtdefid = dtdefid;
        this.recmodtime = recmodtime;
        this.dtpermprofiles = dtpermprofiles;
    }

    /** default constructor */
    public Dtpermprofassign() {
    }

    /** minimal constructor */
    public Dtpermprofassign(long dtdefid, com.owarchitects.forms.commons.db.Dtpermprofiles dtpermprofiles) {
        this.dtdefid = dtdefid;
        this.dtpermprofiles = dtpermprofiles;
    }

    public long getDtrpid() {
        return this.dtrpid;
    }

    public void setDtrpid(long dtrpid) {
        this.dtrpid = dtrpid;
    }

    public long getDtdefid() {
        return this.dtdefid;
    }

    public void setDtdefid(long dtdefid) {
        this.dtdefid = dtdefid;
    }

    public Date getRecmodtime() {
        return this.recmodtime;
    }

    public void setRecmodtime(Date recmodtime) {
        this.recmodtime = recmodtime;
    }

    public com.owarchitects.forms.commons.db.Dtpermprofiles getDtpermprofiles() {
        return this.dtpermprofiles;
    }

    public void setDtpermprofiles(com.owarchitects.forms.commons.db.Dtpermprofiles dtpermprofiles) {
        this.dtpermprofiles = dtpermprofiles;
    }

    public String toString() {
        return new ToStringBuilder(this)
            .append("dtrpid", getDtrpid())
            .toString();
    }

}
