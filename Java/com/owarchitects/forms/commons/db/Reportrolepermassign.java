package com.owarchitects.forms.commons.db;

import java.io.Serializable;
import java.util.Date;
import org.apache.commons.lang.builder.ToStringBuilder;


/** @author Hibernate CodeGenerator */
public class Reportrolepermassign implements Serializable {

    /** identifier field */
    private long rrpid;

    /** persistent field */
    private long reportinfoid;

    /** nullable persistent field */
    private Date recmodtime;

    /** persistent field */
    private Serializable permlevel;

    /** persistent field */
    private com.owarchitects.forms.commons.db.Resourceroles resourceroles;

    /** full constructor */
    public Reportrolepermassign(long reportinfoid, Date recmodtime, Serializable permlevel, com.owarchitects.forms.commons.db.Resourceroles resourceroles) {
        this.reportinfoid = reportinfoid;
        this.recmodtime = recmodtime;
        this.permlevel = permlevel;
        this.resourceroles = resourceroles;
    }

    /** default constructor */
    public Reportrolepermassign() {
    }

    /** minimal constructor */
    public Reportrolepermassign(long reportinfoid, Serializable permlevel, com.owarchitects.forms.commons.db.Resourceroles resourceroles) {
        this.reportinfoid = reportinfoid;
        this.permlevel = permlevel;
        this.resourceroles = resourceroles;
    }

    public long getRrpid() {
        return this.rrpid;
    }

    public void setRrpid(long rrpid) {
        this.rrpid = rrpid;
    }

    public long getReportinfoid() {
        return this.reportinfoid;
    }

    public void setReportinfoid(long reportinfoid) {
        this.reportinfoid = reportinfoid;
    }

    public Date getRecmodtime() {
        return this.recmodtime;
    }

    public void setRecmodtime(Date recmodtime) {
        this.recmodtime = recmodtime;
    }

    public Serializable getPermlevel() {
        return this.permlevel;
    }

    public void setPermlevel(Serializable permlevel) {
        this.permlevel = permlevel;
    }

    public com.owarchitects.forms.commons.db.Resourceroles getResourceroles() {
        return this.resourceroles;
    }

    public void setResourceroles(com.owarchitects.forms.commons.db.Resourceroles resourceroles) {
        this.resourceroles = resourceroles;
    }

    public String toString() {
        return new ToStringBuilder(this)
            .append("rrpid", getRrpid())
            .toString();
    }

}
