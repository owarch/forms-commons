 
/**
 *
 * This program is part of:
 *
 *    FORMS [Forms-Oriented Research Management System]
 *
 * Copyright (C) 2009 OpenWare Architects.  All Rights Reserved.
 *
 * FORMS is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * FORMS is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with FORMS.  If not, see <http://www.gnu.org/licenses/>.
 *
 */
 
package com.owarchitects.forms.commons.db;

/**
 *  MainDAO.java  -  FORMSMainDB Data Access Object with convenience methods
 */

import com.owarchitects.forms.commons.comp.*;
import java.util.*;
import java.lang.reflect.*;
import org.hibernate.*;
import java.lang.annotation.*;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.orm.hibernate3.*;

@Repository
//@Transactional
public class MainDAO extends FORMSDao {

   public void save(Object o,boolean isMetaData) {
      if (isMetaData) {
         updateModTime(o);
      }
      getCurrentSession().save(o);
   }

   public void update(Object o,boolean isMetaData) {
      if (isMetaData) {
         updateModTime(o);
      }
      getCurrentSession().update(o);
   }

   public void saveOrUpdate(Object o,boolean isMetaData) {
      if (isMetaData) {
         updateModTime(o);
      }
      getCurrentSession().saveOrUpdate(o);
   }

   public void merge(Object o,boolean isMetaData) {
      if (isMetaData) {
         updateModTime(o);
      }
      getCurrentSession().merge(o);
   }

   public void persist(Object o,boolean isMetaData) {
      if (isMetaData) {
         updateModTime(o);
      }
      getCurrentSession().save(o);
   }

   public void replicate(Object o,boolean isMetaData) {
      if (isMetaData) {
         updateModTime(o);
      }
      getCurrentSession().replicate(o,ReplicationMode.EXCEPTION);
   }

   public void replicate(Object o,ReplicationMode r,boolean isMetaData) {
      if (isMetaData) {
         updateModTime(o);
      }
      getCurrentSession().replicate(o,r);
   }

   public void delete(Object o,boolean isMetaData) {
      if (isMetaData) {
         updateModTime(o);
      }
      getCurrentSession().delete(o);
   }

   public void save(Object o) {
      save(o,true);
   }

   public void update(Object o) {
      update(o,true);
   }

   public void saveOrUpdate(Object o) {
      saveOrUpdate(o,true);
   }

   public void merge(Object o) {
      merge(o,true);
   }

   public void persist(Object o) {
      persist(o,true);
   }

   public void replicate(Object o) {
      replicate(o,ReplicationMode.EXCEPTION,true);
   }

   public void replicate(Object o,ReplicationMode r) {
      replicate(o,r,true);
   }

   public void delete(Object o) {
      delete(o,true);
   }

   private void updateModTime(Object o) {

      Class c=o.getClass();
      // NOTE:  If using _transfer class _transfer should be removed here to refer to the 
      //        main table
      String classname=c.getName().replace("_transfer","");
      // Update record modification time
      try {
         Method m=c.getMethod("setRecmodtime",new Class[] { Date.class } );
         m.invoke( o,new Object[] { new Date() } );
      } catch (Exception rmue) {
         // Do nothing
      }

      // Update table modification time
      Tablemodtime t=(Tablemodtime)this.execUniqueQuery(
         "select t from Tablemodtime t where t.classname='" + classname + "'"
         );
      if (t==null) {
         t=new Tablemodtime();
         t.setClassname(classname);
      }
      t.setTablemodtime(new Date());
      this.saveOrUpdate(t,false);

   }

}


