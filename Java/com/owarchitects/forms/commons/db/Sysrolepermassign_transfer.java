package com.owarchitects.forms.commons.db;

import java.io.Serializable;
import java.util.Date;
import org.apache.commons.lang.builder.ToStringBuilder;


/** @author Hibernate CodeGenerator */
public class Sysrolepermassign_transfer implements Serializable {

    /** identifier field */
    private long srpid;

    /** persistent field */
    private long rroleid;

    /** persistent field */
    private long pcatid;

    /** persistent field */
    private Serializable permlevel;

    /** nullable persistent field */
    private Date recmodtime;

    /** full constructor */
    public Sysrolepermassign_transfer(long srpid, long rroleid, long pcatid, Serializable permlevel, Date recmodtime) {
        this.srpid = srpid;
        this.rroleid = rroleid;
        this.pcatid = pcatid;
        this.permlevel = permlevel;
        this.recmodtime = recmodtime;
    }

    /** default constructor */
    public Sysrolepermassign_transfer() {
    }

    /** minimal constructor */
    public Sysrolepermassign_transfer(long srpid, long rroleid, long pcatid, Serializable permlevel) {
        this.srpid = srpid;
        this.rroleid = rroleid;
        this.pcatid = pcatid;
        this.permlevel = permlevel;
    }

    public long getSrpid() {
        return this.srpid;
    }

    public void setSrpid(long srpid) {
        this.srpid = srpid;
    }

    public long getRroleid() {
        return this.rroleid;
    }

    public void setRroleid(long rroleid) {
        this.rroleid = rroleid;
    }

    public long getPcatid() {
        return this.pcatid;
    }

    public void setPcatid(long pcatid) {
        this.pcatid = pcatid;
    }

    public Serializable getPermlevel() {
        return this.permlevel;
    }

    public void setPermlevel(Serializable permlevel) {
        this.permlevel = permlevel;
    }

    public Date getRecmodtime() {
        return this.recmodtime;
    }

    public void setRecmodtime(Date recmodtime) {
        this.recmodtime = recmodtime;
    }

    public String toString() {
        return new ToStringBuilder(this)
            .append("srpid", getSrpid())
            .toString();
    }

}
