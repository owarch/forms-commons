package com.owarchitects.forms.commons.db;

import java.io.Serializable;
import java.util.Date;
import org.apache.commons.lang.builder.ToStringBuilder;


/** @author Hibernate CodeGenerator */
public class Dtrolepermassign implements Serializable {

    /** identifier field */
    private long dtrpid;

    /** persistent field */
    private long dtdefid;

    /** nullable persistent field */
    private Date recmodtime;

    /** persistent field */
    private Serializable permlevel;

    /** persistent field */
    private com.owarchitects.forms.commons.db.Resourceroles resourceroles;

    /** full constructor */
    public Dtrolepermassign(long dtdefid, Date recmodtime, Serializable permlevel, com.owarchitects.forms.commons.db.Resourceroles resourceroles) {
        this.dtdefid = dtdefid;
        this.recmodtime = recmodtime;
        this.permlevel = permlevel;
        this.resourceroles = resourceroles;
    }

    /** default constructor */
    public Dtrolepermassign() {
    }

    /** minimal constructor */
    public Dtrolepermassign(long dtdefid, Serializable permlevel, com.owarchitects.forms.commons.db.Resourceroles resourceroles) {
        this.dtdefid = dtdefid;
        this.permlevel = permlevel;
        this.resourceroles = resourceroles;
    }

    public long getDtrpid() {
        return this.dtrpid;
    }

    public void setDtrpid(long dtrpid) {
        this.dtrpid = dtrpid;
    }

    public long getDtdefid() {
        return this.dtdefid;
    }

    public void setDtdefid(long dtdefid) {
        this.dtdefid = dtdefid;
    }

    public Date getRecmodtime() {
        return this.recmodtime;
    }

    public void setRecmodtime(Date recmodtime) {
        this.recmodtime = recmodtime;
    }

    public Serializable getPermlevel() {
        return this.permlevel;
    }

    public void setPermlevel(Serializable permlevel) {
        this.permlevel = permlevel;
    }

    public com.owarchitects.forms.commons.db.Resourceroles getResourceroles() {
        return this.resourceroles;
    }

    public void setResourceroles(com.owarchitects.forms.commons.db.Resourceroles resourceroles) {
        this.resourceroles = resourceroles;
    }

    public String toString() {
        return new ToStringBuilder(this)
            .append("dtrpid", getDtrpid())
            .toString();
    }

}
