
/**
 *
 * This program is part of:
 *
 *    FORMS [Forms-Oriented Research Management System]
 *
 * Copyright (C) 2009 OpenWare Architects LLC.  All Rights Reserved.
 *    http://forms.sourceforge.net/
 *
 * FORMS is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * FORMS is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with FORMS.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

//////////////////////////////////////////////////////////////////////////////////////////////
//                                                                                          //
// RMSScripts.js                                                                            //
//                                                                                          //
//    This program contains most of the functions implemented by HTML forms in the          //
//    FORMS application                                                                     //
//                                                                                          //
//////////////////////////////////////////////////////////////////////////////////////////////

var _rmsPassValueObject;
var _alreadyNotify;
var _appName;
var _backupJS;
var _bgColor="#ffffff";
var _borderColor="#000000";
var _changeField;
var _changeValidate;
var _conCode;
var _conList;
var _conPopUp;
var _continueServlet;
var _thisvalue;
var _docReady=0;
var _environ;
var _errArray=[];
var _errTextArray=[];
var _errorColor="#ffaaaa";
var _fColor="#00ff00";
var _formNodes;
var _interimNoSaveNotify;
var _interimTime;
var _iSaveInt;
var _myDoc;
var _numErrors;
var _objType;
var _oldValue;
var _preSubmit;
var _resetVar;
var _scrptsrc;
var _serverName;
var _submitServlet;
var _validateStart="Y";
var _varCaps="N";
var _varInteger="N";
var _varLength;
var _varMissingArray;
var _varPattern;
var _varPopUp;
var _varRequired;
var _saveStatus="OK";
var _loadScriptRun="N";
var _insertLoadScriptHere="X";
var _alreadyWarn="N";
var _globalElementArray=new Array();
var _globalNameArray=new Array();
// Default interim save interval 15 minutes
var _iSaveInt=15;
var _importwin_;

// To Dramatically increase processing speed, store single form element array in memory
var allE=null;
var useAllE=false;

_resetVar=0;
_numErrors=0;
_errArray=new Array();
_errTextArray=new Array();
_changeValidate=0;
_preSubmit=0;
_alreadyNotify=0;
_interimTime=new Date().getTime();
_interimNoSaveNotify=false;

////////////////////
////////////////////
////////////////////
////////////////////
////////////////////

function beep() {
   java.awt.Toolkit.getDefaultToolkit().beep();
}

// TRIM function used below
function trim(s) {
  if (s!=null && s!=undefined) {
     // this step is important to so function always returns a string rather
     // than possibly returning a number.  Returning a number could be problematic
     // as a numeric zero evaluates equal to a blank string.
     s=s.toString();
     if (s.length>=1) {
        while (s.toString().substr(0,1) == ' ') {
          s = s.toString().substr(1,s.length);
        }
        while (s.toString().substr(s.length-1,s.length) == ' ') {
          s = s.toString().substr(0,s.length-1);
        }
     }
  }
  return s;
}

// create nConvert function (empty/null strings normally convert to zero)

function nConvert(inVal) {
   if (inVal!=null && trim(inVal)!='') {
      return Number(inVal);
   } else {
      return Number(NaN);
   }
}

// RETURN FIRST INSTANCE OF ELEMENT MATCHING NAME
function getFormElementById(inString) {
   storeEleArrayInMemory();
   // Returns first form element matching name
   for (var i=0;i<document.forms.length;i++) {
      var e=getEleArray(i);
      for (var j=0;j<e.length;j++) {
         if (e[j].name==inString) {
            return e[j];
         }
      }
      // IF NO MATCH, TRY AGAIN IGNORING CASE (SAS IS CASE-INSENSITIVE)
      for (var j=0;j<e.length;j++) {
         if (e[j].name.toLowerCase()==inString.toLowerCase()) {
            return e[j];
         }
      }
   }
}

// RETURN FIRST INSTANCE OF ELEMENT MATCHING NAME
function getFormElementByIdPassDoc(dc,inString) {
   storeEleArrayInMemoryPassDoc(dc);
   // Returns first form element matching name
   for (var i=0;i<dc.forms.length;i++) {
      var e=getEleArray(i);
      for (var j=0;j<e.length;j++) {
         if (e[j].name==inString) {
            return e[j];
         }
      }
      // IF NO MATCH, TRY AGAIN IGNORING CASE (SAS IS CASE-INSENSITIVE)
      for (var j=0;j<e.length;j++) {
         if (e[j].name.toLowerCase()==inString.toLowerCase()) {
            return e[j];
         }
      }
   }
}

// Initialize Global Arrays
function initializeGlobalArrays() {
   // want _docReady=0 for this funct
   _drv=_docReady;
   _docReady=0;
   storeEleArrayInMemory();
   // Returns first form element matching name
   for (var i=0;i<document.forms.length;i++) {
      var e=getEleArray(i);
      for (var j=0;j<e.length;j++) {
         if (e[j].name.toLowerCase().substr(0,7)=="global_") {
            // set element value to field value
            var v=getFormElementById(e[j].name.substr(7)).value;
            if (v!=null) {
               e[j].value=v;
            }   
            _globalElementArray.push(e[j]);
            var n=e[j].name;
            var already=false
            for (var k=1;k<_globalNameArray.length;k++) {
               if (_globalNameArray[k]==n) {
                  already=true;
               }
            }
            if (!already) {
               _globalNameArray.push(e[j].name);
            }
         }
      }
   }
   // Point cursor to initial field (assumed soon after formdataok)
   var ele=document.getElementById('FormDataOK');
   if (ele!=null) {
      try {
         ele.form[(getIndex(ele)+1) % ele.form.length].focus();
      } catch (e1) {
         try {
            ele.form[(getIndex(ele)+2) % ele.form.length].focus();
         } catch (e2) {
            try {
               ele.form[(getIndex(ele)+3) % ele.form.length].focus();
            } catch (e3) { }
         }
      }
   }
   _docReady=_drv;
}

// Does field have global equivalent fields?
function hasGlobal(inString) {
   var hasglobal=false;
   for (var i=1;i<_globalNameArray.length;i++) {
      if (_globalNameArray[i].substr(0,7).match(/[Gg][Ll][Oo][Bb][Aa][Ll]_/) && 
          _globalNameArray[i].substr(7)==inString) {
          hasglobal=true;
      }
   }
   return hasglobal;
}

// Return global equivalent field array
function getGlobalElements(inString) {
   var _elementArray=new Array();
   for (var l=0;l<_globalElementArray.length;l++) {
      if (_globalElementArray[l].name.substr(0,7).match(/[Gg][Ll][Oo][Bb][Aa][Ll]_/) && 
          _globalElementArray[l].name.substr(7)==inString) {
         _elementArray.push(_globalElementArray[l]);
      }
   }
   return _elementArray;
}

// To Dramatically increase processing speed, store single form element array in memory
function storeEleArrayInMemory() {
   if (document.forms.length==1 && allE==null) {
      var tempAllE=document.forms[0].elements;
      allE=new Array();
      for (var i=0;i<tempAllE.length;i++) {
         try {
            if (tempAllE[i].name.toLowerCase()!="_printfield_") {
               allE.push(tempAllE[i]);
            }   
         } catch (nme) {
            allE.push(tempAllE[i]);
         }
      }
      useAllE=true;
   }   
}

function storeEleArrayInMemoryPassDoc(dc) {
   if (dc.forms.length==1 && allE==null) {
      var tempAllE=dc.forms[0].elements;
      allE=new Array();
      for (var i=0;i<tempAllE.length;i++) {
         if (tempAllE[i].name.toLowerCase()!="_printfield_") {
            allE.push(tempAllE[i]);
         }   
      }
      useAllE=true;
   }   
}

// Return best array
function getEleArrayPassDoc(dc,i) {
   if (!useAllE)
      return dc.forms[i].elements;
   else 
      return allE;
}

// Return best array
function getEleArray(i) {
   if (!useAllE)
      return document.forms[i].elements;
   else 
      return allE;
}

// RETURN ARRAY OF ALL ELEMENTS MATCHING NAME
function getFormElementArrayByIdPassDoc(dc,inString) {

   storeEleArrayInMemoryPassDoc(dc);

   var _elementArray=new Array();
   // construct element array
   for (var i=0;i<dc.forms.length;i++) {
      var e=getEleArrayPassDoc(dc,i);
      for (var j=0;j<e.length;j++) {
         if (e[j].name==inString) {
            _elementArray.push(e[j]);
         }
      }
   }
   // IF NO MATCH, TRY AGAIN IGNORING CASE (SAS IS CASE-INSENSITIVE)
   if (_elementArray.length<1) {
      for (var i=0;i<dc.forms.length;i++) {
         var e=getEleArray(dc,i);
         for (var j=0;j<e.length;j++) {
            if (e[j].name.toLowerCase()==inString.toLowerCase()) {
               _elementArray.push(e[j]);
            }
         }
      }
   }
   return _elementArray;
}

// RETURN ARRAY OF ALL ELEMENTS MATCHING NAME
function getFormElementArrayById(inString) {

   storeEleArrayInMemory();

   var _elementArray=new Array();
   // construct element array
   for (var i=0;i<document.forms.length;i++) {
      var e=getEleArray(i);
      for (var j=0;j<e.length;j++) {
         if (e[j].name==inString) {
            _elementArray.push(e[j]);
         }
      }
   }
   // IF NO MATCH, TRY AGAIN IGNORING CASE (SAS IS CASE-INSENSITIVE)
   if (_elementArray.length<1) {
      for (var i=0;i<document.forms.length;i++) {
         var e=getEleArray(i);
         for (var j=0;j<e.length;j++) {
            if (e[j].name.toLowerCase()==inString.toLowerCase()) {
               _elementArray.push(e[j]);
            }
         }
      }
   }
   return _elementArray;
}

// Set field to read only
function setEncodedFormValue(thisobj,thisval) {
   setFormValue(thisobj,decode64(thisval));
}

function setFormValue(thisobj,thisval) {
   try {
      // FOR SINGLETON OBJECTS
      // Returns first form element matching name
      if (thisobj.type.toLowerCase()!="radio" && thisobj.type.toLowerCase()!="checkbox") thisobj.value=thisval;
      else { 
         var inArray=getFormElementArrayById(thisobj.name);
         for(var i=0;i<inArray.length;i++) {
            if (inArray[i].value==thisval) inArray[i].checked='true';
         }
      }
   } catch (e) {
      // FOR OBJECT ARRAYS
      for (var j=0;j<thisobj.length;j++) {
         if (thisobj[j].type.toLowerCase()!="radio" && thisobj[j].type.toLowerCase()!="checkbox") thisobj[j].value=thisval;
         else { 
            var inArray=getFormElementArrayById(thisobj[j].name);
            for(var i=0;i<inArray.length;i++) {
               if (inArray[i].value==thisval) inArray[i].checked='true';
            }
            // already went through element array, so stop here.
            continue;
         }
      }
   }
}

// Retreive form values into XML file for transport
function getXmlTransStringFromDoc() {

   storeEleArrayInMemory();

   var xmlt="";
   // LOOP THROUGH FORM VALUES, OUTPUTING TO XML
   for (var i=0;i<document.forms.length;i++) {
      var e=getEleArray(i);
      for (var j=0;j<e.length;j++) {
         var _name=e[j].name;
         var _value=e[j].value.replace(/</g,"&lt;").replace(/>/g,"&gt;");
         var _checked=e[j].checked;
         var _type=e[j].type;
         if (_type=='button') continue;
         // note:  radio and checkbox values only counted if checked
         else if ((_type=='radio' || _type=='checkbox') && !_checked) continue;
         // note:  output only if contains value
         else if (trim(_value)=="") continue;
         xmlt=xmlt+"<"+_name + ">" + _value + "</" + _name + ">\n";
      }   
   }
   return encodeURI(xmlt);
}

// Retreive form values into XML file for transport
function getXmlTransStringFromObj(obj) {
   var xmlt="";
   // LOOP THROUGH FORM VALUES, OUTPUTING TO XML
   for (var j=0;j<obj.form.length;j++) {
      var _name=obj.form.elements[j].name;
      var _value=obj.form.elements[j].value.replace(/</g,"&lt;").replace(/>/g,"&gt;");
      var _checked=obj.form.elements[j].checked;
      var _type=obj.form.elements[j].type;
      if (_type=='button') continue;
      // note:  radio and checkbox values only counted if checked
      else if ((_type=='radio' || _type=='checkbox') && !_checked) continue;
      // note:  output only if contains value
      else if (trim(_value)=="") continue;
      xmlt=xmlt+"<"+_name + ">" + _value + "</" + _name + ">\n";
   }
   return encodeURI(xmlt);
}

// Set field to read only
function setReadOnly(thisobj) {
   // Returns first form element matching name
   if (thisobj.nodeName.toLowerCase()!="select") thisobj.readOnly='true';
   else thisobj.disabled='true'; 
}

// Set field to writable
function setWritable(thisobj) {
   // Returns first form element matching name
   if (thisobj.nodeName.toLowerCase()!="select") thisobj.readOnly='false';
   else thisobj.disabled='false'; 
}


// RUN ALL FORM VALIDATION
function execValidate() {

   storeEleArrayInMemory();

   if (_docReady!=1 && _validateStart=="N") {
      return;
   }
   // Returns first form element matching name
   var _nameArray=new Array();
   for (var i=0;i<document.forms.length;i++) {
      var e=getEleArray(i);
      for (var j=0;j<e.length;j++) {
         _nameArray.push(e[j].name); 
      }
   }

   // pull unique element names
   _nameArray.sort();
   var _nameArray2=new Array();
   for (var i=0;i<_nameArray.length;i++) {
      try {
         if (_nameArray[i]!=_nameArray[i-1]) {
            _nameArray2.push(_nameArray[i]);
         }
      } catch (e) {}
   }

   // execute validation script for each unique name
   for (var kk1=0;kk1<_nameArray2.length;kk1++) {
      try {
         var vfe=getFormElementById(_nameArray2[kk1]);
         // Alternate data entry screens need to run focus to correctly run validation
         try {
            var junkvar=getFormElementById("FormDataOK").value;
         } catch (ve3) {
            try { vfe.select(); } catch (e2) { }
         }
         // skip verification on readonly elements
         if (!vfe.readOnly) {
            // execute verification script
            vscript_allvars(vfe);
         }
      } catch (e) { }
   }

}

// GET VALUE FOR RADIO BUTTON FIELD
function getRadioValue(inString) {
   var inArray=getFormElementArrayById(inString);
   for(var i=0;i<inArray.length;i++) {
      if (inArray[i].checked) return inArray[i].value;
   }
   return "";
}

// GET VALUE FOR RADIO BUTTON FIELD
function getRadioValuePassDoc(dc,inString) {
   var inArray=getFormElementArrayByIdPassDoc(dc,inString);
   for(var i=0;i<inArray.length;i++) {
      if (inArray[i].checked) return inArray[i].value;
   }
   return "";
}

// GET VALUE FOR FORM ELEMENT REGARDLESS OF TYPE
function getFormElementValueById(inString) {
   var thisobj=getFormElementById(inString);
   if (thisobj.type.toLowerCase()!="radio" && thisobj.type.toLowerCase()!="checkbox") {
      return thisobj.value;
   } else {
      return getRadioValue(inString);
   }

}

// GET VALUE FOR FORM ELEMENT REGARDLESS OF TYPE
function getFormElementValueByIdPassDoc(dc,inString) {
   var thisobj=getFormElementByIdPassDoc(dc,inString);
   if (thisobj.type.toLowerCase()!="radio" && thisobj.type.toLowerCase()!="checkbox") {
      return thisobj.value;
   } else {
      return getRadioValuePassDoc(dc,inString);
   }

}

var InterimSaveDwrReply=null;
var InterimSaveFailNotify=true;

function interimSave() {

   InterimSaveDwrReply = function(replydata) {
      var _reply=replydata.toUpperCase();
      if (!(InterimSaveFailNotify && _reply=="INTERIMSAVEOK")) {
         // Only notify about interim save failures once
         InterimSaveFailNotify=false;
         alert("WARNING:  The interim save function failed.  Although it will continue trying, " + 
               "there will likely be no interim saves during this interview.\n\n" +
               "This is the only notification you will receive regarding this.");
      }
   }

   // Perform Interim Save
   try {
      if (getFormElementById("ID").value.length>0) {
         var _pV=getXmlTransStringFromDoc();
         ProcessFormDwr.interimSave(_pV,InterimSaveDwrReply);
         // Reset time counter
         _interimTime=new Date();
      }
   } catch (e) {
   }
}

function skipThis(ele) {
   ele.blur();
   document.selection.empty();
   ele.form[(getIndex(ele)+1) % ele.form.length].focus();
   ele.form[(getIndex(ele)+1) % ele.form.length].select();
}

function getIndex(ele) {
  var index = -1, i = 0, found = false;
  while (i < ele.form.length && index == -1)
  if (ele.form[i] == ele)index = i;
  else i++;
  return index;
}

function containsElement(arr, ele) {
  var found = false, index = 0;
  while(!found && index < arr.length)
  if(arr[index] == ele)
  found = true;
  else
  index++;
  return found;
}

function autoTab(e,n) {
   if (e.value.length>=n && !containsElement([0,8,9,16,17,18],event.keyCode)) {
      skipThis(e)
   } else {
      e.focus();
      e.select();
   }
}

function permit(e,a) {
   var i=0, ok=false;
   while (i < a.length) {
      if (e.value==a[i] || trim(e.value)=='') {
         ok=true;
         break;
      }
      i++;
   }
   if (!ok) {
      e.value='';
   }
}

////////////////////
////////////////////
////////////////////
////////////////////
////////////////////


// DateTime Validation Script
function validateDate(inFunction,varPattern,rawVal) {
   //
   // SHOULD NOT BE NEEDED
   //if (!(inFunction.match(/(VDT)|(VDTR)/))) {
   //   return 0;
   //}
   if (trim(rawVal).length==0) {
      return 1;
   }
   var haveTimePart=false;
   if (varPattern.toUpperCase().indexOf("HH")>=0) {
      haveTimePart=true;
   }
   //
   if (varPattern.toUpperCase().indexOf("YY")>=0) {
      var mo=NaN;
      var day=NaN;
      var yr=NaN;
      var yrStart;
      var yrString;
      var yrLength;
      yrStart=varPattern.toUpperCase().indexOf("YYYY");
      if (yrStart<0) {
         yrStart=varPattern.toUpperCase().indexOf("YY");
         yrString=rawVal.toString().substr(yrStart,2);
         yrLength=2;
      }
      else {
         yrString=rawVal.toString().substr(yrStart,4);
         if (yrString.length<4) {
            yrString="";
         }
         yrLength=4;
      }
      if (yrString.length>=1) {
         yr=Number(yrString);
      }
      if (yrLength==2) {
         if (yr>=0 && yr<=29) {
            yr=yr+2000;
         }
         else if (yr>=30 && yr<=99) {
            yr=yr+1900;
         }
      }
      if (yrString.match(/[.-]/)) {
         yr=NaN;
      }
      //
      var moStart=varPattern.toUpperCase().indexOf("MM");
      var moString=rawVal.toString().substr(moStart,2);
      if (moString.length>=1) {
         // Subtract 1 because mo runs 0-11 in Date object
         mo=(Number(moString)-1);
         if (mo<0 || mo>11 || moString.match(/[.-]/)) {
            mo=NaN;
         }
      }
      //
      var dayStart=varPattern.toUpperCase().indexOf("DD");
      var dayString=rawVal.toString().substr(dayStart,2);
      if (dayString.length>=1) {
         day=Number(dayString);
         if (day<1 || day>31 || dayString.match(/[.-]/)) {
            day=NaN;
         }
      }
      //
      var date=new Date(yr,mo,day);
      // Check that slashes are in correct location
      var minVal=Math.min(rawVal.toString().length,varPattern.length);
      if (minVal>=1) {
         for (var i=1;i<minVal;i++) {
            if (varPattern.charAt(i).valueOf()=="/".valueOf()) {
               if (!(rawVal.toString().charAt(i).valueOf()=="/".valueOf())) {
                  day=NaN;
               }
            }
         }
      }
      // CHECK OVERALL LENGTH
      if (rawVal.length!=varPattern.length) {
         day=NaN;
      }
      // Have to check getDate() (returns day) against day variable because
      // date function will do wierd things like turn Date(02,32,2004) to 03/04/2004
      if (date.getDate()==day && day>0) {
         if (!haveTimePart) {
            return 1;
         } else {
            // Continue to time part processing
         }
      } else {
         return 0;
      }
   }
   if (haveTimePart) {
      var hour=NaN;
      var min=NaN;
      //
      var hourStart=varPattern.toUpperCase().indexOf("HH");
      var hourString=rawVal.toString().substr(hourStart,2);
      if (hourString.length>=1) {
         hour=Number(hourString);
         if (hour<0 || hour>23 || hourString.match(/[.-]/)) {
            hour=NaN
         }
      }
      // Use lastIndexOf here in case searching a DateTime field which will have MM for month
      var minStart=varPattern.toUpperCase().lastIndexOf("MM");
      var minString=rawVal.toString().substr(minStart,2);
      if (minString.length>=1) {
         min=Number(minString);
         if (min<0 || min>59 || minString.match(/[.-]/)) {
            min=NaN
         }
      }
      // CHECK OVERALL LENGTH
      if (rawVal.length!=varPattern.length) {
         min=NaN;
      }
      // Check that colons are in correct location
      var minVal=Math.min(rawVal.toString().length,varPattern.length);
      if (minVal>=1) {
         for (var i=1;i<minVal;i++) {
            if (varPattern.charAt(i).valueOf()==":".valueOf()) {
               if (!(rawVal.toString().charAt(i).valueOf()==":".valueOf())) {
                  min=NaN;
               }
            }
         }
      }
      // CHECK OVERALL LENGTH
      if (rawVal.length!=varPattern.length) {
         min=NaN;
      }
      // Have to check getDate() (returns day) against day variable because
      // date function will do wierd things like turn Date(02,32,2004) to 03/04/2004
      if (hour>=0 && min>=0) {
         return 1;
      } else {
         return 0;
      }
   }
}

////////////////////
////////////////////
////////////////////
////////////////////
////////////////////

// Text Field Pattern Validation Script
function validateTextPattern(inFunction,varPattern,rawVal) {
   if (rawVal.match("^" + varPattern + "$")) return 1;
   else return 0;
}

////////////////////
////////////////////
////////////////////
////////////////////
////////////////////


// Numeric Field Pattern Validation Script
function validateNumericPattern(inFunction,varPattern,rawVal) {
   if (rawVal.match("^" + varPattern + "$")) return 1;
   else return 0;
}

////////////////////
////////////////////
////////////////////
////////////////////
////////////////////


// Main Validation Script
function validateMain(inFunction,inVar,rawVal,varPopUp,varLength,varPattern,varMissingArray) {

   _saveStatus="NR";

   var isInError=false;
   var errReason="";
   var msgVar="";

   // to eliminate errors
   if (rawVal==null) {
      rawVal=" ";
   }

   rawVal=rawVal.toString();

   if (!(rawVal==null || rawVal=="")) {
      rawVal=trim(rawVal);
   }

   // CHECK REQUIRED STATUS IF CALLING FUNCTION IS INDICATES REQUIRED VARIABLE
   if (inFunction.match(/(VBNR)|(VBIR)|(VBTR)|(VDTR)|(VRBR)|(VDDR)/)) {
      if (rawVal == null || rawVal=="" || rawVal.toString().match(/^ *$/)) {
         //THIS MESSAGE DISPLAYS FOR REQUIRED VALUES
         if (varPopUp.toUpperCase()=="Y" && _docReady==1 && _preSubmit!=1 && _resetVar!=1) {
           var nButton=alert({ cMsg: "Required Value!!!",cTitle: "Required Value", nIcon: 2 });
         }
         isInError=true;
         errReason="Field Value Missing";
      }
   }

   // CHECK VARIABLE LENGTH
   if (!isInError && varLength>0) {

      if (rawVal.toString().length>varLength) {

         //THIS MESSAGE DISPLAYS FOR REQUIRED VALUES
         if (_docReady==1 && _preSubmit!=1 && _resetVar!=1 && varPopUp.toUpperCase()=="Y") {
           var nButton=alert({ cMsg: "ERROR:  The length of the value you entered exceeds the" +
               "length of the dataset variable.  This value will be trucated in the dataset.  \n\n" +
               "   The storage length for this variable is:  " + varLength.toString(),
               cTitle: "Variable Length Exceeded", nIcon: 2 });
         }
         isInError=true;
         errReason="Invalid Field Length";
      }
   }
   // IGNORE NEGATIVE VALUES FOR NUMBERS
   if (inFunction.match(/(VBI)|(VBIR)|(VBN)|(VBNR)/)) {
      rawVal=rawVal.replace(/^-/,"");
   }

   // THIS IF STATEMENT DEFINES VALID VALUES
   if (  !isInError &&
             (inFunction.match(/(VBNR)|(VBN)|(VBIR)|(VBI)|(VDT)|(VDTR)/)) && (
             (inFunction.match(/(VBI)|(VBIR)/) && ((rawVal.search(/[.]/)>0) || !((rawVal%1==0) || 
                   (varMissingArray.indexOf(rawVal.toString().toUpperCase())>=0 && rawVal.toString().length==1)
              ))) ||
             (inFunction.match(/(VBNR)|(VBN)/) && (!((rawVal%1>=0) || 
                   (varMissingArray.indexOf(rawVal.toString().toUpperCase())>=0 && rawVal.toString().length==1)
              ))) ||
             (inFunction.match(/(VDT)|(VDTR)/) && (!((validateDate(inFunction,varPattern,rawVal)==1) ||
                   (varMissingArray.indexOf(rawVal.toString().toUpperCase())>=0 && rawVal.toString().length==1)
              )))
            )
           ) {
      // THIS MESSAGE DISPLAYS FOR INVALID VALUES
      if (_docReady==1 && _preSubmit!=1 && _resetVar!=1) {
         // PUT TOGETHER MESSAGE STRING
         msgVar="Invalid value:  Value must be ";
         if (inFunction=="VBNR" || inFunction=="VBN") {
            msgVar=msgVar + "a numeric field ";
         }
         else if (inFunction=="VBIR" || inFunction=="VBI") {
            msgVar=msgVar + "an integer field ";
         }
         msgVar=msgVar + "or valid missing value identifier.\n\n";
         msgVar=msgVar + "        Valid missing value codes are:  ";

         // obtain valid missing values from array
         for (var i=0;i<varMissingArray.length;i++) {
            if (!(varMissingArray[i]==null || varMissingArray[i]=="")) {
               if (i^=0) { 
                  msgVar=msgVar + ", ";
               }
               msgVar=msgVar + varMissingArray[i];
            }
         }
      }
      if (varPopUp.toUpperCase()=="Y" && _docReady==1 && _preSubmit!=1 && _resetVar!=1) {
         // validateRmsNumericRequired
         alert( { cMsg: msgVar, cTitle: "Invalid Value", nIcon: 2 });
      }
      isInError=true;
      if (msgVar==null || msgVar==undefined || msgVar.length<=0) {
         errReason="Invalid Value";
      } else {
         errReason=msgVar;
      }
   }
   // EVALUATE PATTERNS
   else if (!isInError && varPattern!=undefined && varPattern!="NONE" && (!(inFunction.match(/(VDT)|(VDTR)/)))) {
      if (
           (inFunction.match(/(VBT)|(VBTR)/) && (!(validateTextPattern(inFunction,varPattern,rawVal)==1))) || 
           (inFunction.match(/(VBI)|(VBIR)|(VBN)|(VBNR)/) && (!(validateNumericPattern(inFunction,varPattern,rawVal)==1)) &&
             (!(varMissingArray.indexOf(rawVal.toString().toUpperCase())>=0 && rawVal.toString().length==1)) 
           ) 
         ) {
         // Create missing value matching pattern 
         matchList="";
         if (varMissingArray!="_NA_" && varMissingArray!="NONE" && varMissingArray!="" &&
             varMissingArray!=null && varMissingArray!=undefined) {
            matchList=varMissingArray.replace(/,/g,"");
         } 
         if (matchList.length>1) {
            matchExp=new RegExp("^ *[" + matchList + "] *$","i");
         } else {
            matchExp=new RegExp("^ *$");
         }
         // Evaluate constraint code
         if (!(rawVal.toString().match(matchExp))) { 
            if (_docReady==1 && _preSubmit!=1 && _resetVar!=1 && varPopUp.toUpperCase()=="Y") {
               // PUT TOGETHER MESSAGE STRING
               msgVar="Invalid value:  Value must match pattern: " + varPattern;
               // validateRmsNumericRequired
               alert( { cMsg: msgVar, cTitle: "Invalid Value", nIcon: 2 });
            }
            isInError=true;
            errReason="Value doesn't match pattern (" + varPattern + ")";
         }
      }
   }
   // Update _errArray & _numErrors
   errUpdate(inVar,isInError,errReason);

   // Put this section in try block so alternate data entry screens have functional
   // validation
   try {

      // MODIFIY FormDataOK FIELD BASED ON ERROR FLAG
      if (_numErrors>=1) {
         getFormElementById("FormDataOK").value=0;
         getFormElementById("FormDataOK").style.backgroundColor=_errorColor;
      }
      else {
         getFormElementById("FormDataOK").value=1;
         getFormElementById("FormDataOK").style.backgroundColor=_bgColor;
      }
      // Make sure RMS information has been passed
      if (_docReady==1) {
         _serverName=_rmsPassValueObject.servername;
         if ((_serverName==null || _serverName==undefined || _serverName.length<1) && _alreadyNotify!=1) {
            _alreadyNotify=1;
            alert("WARNING!!!!:  The RMS Server environment is unknown.  The form will not be able " +
                      "to be submitted.  Please reload this form from within the RMS environment." + "\n\n" +
                      "If this form was loaded from within the RMS, an error has occurred.  Please " +
                      "try reloading this form, by returning to the previous selection screen and " +
                      "bringing up the form again, so the form has the information it needs to be submitted.");
         } 
      }
   } catch (em) {
      // DO NOTHING
   }

   // SET VARIABLE WITH COLOR THAT WILL BE USED FOR FIELD COLOR

   if (isInError && _resetVar!=1 && _loadScriptRun=="Y") {
      _fColor=_errorColor;
   }
   else {
      _fColor=_bgColor;
      if (_bgColor==undefined) {
         _fColor="#ffffff";
      }
   }

   if (_docReady==1 && _preSubmit!=1 && _resetVar!=1) {

      // DO INTERIM SAVE IF NECESSARY
      if (_iSaveInt>=1 && new Date().getTime()-_interimTime>(_iSaveInt*60*1000)) {
         interimSave();
      }
   }

   // Set global fields, if any
   if (_docReady==1 && hasGlobal(inVar)) {
      var garray=getGlobalElements(inVar);
      for (var gi=0;gi<garray.length;gi++) {
         garray[gi].value=rawVal;
      }
   } 
   
}

// errUpdate function (updates _errArray and _numErrors)
function errUpdate(inVar,isInError,errReason) {
   already=false;
   for (var i=0;i<_errArray.length;i++) {
      if (inVar==_errArray[i]) {
         already=true;
         if (!isInError) {
            _errArray.splice(i,1);
            _errTextArray.splice(i,1);
         } else {
            // update reason
            _errTextArray.splice(i,1,errReason);
         }
      }
   }
   // add field to _errArray if it isn't already in
   if (isInError && !already) {
      _errArray.push(inVar);
      _errTextArray.push(errReason);
   } 
   _numErrors=_errArray.length;
}

////////////////////
////////////////////
////////////////////
////////////////////
////////////////////

function tf_focus(thisobj) {
   if (_docReady==1) {
      try {
         eval("e_" + thisobj.name + "(thisobj)");
      } catch (e) {
         eval("window.document.e_" + thisobj.name + "(thisobj)");
      }
      if (thisobj.readOnly) {
         skipThis(thisobj);
      } else {
         thisobj.select();
      }
   }   
   _changeField="_EV_";

}

function tf_blur(thisobj) {

   //Trim Field (if statement used to cause
   //   validate script to execute only if value is changed.  Otherwise
   //   popups come up twice)  Actually, for some reason popups don't
   //   come up twice anyway when value change is in an "IF" statement.
   //   This is probably a bug in Adobe, but it works for us.
    
   if (thisobj.value!=null && thisobj.value!=trim(thisobj.value)) {
      thisobj.value=trim(thisobj.value);
   }
   _changeValidate=0;
   tf_validate(thisobj);

}

function tf_validate(thisobj,passstring,passloc) {
 
   // skip validation while typing
   if (event.type=="keyup") {
      return;
   }
   if (thisobj.type.toLowerCase()=="textarea") {
      // _varPattern not set for textarea fields in GUI.  Set it here.  Otherwise it
      // will pick pattern from previous field.
      _varPattern="NONE";
   }
   if ((_docReady==1 && 
        (_changeField==thisobj.name || _changeField=="_EV_")) ||
       (_validateStart=="Y" && _docReady!=1)) {
    
   if (thisobj.value!=null && thisobj.value!=undefined) {
      // Code to change case
      tempvar=thisobj.value;
      tempVP=_varPattern;
      if (tempvar!=null && tempvar.length>=1 && _varCaps=='Y') {
         thisobj.value=thisobj.value.toUpperCase();
      }
      // Code to handle SSN's
      tempvar=trim(thisobj.value);
      if (tempVP=="999-99-9999" || tempVP=="999999999") {
         if (tempvar.length==9 && tempvar.match(/^[0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9]$/)) {
            tempvar=tempvar.substr(0,3) + "-" + tempvar.substr(3,2) + "-" + tempvar.substr(5,4);
         } else if (tempvar.length==11 && tempvar.match(/^[0-9][0-9][0-9][\W][0-9][0-9][\W][0-9][0-9][0-9][0-9]$/)) {
            tempvar=tempvar.substr(0,3) + "-" + tempvar.substr(4,2) + "-" + tempvar.substr(7,4);
         } 
         if (tempVP=="999999999" &&
             tempvar.match(/^[0-9][0-9][0-9][\W][0-9][0-9][\W][0-9][0-9][0-9][0-9]$/)) {
            tempvar=tempvar.replace(/[\W]/g,"");
         }
         thisobj.value=tempvar;
      // Code to handle Phone Numbers
      } else if (tempVP=="999-999-9999" || 
                 tempVP=="(999) 999-9999" ||
                 tempVP=="9999999999" ||
                 tempVP=="999-999-9999xaZZZZZZ" || 
                 tempVP=="(999) 999-9999xaZZZZZZ" ||
                 tempVP=="999999999xaZZZZZZ" 
                 ) {
         if (tempvar.length>=10) {        
            tempvar=tempvar.replace(/[\D]/g,"");
            if (tempvar.length==10) {        
               if (tempVP=="999-999-9999" ||
                   tempVP=="999-999-9999xaZZZZZZ"
                  ) {
                  tempvar=tempvar.substr(0,3) + "-" + tempvar.substr(3,3) + "-" + tempvar.substr(6,4);
               } else if (tempVP=="(999) 999-9999" || 
                          tempVP=="(999) 999-9999xaZZZZZZ"
                 ) {
                  tempvar="(" + tempvar.substr(0,3) + ") " + tempvar.substr(3,3) + "-" + tempvar.substr(6,4);
               }
            } else if (tempvar.length>10) {        
               if (tempVP=="999-999-9999xaZZZZZZ") {
                  tempvar=tempvar.substr(0,3) + "-" + tempvar.substr(3,3) + "-" + tempvar.substr(6,4) + " X" + tempvar.substr(10);
               } else if (tempVP=="(999) 999-9999xaZZZZZZ") {
                  tempvar="(" + tempvar.substr(0,3) + ") " + tempvar.substr(3,3) + "-" + tempvar.substr(6,4) + " X" + tempvar.substr(10);
               }
            }

         }
         thisobj.value=tempvar;
      } 
   } 

   if (_changeValidate!=1) {
    
   // Execute validation script for this object .
   eval("validateRmsTextField(\"" + thisobj.name + "\",thisobj.value," +
        _varLength +  "," +
        "\"" + _varRequired +  "\"" + "," +
        "\"" + _varPopUp +  "\"" + "," +
        "\"" + _varPattern +  "\"" + "," +
        "\"" + _varMissingArray +  "\"" + "," +
        "\"" + _conList +  "\"" + "," +
        "\"" + _conCode +  "\"" + "," +
        "\"" + _conPopUp +  "\"" +
      ");");
   
   if (passloc==1) {
      eval(passstring);
   }

   // This statement must exist and be LAST!  Set field color.
   var inArray=getFormElementArrayById(thisobj.name);
   for(i=0;i<inArray.length;i++) {
      inArray[i].style.backgroundColor=_fColor; 
   }
   }
   else {
      // this statement in the else clause keeps adobe from issuing a
      // "validation failed" message.  This is probably a bug.
      var inArray=getFormElementArrayById(thisobj.name);
      for(i=0;i<inArray.length;i++) {
         inArray[i].style.backgroundColor=_fColor; 
      }
   }
   // RESET FIELD
   _varMissingArray="_NA_";
   }

}

function tf_keyup(thisobj) {

   _changeField = thisobj.name;
   _scrptsrc = null;
   _changeValidate=1;
    
   tf_validate(thisobj);
   if (_varCaps.toUpperCase()=="Y") {
      thisobj.value=thisobj.value.toUpperCase();
   }
   if (thisobj.value.length>_varLength) {
      thisobj.value=thisobj.value.substr(0,_varLength);
      thisobj.style.backgroundColor=_errorColor; 
   }
   onChangeText();
   _changeValidate=0;

   // Select existing value when field is tabbed to
   if (event.keyCode==9) {
      thisobj.select();
   }

}

// Validation script for RMS_Text_Field
function validateRmsTextField(inVar,rawVal,varLength,varRequired,varPopUp,varPattern,varMissingArray,conList,conCode,conPopUp) {

   if (varRequired.toUpperCase()=="Y") {
      inFunction="VBTR";
   } else {
      inFunction="VBT";
   }
   if (varMissingArray==null || varMissingArray==undefined || varMissingArray=="") {
      varMissingArray="_NA_";
   }
   varMissingArray=varMissingArray.replace(/,/g,"");
   validateMain(inFunction,inVar,rawVal,varPopUp,varLength,varPattern,varMissingArray);
   // check colors to determine error status
   if (conList.toUpperCase()!="NONE" && _fColor!=_errorColor) {
      addRestrictList(inVar,rawVal,conList,conPopUp,varMissingArray);
   }
   if (conCode.toUpperCase()!="NONE" && _fColor!=_errorColor) {
      addRestrictCode(inVar,rawVal,conCode,conPopUp,varMissingArray);
   }
}

////////////////////
////////////////////
////////////////////
////////////////////
////////////////////

function nf_focus(thisobj) {
   if (_docReady==1) {
      try {
         eval("e_" + thisobj.name + "(thisobj)");
      } catch (e) {
         eval("window.document.e_" + thisobj.name + "(thisobj)");
      }
      if (thisobj.readOnly) {
         skipThis(thisobj);
      } else {
         thisobj.select();
      }
   }   
   _changeField="_EV_";
}

function nf_blur(thisobj) {

   // Trim Field (if statement used to cause
   //   validate script to execute only if value is changed.  Otherwise
   //   popups come up twice)  Actually, for some reason popups don't
   //   come up twice anyway when value change is in an "IF" statement.
   //   This is probably a bug in Adobe, but it works for us.
    
   if (thisobj.value!=null && thisobj.value!=trim(thisobj.value)) {
      thisobj.value=trim(thisobj.value);
   }
   _changeValidate=0;
   nf_validate(thisobj);

}

function nf_validate(thisobj,passstring,passloc) {
 
   // skip validation while typing
   if (event.type=="keyup") {
      return;
   }

   if ((_docReady==1 && (_changeField==thisobj.name || _changeField=="_EV_")) ||
       (_validateStart=="Y" && _docReady!=1)) {

      tempvar=thisobj.value;
      if (tempvar!=null) {
         thisobj.value=thisobj.value.toUpperCase();
      }
       
      if (_changeValidate!=1) {
    
         // Execute validation script for this object .
         eval("validateRmsNumericField(\"" + thisobj.name + "\",thisobj.value," +
              "\"" + _varInteger +  "\"" + "," +
              "\"" + _varRequired +  "\"" + "," +
              "\"" + _varPopUp +  "\"" + "," +
              "\"" + _varMissingArray +  "\"" + "," +
              "\"" + _varPattern +  "\"" + "," +
              "\"" + _conList +  "\"" + "," +
              "\"" + _conCode +  "\"" + "," +
              "\"" + _conPopUp +  "\"" +
            ");");
   
         if (passloc==1) {
            eval(passstring);
         }
          
         // This statement must exist and be LAST!  Set field color.
         var inArray=getFormElementArrayById(thisobj.name);
         for(i=0;i<inArray.length;i++) {
            inArray[i].style.backgroundColor=_fColor; 
         }
      }
      else {
         // this statement in the else clause keeps adobe from issuing a
         // "validation failed" message.  This is probably a bug.
         var inArray=getFormElementArrayById(thisobj.name);
         for(i=0;i<inArray.length;i++) {
            inArray[i].style.backgroundColor=_fColor; 
         }
      }
      // RESET FIELD
      _varMissingArray="_NA_";
   }

}

function nf_keyup(thisobj) {

   _changeField = thisobj.name;
   _scrptsrc = null;
   thisobj.value = thisobj.value.toUpperCase();
    
   _changeValidate=1;
   nf_validate(thisobj);
   onChangeNum();
   _changeValidate=0;

   // Select existing value when field is tabbed to
   if (event.keyCode==9) {
      thisobj.select();
   }

}

// Validation script for RMS_Numeric_Field
function validateRmsNumericField(inVar,rawVal,varInteger,varRequired,varPopUp,varMissingArray,
      varPattern,conList,conCode,conPopUp) {

   if (varInteger.toUpperCase()=="Y" && varRequired.toUpperCase()=="Y") {
      inFunction="VBIR";
   } else if (varInteger.toUpperCase()=="Y" && varRequired.toUpperCase()=="N") {
      inFunction="VBI";
   } else if (varInteger.toUpperCase()=="N" && varRequired.toUpperCase()=="Y") {
      inFunction="VBNR";
   } else if (varInteger.toUpperCase()=="N" && varRequired.toUpperCase()=="N") {
      inFunction="VBN";
   }
   varLength=-1;
   if (varMissingArray=="NONE") {
      varMissingArray="";
   }
   varMissingArray=varMissingArray.replace(/,/g,"");
   validateMain(inFunction,inVar,rawVal,varPopUp,varLength,varPattern,varMissingArray);
   // check colors to determine error status
   if (conList.toUpperCase()!="NONE" && _fColor!=_errorColor) {
      addRestrictList(inVar,rawVal,conList,conPopUp,varMissingArray);
   }
   if (conCode.toUpperCase()!="NONE" && _fColor!=_errorColor) {
      addRestrictCode(inVar,rawVal,conCode,conPopUp,varMissingArray);
   }
}

////////////////////
////////////////////
////////////////////
////////////////////
////////////////////


function rb_focus(thisobj) {
   if (_docReady==1) {
      try {
         eval("e_" + thisobj.name + "(thisobj)");
      } catch (e) {
         eval("window.document.e_" + thisobj.name + "(thisobj)");
      }
   }   
   _changeField="_EV_";

}

function rb_click(thisobj) {

   _changeValidate=0;
   rb_validate(thisobj);

}

function rb_validate(thisobj,passstring,passloc) {

   if ((_docReady==1 && 
        (_changeField==thisobj.name || _changeField=="_EV_")) ||
       (_validateStart=="Y" && _docReady!=1 )) {

      // Execute validation script for this object .
      eval("validateRmsRadioBox(\"" + thisobj.name + "\",getRadioValue(thisobj.name)," +
           "\"" + _varRequired +  "\"" + "," +
           "\"" + _varPopUp +  "\"" + "," +
           "\"" + _conCode +  "\"" + "," +
           "\"" + _conPopUp +  "\"" +
         ");");
      
      if (passloc==1) {
         eval(passstring);
      }
   
      // Change field color
      var inArray=getFormElementArrayById(thisobj.name);
      for(i=0;i<inArray.length;i++) {
         inArray[i].style.backgroundColor=_fColor; 
      }
    
   }
}

function rb_keyup(thisobj) {

   _changeValidate=0;
   rb_validate(thisobj);

}

// Validation script for RMS_Radio_Box
function validateRmsRadioBox(inVar,rawVal,varRequired,varPopUp,conCode,conPopUp) {

   if (varRequired.toUpperCase()=="Y") {
      inFunction="VRBR";
   } else {
      inFunction="VRB";
   }
   varLength=-1;
   varPattern="NONE";
   varMissingArray="_NA_";
   validateMain(inFunction,inVar,rawVal,varPopUp,varLength,varPattern,varMissingArray);
   if (conCode.toUpperCase()!="NONE" && _fColor!=_errorColor) {
      addRestrictCode(inVar,rawVal,conCode,conPopUp,varMissingArray);
   }

}

////////////////////
////////////////////
////////////////////
////////////////////
////////////////////

function dd_focus(thisobj) {
   if (_docReady==1) {
      try {
         eval("e_" + thisobj.name + "(thisobj)");
      } catch (e) {
         eval("window.document.e_" + thisobj.name + "(thisobj)");
      }
   }   
   _changeField="_EV_";

}

function dd_click(thisobj) {

   _changeField = thisobj.name;
   dd_validate(thisobj);
   _scrptsrc = null;

}

function dd_validate(thisobj,passstring,passloc) {

   if ((_docReady==1 && 
        (_changeField==thisobj.name || _changeField=="_EV_")) ||
       (_validateStart=="Y" && _docReady!=1)) {
       
      // Execute validation script for this object .
      eval("validateRmsDropDown(\"" + thisobj.name + "\",thisobj.value," +
           "\"" + _varRequired +  "\"" + "," +
           "\"" + _varPopUp +  "\"" + "," +
           "\"" + _conCode +  "\"" + "," +
           "\"" + _conPopUp +  "\"" +
         ");");

      // This statement must exist and be LAST!  This changes field color according
      // to the output of above functions.;

      // Change field color
      var inArray=getFormElementArrayById(thisobj.name);
      for(i=0;i<inArray.length;i++) {
         inArray[i].style.backgroundColor=_fColor; 
      }
ackgroundColor=_fColor;
   }

   if (passloc==1) {
      eval(passstring);
   }

}

function dd_keyup(thisobj) {

   _changeField = thisobj.name;
   dd_validate(thisobj);
   _scrptsrc = null;

}

// Validation script for RMS_Drop_Down
function validateRmsDropDown(inVar,rawVal,varRequired,varPopUp,conCode,conPopUp) {

   if (varRequired.toUpperCase()=="Y") {
      inFunction="VDDR";
   } else {
      inFunction="VDD";
   }
   varLength=-1;
   varPattern="NONE";
   varMissingArray="_NA_";
   validateMain(inFunction,inVar,rawVal,varPopUp,varLength,varPattern,varMissingArray);
   if (conCode.toUpperCase()!="NONE" && _fColor!=_errorColor) {
      addRestrictCode(inVar,rawVal,conCode,conPopUp,varMissingArray);
   }
}

////////////////////
////////////////////
////////////////////
////////////////////
////////////////////

function cb_focus(thisobj) {
   if (_docReady==1) {
      try {
         eval("e_" + thisobj.name + "(thisobj)");
      } catch (e) {
         eval("window.document.e_" + thisobj.name + "(thisobj)");
      }
   }   
   _changeField = thisobj.name;
   _scrptsrc = null;

}

function cb_blur(thisobj) {

   cb_validate(thisobj);

}

function cb_validate(thisobj,passstring,passloc) {

   if ((_docReady==1 && 
        (_changeField==thisobj.name || _changeField=="_EV_")) ||
       (_validateStart=="Y" && _docReady!=1)) {
       
      // Execute validation script for this object .
      eval("validateRmsCheckBox(\"" + thisobj.name + "\",thisobj.value," +
           "\"" + _conCode +  "\"" + "," +
           "\"" + _conPopUp +  "\"" +
         ");");

      if (passloc==1) {
         eval(passstring);
      }
       
      // This statement must exist and be LAST!  This changes field color according
      // to the output of above functions.;
      // Change field color
      var inArray=getFormElementArrayById(thisobj.name);
      for(i=0;i<inArray.length;i++) {
         inArray[i].style.backgroundColor=_fColor; 
      }
   }

}

function cb_keyup(thisobj) {

   cb_validate(thisobj);

}

// Validation script for RMS_Check_Box
function validateRmsCheckBox(inVar,rawVal,conCode,conPopUp) {

   inFunction="VCB";
   varLength=-1;
   varPopUp="N";
   varPattern="NONE";
   varMissingArray="_NA_";
   validateMain(inFunction,inVar,rawVal,varPopUp,varLength,varPattern,varMissingArray);
   if (conCode.toUpperCase()!="NONE" && _fColor!=_errorColor) {
      addRestrictCode(inVar,rawVal,conCode,conPopUp,varMissingArray);
   }
}

////////////////////
////////////////////
////////////////////
////////////////////
////////////////////

function dt_focus(thisobj) {
   if (_docReady==1) {
      try {
         eval("e_" + thisobj.name + "(thisobj)");
      } catch (e) {
         eval("window.document.e_" + thisobj.name + "(thisobj)");
      }
      if (thisobj.readOnly) {
         skipThis(thisobj);
      } else {
         thisobj.select();
      }
   }   
   _changeField="_EV_";

}

function dt_blur(thisobj) {

   // Trim Field (if statement used to cause
   //   validate script to execute only if value is changed.  Otherwise
   //   popups come up twice)  Actually, for some reason popups don't
   //   come up twice anyway when value change is in an "IF" statement.
   //   This is probably a bug in Adobe, but it works for us.

   if (thisobj.value!=null && thisobj.value!=trim(thisobj.value)) {
      thisobj.value=trim(thisobj.value);
   }
   _changeValidate=0;
   dt_validate(thisobj);

}

function dt_validate(thisobj,passstring,passloc) {
 
   // skip validation while typing
   if (event.type=="keyup") {
      return;
   }

   _oldValue=thisobj.value;

   if ((_docReady==1 && 
        (_changeField==thisobj.name || _changeField=="_EV_")) ||
       (_validateStart=="Y" && _docReady!=1)) {
      // Handle most common changes here.  Works better than onChangeDate.
      if (thisobj.value!=null && thisobj.value!=undefined) {
         tempvar=thisobj.value;
         tempVP=_varPattern;
         if (tempvar!=null) {
            thisobj.value=thisobj.value.toUpperCase();
         }
         tempvar=trim(thisobj.value);
         tempvar=tempvar.replace(/^[0-9][\W]/,"0$&");
         // Needs to be run twice to correctly do global change
         tempvar=tempvar.replace(/[^0-9][0-9][\W]/g,function(x) { return x.substring(0,1) + "0" + x.substring(1); } );
         tempvar=tempvar.replace(/[^0-9][0-9][\W]/g,function(x) { return x.substring(0,1) + "0" + x.substring(1); } );
         tempvar=tempvar.replace(/[\W][0-9]$/,function(x) { return x.substring(0,1) + "0" + x.substring(1); } );
         tempvar=tempvar.replace(/[\W]/g,"");
         if (tempVP.toUpperCase()=="MM/DD/YY") {
            if (tempvar.length==6) {
               tempvar=tempvar.substr(0,2) + "/" + tempvar.substr(2,2) + "/" + tempvar.substr(4,2);
            } else if (tempvar.length==8) {
               tempvar=tempvar.substr(0,2) + "/" + tempvar.substr(2,2) + "/" + tempvar.substr(6,2);
            }
         } else if (tempVP.toUpperCase()=="MMDDYY") {
            if (tempvar.length==8) {
               tempvar=tempvar.substr(0,4) + tempvar.substr(6,2);
            }
         } else if (tempVP.toUpperCase()=="MM/DD/YYYY") {
            if (tempvar.length==8) {
               tempvar=tempvar.substr(0,2) + "/" + tempvar.substr(2,2) + "/" + tempvar.substr(4,4);
            } else if (tempvar.length==6) {
               if (tempvar.substr(4,1).match(/[01]/)) {
                  tempvar=tempvar.substr(0,2) + "/" + tempvar.substr(2,2) + "/20" + tempvar.substr(4,2);
               } else {
                  tempvar=tempvar.substr(0,2) + "/" + tempvar.substr(2,2) + "/19" + tempvar.substr(4,2);
               }
            }
         } else if (tempVP.toUpperCase()=="MMDDYYYY") {
            if (tempvar.length==6) {
               if (tempvar.substr(4,1).match(/[01]/)) {
                  tempvar=tempvar.substr(0,2) + tempvar.substr(2,2) + "20" + tempvar.substr(4,2);
               } else {
                  tempvar=tempvar.substr(0,2) + tempvar.substr(2,2) + "19" + tempvar.substr(4,2);
               }
            }
         } else if (tempVP.toUpperCase()=="HH:MM") {
            if (tempvar.length==4) {
               tempvar=tempvar.substr(0,2) + ":" + tempvar.substr(2,2);
            } else if (tempvar.length==3) {
               tempvar="0" + tempvar;
            }
         } else if (tempVP.toUpperCase()=="HHMM") {
            if (tempvar.length==3) {
               tempvar="0" + tempvar;
            }
         } else if (tempVP.toUpperCase()=="MM/DD/YY:HH:MM") {
            if (tempvar.length==12) {
               tempvar=tempvar.substr(0,2) + "/" + tempvar.substr(2,2) + "/" + tempvar.substr(6,2) + ":" +
                       tempvar.substr(8,2) + ":" + tempvar.substr(10,2);
            } else if (tempvar.length==10) {
               tempvar=tempvar.substr(0,2) + "/" + tempvar.substr(2,2) + "/" + tempvar.substr(4,2) + ":" +
                       tempvar.substr(6,2) + ":" + tempvar.substr(8,2);
            }
         } else if (tempVP.toUpperCase()=="MM/DD/YYYY:HH:MM") {
            if (tempvar.length==12) {
               tempvar=tempvar.substr(0,2) + "/" + tempvar.substr(2,2) + "/" + tempvar.substr(4,4) + ":" +
                       tempvar.substr(8,2) + ":" + tempvar.substr(10,2);
            } else if (tempvar.length==10) {
               if (tempvar.substr(4,1).match(/[01]/)) {
                  tempvar=tempvar.substr(0,2) + "/" + tempvar.substr(2,2) + "/20" + tempvar.substr(4,2) + ":" +
                       tempvar.substr(6,2) + ":" + tempvar.substr(8,2);
               } else {
                  tempvar=tempvar.substr(0,2) + "/" + tempvar.substr(2,2) + "/19" + tempvar.substr(4,2) + ":" +
                       tempvar.substr(6,2) + ":" + tempvar.substr(8,2);
               }
            }
         }
         thisobj.value=tempvar;
      }

      if (_changeValidate!=1) {
 
      // Execute validation script for this object .
      eval("validateRmsDateTimeField(\"" + thisobj.name + "\",thisobj.value," +
           "\"" + _varRequired +  "\"" + "," +
           "\"" + _varPopUp +  "\"" + "," +
           "\"" + _varMissingArray +  "\"" + "," +
           "\"" + _varPattern +  "\"" + "," +
           "\"" + _conCode +  "\"" + "," +
           "\"" + _conPopUp +  "\"" +
         ");");
        
   
      if (passloc==1) {
         eval(passstring);
      }

      // revert to original value if field in error
      if (_fColor==_errorColor && _errorColor!=_bgColor) {
         thisobj.value=_oldValue;
      }
       
      //SAS:dateField
       
      // This statement must exist and be LAST!  Set field color.
      // Change field color
      var inArray=getFormElementArrayById(thisobj.name);
      for(i=0;i<inArray.length;i++) {
         inArray[i].style.backgroundColor=_fColor; 
      }
      }
      else {
         // this statement in the else clause keeps adobe from issuing a
         // "validation failed" message.  This is probably a bug.
         var inArray=getFormElementArrayById(thisobj.name);
         for(i=0;i<inArray.length;i++) {
            inArray[i].style.backgroundColor=_fColor; 
         }
      }
      // RESET FIELD
      _varMissingArray="_NA_";
   }
}

function dt_keyup(thisobj) {

   _changeField = thisobj.name;
   _scrptsrc = null;
   _changeValidate=1;
 
   dt_validate(thisobj);
   if (_varCaps.toUpperCase()=="Y") {
      thisobj.value=thisobj.value.toUpperCase();
   }
   onChangeDate();
   _changeValidate=0;

   // Select existing value when field is tabbed to
   if (event.keyCode==9) {
      thisobj.select();
   }

}

// Validation script for RMS_Date_Field
function validateRmsDateTimeField(inVar,rawVal,varRequired,varPopUp,varMissingArray,varPattern,conCode,conPopUp) {

   if (varRequired.toUpperCase()=="Y") {
      inFunction="VDTR";
   } else {
      inFunction="VDT";
   }
   varLength=-1;
   if (varMissingArray=="NONE") {
      varMissingArray="";
   }
   varMissingArray=varMissingArray.replace(/,/g,"");
   validateMain(inFunction,inVar,rawVal,varPopUp,varLength,varPattern,varMissingArray);
   if (conCode.toUpperCase()!="NONE" && _fColor!=_errorColor) {
      addRestrictCode(inVar,rawVal,conCode,conPopUp,varMissingArray);
   }
}

////////////////////
////////////////////
////////////////////
////////////////////
////////////////////

function dc_focus(thisobj) {
   if (_docReady==1) {
      try {
         eval("e_" + thisobj.name + "(thisobj)");
      } catch (e) {
         eval("window.document.e_" + thisobj.name + "(thisobj)");
      }
      if (thisobj.readOnly) {
         skipThis(thisobj);
      } else {
         thisobj.select();
      }
   }   
   _changeField="_EV_";

}

function dc_blur(thisobj) {

   // Trim Field (if statement used to cause
   //   validate script to execute only if value is changed.  Otherwise
   //   popups come up twice)  Actually, for some reason popups don't
   //   come up twice anyway when value change is in an "IF" statement.
   //   This is probably a bug in Adobe, but it works for us.

   if (thisobj.value!=null && thisobj.value!=trim(thisobj.value)) {
      thisobj.value=trim(thisobj.value);
   }
   _changeValidate=0;
   dc_validate(thisobj);

}

function dc_validate(thisobj,passstring,passloc) {

   // skip validation while typing
   if (event.type=="keyup") {
      return;
   }

   if ((_docReady==1 && 
        (_changeField==thisobj.name || _changeField=="_EV_")) ||
       (_validateStart=="Y" && _docReady!=1)) {


      // Handle most common changes here.  Works better than onChangeDate.
      if (thisobj.value!=null && thisobj.value!=undefined) {
         tempvar=thisobj.value;
         if (tempvar!=null) {
            thisobj.value=thisobj.value.toUpperCase();
         }
         tempvar=trim(thisobj.value);
         tempvar=tempvar.replace(/[^0-9]/g,"");
         if (tempvar.length==10) {
            tempvar=tempvar.substr(0,2) + ":" + tempvar.substr(2,2) + "." + tempvar.substr(4,2) + "." +
                    tempvar.substr(6,2) + "." + tempvar.substr(8,2);
            thisobj.value=tempvar;
         }
      }

      // Execute validation script for this object .
      eval("validateBmcDrugCode(\"" + thisobj.name + "\",thisobj.value," +
           "\"" + _varRequired +  "\"" + "," +
           "\"" + _varPopUp +  "\"" + "," +
           "\"" + _conCode +  "\"" + "," +
           "\"" + _conPopUp +  "\"" +
         ");");
   
      if (passloc==1) {
         eval(passstring);
      }
       
      // This statement must exist and be LAST!  Set field color.
      var inArray=getFormElementArrayById(thisobj.name);
      for(i=0;i<inArray.length;i++) {
         inArray[i].style.backgroundColor=_fColor; 
      }
    
   }

}

function dc_keyup(thisobj) {

   _changeField = thisobj.name;
   _scrptsrc = null;
   _changeValidate=1;
 
   dt_validate(thisobj);
   if (_varCaps.toUpperCase()=="Y") {
      thisobj.value=thisobj.value.toUpperCase();
   }
   onChangeDrugCode();
   _changeValidate=0;

   // Select existing value when field is tabbed to
   if (event.keyCode==9) {
      thisobj.select();
   }

}

// Validation script for BMC_DRUG_CODE (VALIDATE AS A TEXT FIELD - THIS FIELD IS PROPRIETARY
// TO BMC BECAUSE IT CONTAINS AHFS CODING INFORMATION WE DON'T WANT PROPRIETARY CODE IN
// THE VALIDATEMAIN FUNCTION TO MAKE EXPORT OUTSIDE BMC EASIER)
// Validation script for RMS_Text_Field
function validateBmcDrugCode(inVar,rawVal,varRequired,varPopUp,conCode,conPopUp) {

   if (varRequired.toUpperCase()=="Y") {
      inFunction="VBTR";
   } else {
      inFunction="VBT";
   }
   varMissingArray="_NA_";
   validateMain(inFunction,inVar,rawVal,varPopUp,"14","XXXXXXXXXXXXXX",varMissingArray);
   if (conCode.toUpperCase()!="NONE" && _fColor!=_errorColor) {
      addRestrictCode(inVar,rawVal,conCode,conPopUp,varMissingArray);
   }
}

////////////////////
////////////////////
////////////////////
////////////////////
////////////////////

// ADDITIONAL VARIABLE RESTRICTIONS
function addRestrictCode(inVar,rawVal,conCode,conPopUp,varMissingArray) {
  // CATCH AND DISPLAY ANY ERRORS
  try {
      isInError=false;

      // Create missing value matching pattern 
      matchList="";
      if (varMissingArray!="_NA_" && varMissingArray!="NONE") {
         matchList=varMissingArray.replace(/,/g,"");
      } 
      if (matchList.length>1) {
         matchExp=new RegExp("^ *[" + matchList + "] *$","i");
      } else {
         matchExp=new RegExp("^ *$");
      }

      conCode=conCode.replace(/&gt;/g,">");
      conCode=conCode.replace(/&lt;/g,"<");

      // Evaluate constraint code
      if ((!eval(conCode)) && 
            (!(rawVal==null || rawVal=="" || rawVal.toString().match(matchExp)) ||
               rawVal==0
            )
         ) {
            // SET ERROR FLAG AND INCREMENT numERRORS
           isInError=true;
           // Update _errArray & _numErrors
           errUpdate(inVar,isInError,"CONSTRAINT CODE VIOLATED (" + conCode + ")");
           getFormElementById("FormDataOK").rawValue=0;
           if (_docReady==1 && _resetVar!=1) {
              _fColor=_errorColor;
              // DISPLAY ERROR MESSAGE IF TEXT IS PASSED
              if (conPopUp.toString()!="undefined" && conPopUp.toString()!="null" && conPopUp!="NONE") {
                 alert(conPopUp);
              }
           }
      }
  } catch(e) {
     alert("The following code passed to the addRestrictCode function either contains errors" +
	           " or is incompatible with the addRestrictCode function.  The code is: \n\n" +
               "      " + conCode + "\n\nDetails:\n\n   " + e.name + ":  " + e.message);
  }
}

////////////////////
////////////////////
////////////////////
////////////////////
////////////////////

// VARIABLE LIST RESTRICTIONS
function addRestrictList(inVar,rawVal,conList,conPopUp,varMissingArray) {
  // CATCH AND DISPLAY ANY ERRORS
  try {
        isInError=false;
        // create matchlist var
        var matchArray=conList.split(",");
        var matchList="";
        for (var i=0;i<matchArray.length;i++) {
           if (matchArray[i]!=null) {
              matchList=matchList + "|^ *" + trim(matchArray[i]) + " *$";
           }  
        }
        if (varMissingArray!="_NA_" && varMissingArray!="NONE") {
           for (var i=0;i<varMissingArray.length;i++) {
              if (matchArray[i]!=null) {
                 matchList=matchList + "|^ *" + trim(varMissingArray[i]) + " *$";
              }  
           }
        }
        if (matchList.substr(0,1)=="|") {
           matchList=matchList.substr(1);
        }
        if (rawVal!=null && rawVal!=undefined && rawVal.length>=1) {
           junkvar=eval("rawVal.toString().match(/" + matchList + "/)");
           if (junkvar==null || (junkvar!=rawVal && (Number(junkvar)!=Number(rawVal) && Number(junkvar)>0))) {
              // SET ERROR FLAG AND INCREMENT numERRORS
              isInError=true;
              // Update _errArray & _numErrors
              errUpdate(inVar,isInError,"Invalid value - Value list (" + conList + ")");
              getFormElementById("FormDataOK").rawValue=0;
              if (_docReady==1 && _resetVar!=1) {
                 _fColor=_errorColor;
                 // DISPLAY ERROR MESSAGE IF TEXT IS PASSED
                 if (conPopUp.toString()!="undefined" && conPopUp.toString()!="null" && conPopUp!="NONE") {
                    alert(conPopUp);
                 }
              }
           }
        }
  } catch(e) {
     alert("The following code passwd to the addRestrictList function either contains errors" +
	           "or is incompatible with the addRestrictList function.  The code is: \n\n" +
               "      " + conList + "\n\nDetails:\n\n   " + e.name + ":  " + e.message);
  }
}


////////////////////
////////////////////
////////////////////
////////////////////
////////////////////

// Function for change event on rmsDateTimeField
function onChangeDate() {
   // Currently nothing done.  No conversions from PDF JavaScript.  Most common conversions
   // take place in validate
   return;
}

////////////////////
////////////////////
////////////////////
////////////////////
////////////////////

// Function for change event on bmcDrugCode
function onChangeDrugCode() {
   // Currently nothing done.  No conversions from PDF JavaScript.  Most common conversions
   // take place in validate
   return;

}

////////////////////
////////////////////
////////////////////
////////////////////
////////////////////

// Function for change event on rmsTextField
// (empty object added as a placeholder for future development so object would
//  have something to call and not need to be change when development occurred)
function onChangeText() {
   // Currently nothing done.  No conversions from PDF JavaScript.  Most common conversions
   // take place in validate
   return;
   
}

////////////////////
////////////////////
////////////////////
////////////////////
////////////////////

// Function for change event on rmsNumericField 
// (empty object added as a placeholder for future development so object would
//  have something to call and not need to be change when development occurred)
function onChangeNum() {
   // NO CURRENT CONTENT
}

////////////////////
////////////////////
////////////////////
////////////////////
////////////////////

// Hide content from participants
function participantHide(thisobj) {
   if (_rmsPassValueObject.rightslevel=="participant") {
         thisobj.style.visibility="hidden";
   }
}

/////////////
// BUTTONS //
/////////////

////
////
function validate_dialog(inString) {
   alertString="";
   alertString="There are <span style='font-size=12pt;color=#DD2200'>" + _numErrors + "</span> validation problems reported.<BR><BR>";
   for (var i=0;i<_errArray.length;i++) {
      alertString=alertString + "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<span style='color=#0022DD'>" + _errArray[i] + "</span><span style='color=#888888'>&nbsp;&nbsp;&nbsp;(" + _errTextArray[i] + ")</span><BR>";
   }
            var body=ipop.document.body;
            try {
            } catch (ve2) {
               // DO NOTHING
            }
            body.style.backgroundColor="#CCCCCC";
            body.style.border="solid black 2px";
            body.innerHTML=
'   <table height=100% width=100% cellspacing=0 cellpadding=0>' +
'     <tr><td valign=top align=right>' +
'   <img align=right onclick="parent.db();parent.ipop.hide()" src="images/closebutton.jpg" hspace=5 vspace=5>' +
'     </td></tr>' +
'     <tr><td align=center height=90%>' +
'        <table bgcolor=FFFFFF cellpadding=0 width=95% height=95%>' +
'         <tr align=center valign=center>' +
'         <td align=left valign=top style="font-size:10pt">' +
alertString +
'         </td></tr>' +
'        </table>' +
'     </td></tr>' +
'   </table>' +
' ' 
;
         ipop.show(screenLeft+(document.body.clientWidth/2)-300,screenTop+30,600,450);

}

function formvalidate_dialog(inString) {
            var body=ipop.document.body;
            body.style.backgroundColor="#CCCCCC";
            body.style.border="solid black 2px";
            body.innerHTML=
'   <table height=100% width=100% cellspacing=0 cellpadding=0>' +
'     <tr><td valign=top align=right>' +
'   <img align=right onclick="parent.db();parent.ipop.hide()" src="images/closebutton.jpg" hspace=5 vspace=5>' +
'     </td></tr>' +
'     <tr><td align=center height=90%>' +
'        <table bgcolor=FFFFFF cellpadding=0 width=95% height=95%>' +
'         <tr align=center valign=center>' +
'         <td align=left valign=top style="font-size:10pt">' +
inString +
'         </td></tr>' +
'        </table>' +
'     </td></tr>' +
'   </table>' +
' ' 
;
         ipop.show(screenLeft+(document.body.clientWidth/2)-300,screenTop+30,600,450);

}

function validate_click() {
   // Run form's developer added validation and display result

   var formval=ValidateFunc();

   _changeField="_EV_";
   execValidate();
   // set validatestart to "Y" so SubmitButton knows validation has been run.
   _validateStart="Y";
    
   if (formval!='OK') {
      formvalidate_dialog(formval);
   } else if (_numErrors>0) {
      validate_dialog(null);
   } else {
      alert("There are no validation problems reported.");
   }

}

function resetForm() {
   // Returns first form element matching name
   for (var i=0;i<document.forms.length;i++) {
      document.forms[i].reset();
   }
}

function reset_click() {
   if (confirm( "WARNING:  If you continue, you will lose any unsaved" +
                "\ndata in this form.  Are you sure you want to continue?")) {
      try { var formdefid_=getFormElementValueById("formdefid_"); } catch (e) { }
      try { var checkenrollform_=getFormElementValueById("checkenrollform_"); } catch (e) { }          
      resetForm();
      try { getFormElementById("formdefid_").value=formdefid_; } catch (e) { }          
      try { getFormElementById("checkenrollform_").value=checkenrollform_; } catch (e) { }          
      // Retreive info saved above.
      _resetVar=1;
      _changeField="_EV_";
      _numErrors="0";
      _errArray=new Array();
      execValidate();
      _resetVar=0;
   }
}


function reset_mouseenter(thisobj) {
  //_docReady=0;
}

function reset_mouseexit(thisobj) {
  //_docReady=1;
}

////
////

// Base64 code from Tyler Akins -- http://rumkin.com

var keyStr = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/=";
//var keyStr = "!@#$%^&*();<>[]{},.|\-_'?~AbCdEfGhIjKlMnOpQrStUvWxYz0123456789+/=";

function encode64(input) {
   var output = "";
   var chr1, chr2, chr3;
   var enc1, enc2, enc3, enc4;
   var i = 0;

   do {
      chr1 = input.charCodeAt(i++);
      chr2 = input.charCodeAt(i++);
      chr3 = input.charCodeAt(i++);

      enc1 = chr1 >> 2;
      enc2 = ((chr1 & 3) << 4) | (chr2 >> 4);
      enc3 = ((chr2 & 15) << 2) | (chr3 >> 6);
      enc4 = chr3 & 63;

      if (isNaN(chr2)) {
         enc3 = enc4 = 64;
      } else if (isNaN(chr3)) {
         enc4 = 64;
      }

      output = output + keyStr.charAt(enc1) + keyStr.charAt(enc2) + 
         keyStr.charAt(enc3) + keyStr.charAt(enc4);
   } while (i < input.length);
   
   return output;
}

function decode64(input) {
   var output = "";
   var chr1, chr2, chr3;
   var enc1, enc2, enc3, enc4;
   var i = 0;

   // MRH commented out the following
   /*
   // remove all characters that are not A-Z, a-z, 0-9, +, /, or =
   input = input.replace(/[^A-Za-z0-9\+\/\=]/g, "");
   */

   do {
      enc1 = keyStr.indexOf(input.charAt(i++));
      enc2 = keyStr.indexOf(input.charAt(i++));
      enc3 = keyStr.indexOf(input.charAt(i++));
      enc4 = keyStr.indexOf(input.charAt(i++));

      chr1 = (enc1 << 2) | (enc2 >> 4);
      chr2 = ((enc2 & 15) << 4) | (enc3 >> 2);
      chr3 = ((enc3 & 3) << 6) | enc4;

      output = output + String.fromCharCode(chr1);

      if (enc3 != 64) {
         output = output + String.fromCharCode(chr2);
      }
      if (enc4 != 64) {
         output = output + String.fromCharCode(chr3);
      }
   } while (i < input.length);

   return output;
}

// Pad an integer with leading zeros
function padInt(n,numdig) { 
   n=n.toString(); 
   var pad=''; 
   if (numdig>n.length) { 
      for (i=0;i<(numdig-n.length);i++) { 
          pad+='0'; 
      } 
   } 
   return pad+n.toString(); 
} 

// Write string to external file (local save script)
function writeToFile(instr) {

   if (confirm("WARNING!!! - LocalSave is not an alternative to submitting the form.\n\n" +
               "LocalSave should only be used if for some reason data cannot be submitted\n" +
               "to the server.\n\n" +
               "Saved files should be removed after they have been imported back into the\n" +
               "form and submitted to the server.  They contain retreivable subject data\n" +
               "and should not be saved permanently on your hard drive.\n\n" +
               "LocalSave files should never be saved to a public computer.  Please save\n" +
               "to a jump drive if you are on a public computer.\n\n" +
               "Press \"OK\" to continue or Cancel to cancel this operation")) {
      // NOTE:  Currently this section is not functioning with IE8
      var w=window.frames.w;
      if (!w) {
         w=document.createElement('iframe');
         w.id='w';
         w.style.display='none';
         document.body.insertBefore(w);
         w=window.frames.w;
         if (!w) {
            w=window.open('','_tempwin_');
            if (!w) {
               alert('ERROR:  Could not create file.'); 
               return;
            }
         }
      }
      var d=w.document;
      var dt=new Date();
      var dts=padInt(dt.getFullYear(),4) + padInt(dt.getMonth()+1,2) + padInt(dt.getDate(),2) + "_" +
              padInt(dt.getHours(),2) + padInt(dt.getMinutes(),2) + padInt(dt.getSeconds(),2);
      name="FORMS_" + dts + ".SAV";
      d.open('text/plain','replace');
      d.write(instr);
      d.close();
      if(d.execCommand('SaveAs',null,name)){
         alert('Data has been saved to:  ' + name);
      } else {
         alert('The data have not been saved.');
      }
      w.close();

      // NOTES:  The above section is currently not working well with IE8.  Below is a 
      // possible candidate for replacement, though it doesn't work well with HTTPS without
      // additional steps taken by the use to trust it.  Leaving the above code in place for
      // now as hopefully IE8 will be updated to allow it to function.  Otherwise the following
      // may become a possibility.  The code below is not yet ready for use without some mods,
      // but it is included here as a possible future direction for the local save.
      /*
      // Open dialog box
      var dialog = new ActiveXObject("SAFRCFileDlg.FileSave");
      dialog.FileName = name.toLowerCase();
      dialog.FileType = "text/plain";
      var rc = dialog.OpenFileSaveDlg();

      if (rc) {
         stringToFile(instr,dialog.FileName);
      } else {
         alert('The data have not been saved.');
      }

      // Write string to external file (generic string script)
      function stringToFile(instr,name) {
      
         // Write output to file
         var forReading = 1, forWriting = 2, forAppending = 8;
         fs = new ActiveXObject( "Scripting.FileSystemObject" );
         fs.CreateTextFile(name);
         os = fs.GetFile(name);
         os = os.OpenAsTextStream( forWriting, 0 );
         os.write(instr);
         os.Close();
         alert('Data has been saved to:  ' + name);
      
      }

      */

   } else {
      alert('The data have not been saved.');
   }
   return;
}

function unnull(s) {
  var r = new RegExp( '^\s*' + String.fromCharCode( 255, 254 ) );
  return s.replace( r, '' ).replace( /((\r?)\n )\1/g, '$2\n' ).replace( /(\s(.))/g, '$2' );
}

// Write string to external file (generic string script)
function stringToFile(instr,name) {

   var w=window.frames.w;
   if (!w) {
      w=document.createElement('iframe');
      w.id='w';
      w.style.display='none';
      document.body.insertBefore(w);
      w=window.frames.w;
      if (!w) {
         w=window.open('','_tempwin_');
         if (!w) {
            alert('ERROR:  Could not create file.'); 
            return;
         }
      }
   }
   var d=w.document;
   d.open('text/plain','replace');
   d.charset="UTF-8"; 
   d.write(instr);
   d.close();
   if(d.execCommand('SaveAs',true,name)){
      alert('File has been saved.');
   } else {
      alert('The file have not been saved.');
   }
   w.close();

}
   
// Export form contents to local file
function localExport() {
   var junkstr=encode64(getXmlTransStringFromDoc());
   writeToFile(junkstr);
}

function localImport() {

// Display warning message if data haven't been submitted
if (_saveStatus!="OK") {
   if (!confirm("WARNING!!!:  It appears there have been changes to values in this form     \n" +
          "that haven't been submitted to RMS.  If you continue importing data\n" +
          "into this form, all current data will be lost.\n\n" +
          "Press CANCEL to remain in form and submit")) {
      return;       
   }
} 
   
var width = 700;
var height = 400;
var left = parseInt(screenLeft+(document.body.clientWidth/2) - (width/2));
var top = parseInt(screenTop+30);
var wspecs = "width=" + width + ",height=" + height + 
             ",status=yes,resizable=yes,left=" + left + ",top=" + top + 
             "screenX=" + left + ",screenY=" + top;
var w=window.open("","_importwin_",wspecs);
_importwin_=w;
w.document.writeln('<html>');
w.document.writeln('<head>');
w.document.writeln('   <title>FORMS Form Data Import</title>');
w.document.writeln('   <script>');
w.document.writeln('       function asyncUpload() { ');
w.document.writeln('           var imprtFile = document.getElementById("imprtFile"); ');
w.document.writeln('           var fileParent = imprtFile.parentNode; ');
w.document.writeln('           // create a div with a hidden iframe and form ');
w.document.writeln('           var theDiv = document.createElement(\'div\'); ');
w.document.writeln('           theDiv.style.display = "none"; ');
w.document.writeln('           theDiv.innerHTML = ');
w.document.writeln('               \'<iframe id="hidden_frame" name="hidden_frame" src=""></iframe>\' + ');
w.document.writeln('               \'<form id="hidden_form" target="hidden_frame" action="importlocaldatadwr.do" enctype="multipart/form-data" method="post"></form>\'; ');
w.document.writeln('           document.body.appendChild(theDiv); ');
w.document.writeln('           var hiddenForm = document.getElementById("hidden_form"); ');
w.document.writeln('           fileParent.removeChild(imprtFile); ');
w.document.writeln('           // attach the file to the hidden form ');
w.document.writeln('           hiddenForm.appendChild(imprtFile); ');
w.document.writeln('           hiddenForm.submit(); ');
w.document.writeln('           hiddenForm.removeChild(imprtFile); ');
w.document.writeln('           // put the file back where it came from ');
w.document.writeln('           fileParent.appendChild(imprtFile); ');
w.document.writeln('           //parent.opener.getFormElementById("ValidateButton").click(); ');
w.document.writeln('           return;');
w.document.writeln('       } ');
w.document.writeln('   </script>');
w.document.writeln('</head> ');
w.document.writeln('<body bgcolor=#CCCCCC onbeforeunload="alert(\"what???\")"> ');
w.document.writeln('   <center>');
//w.document.writeln('   <table cellpadding=25 width=20% height=100%>');
w.document.writeln('   <table cellpadding=25 width=20%>');
w.document.writeln('    <tr align=center valign=top>');
w.document.writeln('    <td align=center valign=top>');
w.document.writeln('   <table border=2 bgcolor=#E0E0E0 rules=none align=center cellpadding=25>');
w.document.writeln('      <tr align=right>');
w.document.writeln('        <td width=20% valign=top align=right style="padding-bottom:0;padding-top:5;padding-right:5">');
w.document.writeln('        </td>');
w.document.writeln('      </tr>');
w.document.writeln('      <tr align=center>');
w.document.writeln('      <td align=center style="padding-top:10">');
w.document.writeln('         <table border=1 bgcolor=white cellpadding=5>');
w.document.writeln('            <tr><td style="padding-right:30;padding-left:30">');
w.document.writeln('            <nobr>');
w.document.writeln('            <br><font color=#002288 size=4>Select File for Import</font><br><br></font> ');
w.document.writeln('   <div><input name="imprtFile" id="imprtFile" size=50 type="file"></div><br> ');
w.document.writeln(' ');
w.document.writeln('         </table>');
w.document.writeln('      </td>   ');
w.document.writeln('      </tr>   ');
w.document.writeln('');
w.document.writeln('      <tr align=center>');
w.document.writeln('      <td align=center style="padding-top:5">');
w.document.writeln('         <table cellpadding=2 align=center border=0>');
w.document.writeln('         <tr>');
w.document.writeln('         <td align=center>');
w.document.writeln('      <input type="button"  LANGUAGE="JScript" name="edit"');
w.document.writeln('             value="Import File" ');
w.document.writeln('             ONCLICK=\'parent.db();asyncUpload();\'');
w.document.writeln('             style="width:100%; padding-left: 15; padding-right: 15; background-color:#D5D5D5"');
w.document.writeln('             onMouseOver="style.backgroundColor=\'#C0C0C0\';" onMouseOut="style.backgroundColor=\'#D5D5D5\'"');
w.document.writeln('             >');
w.document.writeln('         </td>');
w.document.writeln('         <td align=center>');
w.document.writeln('      <input type="button"  LANGUAGE="JScript" name="delete"');
w.document.writeln('             value="Cancel" ');
w.document.writeln('             ONCLICK=\'parent.db();window.close();\'');
w.document.writeln('             style="padding-left: 15; padding-right: 15; background-color:#D5D5D5"');
w.document.writeln('             onMouseOver="style.backgroundColor=\'#C0C0C0\';" onMouseOut="style.backgroundColor=\'#D5D5D5\'"');
w.document.writeln('             >');
w.document.writeln('         </td>');
w.document.writeln('         </tr>');
w.document.writeln('         </table>');
w.document.writeln('      </td>');
w.document.writeln('      </tr>');
w.document.writeln('   </table>');
w.document.writeln('    </td>');
w.document.writeln('    </tr>');
w.document.writeln('   </table>');
w.document.writeln('   </center>');
w.document.writeln('</body> ');
w.document.writeln('</html> ');
w.focus();

}

function local_click() {
         //ipop=window.createPopup();
         var body=ipop.document.body;
         body.style.backgroundColor="#CCCCCC";
         body.style.border="solid black 2px";
         body.innerHTML=
'   <center>' +
'   <table cellpadding=25 width=20% height=100%>' +
'    <tr align=center valign=top>' +
'    <td align=center valign=top>' +
'   <table border=2 bgcolor=#E0E0E0 rules=none align=center cellpadding=25>' +
'      <tr align=right>' +
'        <td width=20% valign=top align=right style="padding-bottom:0;padding-top:5;padding-right:5">' +
'        </td>' +
'      </tr>' +
'      <tr align=center>' +
'      <td align=center style="padding-top:10">' +
'         <table border=1 bgcolor=white cellpadding=5>' +
'            <tr><td style="padding-right:30;padding-left:30">' +
'            <font color=#002288 size=3><font color=#AA0000>This facility allows you to save form data ' +
                'to your hard drive or retreive a previously saved file</font><br><br> ' +
'               &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;What do you want to do?</font>' +
'            </tr></td>' +
'         </table>' +
'      </td>   ' +
'      </tr>   ' +
'' +
'' +
'      <tr align=center>' +
'      <td align=center style="padding-top:5">' +
'         <table cellpadding=2 align=center border=0>' +
'         <tr>' +
'         <td colspan=2 align=center>' +
'      <input type="button"  LANGUAGE="JScript" name="nb"' +
'             value="Save Current Form Data" ' +
'            ONCLICK=\'parent.db();parent.localExport()\'' + 
'            >' +
'         </td>' +
'         <td colspan=2 align=center>' +
'      <input type="button"  LANGUAGE="JScript" name="edit"' +
'             value="Import previously saved data" ' +
'             ONCLICK=\'parent.db();parent.localImport()\'' + 
'             >' +
'         </td>' +
'         </tr>' +
'         <tr>' +
'         <td colspan=4 align=center>' +
'      <input type="button"  LANGUAGE="JScript" name="cancel"' +
'             value="Cancel" ' +
'             style="padding-left:50;padding-right:50" ' +
'            ONCLICK=\'parent.db();parent.ipop.hide();\'' + 
'            >' +
'         </td>' +
'         </tr>' +
'      </tr>' +
'   </table>' +
'    </td>' +
'    </tr>' +
'   </table>' +
'   </center>' +
' ' 
;
         ipop.show(screenLeft+(document.body.clientWidth/2)-300,screenTop+30,600,450);
}

////
////

function view_click() {
   var ss=document.styleSheets;
   // Switch between standard and pen-tablet optimized style sheet information
   // Example:  document.styleSheets[1].rules[1].style.cssText
   for (var i=0;i<ss.length;i++) {
      if (ss[i].media.toUpperCase()=="SCREEN") {
         var t=encodeURI(ss[i].cssText);
         if (t.match(/width:(%20)*700px/i)) {
            eval("_ruleText" + i + "=\"" + t + "\";");
            ss[i].cssText=decodeURI(t.replace(/700px/gi,"725px").replace(/font-size:(%20)[\d]*pt/gi,"FONT-SIZE:%2011pt"));
         } else if (t.match(/width:(%20)*725px/i)) {
            eval("ss[i].cssText=decodeURI(_ruleText" + i + ");");
         }   
      }
   }
}

////
////

function score_click() {
   ScoreFunc();
}

var ipop=window.createPopup();
var apop=window.createPopup();
var body=null;
var ProcessFormDwrReply=null;
var _pV="";
var already_clicked;

function db() {
   var doc=ipop.document;
   var earray=doc.getElementsByTagName("input");
   for (var i=0;i<earray.length;i++) earray[i].disabled=true;
}

function submit_click(thisobj) {

var _replytext=
'   <center>' +
'   <table cellpadding=25 width=20% height=100%>' +
'    <tr align=center valign=top>' +
'    <td align=center valign=top>' +
'   <table border=2 bgcolor=#E0E0E0 rules=none align=center cellpadding=25>' +
'      <tr align=right>' +
'        <td width=20% valign=top align=right style="padding-bottom:0;padding-top:5;padding-right:5">' +
'        </td>' +
'      </tr>' +
'      <tr align=center>' +
'      <td align=center style="padding-top:10">' +
'         <table border=1 bgcolor=white cellpadding=5>' +
'            <tr><td style="padding-right:30;padding-left:30">' +
'                 XXXXXXXX' +
'            </tr></td>' +
'         </table>' +
'      </td>   ' +
'      </tr>   ' +
'' +
'' +
'      <tr align=center>' +
'      <td align=center style="padding-top:5">' +
'         <table cellpadding=2 align=center border=0>' +
'         <tr>' +
'         <td>' +
'      <input type="button"  LANGUAGE="JScript" name="temp"' +
'             value="Proceed to FORMS" ' +
'             ONCLICK="parent.db();parent.location.replace(\'' + "YYYYYYYY" + '\');"' +
'             style="padding-left: 15; padding-right: 15; background-color:#D5D5D5"' +
'             onMouseOver="style.backgroundColor=\'#C0C0C0\';" onMouseOut="style.backgroundColor=\'#D5D5D5\'"' +
'             >' +
'         </td>' +
'         <td align=center>' +
'      <input type="button"  LANGUAGE="JScript" name="delete"' +
'             value="Return to Form" ' +
'             ONCLICK=\'parent.db();parent.ipop.hide();\'' +
'             style="padding-left: 15; padding-right: 15; background-color:#D5D5D5"' +
'             onMouseOver="style.backgroundColor=\'#C0C0C0\';" onMouseOut="style.backgroundColor=\'#D5D5D5\'"' +
'             >' +
'         </td>' +
'         </tr>' +
'         </table>' +
'      </td>' +
'      </tr>' +
'   </table>' +
'    </td>' +
'    </tr>' +
'   </table>' +
'   </center>' +
' ' 
;

var _aReplytext=
'   <center>' +
'   <table cellpadding=25 width=20% height=100%>' +
'    <tr align=center valign=top>' +
'    <td align=center valign=top>' +
'   <table border=2 bgcolor=#E0E0E0 rules=none align=center cellpadding=25>' +
'      <tr align=right>' +
'        <td width=20% valign=top align=right style="padding-bottom:0;padding-top:5;padding-right:5">' +
'        </td>' +
'      </tr>' +
'      <tr align=center>' +
'      <td align=center style="padding-top:10">' +
'         <table border=1 bgcolor=white cellpadding=5>' +
'            <tr><td style="padding-right:30;padding-left:30">' +
'                 XXXXXXXX' +
'            </tr></td>' +
'         </table>' +
'      </td>   ' +
'      </tr>   ' +
'' +
'' +
'      <tr align=center>' +
'      <td align=center style="padding-top:5">' +
'         <table cellpadding=2 align=center border=0>' +
'         <tr>' +
'         <td align=center>' +
'      <input type="button"  LANGUAGE="JScript" name="delete"' +
'             value="Return to Form" ' +
'             ONCLICK=\'parent.db();parent.apop.hide();\'' +
'             style="padding-left: 15; padding-right: 15; background-color:#D5D5D5"' +
'             onMouseOver="style.backgroundColor=\'#C0C0C0\';" onMouseOut="style.backgroundColor=\'#D5D5D5\'"' +
'             >' +
'         </td>' +
'         </tr>' +
'         </table>' +
'      </td>' +
'      </tr>' +
'   </table>' +
'    </td>' +
'    </tr>' +
'   </table>' +
'   </center>' +
' ' 
;

   if (already_clicked==1) {
      var abody=apop.document.body;
      abody.style.backgroundColor="#CCCCCC";
      abody.style.border="solid black 2px";
      abody.innerHTML=_aReplytext.replace(/XXXXXXXX/,
          '<font size="4" color="#990011">' +
          '<nobr>Duplicate submit request cancelled.  Waiting for a response</nobr><nobr>from the server.</nobr><br><br>' +
          '<small><small>If response is not received in an acceptable time or you feel you have received ' +
          'this message in error, you may want to consider doing a local save (a save to your ' +
          'hard drive) and trying to submit the data again later.  Click the local save button ' +
          'and follow the subsequent menus to do a local save</small></small>' +
          '</font>' 
      ).replace(/YYYYYYYY/,_continueServlet);
      apop.show(screenLeft+(document.body.clientWidth/2)-300,screenTop+30,600,450);
      return;
   }
   already_clicked=1;

   // close import window if open
   try {
      _importwin_.close();
   } catch (e) { }

   if (!SubmitFunc()) {
      return;
   }

   _pV=getXmlTransStringFromObj(thisobj);

    EcodeDwrUtilReply = function(replydata)
    {
         alert("IMPORTANT!!!  An ID/ECODE mismatch could be due either to an invalid ID or ECODE.\n" +
               "Be sure to check that the ID entered is the correct one.")
         var body=ipop.document.body;
         body.style.backgroundColor="#CCCCCC";
         body.style.border="solid black 2px";
         body.innerHTML=
'   <center>' +
'   <table cellpadding=25 width=20% height=100%>' +
'    <tr align=center valign=top>' +
'    <td align=center valign=top>' +
'   <table border=2 bgcolor=#E0E0E0 rules=none align=center cellpadding=25>' +
'      <tr align=right>' +
'        <td width=20% valign=top align=right style="padding-bottom:0;padding-top:5;padding-right:5">' +
'        </td>' +
'      </tr>' +
'      <tr align=center>' +
'      <td align=center style="padding-top:10">' +
'         <table border=1 bgcolor=white cellpadding=5>' +
'            <tr><td style="padding-right:30;padding-left:30">' +
'            <nobr>' +
'            <font color=#002288 size=3><font color=#002288><b>The ECODE for this Patient is:  ' + 
                  replydata + ' </b></font><br><br> ' +
'               &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;What do you want to do?</font>' +
'            </nobr>' +
'            </tr></td>' +
'         </table>' +
'      </td>   ' +
'      </tr>   ' +
'' +
'' +
'      <tr align=center>' +
'      <td align=center style="padding-top:5">' +
'         <table cellpadding=2 align=center border=0>' +
'         <tr>' +
'         <td>' +
'      <input type="button"  LANGUAGE="JScript" name="nb"' +
'             value="Set ECODE and Return to Form" ' +
'            ONCLICK=\'parent.db();parent.getFormElementById("Ecode").value="' + 
                  replydata + '";parent.ipop.hide();\'' +
'            style="width:100%; padding-left: 10; padding-right: 10; background-color:#D5D5D5"' +
'             onMouseOver="style.backgroundColor=\'#C0C0C0\';" onMouseOut="style.backgroundColor=\'#D5D5D5\'"' +
'            >' +
'         </td>' +
'         <td>' +
'      <input type="button"  LANGUAGE="JScript" name="edit"' +
'             value="Return Without Changing Ecode" ' +
'            ONCLICK=\'parent.db();parent.ipop.hide();\'' +
'             style="width:100%; padding-left: 10; padding-right: 10; background-color:#D5D5D5"' +
'             onMouseOver="style.backgroundColor=\'#C0C0C0\';" onMouseOut="style.backgroundColor=\'#D5D5D5\'"' +
'             >' +
'         </td>' +
'         </tr>' +
'         </table>' +
'      </td>' +
'      </tr>' +
'   </table>' +
'    </td>' +
'    </tr>' +
'   </table>' +
'   </center>' +
' ' 
;
         already_clicked=0; 
         apop.hide();
         ipop.show(screenLeft+(document.body.clientWidth/2)-300,screenTop+30,600,450);

    }

    ProcessFormDwrReply = function(replydata)
    {

      if (replydata=="APPENDED") {
         _saveStatus="OK";
         var body=ipop.document.body;
         body.style.backgroundColor="#CCCCCC";
         body.style.border="solid black 2px";
         body.innerHTML=_replytext.replace(/XXXXXXXX/,
             '<nobr><font size="4" color="#002288">' +
             'Record appended to database.' +
             '</font></nobr>' 
         ).replace(/YYYYYYYY/,_continueServlet);
         already_clicked=0;
         apop.hide();
         ipop.show(screenLeft+(document.body.clientWidth/2)-300,screenTop+30,600,450);
      } else if (replydata=="SAVEDSYNC") {
         _saveStatus="OK";
         var body=ipop.document.body;
         body.style.backgroundColor="#CCCCCC";
         body.style.border="solid black 2px";
         body.innerHTML=_replytext.replace(/XXXXXXXX/,
             '<nobr><font size="4" color="#002288">' +
             'Record has been saved to Sync directory.' +
             '</font></nobr>' 
         ).replace(/YYYYYYYY/,_continueServlet);
         already_clicked=0;
         apop.hide();
         ipop.show(screenLeft+(document.body.clientWidth/2)-300,screenTop+30,600,450);
      } else if (replydata=="ERROR") {
         var body=ipop.document.body;
         body.style.backgroundColor="#CCCCCC";
         body.style.border="solid black 2px";
         body.innerHTML=_replytext.replace(/XXXXXXXX/,
             '<nobr><font size="4" color="#DD0000">' +
             'An error has occurred processing this request.<br><br></nobr>' +
             '<font size="2" color=#002288>A FORMS Temporary Record has been saved and can be used ' +
             'to resubmit the form record later once the causes of this error have been resolved.</font>' +
             '</font>' 
         ).replace(/YYYYYYYY/,_continueServlet);
         already_clicked=0;
         apop.hide();
         ipop.show(screenLeft+(document.body.clientWidth/2)-300,screenTop+30,600,450);
      } else if (replydata=="BADERROR") {
         var body=ipop.document.body;
         body.style.backgroundColor="#CCCCCC";
         body.style.border="solid black 2px";
         body.innerHTML=_replytext.replace(/XXXXXXXX/,
             '<nobr><font size="4" color="#DD0000">' +
             'An error has occurred processing this request.<br><br></nobr>' +
             '<font size="2" color=#002288>Please click the "Local Save" button and save a local copy of ' +
             'your form data to be submitted later.</font>' +
             '</font>' 
         ).replace(/YYYYYYYY/,_continueServlet);
         already_clicked=0;
         apop.hide();
         ipop.show(screenLeft+(document.body.clientWidth/2)-300,screenTop+30,600,450);
      } else if (replydata=="PERMERROR") {
         var body=ipop.document.body;
         body.style.backgroundColor="#CCCCCC";
         body.style.border="solid black 2px";
         body.innerHTML=_replytext.replace(/XXXXXXXX/,
             '<font size="4" color="#DD0000">' +
             'Your account does not have permission to permform this action for this form.<br><br>' + 
             '<font size="2" color=#002288>A FORMS Temporary Record has been saved and can be used ' +
             'to resubmit the form record later either by a user with the approprate permissions or ' +
             'after permissions have been granted for your account</font>' +
             '</font>' 
         ).replace(/YYYYYYYY/,_continueServlet);
         already_clicked=0;
         apop.hide();
         ipop.show(screenLeft+(document.body.clientWidth/2)-300,screenTop+30,600,450);
      } else if (replydata=="SYNCERROR") {
         var body=ipop.document.body;
         body.style.backgroundColor="#CCCCCC";
         body.style.border="solid black 2px";
         body.innerHTML=_replytext.replace(/XXXXXXXX/,
             '<font size="4" color="#DD0000">' +
             'ERROR:  Could not save to the sync directory.  Please retry or do a local save and contact an administrator.' +
             '</font>' 
         ).replace(/YYYYYYYY/,_continueServlet);
         already_clicked=0;
         apop.hide();
         ipop.show(screenLeft+(document.body.clientWidth/2)-300,screenTop+30,600,450);
      } else if (replydata=="NOVERIFY") {
         var body=ipop.document.body;
         body.style.backgroundColor="#CCCCCC";
         body.style.border="solid black 2px";
         body.innerHTML=_replytext.replace(/XXXXXXXX/,
             '<font size="3" color="#002288">' +
             "WARNING:  Append process proceeded without error, but database record could " +
             "not be verified.  Please verify that record is in dataset." +
             '</font>' 
         ).replace(/YYYYYYYY/,_continueServlet);
         apop.hide();
         already_clicked=0;
         ipop.show(screenLeft+(document.body.clientWidth/2)-300,screenTop+30,600,450);
      } else if (replydata.substr(0,9)=="ECODEPROB") {
         //ipop=window.createPopup();
         var sarray=replydata.split(":::");
         var body=ipop.document.body;
         body.style.backgroundColor="#CCCCCC";
         body.style.border="solid black 2px";
         body.innerHTML=
'   <center>' +
'   <table cellpadding=25 width=20% height=100%>' +
'    <tr align=center valign=top>' +
'    <td align=center valign=top>' +
'   <table border=2 bgcolor=#E0E0E0 rules=none align=center cellpadding=25>' +
'      <tr align=right>' +
'        <td width=20% valign=top align=right style="padding-bottom:0;padding-top:5;padding-right:5">' +
'        </td>' +
'      </tr>' +
'      <tr align=center>' +
'      <td align=center style="padding-top:10">' +
'         <table border=1 bgcolor=white cellpadding=5>' +
'            <tr><td style="padding-right:30;padding-left:30">' +
'            <nobr>' +
'            <font color=#002288 size=2><font color=#AA0000>ERROR!!! There is a mismatch between the Patient ID and the ECODE field.  Please check these.</font><br><br> ' +
'               &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;What do you want to do?</font>' +
'            </nobr>' +
'            </tr></td>' +
'         </table>' +
'      </td>   ' +
'      </tr>   ' +
'' +
'' +
'      <tr align=center>' +
'      <td align=center style="padding-top:5">' +
'         <table cellpadding=2 align=center border=0>' +
'         <tr>' +
'         <td colspan=2>' +
'      <input type="button"  LANGUAGE="JScript" name="nb"' +
'             value="Upload Form to HOLD Dataset" ' +
'            ONCLICK=\'parent.db();parent.ProcessFormDwr.submitHold(parent.HoldDataReply);\'' +
'            style="width:100%; padding-left: 40; padding-right: 40; background-color:#D5D5D5"' +
'             onMouseOver="style.backgroundColor=\'#C0C0C0\';" onMouseOut="style.backgroundColor=\'#D5D5D5\'"' +
'            >' +
'         </td>' +
'         </tr>' +
'         <tr>' +
'         <td colspan=2>' +
'      <input type="button"  LANGUAGE="JScript" name="edit"' +
'             value="Lookup ECODE for this IDNO" ' +
'             ONCLICK=\'parent.db();parent.EcodeDwrUtil.getEcode("' + sarray[1] + '",parent.EcodeDwrUtilReply);\'' +
'             style="width:100%; padding-left: 15; padding-right: 15; background-color:#D5D5D5"' +
'             onMouseOver="style.backgroundColor=\'#C0C0C0\';" onMouseOut="style.backgroundColor=\'#D5D5D5\'"' +
'             >' +
'         </td>' +
'         </tr>' +
'         <tr>' +
'         <td>' +
'      <input type="button"  LANGUAGE="JScript" name="temp"' +
'             value="Proceed to FORMS" ' +
'             ONCLICK="parent.db();parent.location.replace(\'' + _continueServlet + '\');"' +
'             style="padding-left: 15; padding-right: 15; background-color:#D5D5D5"' +
'             onMouseOver="style.backgroundColor=\'#C0C0C0\';" onMouseOut="style.backgroundColor=\'#D5D5D5\'"' +
'             >' +
'         </td>' +
'         <td align=center>' +
'      <input type="button"  LANGUAGE="JScript" name="delete"' +
'             value="Return to Form" ' +
'             ONCLICK=\'parent.db();parent.ipop.hide();\'' +
'             style="padding-left: 15; padding-right: 15; background-color:#D5D5D5"' +
'             onMouseOver="style.backgroundColor=\'#C0C0C0\';" onMouseOut="style.backgroundColor=\'#D5D5D5\'"' +
'             >' +
'         </td>' +
'         </tr>' +
'         </table>' +
'      </td>' +
'      </tr>' +
'   </table>' +
'    </td>' +
'    </tr>' +
'   </table>' +
'   </center>' +
' ' 
;
         already_clicked=0; 
         apop.hide();
         ipop.show(screenLeft+(document.body.clientWidth/2)-350,screenTop+30,700,450);

      } else if (replydata.substr(0,20)=="ALREADY_NOTPERMITTED" || replydata.substr(0,12)=="NOTPERMITTED") {
         var sarray=replydata.split(":::");
         var sinfo=sarray[1];
         //ipop=window.createPopup();
         var body=ipop.document.body;
         body.style.backgroundColor="#CCCCCC";
         body.style.border="solid black 2px";
         if (replydata.substr(0,20)=="ALREADY_NOTPERMITTED") {
            var msg='ERROR:  Data has already been entered for this key field combination,  but<br>' +
                    '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;' +
                    'your account lacks permission to edit this data:<br><br>';
         } else {
            var msg='ERROR:  Your account lacks permission to save data to the study<br>' +
                    '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;' +
                    'database for the current form:<br><br>';
         }
         if (sinfo!=null && sinfo!="null") {
            msg=msg+'&nbsp;&nbsp;&nbsp;&nbsp(' + sinfo + ')'; 
         }
         body.innerHTML=
'   <center>' +
'   <table cellpadding=25 width=20% height=100%>' +
'    <tr align=center valign=top>' +
'    <td align=center valign=top>' +
'   <table border=2 bgcolor=#E0E0E0 rules=none align=center cellpadding=25>' +
'      <tr align=right>' +
'        <td width=20% valign=top align=right style="padding-bottom:0;padding-top:5;padding-right:5">' +
'        </td>' +
'      </tr>' +
'      <tr align=center>' +
'      <td align=center style="padding-top:10">' +
'         <table border=1 bgcolor=white cellpadding=5>' +
'            <tr><td style="padding-right:30;padding-left:30">' +
'            <nobr>' +
'            <font color=#002288 size=2><font color=#AA0000>' + msg + 
'                      </font><br><br> ' +
'               &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;What do you want to do?</font>' +
'            </nobr>' +
'            </tr></td>' +
'         </table>' +
'      </td>   ' +
'      </tr>   ' +
'' +
'' +
'      <tr align=center>' +
'      <td align=center style="padding-top:5">' +
'         <table cellpadding=2 align=center border=0>' +
'         <tr>' +
'         <td colspan=2>' +
'      <input type="button"  LANGUAGE="JScript" name="nb"' +
'             value="Upload Form to HOLD Dataset" ' +
'            ONCLICK=\'parent.db();parent.ProcessFormDwr.submitHold(parent.HoldDataReply);\'' +
'            style="width:100%; padding-left: 40; padding-right: 40; background-color:#D5D5D5"' +
'             onMouseOver="style.backgroundColor=\'#C0C0C0\';" onMouseOut="style.backgroundColor=\'#D5D5D5\'"' +
'            >' +
'         </td>' +
'         </tr>' +
'         <tr>' +
'         <td>' +
'      <input type="button"  LANGUAGE="JScript" name="temp"' +
'             value="Proceed to FORMS" ' +
'             ONCLICK="parent.db();parent.location.replace(\'' + _continueServlet + '\');"' +
'             style="padding-left: 15; padding-right: 15; background-color:#D5D5D5"' +
'             onMouseOver="style.backgroundColor=\'#C0C0C0\';" onMouseOut="style.backgroundColor=\'#D5D5D5\'"' +
'             >' +
'         </td>' +
'         <td align=center>' +
'      <input type="button"  LANGUAGE="JScript" name="delete"' +
'             value="Return to Form" ' +
'             ONCLICK=\'parent.db();parent.ipop.hide();\'' +
'             style="padding-left: 15; padding-right: 15; background-color:#D5D5D5"' +
'             onMouseOver="style.backgroundColor=\'#C0C0C0\';" onMouseOut="style.backgroundColor=\'#D5D5D5\'"' +
'             >' +
'         </td>' +
'         </tr>' +
'         </table>' +
'      </td>' +
'      </tr>' +
'   </table>' +
'    </td>' +
'    </tr>' +
'   </table>' +
'   </center>' +
' ' 
;
         already_clicked=0; 
         apop.hide();
         ipop.show(screenLeft+(document.body.clientWidth/2)-300,screenTop+30,600,450);


      } else if (replydata.substr(0,7)=="ALREADY") {
         var sarray=replydata.split(":::");
         var sinfo=sarray[1];
         //ipop=window.createPopup();
         var body=ipop.document.body;
         body.style.backgroundColor="#CCCCCC";
         body.style.border="solid black 2px";
         var msg='Data already entered for this key field combination:<br><br>';
         if (sinfo!=null && sinfo!="null") {
            msg=msg+'&nbsp;&nbsp;&nbsp;&nbsp(' + sinfo + ')'; 
         }
         body.innerHTML=
'   <center>' +
'   <table cellpadding=25 width=20% height=100%>' +
'    <tr align=center valign=top>' +
'    <td align=center valign=top>' +
'   <table border=2 bgcolor=#E0E0E0 rules=none align=center cellpadding=25>' +
'      <tr align=right>' +
'        <td width=20% valign=top align=right style="padding-bottom:0;padding-top:5;padding-right:5">' +
'        </td>' +
'      </tr>' +
'      <tr align=center>' +
'      <td align=center style="padding-top:10">' +
'         <table border=1 bgcolor=white cellpadding=5>' +
'            <tr><td style="padding-right:30;padding-left:30">' +
'            <nobr>' +
'            <font color=#002288 size=2><font color=#AA0000>' + msg + 
'                      </font><br><br> ' +
'               &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;What do you want to do?</font>' +
'            </nobr>' +
'            </tr></td>' +
'         </table>' +
'      </td>   ' +
'      </tr>   ' +
'' +
'' +
'      <tr align=center>' +
'      <td align=center style="padding-top:5">' +
'         <table cellpadding=2 align=center border=0>' +
'         <tr>' +
'         <td colspan=2>' +
'      <input type="button"  LANGUAGE="JScript" name="nb"' +
'             value="Upload Form to HOLD Dataset" ' +
'            ONCLICK=\'parent.db();parent.ProcessFormDwr.submitHold(parent.HoldDataReply);\'' +
'            style="width:100%; padding-left: 40; padding-right: 40; background-color:#D5D5D5"' +
'             onMouseOver="style.backgroundColor=\'#C0C0C0\';" onMouseOut="style.backgroundColor=\'#D5D5D5\'"' +
'            >' +
'         </td>' +
'         </tr>' +
'         <tr>' +
'         <td colspan=2>' +
'      <input type="button"  LANGUAGE="JScript" name="edit"' +
'             value="Replace Existing Record" ' +
'             ONCLICK=\'parent.db();parent.ProcessFormDwr.submitReplace(parent.ReplaceReply);\'' +
'             style="width:100%; padding-left: 15; padding-right: 15; background-color:#D5D5D5"' +
'             onMouseOver="style.backgroundColor=\'#C0C0C0\';" onMouseOut="style.backgroundColor=\'#D5D5D5\'"' +
'             >' +
'         </td>' +
'         </tr>' +
'         <tr>' +
'         <td>' +
'      <input type="button"  LANGUAGE="JScript" name="temp"' +
'             value="Proceed to FORMS" ' +
'             ONCLICK="parent.db();parent.location.replace(\'' + _continueServlet + '\');"' +
'             style="padding-left: 15; padding-right: 15; background-color:#D5D5D5"' +
'             onMouseOver="style.backgroundColor=\'#C0C0C0\';" onMouseOut="style.backgroundColor=\'#D5D5D5\'"' +
'             >' +
'         </td>' +
'         <td align=center>' +
'      <input type="button"  LANGUAGE="JScript" name="delete"' +
'             value="Return to Form" ' +
'             ONCLICK=\'parent.db();parent.ipop.hide();\'' +
'             style="padding-left: 15; padding-right: 15; background-color:#D5D5D5"' +
'             onMouseOver="style.backgroundColor=\'#C0C0C0\';" onMouseOut="style.backgroundColor=\'#D5D5D5\'"' +
'             >' +
'         </td>' +
'         </tr>' +
'         </table>' +
'      </td>' +
'      </tr>' +
'   </table>' +
'    </td>' +
'    </tr>' +
'   </table>' +
'   </center>' +
' ' 
;
         already_clicked=0; 
         apop.hide();
         ipop.show(screenLeft+(document.body.clientWidth/2)-300,screenTop+30,600,450);

      } else if (replydata.substr(0,12)=="NOENROLLMENT") {
         var sarray=replydata.split(":::");
         var sinfo=sarray[1];
         //ipop=window.createPopup();
         var body=ipop.document.body;
         body.style.backgroundColor="#CCCCCC";
         body.style.border="solid black 2px";
         var msg='Warning:  No enrollment records exist for this ID:<br><br>';
         if (sinfo!=null && sinfo!="null") {
            msg=msg+'&nbsp;&nbsp;&nbsp;&nbsp(' + sinfo + ')'; 
         }
         body.innerHTML=
'   <center>' +
'   <table cellpadding=25 width=20% height=100%>' +
'    <tr align=center valign=top>' +
'    <td align=center valign=top>' +
'   <table border=2 bgcolor=#E0E0E0 rules=none align=center cellpadding=25>' +
'      <tr align=right>' +
'        <td width=20% valign=top align=right style="padding-bottom:0;padding-top:5;padding-right:5">' +
'        </td>' +
'      </tr>' +
'      <tr align=center>' +
'      <td align=center style="padding-top:10">' +
'         <table border=1 bgcolor=white cellpadding=5>' +
'            <tr><td style="padding-right:30;padding-left:30">' +
'            <nobr>' +
'            <font color=#002288 size=2><font color=#AA0000>' + msg + 
'                      </font><br><br> ' +
'               &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;What do you want to do?</font>' +
'            </nobr>' +
'            </tr></td>' +
'         </table>' +
'      </td>   ' +
'      </tr>   ' +
'' +
'' +
'      <tr align=center>' +
'      <td align=center style="padding-top:5">' +
'         <table cellpadding=2 align=center border=0>' +
'         <tr>' +
'         <td colspan=2>' +
'      <input type="button"  LANGUAGE="JScript" name="nb"' +
'             value="Upload Form to HOLD Dataset" ' +
'            ONCLICK=\'parent.db();parent.ProcessFormDwr.submitHold(parent.HoldDataReply);\'' +
'            style="width:100%; padding-left: 40; padding-right: 40; background-color:#D5D5D5"' +
'             onMouseOver="style.backgroundColor=\'#C0C0C0\';" onMouseOut="style.backgroundColor=\'#D5D5D5\'"' +
'            >' +
'         </td>' +
'         </tr>' +
'         <tr>' +
'         <td colspan=2>' +
'      <input type="button"  LANGUAGE="JScript" name="edit"' +
'             value="Submit Anyway" ' +
'             ONCLICK=\'parent.db();parent.ProcessFormDwr.submitAnyway(parent.AnywayReply);\'' +
'             style="width:100%; padding-left: 15; padding-right: 15; background-color:#D5D5D5"' +
'             onMouseOver="style.backgroundColor=\'#C0C0C0\';" onMouseOut="style.backgroundColor=\'#D5D5D5\'"' +
'             >' +
'         </td>' +
'         </tr>' +
'         <tr>' +
'         <tr>' +
'         <td>' +
'      <input type="button"  LANGUAGE="JScript" name="temp"' +
'             value="Proceed to FORMS" ' +
'             ONCLICK="parent.db();parent.location.replace(\'' + _continueServlet + '\');"' +
'             style="padding-left: 15; padding-right: 15; background-color:#D5D5D5"' +
'             onMouseOver="style.backgroundColor=\'#C0C0C0\';" onMouseOut="style.backgroundColor=\'#D5D5D5\'"' +
'             >' +
'         </td>' +
'         <td align=center>' +
'      <input type="button"  LANGUAGE="JScript" name="delete"' +
'             value="Return to Form" ' +
'             ONCLICK=\'parent.db();parent.ipop.hide();\'' +
'             style="padding-left: 15; padding-right: 15; background-color:#D5D5D5"' +
'             onMouseOver="style.backgroundColor=\'#C0C0C0\';" onMouseOut="style.backgroundColor=\'#D5D5D5\'"' +
'             >' +
'         </td>' +
'         </tr>' +
'         </table>' +
'      </td>' +
'      </tr>' +
'   </table>' +
'    </td>' +
'    </tr>' +
'   </table>' +
'   </center>' +
' ' 
;
         already_clicked=0; 
         apop.hide();
         ipop.show(screenLeft+(document.body.clientWidth/2)-300,screenTop+30,600,450);

      } else if (replydata.substr(0,6)=="LOCKED") {

         var body=ipop.document.body;
         var sarray=replydata.split(":::");
         var sinfo=sarray[1];
         body.style.backgroundColor="#CCCCCC";
         body.style.border="solid black 2px";
         body.innerHTML=_replytext.replace(/XXXXXXXX/,
             '<font size="3" color="#002288">ERROR:  ' +
             'The record you were submitting already exists and is locked.  ' +
             ' Your form has been saved to the sync directory.<br><br>&nbsp;&nbsp;&nbsp;' + sinfo +
             '</font>' 
         ).replace(/YYYYYYYY/,_continueServlet);
         if (replydata.match(/successfully.*sync directory/i)) {
            _saveStatus="OK";
         }
         already_clicked=0;
         apop.hide();
         ipop.show(screenLeft+(document.body.clientWidth/2)-300,screenTop+30,600,450);

      } else if (replydata.substr(0,8)=="NOKEYVAL") {

         var body=ipop.document.body;
         var sarray=replydata.split(":::");
         var sinfo=sarray[1];
         body.style.backgroundColor="#CCCCCC";
         body.style.border="solid black 2px";
         body.innerHTML=_replytext.replace(/XXXXXXXX/,
             '<font size="3" color="#002288">ERROR:  ' +
             'All key fields in the form must contain data. <br><br>' +
             ' &nbsp;&nbsp;&nbsp;' + sinfo +
             '</font>' 
         ).replace(/YYYYYYYY/,_continueServlet);
         if (replydata.match(/successfully.*sync directory/i)) {
            _saveStatus="OK";
         }
         already_clicked=0;
         apop.hide();
         ipop.show(screenLeft+(document.body.clientWidth/2)-300,screenTop+30,600,450);

      } else {
         var body=ipop.document.body;
         body.style.backgroundColor="#CCCCCC";
         body.style.border="solid black 2px";
         body.innerHTML=_replytext.replace(/XXXXXXXX/,
             '<font size="3" color="#002288">ERROR:  ' +
             replydata +
             '</font>' 
         ).replace(/YYYYYYYY/,_continueServlet);
         if (replydata.match(/successfully.*sync directory/i)) {
            _saveStatus="OK";
         }
         already_clicked=0;
         apop.hide();
         ipop.show(screenLeft+(document.body.clientWidth/2)-300,screenTop+30,600,450);
      }
    }

    ReplaceReply = function(replydata)
    {
      if (replydata=="REPLACED") {
         _saveStatus="OK";
         var body=ipop.document.body;
         body.style.backgroundColor="#CCCCCC";
         body.style.border="solid black 2px";
         body.innerHTML=_replytext.replace(/XXXXXXXX/,
             '<nobr><font size="4" color="#002288">' +
             "Record sucessfully replaced." +
             '</font></nobr>' 
         ).replace(/YYYYYYYY/,_continueServlet);
         already_clicked=0;
         apop.hide();
         ipop.show(screenLeft+(document.body.clientWidth/2)-300,screenTop+30,600,450);
      } else if (replydata=="SAVEDSYNC") {
         _saveStatus="OK";
         var body=ipop.document.body;
         body.style.backgroundColor="#CCCCCC";
         body.style.border="solid black 2px";
         body.innerHTML=_replytext.replace(/XXXXXXXX/,
             '<nobr><font size="4" color="#002288">' +
             'Record has been saved to Sync directory.' +
             '</font></nobr>' 
         ).replace(/YYYYYYYY/,_continueServlet);
         already_clicked=0;
         apop.hide();
         ipop.show(screenLeft+(document.body.clientWidth/2)-300,screenTop+30,600,450);
      } else {
         alert("ERROR:  " + replydata);
      } 
    }

    TempRecordReply = function(replydata)
    {
      if (replydata=="TEMPAPPEND") {
         var body=ipop.document.body;
         body.style.backgroundColor="#CCCCCC";
         body.style.border="solid black 2px";
         body.innerHTML=_replytext.replace(/XXXXXXXX/,
             '<nobr><font size="4" color="#002288">' +
             "Saved RMS Temporary Record." +
             '</font></nobr>' 
         ).replace(/YYYYYYYY/,_continueServlet);
         already_clicked=0;
         apop.hide();
         ipop.show(screenLeft+(document.body.clientWidth/2)-300,screenTop+30,600,450);
      } else {
         var body=ipop.document.body;
         body.style.backgroundColor="#CCCCCC";
         body.style.border="solid black 2px";
         body.innerHTML=_replytext.replace(/XXXXXXXX/,
             '<nobr><font size="4" color="#002288">ERROR:  ' +
             replydata +
             '</font></nobr>' 
         ).replace(/YYYYYYYY/,_continueServlet);
         already_clicked=0;
         apop.hide();
         ipop.show(screenLeft+(document.body.clientWidth/2)-300,screenTop+30,600,450);
      } 
    }

    AnywayReply = function(replydata)
    {
      if (replydata=="APPENDED") {
         _saveStatus="OK";
         var body=ipop.document.body;
         body.style.backgroundColor="#CCCCCC";
         body.style.border="solid black 2px";
         body.innerHTML=_replytext.replace(/XXXXXXXX/,
             '<nobr><font size="4" color="#002288">' +
             "Record sucessfully appended to dataset." +
             '</font></nobr>' 
         ).replace(/YYYYYYYY/,_continueServlet);
         already_clicked=0; 
         apop.hide();
         ipop.show(screenLeft+(document.body.clientWidth/2)-300,screenTop+30,600,450);
      } else if (replydata=="SAVEDSYNC") {
         _saveStatus="OK";
         var body=ipop.document.body;
         body.style.backgroundColor="#CCCCCC";
         body.style.border="solid black 2px";
         body.innerHTML=_replytext.replace(/XXXXXXXX/,
             '<nobr><font size="4" color="#002288">' +
             'Record has been saved to Sync directory.' +
             '</font></nobr>' 
         ).replace(/YYYYYYYY/,_continueServlet);
         already_clicked=0;
         apop.hide();
         ipop.show(screenLeft+(document.body.clientWidth/2)-300,screenTop+30,600,450);
      } else {
         var body=ipop.document.body;
         body.style.backgroundColor="#CCCCCC";
         body.style.border="solid black 2px";
         body.innerHTML=_replytext.replace(/XXXXXXXX/,
             '<nobr><font size="4" color="#002288">ERROR:  ' +
             replydata +
             '</font></nobr>' 
         ).replace(/YYYYYYYY/,_continueServlet);
         already_clicked=0;
         apop.hide();
         ipop.show(screenLeft+(document.body.clientWidth/2)-300,screenTop+30,600,450);
      } 
    }

    HoldDataReply = function(replydata)
    {
      if (replydata=="HOLDAPPEND") {
         _saveStatus="OK";
         var body=ipop.document.body;
         body.style.backgroundColor="#CCCCCC";
         body.style.border="solid black 2px";
         body.innerHTML=_replytext.replace(/XXXXXXXX/,
             '<nobr><font size="4" color="#002288">' +
             "Record sucessfully appended to HOLD dataset." +
             '</font></nobr>' 
         ).replace(/YYYYYYYY/,_continueServlet);
         already_clicked=0; 
         apop.hide();
         ipop.show(screenLeft+(document.body.clientWidth/2)-300,screenTop+30,600,450);
      } else if (replydata=="SAVEDSYNC") {
         _saveStatus="OK";
         var body=ipop.document.body;
         body.style.backgroundColor="#CCCCCC";
         body.style.border="solid black 2px";
         body.innerHTML=_replytext.replace(/XXXXXXXX/,
             '<nobr><font size="4" color="#002288">' +
             'Record has been saved to Sync directory.' +
             '</font></nobr>' 
         ).replace(/YYYYYYYY/,_continueServlet);
         already_clicked=0;
         apop.hide();
         ipop.show(screenLeft+(document.body.clientWidth/2)-300,screenTop+30,600,450);
      } else {
         var body=ipop.document.body;
         body.style.backgroundColor="#CCCCCC";
         body.style.border="solid black 2px";
         body.innerHTML=_replytext.replace(/XXXXXXXX/,
             '<nobr><font size="4" color="#002288">ERROR:  ' +
             replydata +
             '</font></nobr>' 
         ).replace(/YYYYYYYY/,_continueServlet);
         already_clicked=0;
         apop.hide();
         ipop.show(screenLeft+(document.body.clientWidth/2)-300,screenTop+30,600,450);
      } 
    }

    HoldDataNoPrevReply = function(replydata)
    {
      if (replydata=="HOLDAPPEND") {
         _saveStatus="OK";
         var body=ipop.document.body;
         body.style.backgroundColor="#CCCCCC";
         body.style.border="solid black 2px";
         body.innerHTML=_replytext.replace(/XXXXXXXX/,
             '<nobr><font size="4" color="#002288">' +
             "Record sucessfully appended to HOLD dataset." +
             '</font></nobr>' 
         ).replace(/YYYYYYYY/,_continueServlet);
         already_clicked=0;
         apop.hide();
         ipop.show(screenLeft+(document.body.clientWidth/2)-300,screenTop+30,600,450);
      } else {
         var body=ipop.document.body;
         body.style.backgroundColor="#CCCCCC";
         body.style.border="solid black 2px";
         body.innerHTML=_replytext.replace(/XXXXXXXX/,
             '<nobr><font size="4" color="#002288">ERROR:  ' +
             replydata +
             '</font></nobr>' 
         ).replace(/YYYYYYYY/,_continueServlet);
         already_clicked=0;
         apop.hide();
         ipop.show(screenLeft+(document.body.clientWidth/2)-300,screenTop+30,600,450);
      } 
    }

   // return userType (particpants screens may differ)
   var userType="staff";
   if (_rmsPassValueObject.rightslevel=="participant") {
      userType="participant";
   }
   // Check if user entered a value for ID
   /*
   idVal=getFormElementById("ID").value;
   if (idVal==null || idVal.length==0) { 
       alert("ERROR:  An ID value is required for submission.");
       already_clicked=0;
       apop.hide();
       return;
   } 
   */
      
   // Examine FormDataOK value    
   _validateRun=_validateStart;
   if (_validateRun!="Y") {
      // Alert box text for case when validation has not been run
      alertString="WARNING:  This form's validation may not be up-to-date since validation was not run at " +
                  "form startup.  You may consider running validation before submission by clicking " + 
                  "the 'Validation' button. \n\n" +
                  "Validation will run before data is submitted."
      alert(alertString);            
      validate_click();            
   }     
   var okVal=getFormElementById("FormDataOK").value;
   var okVar=true;
   if (okVal!=1) {
      // Alert box text for staff
      if (userType!="participant") {
         alertString="The FormDataOK value indicates there may be a problem with the data.  Do you" +
                    " want to continue this form submission?\n\nPress the VALIDATE Button on the form for a list" +
                    " of possible problems.";
         if (confirm(alertString)) {            
            okVar=true;
         } else {
            okVar=false;
         }
      // Alert box text for study participants
      } else {
         alertString="The FormDataOK value indicates there may be a problem with the data.  This may " +
                    " be due to fields you either inadvertently negleted or opted not to answer.  Do you" + 
                    " want to continue this form submission?\n\nPress the VALIDATE Button on the form for a list" +
                    " of possible problems.";
         if (confirm(alertString)) {            
            okVar=true;
         } else {
            okVar=false;
         }
      }
   } 
   // Submission URL is based on RMS Environment
   _myDoc=event.target;
   if (!okVar) {
       already_clicked=0;
       apop.hide();
   } else if (okVar) {

      _serverName=_rmsPassValueObject.servername;
      _continueServlet=_rmsPassValueObject.continueservlet;

      if (_serverName!=null && _serverName.length>=6) {

         ProcessFormDwr.submitRequest(_pV,ProcessFormDwrReply);
         /*
         // If electronic interview, submit to final, else raise destination dialog
            
         if (getFormElementById("DataSource").value.toString().toUpperCase().substr(0,1)!="P") {

            ProcessFormDwr.submitRequest(_pV,ProcessFormDwrReply);

         } else {

            var body=ipop.document.body;
            body.style.backgroundColor="#CCCCCC";
            body.style.border="solid black 2px";
            body.innerHTML=
'   <center>' +
'   <table cellpadding=25 width=20% height=100%>' +
'    <tr align=center valign=top>' +
'    <td align=center valign=top>' +
'   <table border=2 bgcolor=#E0E0E0 rules=none align=center cellpadding=25>' +
'      <tr align=right>' +
'        <td width=20% valign=top align=right style="padding-bottom:0;padding-top:5;padding-right:5">' +
'        </td>' +
'      </tr>' +
'      <tr align=center>' +
'      <td align=center style="padding-top:10">' +
'         <table border=1 bgcolor=white cellpadding=5>' +
'            <tr><td style="padding-right:30;padding-left:30">' +
'            <nobr>' +
'            <font color=#002288 size=3><font color=#002288><b>Submit Location:</b></font><br><br></font>' +
'            </nobr>' +
'            </tr></td>' +
'         </table>' +
'      </td>   ' +
'      </tr>   ' +
'' +
'' +
'      <tr align=center>' +
'      <td align=center style="padding-top:5">' +
'         <table cellpadding=2 align=center border=0>' +
'         <tr>' +
'         <td>' +
'      <input type="button"  LANGUAGE="JScript" name="nb"' +
'             value="Submit to FINAL Dataset" ' +
'            ONCLICK=\'parent.db();parent.ProcessFormDwr.submitRequest(parent._pV,parent.ProcessFormDwrReply);\'' +
'            style="width:100%; padding-left: 10; padding-right: 10; background-color:#D5D5D5"' +
'             onMouseOver="style.backgroundColor=\'#C0C0C0\';" onMouseOut="style.backgroundColor=\'#D5D5D5\'"' +
'            >' +
'         </td>' +
'         </tr>' +
'         <tr>' +
'         <td>' +
'      <input type="button"  LANGUAGE="JScript" name="edit"' +
'             value="Submit to HOLD Dataset" ' +
'            ONCLICK=\'parent.db();parent.ProcessFormDwr.submitHold(parent._pV,parent.HoldDataNoPrevReply);\'' +
'            style="width:100%; padding-left: 10; padding-right: 10; background-color:#D5D5D5"' +
'             onMouseOver="style.backgroundColor=\'#C0C0C0\';" onMouseOut="style.backgroundColor=\'#D5D5D5\'"' +
'             >' +
'         </td>' +
'         </tr>' +
'         <tr>' +
'         <td>' +
'      <input type="button"  LANGUAGE="JScript" name="edit"' +
'             value="Return to Form" ' +
'            ONCLICK=\'parent.db();parent.ipop.hide();\'' +
'             style="width:100%; padding-left: 10; padding-right: 10; background-color:#D5D5D5"' +
'             onMouseOver="style.backgroundColor=\'#C0C0C0\';" onMouseOut="style.backgroundColor=\'#D5D5D5\'"' +
'             >' +
'         </td>' +
'         </tr>' +
'         </table>' +
'      </td>' +
'      </tr>' +
'   </table>' +
'    </td>' +
'    </tr>' +
'   </table>' +
'   </center>' +
' ' 
;
         already_clicked=0; 
         apop.hide();
         ipop.show(screenLeft+(document.body.clientWidth/2)-300,screenTop+30,600,450);


         } 
         */
      } 
   } 
}

function submit_mouseenter(thisobj) {
  _docReady=0;
}

function submit_mouseexit(thisobj) {
  _docReady=1;
}

function submit_docready(thisobj) {
  // NO CURRENT CONTENT
}

function saveStatus() {
   // close import window if open
   try {
      _importwin_.close();
   } catch (e) { }
   // Display warning message if data haven't been submitted
   if (_saveStatus!="OK") {
      return "WARNING:  It appears there have been changes to values in this form     \n" +
             "that haven't been submitted to RMS.\n\n" +
             "Press CANCEL to remain in form and submit";
   } else {
      return ;            
   }
}

////////////////////
////////////////////
////////////////////
////////////////////
////////////////////

function escript(thisobj) {

   // Try block added for alternate entry interfaces.  Some fields may not have an
   // associated validation function.
   try {
      try {
         eval("e_" + event.srcElement.name + "(thisobj)");
      } catch (e1) {   
         eval("e_" + event.srcElement.name.toLowerCase() + "(thisobj)");
      }   
      _thisvalue=thisobj.value;
      if (_objType=="DT") {
         if (event.type=="keyup") {
            dt_keyup(thisobj);
         } else if (event.type=="focus") {
            dt_focus(thisobj);
         } else if (event.type=="blur") {
            dt_blur(thisobj);
         } 
      } else if (_objType=="NF") {
         if (event.type=="keyup") {
            nf_keyup(thisobj);
         } else if (event.type=="focus") {
            nf_focus(thisobj);
         } else if (event.type=="blur") {
            nf_blur(thisobj);
         } 
      } else if (_objType=="CF" || _objType=="TF") {
         if (event.type=="keyup") {
            tf_keyup(thisobj);
         } else if (event.type=="focus") {
            tf_focus(thisobj);
         } else if (event.type=="blur") {
            tf_blur(thisobj);
         } 
      } else if (_objType=="DD") {
         if (event.type=="keyup") {
            dd_keyup(thisobj);
         } else if (event.type=="focus") {
            dd_focus(thisobj);
         } else if (event.type=="click") {
            dd_click(thisobj);
         } 
      } else if (_objType=="RB") {
         if (event.type=="keyup") {
            rb_keyup(thisobj);
         } else if (event.type=="focus") {
            rb_focus(thisobj);
         } else if (event.type=="click") {
            rb_click(thisobj);
         } 
      } else if (_objType=="DC") {
         if (event.type=="keyup") {
            dc_keyup(thisobj);
         } else if (event.type=="focus") {
            dc_focus(thisobj);
         } else if (event.type=="blur") {
            dc_blur(thisobj);
         } 
      }

   } catch (e) {
      // DO NOTHING
   }
      
}

function vscript(thisobj) {

   _objType="";
   try {
   eval("e_" + thisobj.name + "(thisobj)");
   } catch (e) {
   eval("window.document.e_" + thisobj.name + "(thisobj)");
   }

   _thisvalue=thisobj.value;
   if (_objType=="DT") {
      dt_focus(thisobj);
      dt_blur(thisobj);
      dt_validate(thisobj);
   } else if (_objType=="NF") {
      nf_focus(thisobj);
      nf_blur(thisobj);
      nf_validate(thisobj);
   } else if (_objType=="CF" || _objType=="TF") {
      tf_focus(thisobj);
      tf_blur(thisobj);
      tf_validate(thisobj);
   } else if (_objType=="DD") {
      dd_focus(thisobj);
      dd_click(thisobj);
      dd_validate(thisobj);
   } else if (_objType=="RB") {
      rb_focus(thisobj);
      rb_click(thisobj);
      rb_validate(thisobj);
   } else if (_objType=="DC") {
      dc_focus(thisobj);
      dc_blur(thisobj);
      dc_validate(thisobj);
   }

}

// This is a modification of the vscript function above to be called by the validate button 
// javascript.  It should not have multiple validate calls for each field using the click & focus
// methods.  Only dc_validate should be called.
function vscript_allvars(thisobj) {

   _objType="";
   try {
   eval("e_" + thisobj.name + "(thisobj)");
   } catch (e) {
   eval("window.document.e_" + thisobj.name + "(thisobj)");
   }

   _thisvalue=thisobj.value;
   if (_objType=="DT") {
      dt_validate(thisobj);
   } else if (_objType=="NF") {
      nf_validate(thisobj);
   } else if (_objType=="CF" || _objType=="TF") {
      tf_validate(thisobj);
   } else if (_objType=="DD") {
      dd_validate(thisobj);
   } else if (_objType=="RB") {
      rb_validate(thisobj);
   } else if (_objType=="DC") {
      dc_validate(thisobj);
   }

}

////
////

////////////////////
////////////////////
////////////////////
////////////////////
////////////////////

///////////////////////////////////////////////////////////////////////////////////////
// Function:  SumFunc  (Summation Function)                                          //
//                                                                                   //
//    Input:  a list of variables in a string of the form "VAR1,VAR2,VAR3,.....VARN" //
//  Returns:  an Integer sum or NaN                                                  //
//                                                                                   //
//    Notes:  Be careful with this one.  In order to accomplish one of the BDI       //
//            calculations, this function is designed to pull in all the numeric     //
//            characters from the incoming value and compress out any non-numeric    //
//            characters.  Thus, "1a1" will translate to "11".  This probably will   //
//            not cause problems in out case but be aware of it.                     //
//                                                                                   //
//            THIS FUNCTION PRORATES SCORES WHERE <= 10% OF VALUES ARE MISSING!!!    //
//                                                                                   //
///////////////////////////////////////////////////////////////////////////////////////

function SumFunc(inString) {

   var sumArray = inString.split(",");    // Create Array of input variables
   var sumVar=0;                          // Summation Variable
   var nMiss=0;                           // Non-Missing Count
   // Keep track of isMiss to avoid running through entire loop when many missing
   var isMiss=0;                          // Is-Missing Count
   var tempvar;

   // Loop over variable list incrementing nMiss & computing sumVar
   for (var i=0; i<sumArray.length; i++) {

      // Use try/catch block because sometimes we pass form variables and sometimes
      // javascript fields.
      try {
         eval("tempvar=getFormElementValueById('" + sumArray[i] + "').toString()");
      } catch (e) {
         eval("tempvar=(" + sumArray[i] + ").toString()");
      }
      // KEEP ONLY NUMERIC PORTION
      tempvar=tempvar.replace(/[^0-9]/g,"");
      // If has a value, increment nMiss count
      if (tempvar.toString().length>=1) {
         nMiss++;
      } else {
         isMiss++;
         // Exit if too many missing to generate SUM, otherwise return prorated sum
         if ((isMiss/sumArray.length)>0.10) {
            return NaN;
         }
      }
      // Add to sum.
      if (!(Number(tempvar).NaN)) {
         sumVar=sumVar + Number(tempvar).valueOf();
      }

   }
   //
   // return prorated score
   //
   sumVar=Math.round(sumVar+(((sumVar)/nMiss)*(sumArray.length-nMiss)));
   return sumVar;

}

////////////////////
////////////////////
////////////////////
////////////////////
////////////////////

/* ================================================================
 * License for 'jt_.js' (was 'jt_utils.js'), 'jt_DialogBox' and all
 * related code: http://www.wingo.com/jt_/
 * 
 * Redistribution and use in source and binary forms, with or without modification,
 * are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain this copyright notice and link
 *    to this License, list of conditions and the following disclaimer:
 *
 *  Copyright (c) 2002-2008 Joseph.Oster, wingo.com - All rights reserved.
 *  license: http://www.wingo.com/jt_/license.html
 *
 * 2. Products derived from this software may not use the "wingo.com" name
 *    nor may "wingo.com" appear in their names without prior written
 *    permission of Joseph.Oster, wingo.com.
 * 
 * THIS SOFTWARE IS PROVIDED ``AS IS'' AND ANY EXPRESSED OR IMPLIED WARRANTIES,
 * INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND
 * FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL
 * Joseph.Oster, wingo.com OR ITS CONTRIBUTORS BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
 * THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * ====================================================================
 * 
 * This software consists solely of contributions made by Joseph.Oster, wingo.com
 */

/**
* jt_ProgressBar.js - Progress Bar widget
*
* @version 29 Apr 2005
* @author Joseph Oster, wingo.com, Copyright (c) 2005-2007
*/

jt_ProgressBar = function(parent, width, fontSize, barClass) {
    // constructor for Progress Bar object; 'width' and 'fontSize' in pixels
    this.pixels = width;
    this.outerDIV = document.createElement("div");
    this.outerDIV.style.border = "1px solid #000000";
    this.outerDIV.style.background = "#FFFFFF";
    this.outerDIV.style.fontFamily = "Arial,Verdana";
    this.outerDIV.style.fontSize = fontSize + "px";
    this.outerDIV.style.width = (width + 2) + "px";
    this.outerDIV.style.textAlign = "left";
    parent.appendChild(this.outerDIV);

    this.fillDIV = document.createElement("div");
    this.fillDIV.style.textAlign = "right";
    this.fillDIV.style.overflow = "hidden";
    this.fillDIV.innerHTML = "x";
    this.fillDIV.style.width = "0px";
    if (barClass) this.fillDIV.className = barClass;
    else {
        this.fillDIV.style.background = "#00008b";
        this.fillDIV.style.border = "1px solid #FFFFFF";
        this.fillDIV.style.color = "#FFFFFF";
        }
    this.outerDIV.appendChild(this.fillDIV);
}

jt_ProgressBar.prototype.setPercent = function(pct) {
    // expects 'pct' values between 0.0 and 1.0
    var fillPixels;
    if (pct < 1.0) fillPixels = Math.round(this.pixels * pct);
    else { // avoid round off error
        pct = 1.0;
        fillPixels = this.pixels;
        }
    this.fillDIV.innerHTML = Math.round(100 * pct) + "%";
    this.fillDIV.style.width = fillPixels + "px";
}

