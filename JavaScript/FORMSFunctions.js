

/**
 *
 * This program is part of:
 *
 *    FORMS [Forms-Oriented Research Management System]
 *
 * Copyright (C) 2009 OpenWare Architects LLC.  All Rights Reserved.
 *    http://forms.sourceforge.net/
 *
 * FORMS is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * FORMS is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with FORMS.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

// FORMSFunctions.js - Miscellaneous functions used in FORMS //

   var navWinVar="";

   // Open Navigation Window
   function navWin() {
      var x,y,el,pwidth,pheight;
      fadeinx=0;
      document.getElementById("navmenuframe").src="navmenurows.do";
      document.getElementById("navmenuframe").style.display="inline";
      showNavMenu("navmenu");
   }

   // Show Navigation Menu
   var oldx;
   var oldy;
   function showNavMenu(str) {
      if (document.getElementById) {
         /*
         pwidth=document.body.offsetWidth;
         pheight=10000000;
         x=event.clientX+document.body.scrollLeft;
         y=event.clientY+document.body.scrollTop;
         x=x-0;
         y=y-37;
         */
         el= document.getElementById(str);
         /*
         if ((x+parseInt(el.offsetWidth))>=pwidth) {
            x=(pwidth-parseInt(el.offsetWidth));
            el.style.left=x-60+"px";
            oldx=x-60;
         } else {
            el.style.left=x+"px";
            oldx=x;
         }
         if ((y+parseInt(el.offsetHeight))>=pheight) {
            y=(pheight-parseInt(el.offsetHeight));
            el.style.bottom=y+"px";
         } else {
            el.style.top=y+"px";
         }
         */
         el.style.visibility="hidden";
         fadeinx=90;
         fadeIn(el);
         /*
         oldy=y;
         */
         return false;
      }
   }

   // Open Logout Window
   function logWin() {
      var msg="Continue logout?";
      if (confirm(msg)) location.href("logout.do");
   }

   var _readyvar_=0;
   function show(object) {
       if (document.layers && document.layers[object] != null)
           document.layers[object].visibility = 'visible';
       else if (document.all) {
           document.all[object].style.visibility = 'visible';
       }
   }

   function hide(object) {
       if (document.layers && document.layers[object] != null)
           document.layers[object].visibility = 'hidden';
       else if (document.all) {
           document.all[object].style.visibility = 'hidden';
       }
   }

   function checkReadyState() {
      while (_readyvar_==0) {
         show('loading');
         hide('mainscreen');
      }
      if (_readyvar_>=1) {
         show('mainscreen');
         hide('loading');
      }
   }

   function showLoading() {
      hide('mainscreen');
      show('loading');
   }

   var ndots=15;
   var i=ndots-5;
   var rc=-1;
   var inText="TEMP";
   function chngFunc() {
      if (inText=="TEMP") {
         inText=document.getElementById('loadText').innerHTML;
      }
      dots="";
      for (j=1;j<=i;j++) {
         dots=dots + ".";
      }
      dots=dots + "<font color=#CCCCCC>";
      for (j=1;j<=(ndots-i);j++) {
         dots=dots + ".";
      }
      dots=dots + "</font>";
      if (i==ndots) i=0;
      document.getElementById('loadText').innerHTML=inText + dots;
      i=i+1;
   }

   function rollFunc() {
      chngFunc();
      rc=window.setInterval(chngFunc,333);
   }

   function beginFormMenu(formacr,formnameparm,formnumparm,formverparm,formtypeparm,formformatparm) {
      var x,y,el,pwidth,pheight;
      document.getElementById("headeritem").innerHTML=formacr + " Form";
      var earray=document.all.tags("a");
      for (var i=0;i<earray.length;i++) {
         try {
            var junk=earray[i].onclick.toString();
            if (junk.match(/ReplaceText/i)) {
               junk=junk.replace(/ReplaceText/i,formnameparm + "&" + formnumparm + "&" + 
                    formverparm + "&" + formtypeparm + "&" + formformatparm);
               eval('earray[i].onclick=' + junk);
            }
         } catch (e) { }
      }
      fadeinx=0;
      showThisMenu(event);
   }

   function resizeMenu(str,n) {
      el= document.getElementById(str);
      if (el==null) {
         el=parent.document.getElementById(str);
      }   
      el.style.width=n.toString() + '%';
      el.style.height=n.toString() + '%';
      el.style.top=((100-n)/2).toString() + '%';
      el.style.left=((100-n)/2).toString() + '%';
   }


   // Open Administration Window
   function showMenu(str) {
      fadeinx=0;
      el= document.getElementById(str);
      if (el==null) {
         el=parent.document.getElementById(str);
      }   
      el.zIndex=5;
      fadeinx=90;
      fadeIn(el);
   }

   // Open Administration Window
   function admWin(str) {
      var x,y,el,pwidth,pheight;
      fadeinx=0;
      showAdminMenu(str);
   }

   function admWinHW(str,h,w) {
      var x,y,el,pwidth,pheight;
      fadeinx=0;
      showAdminMenuHW(str,h,w);
   }

   // Open Subsequent Administration Window
   function nextAdm(str) {
      var x,y,el,pwidth,pheight;
      fadeinx=0;
      showNextAdminMenu(str);
   }

   // Show Menu
   function showThisMenu() {
      if (document.getElementById) {
         pwidth=document.body.offsetWidth;
         pheight=10000000;
         x=event.clientX+document.body.scrollLeft;
         y=event.clientY+document.body.scrollTop;
         x=x-0;
         y=y-37;
         el= document.getElementById("menu");
         if ((x+parseInt(el.offsetWidth))>=pwidth) {
            x=(pwidth-parseInt(el.offsetWidth));
            el.style.left=x-60+"px";
         } else {
            el.style.left=x+"px";
         }
         if ((y+parseInt(el.offsetHeight))>=pheight) {
            y=(pheight-parseInt(el.offsetHeight));
            el.style.bottom=y+"px";
         } else {
            el.style.top=y+"px";
         }
         el.style.visibility="hidden";
         fadeinx=0;
         fadeIn(el);
         return false;
      }
   }

   // Show Administration Menu
   var oldx;
   var oldy;
   function showAdminMenu(str) {
      if (document.getElementById) {

         pwidth=document.body.offsetWidth;
         pheight=document.body.offsetHeight;
         el= document.getElementById(str);
         x=(pwidth/2)-250;
         y=(pheight/2)-200;
         el.style.left=x+"px";
         el.style.top=y+"px";
         fadeinx=90;
         fadeIn(el);
         return false;

         /*
         pwidth=document.body.offsetWidth;
         pheight=10000000;
         x=event.clientX+document.body.scrollLeft;
         y=event.clientY+document.body.scrollTop;
         x=x-0;
         y=y-37;
         el= document.getElementById(str);
         if ((x+parseInt(el.offsetWidth))>=pwidth) {
            x=(pwidth-parseInt(el.offsetWidth));
            el.style.left=x-60+"px";
            oldx=x-60;
         } else {
            el.style.left=x+"px";
            oldx=x;
         }
         if ((y+parseInt(el.offsetHeight))>=pheight) {
            y=(pheight-parseInt(el.offsetHeight));
            el.style.bottom=y+"px";
         } else {
            el.style.top=y+"px";
         }
         el.style.visibility="hidden";
         fadeinx=90;
         fadeIn(el);
         oldy=y;
         return false;
         */

      }
   }

   function showAdminMenuHW(str,h,w) {
      if (document.getElementById) {

         pwidth=document.body.offsetWidth;
         pheight=document.body.offsetHeight;
         el= document.getElementById(str);
         x=(pwidth/2)-(w/2);
         y=(pheight/2)-(h/2);
         el.style.left=x+"px";
         el.style.top=y+"px";
         fadeinx=90;
         fadeIn(el);
         return false;

      }
   }

   // Show Subsequent Administration Menu
   function showNextAdminMenu(str) {
      if (document.getElementById) {

         pwidth=document.body.offsetWidth;
         pheight=document.body.offsetHeight;
         el= document.getElementById(str);
         x=(pwidth/2)-250;
         y=(pheight/2)-200;
         el.style.left=x+"px";
         el.style.top=y+"px";
         fadeinx=90;
         fadeIn(el);
         return false;

         /*
         pwidth=document.body.offsetWidth;
         pheight=10000000;
         x=oldx+10;
         y=oldy+10;
         el= document.getElementById(str);
         el.style.left=x+"px";
         if ((y+parseInt(el.offsetHeight))>=pheight) {
            y=(pheight-parseInt(el.offsetHeight));
            el.style.bottom=y+"px";
         } else {
            el.style.top=y+"px";
         }
         el.style.visibility="hidden";
         fadeinx=90;
         // Delay this slightly
         setTimeout("fadeIn(el)",50);
         oldx=x;
         oldy=y;
         return false;
         */
      }
   }

   // Fade Menu to Visible
   function fadeIn(el) {
      el.style.zIndex="5";
      el.style.visibility='visible';
      el.filters.alpha.opacity=fadeinx;
      fadeinx += 20;
      goIn=setTimeout("fadeIn(el)", 0);
      if(fadeinx > 100) {
         clearTimeout(goIn);
         el.filters.alpha.opacity=100;
      }
   }

   // Set Menu to Hidden
   function hideMenu(str) {
      try {
         el= document.getElementById(str);
         if (el==null) {
            el=parent.document.getElementById(str);
         }
         el.style.zIndex="-9999";
         el.style.display="block";
      } catch (Exception) { }
   }

   // Set menu display to none
   function noDisplayMenu(str) {
      try {
         el= document.getElementById(str);
         el.style.display="none";
      } catch (Exception) { }
   }

   function deQuote(str) {
      // removes leading and trailing quotes added by DWR
      str=str.replace(/^ *"/,"");
      str=str.replace(/" *$/,"");
      return str;
   }

   // Return frame by name
   function getFrameByName(str) {
      farray=window.frames;
      for (var i=0;i<farray.length;i++) {
         if (farray[i].name==str) {
            return farray[i];
         }
      }
      return null;
   }
   
   // click submit button in remote frame 
   function xsubmit(framenm,buttonnm) {
      getFrameByName(framenm).document.getElementById(buttonnm).click();
   }

   // replace current url in remote frame
   function xreplace(framenm,loc) {
      getFrameByName(framenm).location.replace(loc);
   }

   // load new url in remote frame
   function xhref(framenm,loc) {
      getFrameByName(framenm).location.href(loc);
   }

   // print iframe contents
   function formsPrint() {
      var iframes=document.getElementsByTagName("iframe");
      var already=false;
      for (i=0;i<iframes.length;i++) {
         var idval=iframes[i].id;
         if (idval!="navmenuframe" && idval!="wmframe") {
            already=true;
            window[idval].focus();
            window[idval].print();
         }
         if (already) break;
      }
   }

   function siteEdit(site,sitedesc,siteid) {
      getFrameByName('wmframe').location.replace('sitemod.sutil');
      currsite=site;
      currsitedesc=sitedesc;
      currsiteid=siteid;
   }
   function siteDelete(site,sitedesc,siteid) {
      if (confirm("Delete this Site [" + site + "]?")) {
         getFrameByName('wmframe').location.replace('sitedelete.sutil?siteid=' + siteid);
         currsite=site;
         currsitedesc=sitedesc;
         currsiteid=siteid;
      } else {
         alert('Operation Cancelled');
      }
   }

   // Set menu display to none
   function enterKeyFunc(str) {
      if (document.layers) {
        document.captureEvents(Event.KEYDOWN);
      }  
      document.onkeydown =
        function (evt) { 
          var keyCode = evt ? (evt.which ? evt.which : evt.keyCode) : event.keyCode;
          if (keyCode == 13) {
               eval(str);
          }
      }
   }


/*

 // Probably delete this section or invest more time in them.  These functions have undesirable
 // properties and are not in usable shape (beeps play when window is minimized and brought back
 // up and confirmation windows don't allow tabbing)  MRH 11-08-2007

 //formsConfirm("Logout of Forms?","Logout","Cancel","parent.location.href('logout.do')");

   // Emit beep 
   function beep(ins) {
      if (ins==null) {
         document.body.insertAdjacentHTML('beforeEnd','&nbsp;<bgsound src="Sounds/pop.wav">')
      } else {
         document.body.insertAdjacentHTML('beforeEnd','&nbsp;<bgsound src=ins + "/Sounds/pop.wav">')
      }
   }

   var fpop=window.createPopup();

   function formsConfirm(msg,obutton,cbutton,okscript) {

      var body=fpop.document.body;
      // The following code should be working to assign a
      // class, but it currently isn't.  May work under a 
      // later version of IE. For now, assign styles here
      // rather than pull them from CSS file
      //for (var i=0;i<parent.document.styleSheets.length;i++) {
      //   try {
      //      fpop.document.importNode(parent.document.styleSheets[i]);
      //   } catch (e) {
      //      fpop.document.createStyleSheet(parent.document.styleSheets[i]);
      //   }
      //}   
      //body.className='vismenu';
      body.style.backgroundColor="#CCCCCC";
      body.style.borderWidth="2px";
      body.style.borderStyle="solid"
      body.style.borderLeftColor="#eeeeee";
      body.style.borderRightColor="#666666";
      body.style.borderBottomColor="#666666";
      body.style.borderTopColor="#eeeeee";
      body.style.textAlign="center";
      body.style.verticalAlign="middle";
      body.innerHTML=
'<body>' +
'<link rel="stylesheet" type="text/css" href="CSS/FormsDefault.css" title="Default" />' +
'<table width=100% height=100% cellspacing=5>' +
'<tr>' +
'<td width=100% height=10% valign=middle align=center>&nbsp;</td>' +
'</tr>' +
'<tr>' +
'<td width=100% height=50% valign=middle align=center class="aheaderitem">' + msg + '</td>' +
'</tr>' +
'<tr>' +
'<td width=100% height=40% valign=middle align=center>' +
'<button style="width:40%;text-align:center" onclick="parent.fpop.hide();' + okscript + '" onkeypress="if (event.keyCode==13) this.click();" tabindex=1>' + obutton + '</button>&nbsp;&nbsp;' +
'<button style="width:40%;text-align:center" onclick="parent.fpop.hide();" onkeypress="if (event.keyCode==13) this.click();" tabindex=2>' + cbutton + '</button>' +
'</td>' +
'</tr>' +
'</table>' +
'</body>' +
' '
;
      fpop.show((screen.availWidth/2)-175,(screen.availHeight/2)-100,350,200)
      
   }
*/

